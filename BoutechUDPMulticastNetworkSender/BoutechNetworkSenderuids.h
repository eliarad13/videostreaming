//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

// {FF3FCECA-C9D9-4f11-9E76-B571BC8565B5}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0xBA3fce19, 0xF9d9, 0x4f12, 0x4e, 0x56, 0x65, 0x71, 0xbc, 0x85, 0x15, 0x25);


 
// {3645E33E-25DE-4B6B-A377-B9486659C560}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
0x3645e33e, 0x25de, 0x4b6b, 0xa3, 0x77, 0xb9, 0x48, 0x66, 0x59, 0xc5, 0x60);



DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};