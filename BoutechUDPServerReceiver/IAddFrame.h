#pragma once

#define LIVE_FILTER_NAME TEXT("Bauotech UDP Server Receiver")
#define LIVE_OUTPIN_NAME L"Out"

 

// {CF24BB08-37C8-44BD-9A12-683B118F290A}
static const GUID IID_IBoutechLiveSource =
{ 0xcf24bb08, 0x37c8, 0x44bd, { 0x9a, 0x12, 0x68, 0x3b, 0x11, 0x8f, 0x29, 0xa } };

 
// {C21D1B3D-648D-4099-ACEB-050982427E8B}
static const GUID CLSID_CBoutechLiveSourceReceiver =
{ 0xc21d1b3d, 0x648d, 0x4099, { 0xac, 0xeb, 0x5, 0x9, 0x82, 0x42, 0x7e, 0x8b } };

 
 
DECLARE_INTERFACE_(IBoutechLiveSource, IUnknown)
{
  
	STDMETHOD(SetIpAddress)(WCHAR *IpAddress) PURE;
	STDMETHOD(SetPort)(int port) PURE;
	STDMETHOD(SetPinType)(int pinType) PURE;
};

