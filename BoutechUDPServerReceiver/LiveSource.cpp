#include "LiveSource.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <wmcodecdsp.h>
#include "WinAES.h"
#include <fstream>

//https://www.codeproject.com/Articles/158053/DirectShow-Filters-Development-Part-Live-Source

using namespace std;



#define USE_AES  0
 


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

 
void CLiveSourceStream::InitFifo()
{
	fifoBuffer = (uint8_t *)malloc(MAX_FIFO);
}

#if USE_FIFO
void CLiveSource::ReceiveThread3()
{

	uint8_t buffer[RECEIVE_SIZE];
	int count = 0;
	while (m_pOutputPin->m_graphStat == GRAPH_RUN)
	{

		int size = recv(
			ReceivingSocket,
			(char *)buffer,
			RECEIVE_SIZE,
			0);
		if (size > 0)
		{
			m_pOutputPin->AddToFifo(buffer, size);
		}
	}
}
#endif 
  

void CLiveSourceStream::AddToFifo(uint8_t *data, int size)
{

	int x = MAX_FIFO - GetFifoSize();
	if (x < size)
	{
		return;
	}

	int d = MIN(size, MAX_FIFO - fifo_write);
	memcpy(fifoBuffer + fifo_write, data, d);

	EnterCriticalSection(&CriticalSection);
	fifo_write = (fifo_write + d) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);


	size -= d;
	if (size > 0)
	{
		memcpy(fifoBuffer + fifo_write, data, size);
		EnterCriticalSection(&CriticalSection);
		fifo_write = (fifo_write + size) % MAX_FIFO;
		LeaveCriticalSection(&CriticalSection);
	}
}

  
int CLiveSourceStream::GetFifoSize()
{

	EnterCriticalSection(&CriticalSection);

	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return 0;
	}


	if (fifo_write > fifo_read)
	{
		int x = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return x;
	}

	int x = (MAX_FIFO - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return x;
}
  
CLiveSource::CLiveSource(LPUNKNOWN pUnk, HRESULT* phr)
	: CBaseFilter(LIVE_FILTER_NAME, pUnk, &m_critSec, CLSID_CBoutechLiveSourceReceiver)
{
	m_pOutputPin = new CLiveSourceStream(this, &m_critSec, phr); 
	// Initialize the critical section one time only.
	InitializeCriticalSectionAndSpinCount(&m_pOutputPin->CriticalSection, 0x00000400);
	ReceivingSocket = -1;

	strcpy(m_pOutputPin->m_ipAddress, "227.1.1.1");
	m_pOutputPin->m_port = 10400;

#if USE_FIFO
	m_pOutputPin->InitFifo();	
#endif


	if (phr)
	{
		if (m_pOutputPin == NULL)
			*phr = E_OUTOFMEMORY;
		else {
			*phr = S_OK;
		}
	}  
}

CLiveSource::~CLiveSource(void)
{

	if (m_pOutputPin->aes != NULL)
	{	
		delete m_pOutputPin->aes;
		m_pOutputPin->aes = NULL;
	}
	 
	 
	delete m_pOutputPin;
	   
	if (m_pOutputPin->fifoBuffer != NULL)
	{
		free(m_pOutputPin->fifoBuffer);
		m_pOutputPin->fifoBuffer = NULL;
	}
	WSACleanup();
}

void CLiveSourceStream::InitializeAES()
{
	 

	byte key[WinAES::KEYSIZE_128] = { 0xF5, 0x13, 0x1E, 0xD8, 0x7F, 0xB3, 0x06, 0x94, 0x07, 0x7D, 0x85, 0xFD, 0xE0, 0x4C, 0x9C, 0xAC };
	byte iv[WinAES::BLOCKSIZE] = { 0xD8, 0xCD, 0xCF, 0x1E, 0xC2, 0x9E, 0xE8, 0x81, 0x98, 0x5C, 0xD3, 0x5B, 0x7A, 0xB1, 0xDA, 0x2A };
	
	aes = new WinAES();
 
	
	try
	{
		// Oops - no iv
		// aes.SetKey( key, sizeof(key) );

		// Oops - no key
		// aes.SetIv( iv, sizeof(iv) );

		// Set the key and IV
		aes->SetKeyWithIv(key, sizeof(key), iv, sizeof(iv));

		recovered = new byte[1024 * 10];

		// Done with key material - Microsoft owns it now...
		ZeroMemory(key, sizeof(key));


	}
	catch (const WinAESException& e)
	{

	}
	 

}
void CLiveSource::ReceiveThread2()
{
	int SelectTiming;
	 
	while (m_pOutputPin->m_graphStat == GRAPH_RUN)
	{
		m_pOutputPin->AddFrame(ReceivingSocket);
		Sleep(1);
	}
 

} 

const wchar_t *CLiveSource::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

int CLiveSource::Setup()
{
	int iResult;

	if (ReceivingSocket != -1)
	{
		closesocket(ReceivingSocket);
		ReceivingSocket = -1;
	}

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		printf("Server: Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return -1;
	}
	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;

	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return 1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	DWORD ip = inet_addr(m_pOutputPin->m_ipAddress);
	addr.sin_addr.s_addr = ip;
	addr.sin_port = htons(m_pOutputPin->m_port);


	int iOptVal = 0;
	int iOptLen = sizeof(int);
	int recvBufSize;
	getsockopt(ReceivingSocket, SOL_SOCKET, SO_RCVBUF, (char *)&recvBufSize, &iOptLen);

	int a = 65535;
	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_RCVBUF, (char *)&a, sizeof(int)) == -1)
	{

	}


	// bind to receive address
	//
	if (::bind(ReceivingSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}


	return 1;

}

 
STDMETHODIMP CLiveSource::Run(REFERENCE_TIME tStart)
{
	m_pOutputPin->fifo_write = 0;
	m_pOutputPin->fifo_read = 0;
	m_pOutputPin->m_graphStat = GRAPH_RUN;


	int res = Setup();

#if USE_FIFO
	shared_ptr<thread> t1 =	make_shared<thread>(&CLiveSource::ReceiveThread3, this);	
	t1->detach();
#endif 
	
	shared_ptr<thread> t2 = make_shared<thread>(&CLiveSource::ReceiveThread2, this);
	t2->detach();
	

	return CBaseFilter::Run(tStart);
}
 
STDMETHODIMP CLiveSource::Pause()
{
	 
	m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP CLiveSource::Stop()
{

	m_pOutputPin->m_graphStat = GRAPH_STOP;

	return CBaseFilter::Stop();
}

int CLiveSource::GetPinCount()
{
	CAutoLock cAutoLock(&m_critSec);

	return 1;
}

CBasePin* CLiveSource::GetPin(int n)
{
	CAutoLock cAutoLock(&m_critSec);

	return m_pOutputPin;
}

CUnknown* WINAPI CLiveSource::CreateInstance(LPUNKNOWN pUnk, HRESULT *phr)
{
	CUnknown* pNewFilter = new CLiveSource(pUnk, phr);
	 
	if (phr)
	{
		if (pNewFilter == NULL) 
			*phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) 
	{
		::MessageBox(NULL, L"Failed to call to  WSAStartup", L"Boutech Receiver", 0);
	}

	return pNewFilter;
}

STDMETHODIMP CLiveSource::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	CheckPointer(ppv, E_POINTER);

	 
	if (riid == IID_IBoutechLiveSource)
	{
		return GetInterface((IBoutechLiveSource*)m_pOutputPin, ppv);
	} 	
	else if(riid == IID_IAMFilterMiscFlags)
	{
		return GetInterface((IAMFilterMiscFlags*) this, ppv);
	}
	else 
	{
		return CBaseFilter::NonDelegatingQueryInterface(riid, ppv);
	}
}


//=============================================================================



CLiveSourceStream::CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr)
	: CBaseOutputPin(LIVE_FILTER_NAME, pFilter, pLock, phr, LIVE_OUTPIN_NAME),
	m_frameRate(0), m_rtFrameRate(0), m_lastFrame(0),
	m_videoResolutionWidth(640), m_videoResolutionHeight(480)
{
	aes = NULL;
	m_pinType = 0;
	m_bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
	m_bmpInfo.biCompression = BI_RGB;
	m_bmpInfo.biBitCount = 32;
	m_bmpInfo.biHeight = m_videoResolutionHeight;
	m_bmpInfo.biWidth = m_videoResolutionWidth;
	m_bmpInfo.biPlanes = 1;
	m_bmpInfo.biSizeImage = GetBitmapSize(&m_bmpInfo);	
	m_bmpInfo.biClrImportant = 0;
	m_bmpInfo.biClrUsed = 0;
	m_bmpInfo.biXPelsPerMeter = 0;
	m_bmpInfo.biYPelsPerMeter = 0;
	m_videoFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 4;
	m_videoSLAFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 3;

	SetFrameRate(30);
 


#if USE_AES
	InitializeAES();
#endif 

}

CLiveSourceStream::~CLiveSourceStream()
{


#if USE_AES
	if (recovered != NULL)
	{
		delete recovered;
		recovered = NULL;
	}
#endif 
}

HRESULT CLiveSourceStream::CheckMediaType(const CMediaType* pmt)
{
	CheckPointer(pmt, E_POINTER);

	return S_OK;
#if 0 
	if (pmt->majortype == MEDIATYPE_Stream)
		return S_OK;

	if (pmt->majortype != MEDIATYPE_Video)
	{
		return E_INVALIDARG;
	}
	if (pmt->subtype != MEDIASUBTYPE_H264)
	{
		return E_INVALIDARG;
	}
	return S_OK;
#endif 
}

HRESULT CLiveSourceStream::GetMediaType(int iPosition, CMediaType* pmt)
{
	CAutoLock cAutoLock(m_pLock);

	if(iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if(iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)pmt->AllocFormatBuffer(sizeof(VIDEOINFOHEADER));
	if(!vih)
	{
		return E_OUTOFMEMORY;
	}

	vih->bmiHeader = m_bmpInfo;
	if(m_frameRate != 0)
	{
		vih->AvgTimePerFrame = UNITS / m_frameRate;
	}
	else
	{
		vih->AvgTimePerFrame = 0;
	}
	 

	switch (iPosition)
	{
		case 0:
			pmt->SetType(&MEDIATYPE_Video);
			pmt->SetSubtype(&MEDIASUBTYPE_H264);
			pmt->SetFormatType(&FORMAT_VideoInfo);
			pmt->SetTemporalCompression(TRUE /*FALSE*/);
			pmt->SetSampleSize(m_bmpInfo.biSizeImage);
		break;
		case 1:
			pmt->SetType(&MEDIATYPE_Stream);
			pmt->SetSubtype(&GUID_NULL);			
			pmt->SetFormatType(&GUID_NULL);
		break;
	}
	
	return S_OK;
}

 

HRESULT CLiveSourceStream::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest)
{
	CheckPointer(pAlloc,E_POINTER);
	CheckPointer(ppropInputRequest,E_POINTER);

	CAutoLock cAutoLock(m_pLock);
	HRESULT hr = NOERROR;

	ppropInputRequest->cBuffers = 10;
	ppropInputRequest->cbBuffer = 1504 * 1000; // m_bmpInfo.biSizeImage;

	ASSERT(ppropInputRequest->cbBuffer);

	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(ppropInputRequest,&Actual);
	if(FAILED(hr))
	{
		return hr;
	}

	if(Actual.cbBuffer < ppropInputRequest->cbBuffer)
	{
		return E_FAIL;
	}

	pAlloc->Commit();
	//m_pAllocator->Commit();
	m_PushFinished = false;
	
	shared_ptr<thread> t1 = make_shared<thread>(&CLiveSourceStream::ReceiveThread, this);
	t1->detach();
	

	return S_OK;
}

 

void CLiveSourceStream::ReceiveThread()
{
	char pinTypes[3][100] = { "c:\\avc_4_demux.ts", "c:\\avc_row_for_demux.ts", "d:\\truck.ts" };
	 
	ifstream ifile(pinTypes[m_pinType]);
	if (!ifile) {
		return;
	}
	 

	while (true)
	{


	 
		//if (FileExists("c:\\avc_row_for_demux.ts") == true)
		//if (FileExists("c:\\avc_4_demux.ts") == true)
		{
			if (m_PushFinished == false)
			{
				if (PreFileHandle == NULL)
				{
					PreFileHandle = fopen(pinTypes[m_pinType], "r+b");
					//PreFileHandle = fopen("c:\\avc_row_for_demux.ts", "r+b");
				}
				HRESULT hr = S_OK;
				hr = AddFrameFromFile(PreFileHandle);
				if (hr == S_FALSE)
				{
					fclose(PreFileHandle);
					PreFileHandle = NULL;
					return;
				}
			}
			else
			{
				return;
			}
		}
	}

}

STDMETHODIMP CLiveSourceStream::SetBitmapInfo(BITMAPINFOHEADER& bInfo)
{
	if(bInfo.biHeight == 0 || bInfo.biWidth == 0 || bInfo.biBitCount == 0)
	{
		return E_INVALIDARG;
	}

	m_bmpInfo = bInfo;
	return S_OK;
}

  

STDMETHODIMP CLiveSourceStream::AddFrameFromFile(FILE *handle)
{

	//CAutoLock cAutoLock(m_pLock);


	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}


#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 

	int size = fread((char *)pData, 1 , RECEIVE_SIZE, handle);
	if (size == 0)
	{
		m_PushFinished = true;
		pSample->Release();
		return S_FALSE;
	}

	hr = pSample->SetActualDataLength(size);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	/*
	hr = pSample->SetSyncPoint(TRUE);
	if (FAILED(hr))
	{
	pSample->Release();
	return hr;
	}
	*/

#endif 
	hr = this->Deliver(pSample);
	pSample->Release();
	return hr;

}

STDMETHODIMP CLiveSourceStream::AddFrame(int clientSocket)
{
	CAutoLock cAutoLock(m_pLock);

#define SEND_SIZE RECEIVE_SIZE
	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr;
	hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}



#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 

#if !USE_FIFO
	
	int size = recv(
		clientSocket,
		(char *)(pData),
		RECEIVE_SIZE * 100,
		0);
	if (size == -1)
	{
		int lastError = WSAGetLastError();
		pSample->Release();
		return hr;
	}
	hr = pSample->SetActualDataLength(size);
#else 


	int sizeToSend;
	if ((sizeToSend = GetFifoSize()) < SEND_SIZE)
	{
		pSample->Release();
		return hr;
	}

	
	memcpy(pData, fifoBuffer + fifo_read, sizeToSend);
	//fwrite(pData, RECEIVE_SIZE * 3, 1, handle);
	 
	EnterCriticalSection(&CriticalSection);
	fifo_read = (fifo_read + sizeToSend) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);

	hr = pSample->SetActualDataLength(sizeToSend);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
#endif 
 
	hr = this->Deliver(pSample);
	 
	pSample->Release();

#endif 
	return hr;


}
  

STDMETHODIMP CLiveSourceStream::SetPort(int port)
{
	m_port = port;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetIpAddress(WCHAR *IpAddress)
{
	 
	char _ip[100];
	wchar_t array[] = L"Hello World";
	wcstombs(_ip, IpAddress, 100);
	strcpy(m_ipAddress , _ip);
	return S_OK;
}
 
STDMETHODIMP CLiveSourceStream::SetPinType(int pinType)
{

	m_pinType = pinType;

	return S_OK;
}

/////////////////////////
HRESULT CLiveSourceStream::GetMediaSample(IMediaSample** ppSample)
{
	REFERENCE_TIME rtStart = m_lastFrame;
	m_lastFrame += m_rtFrameRate;

	//return this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);

	HRESULT hr;
	hr = this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = (*ppSample)->SetTime(&rtStart, &m_lastFrame);

	return hr;

}

STDMETHODIMP CLiveSourceStream::SetFrameRate(int frameRate)
{
	if(frameRate < 0 || frameRate > 30)
	{
		return E_INVALIDARG;
	}

	m_frameRate = frameRate;
	m_rtFrameRate = UNITS / m_frameRate;
	return S_OK;
}

HRESULT CLiveSourceStream::GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize)
{
	ASSERT(hBmp);

	BITMAP bmp = {0};
	int res = ::GetObject(hBmp, sizeof(BITMAP), &bmp);
	if(res != sizeof(BITMAP))
	{
		return E_FAIL;
	}

	if(bmp.bmBitsPixel != m_bmpInfo.biBitCount ||
		bmp.bmHeight != m_bmpInfo.biHeight ||
		bmp.bmWidth != m_bmpInfo.biWidth)
	{
		return E_INVALIDARG;
	}

	*pSize = bmp.bmWidthBytes * bmp.bmHeight;
	memcpy(*ppData, bmp.bmBits, *pSize);

	return S_OK;
}

  
STDMETHODIMP CLiveSourceStream::Notify(IBaseFilter * pSender, Quality q)
{
	if(q.Proportion <= 0)
	{
		m_rtFrameRate = 1000;        
	}
	else
	{
		m_rtFrameRate = m_rtFrameRate * 1000 / q.Proportion;
		if(m_rtFrameRate > 1000)
		{
			m_rtFrameRate = 1000;    
		}
		else if(m_rtFrameRate < 30)
		{
			m_rtFrameRate = 30;      
		}
	}

	if(q.Late > 0)
	{
		m_rtFrameRate += q.Late;
	}

	return S_OK;
}