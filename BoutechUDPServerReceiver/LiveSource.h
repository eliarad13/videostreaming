#pragma once

#include <streams.h>
#include "IAddFrame.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <thread>
#include "WinAES.h"

#define USE_FIFO 1
using namespace std;

typedef enum GRAPHSTAT
{
	GRAPH_STOP,
	GRAPH_PAUSE,
	GRAPH_RUN,

} GRAPHSTAT;
 
#define RECEIVE_SIZE  1504 
 

int ReadParamsteres();

class CLiveSourceStream;

class CLiveSource : public CBaseFilter, public IAMFilterMiscFlags
{
public:
	DECLARE_IUNKNOWN;

	CLiveSource(LPUNKNOWN pUnk, HRESULT* phr);
	virtual ~CLiveSource(void);

	static CUnknown* WINAPI CreateInstance(LPUNKNOWN pUnk, HRESULT *phr);
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

	int GetPinCount();
	CBasePin* GetPin(int n);

	virtual ULONG STDMETHODCALLTYPE GetMiscFlags( void)
	{
		return AM_FILTER_MISC_FLAGS_IS_SOURCE;
	}

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	void ReceiveThread3();
	void ReceiveThread2();
	int Setup();
	const wchar_t *GetWC(const char *c);
	

private:
	CLiveSourceStream* m_pOutputPin;
	CCritSec m_critSec;
	
	SOCKET  ReceivingSocket;
	struct sockaddr_in addr;
};

class CLiveSourceStream : public CBaseOutputPin, public IBoutechLiveSource 
{
public:

	DECLARE_IUNKNOWN;

	CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr);
	virtual ~CLiveSourceStream();

	int m_videoResolutionWidth;
	int m_videoResolutionHeight;
	int m_videoFrameSize;
	int m_videoSLAFrameSize;

	// CBaseOutputPin overrides
	virtual HRESULT GetMediaType(int iPosition, CMediaType* pmt);
	virtual HRESULT CheckMediaType(const CMediaType *pmt);
	virtual HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest);

	// ISLA3000 Interface
	virtual STDMETHODIMP SetIpAddress(WCHAR *IpAddress);
	virtual STDMETHODIMP SetPort(int port);
	virtual STDMETHODIMP SetPinType(int pinType);


	// ILiveSource members
	virtual STDMETHODIMP AddFrameFromFile(FILE *handle);
	virtual STDMETHODIMP AddFrame(int clientSocket);
	virtual STDMETHODIMP SetFrameRate(int frameRate);
	virtual STDMETHODIMP SetBitmapInfo(BITMAPINFOHEADER& bInfo);

	virtual STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);
	void InitFifo();
	void AddToFifo(uint8_t *data, int size);
	void ReceiveThread();
	bool m_running;
	char			   m_ipAddress[100];
	int                m_port;
	WinAES *aes;

	uint8_t *fifoBuffer = NULL;
	unsigned int fifo_write = 0;
	unsigned int fifo_read = 0;
	 
	CRITICAL_SECTION CriticalSection;
	GRAPHSTAT  m_graphStat = GRAPH_STOP;
	byte *recovered = NULL;
private:
	HRESULT GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize);
	HRESULT GetMediaSample(IMediaSample** ppSample);
	

	void InitializeAES();
	void setSocket(int s);
	int GetFifoSize();
	const int MAX_FIFO = 100000000;

private:
	BITMAPINFOHEADER m_bmpInfo;
	int m_frameRate;
	REFERENCE_TIME m_rtFrameRate; 
	REFERENCE_TIME m_lastFrame; 
	FILE *PreFileHandle = NULL;
	bool m_PushFinished = false;
	int m_pinType;
	
	
};
