﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BauotechDVRStreamerCSLib
{
    public class BauotechDVRStreamer
    {

        int m_streamerId = 0;

        public BauotechDVRStreamer(int streamerId = 0)
        {
            m_streamerId = streamerId;
        }

#if DEBUG
        const string dsPath = @"C:\Program Files\Bauotech\Dll\BauotechDVRStreamer.dll";
#else
        const string dsPath = @"BauotechDVRStreamer.dll";
#endif



        [UnmanagedFunctionPointer(CallingConvention.StdCall)]
        public delegate void DVRStreamerCallback(int streamerId,string fileName, long fileSize, double AvgBitrate, double fileDuration);


        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern bool BSTREAMER_SetDVRStreamerCallback(int streamerId, DVRStreamerCallback p);

        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern bool BSTREAMER_Start(int streamerId, string fileName, ref long totalFilesSize, double startPCR, out int numFiles);

        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern bool BSTREAMER_Stop(int streamerId);

        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern bool BSTREAMER_AddClient(int streamerId, int clientId, string strAddr, int port, bool multicast, int multicastTTL);

        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern void BSTREAMER_Init(int streamerId);

        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern void BSTREAMER_Close(int streamerId);

        [DllImport(dsPath, CallingConvention = CallingConvention.StdCall)]
        public static extern void BSTREAMER_CloseAll();


        public void CloseAll()
        {
            BSTREAMER_CloseAll();
        }

        public bool AddClient(int clientId, string strAddr, int port, bool multicast, int multicastTTL)
        {
            return BSTREAMER_AddClient(m_streamerId, clientId, strAddr, port, multicast, multicastTTL);
        }

        public void Init()
        {
            BSTREAMER_Init(m_streamerId);
        }
        public bool Start(string fileName, ref long totalFilesSize, double startPCR, out int numFiles)
        {
            return BSTREAMER_Start(m_streamerId, fileName, ref totalFilesSize, startPCR, out numFiles);
        }
        public bool Stop()
        {
            return BSTREAMER_Stop(m_streamerId);
        }

        public void Close()
        {
            BSTREAMER_Close(m_streamerId);
        }
        public void SetDVRStreamerCallback(DVRStreamerCallback p)
        {       
            BSTREAMER_SetDVRStreamerCallback(m_streamerId , p);
        }
    }     
}
