#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <winsock2.h>
#include <stdio.h>
#include <Windows.h>
#include <thread>
#include "UDPMulticastClient.h"
#include <wxdebug.h>
#include "LiveSource.h"
#include <io.h> 
#include <ws2ipdef.h>
#define access    _access_s

using namespace std;
 
CUDPMulticastClient::CUDPMulticastClient()
{

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		return;
	}


}

int CUDPMulticastClient::Setup(char *ipAddress, int port, SOCKET *socket)
{
	int iResult;

	if (ReceivingSocket != -1)
	{
		closesocket(ReceivingSocket);
		ReceivingSocket = -1;
		*socket = ReceivingSocket;
	}

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		printf("Server: Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return -2;
	}
	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;

	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return -3;
	}


	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	addr.sin_addr.s_addr = htonl(INADDR_ANY); // differs from sender
	addr.sin_port = htons(port);

	// bind to receive address
	//
	if (::bind(ReceivingSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return -4;
	}

	//m_connectThread = make_shared<thread>(&CUDPMulticastClient::ConnectThread, this);


	IP_MREQ mreq;
	mreq.imr_multiaddr.s_addr = inet_addr(ipAddress);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(ReceivingSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
	{

		return -5;
	}
	*socket = ReceivingSocket;
	return 1;

}
void CUDPMulticastClient::GetAddr(struct sockaddr_in *a)
{
	a = &addr;

}