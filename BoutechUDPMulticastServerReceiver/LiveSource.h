#pragma once

#include <streams.h>
#include "IAddFrame.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <memory>
#include "WinAES.h"
#include "UDPMulticastClient.h"
#include "mpegts.hpp"

typedef enum GRAPHSTAT
{
	GRAPH_STOP,
	GRAPH_PAUSE,
	GRAPH_RUN,

} GRAPHSTAT;
 

#define USE_FIFO 1
 

class CLiveSourceStream;

class CLiveSource : public CBaseFilter, public IAMFilterMiscFlags
{
public:
	DECLARE_IUNKNOWN;

	CLiveSource(LPUNKNOWN pUnk, HRESULT* phr);
	virtual ~CLiveSource(void);

	static CUnknown* WINAPI CreateInstance(LPUNKNOWN pUnk, HRESULT *phr);
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

	int GetPinCount();
	CBasePin* GetPin(int n);

	void InitTSParser();
		
	virtual ULONG STDMETHODCALLTYPE GetMiscFlags( void)
	{
		return AM_FILTER_MISC_FLAGS_IS_SOURCE;
	}

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();
	
	int Setup();
	CLiveSourceStream* g_pOutputPin ;
	void ReceiveThread2();
	void ReceiveThread3();
	SOCKET  ReceivingSocket;
	struct sockaddr_in addr;
	
	bool m_syncRequired;
	shared_ptr<CUDPMulticastClient> m_udpMulticastClient;

private:
	CLiveSourceStream* m_pOutputPin;
	CCritSec m_critSec;


	bool m_canStart;

	util::mpegts_parser tsParser;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;
	time_t m_lastTime;
	int m_timeToLogInSec;

};

class CLiveSourceStream : public CBaseOutputPin, public IBoutechUDPMulticastServerReceiver
{
public:

	DECLARE_IUNKNOWN;

	WinAES *aes;

	CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr);
	virtual ~CLiveSourceStream();

	int m_videoResolutionWidth;
	int m_videoResolutionHeight;
	int m_videoFrameSize;
	int m_videoSLAFrameSize;

	// CBaseOutputPin overrides
	virtual HRESULT GetMediaType(int iPosition, CMediaType* pmt);
	virtual HRESULT CheckMediaType(const CMediaType *pmt);
	virtual HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest);

	virtual STDMETHODIMP ConfigureMulticastServer(WCHAR *IpAddress, int port);
	virtual STDMETHODIMP SetPinType(int pinType);


	void ReceiveThread();
	 

	// ILiveSource members
	virtual STDMETHODIMP AddFrameFromFile(FILE *handle);
	virtual STDMETHODIMP AddFrame(int clientSocket);
	virtual STDMETHODIMP AddFrame(BYTE* pBuffer, DWORD size);
	virtual STDMETHODIMP SetFrameRate(int frameRate);
	virtual STDMETHODIMP SetBitmapInfo(BITMAPINFOHEADER& bInfo);

	void InitFifo();
	void AddToFifo(uint8_t *data, int size);
	int GetFifoSize();
	const int MAX_FIFO = 100000000;
	CRITICAL_SECTION CriticalSection;
	GRAPHSTAT  m_graphStat;
	uint8_t *fifoBuffer;
	unsigned int fifo_write;
	unsigned int fifo_read;

	virtual STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);

	bool m_running;
	char			   m_ipAddress[100];
	int                m_port;
	int m_pinType;	
	

	void InitializeAES();

 
	byte *recovered = NULL;


private:
	HRESULT GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize);
	HRESULT GetMediaSample(IMediaSample** ppSample);

private:
	BITMAPINFOHEADER m_bmpInfo;
	int m_frameRate;
	REFERENCE_TIME m_rtFrameRate; 
	REFERENCE_TIME m_lastFrame; 

	FILE *PreFileHandle = NULL;
	bool m_PushFinished = false;
	

};
