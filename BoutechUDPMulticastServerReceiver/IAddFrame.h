#pragma once

#define LIVE_FILTER_NAME TEXT("Bauotech UDP Multicast Server Receiver")
#define LIVE_OUTPIN_NAME L"Out"





// {4EF98278-E3B9-45D1-B04E-95FDAD0261BE}
static const GUID IID_IBoutechUDPMulticastServerReceiver =
{ 0x4ef98278, 0xe3b9, 0x45d1, { 0xb0, 0x4e, 0x95, 0xfd, 0xad, 0x2, 0x61, 0xbe } };




// {1CE9EB6D-4F1B-43F1-ADF1-1192C40A2915}
static const GUID CLSID_CBoutechUDPMulticastServerReceiver =
{ 0x1ce9eb6d, 0x4f1b, 0x43f1, { 0xad, 0xf1, 0x11, 0x92, 0xc4, 0xa, 0x29, 0x15 } };




DECLARE_INTERFACE_(IBoutechUDPMulticastServerReceiver, IUnknown)
{

	STDMETHOD(ConfigureMulticastServer)(WCHAR *IpAddress, int port) PURE;
	STDMETHOD(SetPinType)(int pinType) PURE;
};

