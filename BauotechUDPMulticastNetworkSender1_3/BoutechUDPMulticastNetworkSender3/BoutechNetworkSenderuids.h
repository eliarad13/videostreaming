//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

 
// {C24FE0A9-5380-46B7-A377-8389F94B702B}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0xc24fe0a9, 0x5380, 0x46b7, 0xa3, 0x77, 0x83, 0x89, 0xf9, 0x4b, 0x70, 0x2b);

  

// {FF477D0B-3003-4B84-937E-5C9ED25E0B3F}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
	0xff477d0b, 0x3003, 0x4b84, 0x93, 0x7e, 0x5c, 0x9e, 0xd2, 0x5e, 0xb, 0x3f);


DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};