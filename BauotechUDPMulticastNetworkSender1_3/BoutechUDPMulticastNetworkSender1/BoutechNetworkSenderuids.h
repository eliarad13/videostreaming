//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

 
// {25BD5B6D-9680-4A57-A506-AFC5248F5D1E}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0x25bd5b6d, 0x9680, 0x4a57, 0xa5, 0x6, 0xaf, 0xc5, 0x24, 0x8f, 0x5d, 0x1e);

// {00F8F53E-AF14-4EA2-8337-882C4389FA11}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
	0xf8f53e, 0xaf14, 0x4ea2, 0x83, 0x37, 0x88, 0x2c, 0x43, 0x89, 0xfa, 0x11);

 

DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};