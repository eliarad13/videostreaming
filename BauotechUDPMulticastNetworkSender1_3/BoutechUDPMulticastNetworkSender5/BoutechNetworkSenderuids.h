//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 
 

// {3F3BC13E-8700-4243-92EC-8BBDDCFEE90A}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0x3f3bc13e, 0x8700, 0x4243, 0x92, 0xec, 0x8b, 0xbd, 0xdc, 0xfe, 0xe9, 0xa);


 

// {E047F413-70DA-4799-BE53-A8B8BF32C067}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
	0xe047f413, 0x70da, 0x4799, 0xbe, 0x53, 0xa8, 0xb8, 0xbf, 0x32, 0xc0, 0x67);



DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};