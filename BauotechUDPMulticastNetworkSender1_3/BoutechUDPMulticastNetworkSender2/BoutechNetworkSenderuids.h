//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

 
// {8821C015-CF3E-4B4D-81D8-6843FC87814E}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0x8821c015, 0xcf3e, 0x4b4d, 0x81, 0xd8, 0x68, 0x43, 0xfc, 0x87, 0x81, 0x4e);

 

// {9E459AC1-23DC-4363-8A49-3A120530AE4D}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
	0x9e459ac1, 0x23dc, 0x4363, 0x8a, 0x49, 0x3a, 0x12, 0x5, 0x30, 0xae, 0x4d);

 

DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};