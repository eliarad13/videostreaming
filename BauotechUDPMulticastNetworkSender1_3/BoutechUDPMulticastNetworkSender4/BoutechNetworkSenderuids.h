//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 
// {A476A830-F51D-47B5-9CF6-169255D6E4FF}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0xa476a830, 0xf51d, 0x47b5, 0x9c, 0xf6, 0x16, 0x92, 0x55, 0xd6, 0xe4, 0xff);

  


// {B8ED8638-F80B-4EA6-B178-06DD0A70F5A2}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
	0xb8ed8638, 0xf80b, 0x4ea6, 0xb1, 0x78, 0x6, 0xdd, 0xa, 0x70, 0xf5, 0xa2);




DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};