//------------------------------------------------------------------------------
// File: BoutechNetworkSender.cpp
 #include <WinSock2.h>
#include <ws2ipdef.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <stdint.h>
#include "BoutechNetworkSenderuids.h"
#include "BoutechUDPMulticastNetworkSender.h"
#include <thread>
using namespace std;
#include <memory>
 
 

//https://www.codeproject.com/Articles/1705/IP-Multicasting-in-C

 
 

// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
	&CLSID_BoutechMulticastNetworkSender,                // Filter CLSID
    L"Bauotech UDP Multicast Network Sender4",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
	L"Bauotech UDP Multicast Network Sender4", &CLSID_BoutechMulticastNetworkSender, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;

 

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
						 CBaseFilter(NAME("CMulticastSenderFilter"), pUnk, pLock, CLSID_BoutechMulticastNetworkSender),
    m_pDump(pDump)
{
 
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}


//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	 
	m_pDump->m_running = false;
	 
	if (m_pDump->socketC != -1)
		closesocket(m_pDump->socketC);
	m_pDump->socketC = -1;

    
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);

    if (m_pDump)
    {
         
    }

    return CBaseFilter::Pause();
}

 
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	CAutoLock cObjectLock(m_pLock);
	m_pDump->ReconnectSocket(m_pDump->m_IpAddress, m_pDump->m_port, m_pDump->m_interfaceAddress);
	m_pDump->m_running = true;
	  
    return CBaseFilter::Run(tStart);
}
 
//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{
 
	
	
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}


//
// BreakConnect
//
// Break a connection
//
HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}

 
 

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

  

//
// Receive
//
// Do something with this media sample
//
STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
	if (m_pDump->m_running == false)
		return S_OK; 

    //CheckPointer(pSample,E_POINTER);

    //CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
	 
    
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr))
	{
        return hr;
    } 
 
	int size = pSample->GetActualDataLength();
	sendto(m_pDump->socketC, (const char *)pbData, size, 0, (sockaddr*)&m_pDump->serverInfo, sizeof(m_pDump->serverInfo));
	 
	return hr;
}
 
//
// EndOfStream
//
STDMETHODIMP CDumpInputPin::EndOfStream(void)
{
    CAutoLock lock(m_pReceiveLock);
    return CRenderedInputPin::EndOfStream();

} // EndOfStream
 
  
HRESULT CDump::ReconnectSocket(const char *IpAddress, const int port, const char *IpInterfaceAddress)
{

	 
	unsigned char one = 1;
	int status;
	 
	if (socketC != -1)
	{
		closesocket(socketC);
		socketC = -1;
	}
	serverInfo.sin_family = AF_INET;
	  

	serverInfo.sin_port = htons(port);
	serverInfo.sin_addr.s_addr = inet_addr(IpAddress);
	socketC = socket(AF_INET, SOCK_DGRAM, 0);
	 
	//send multicast traffic to myself too
	//status = setsockopt(socketC, IPPROTO_IP, IP_MULTICAST_LOOP,	(const char *)&one, sizeof(unsigned char));

	if (strcmp(IpInterfaceAddress , "") != 0)
	{	 		 
		DWORD localadapter = inet_addr(IpInterfaceAddress);
		setsockopt(socketC, IPPROTO_IP, IP_MULTICAST_IF, (char*)&localadapter, sizeof(localadapter));		
	}

	 
	IP_MREQ mreq;
	mreq.imr_multiaddr.s_addr = inet_addr(IpAddress);
	mreq.imr_interface.s_addr = htonl(INADDR_ANY);
	if (setsockopt(socketC, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
	{
		int r = GetLastError();
		::MessageBoxA(NULL, "Cannot add memership: -51422", "UDP Multicast Sender", 0);
		return -5;
	}

	 
	char  multicastTTL = 3;
	if (setsockopt(socketC, IPPROTO_IP, IP_MULTICAST_TTL, (const char *)&multicastTTL,
		sizeof(multicastTTL)) < 0)
	{

	}

	if (::connect(socketC, ( const struct sockaddr *)&serverInfo, sizeof(serverInfo)) < 0)
	{
		::MessageBoxA(NULL, "Cannot Connect: -51421", "UDP Multicast Sender", 0);
		return -6;
	}
 
	return S_OK;
}

 
 
STDMETHODIMP CDumpInputPin::NewSegment(REFERENCE_TIME tStart,
                                       REFERENCE_TIME tStop,
                                       double dRate)
{
    m_tLast = 0;
    return S_OK;

}  

 
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
CUnknown(NAME("BoutechNetworkSender"), pUnk),
m_pFilter(NULL),
m_pPin(NULL),
m_pPosition(NULL),
m_hFile(INVALID_HANDLE_VALUE),
m_pFileName(0),
m_fWriteError(0)
{
	ASSERT(phr);


	int len = sizeof(serverInfo);

	strcpy(m_IpAddress, "234.5.5.9");
	m_port = 9414;
	strcpy(m_interfaceAddress, "");
	

	m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
	if (m_pFilter == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
		return;
	}

	m_pPin = new CDumpInputPin(this, GetOwner(),
		m_pFilter,
		&m_Lock,
		&m_ReceiveLock,
		phr);
	if (m_pPin == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
		return;
	}
	 

	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		return;
	} 

}


CDump::~CDump()
{
	m_running = false;
	
	if (socketC > 0)
		closesocket(socketC);


    delete m_pPin;
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}
 
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

}  

STDMETHODIMP CDump::ConfigureMulticastSender(const WCHAR *IpAddress, const int port , const WCHAR *IpInterfaceAddress)
{
	wcstombs(m_IpAddress, IpAddress, 100);
	m_port = port;
	if (IpInterfaceAddress == NULL)
	{
		wcstombs(m_interfaceAddress, L"", 100);
	}
	else {
		wcstombs(m_interfaceAddress, IpInterfaceAddress, 100);
	}

	return S_OK;
}

 
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);


	 
    // Do we have this interface
	 
	if (riid == IID_IBoutechUDPMulticastSender) {
		return GetInterface((IBoutechUDPMulticastSender *) this, ppv);
	}
	else 
    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Boutech UDP Multicast Network Sender4 Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

}  
  
 

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

