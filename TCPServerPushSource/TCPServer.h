#pragma once
#include <thread>
#include <memory>
using namespace std;

#define FIFO_SIZE 1024 * 1024 * 100


class CTCPServer
{
public:
	CTCPServer(const char *IpAddress, int port) : m_port(port)
	{
		Init();
		strcpy(m_ipAddress, IpAddress);
		ClientSocket = -1;
#if USE_FIFO
		Fifo = (uint8_t *)malloc(FIFO_SIZE);
#endif 
		m_writeIndex = 0;
		m_readIndex = 0;

	}

	CTCPServer(int port) : m_port(port)
	{
		Init();

	}
	~CTCPServer();


	void StartConnectThread(); 
	void AcceptThread();
	int Setup(bool allInterface);
	void Start();
	void Stop();
	void Pause();
	void StartReceiveThread();

#define RECEIVE_SIZE  188 * 7
	
private:
	WSADATA			   wsaData;
	char			   m_ipAddress[100];
	SOCKET             ListenSocket;
	SOCKET			   ClientSocket;
	int                m_port;
	uint8_t			   ReceiveBuf[RECEIVE_SIZE];
	uint8_t			   FrameBuf[RECEIVE_SIZE];
	int                BufLength;
	int                SenderAddrSize;
	int                ByteReceived;	
	int				   ErrorCode;
	bool			   m_running;

	int				   m_writeIndex;
	int				   m_readIndex;
	uint8_t			   *Fifo;

	shared_ptr<thread> m_startMpeg2ConnectThread;
	shared_ptr<thread> m_acceptThread;
	shared_ptr<thread> m_receiveThread;
	shared_ptr<thread> m_frameThread;

private:
	void Process4Mpeg2Connect();
	void ReceiveThread();
	void FrameThread();
	int recvfromTimeOutTCP(SOCKET socket, long sec, long usec);
	void PrintError(const char *msg);
	const wchar_t *GetWC(const char *c);
	void ProcessMessage(int SelectTiming);

	void Init();	 

	void Enqueue(uint8_t *buffer, int size);
	int Dequeue();

	int GetFifoSize();

};

