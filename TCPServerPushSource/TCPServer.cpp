#include <stdint.h>
#include <iostream>
#include <memory>
#include <winsock2.h>
#include <stdio.h>
#include <Windows.h>
#include <thread>
#include "TCPServer.h"
#include <wxdebug.h>
#include "LiveSource.h"
#include  <io.h>


using namespace std;

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))
const int MAX_FIFO = 100000000;


CRITICAL_SECTION CriticalSection;
extern CLiveSourceStream* g_pOutputPin;
#define USE_FIFO 0
extern GRAPHSTAT  m_graphStat;

uint8_t *fifoBuffer = NULL;
unsigned int fifo_write = 0;
unsigned int fifo_read = 0;

CTCPServer::~CTCPServer()
{

	m_running = false;
	if (Fifo != NULL)
	{
		delete Fifo;
		Fifo = NULL;
	}
}

void InitFifo()
{
	fifoBuffer = (uint8_t *)malloc(MAX_FIFO);
}


void CTCPServer::Init()
{

	WSAStartup(MAKEWORD(2, 2), &wsaData);
	// Initialize the critical section one time only.
	InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x00000400);
	m_running = false;
	BufLength = RECEIVE_SIZE;
	InitFifo();
}


void CTCPServer::ReceiveThread()
{
	int SelectTiming;
	while (m_running)
	{
		if (ClientSocket == -1)
		{
			Sleep(1000);
			continue;
		}
		SelectTiming = recvfromTimeOutTCP(ClientSocket, 1, 0);
		if (SelectTiming == 1)
			ProcessMessage(SelectTiming);
	}

} 

void CTCPServer::FrameThread()
{
	int size;
	while (m_running)
	{
		Dequeue();
	}
}


int CTCPServer::GetFifoSize()
{
	EnterCriticalSection(&CriticalSection);

	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return 0;
	}


	if (fifo_write > fifo_read)
	{
		int x = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return x;
	}

	int x = (MAX_FIFO - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return x;
}


void CTCPServer::Enqueue(uint8_t *data, int size)
{

	int x = MAX_FIFO - GetFifoSize();
	if (x < size)
	{
		return;
	}

	int d = MIN(size, MAX_FIFO - fifo_write);
	memcpy(fifoBuffer + fifo_write, data, d);

	EnterCriticalSection(&CriticalSection);
	fifo_write = (fifo_write + d) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);


	size -= d;
	if (size > 0)
	{
		memcpy(fifoBuffer + fifo_write, data, size);
		EnterCriticalSection(&CriticalSection);
		fifo_write = (fifo_write + size) % MAX_FIFO;
		LeaveCriticalSection(&CriticalSection);
	}
}

 

int CTCPServer::Dequeue()
{
	int size = GetFifoSize();
	if (size == 0)
	{
		return 0;
	}

	if ((fifo_read + size) < MAX_FIFO)
	{
		g_pOutputPin->AddFrame((BYTE *)fifoBuffer + fifo_read, size);
		fifo_read += size;
	}
	else 
	{
		g_pOutputPin->AddFrame((BYTE *)fifoBuffer + fifo_read, MAX_FIFO - fifo_read);
		fifo_read = 0;
	}
	 
}


bool FileExists(const std::string &Filename)
{
	return access(Filename.c_str(), 0) == 0;
}

FILE *PreFileHandle = NULL;
bool m_PushFinished = false;


void CTCPServer::StartConnectThread()
{
	m_startMpeg2ConnectThread = make_shared<thread>(&CTCPServer::Process4Mpeg2Connect, this);
}

void CTCPServer::Process4Mpeg2Connect()
{
	 
	while (true)
	{		 
		if (FileExists("c:\\avc_4_demux.ts") == true)
		{
			if (PreFileHandle == NULL)
			{
				PreFileHandle = fopen("c:\\avc_4_demux.ts", "r+b");
			}
			if (m_PushFinished == false)
			{
				g_pOutputPin->AddFrameFromFile(PreFileHandle);
			}
			else
			{
				break;
			}			  
		}	 
	}
}
 
void CTCPServer::ProcessMessage(int SelectTiming)
{
	SOCKADDR_IN        SenderAddr;
	SenderAddrSize = sizeof(SenderAddr);

	switch (SelectTiming)
	{
		case 0:
			// Timed out, do whatever you want to handle this situation
			printf("Server: Timeout lor while waiting you bastard client!...\n");
		break;
		case -1:
			// Error occurred, maybe we should display an error message?
			// Need more tweaking here and the recvfromTimeOutUDP()...
			printf("Server: Some error encountered with code number: %ld\n", WSAGetLastError());
		break;
		default:
		{			 
			while (m_running)
			{
 
				if (m_graphStat == GRAPH_RUN)
				{
					g_pOutputPin->AddFrame(ClientSocket);
				}
				else
				{ 
					Sleep(100);
				}					 
 			}			 
		}
		break;
	}
}

// A sample of the select() return value

int CTCPServer::recvfromTimeOutTCP(SOCKET socket, long sec, long usec)

{

	// Setup timeval variable

	struct timeval timeout;

	struct fd_set fds;



	timeout.tv_sec = sec;

	timeout.tv_usec = usec;

	// Setup fd_set structure

	FD_ZERO(&fds);

	FD_SET(socket, &fds);

	// Return value:

	// -1: error occurred

	// 0: timed out

	// > 0: data ready to be read

	return ::select(0, &fds, 0, 0, &timeout);

}

const wchar_t *CTCPServer::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

void CTCPServer::PrintError(const char *msg)
{

	char buffer[100];
	sprintf(buffer, "%s %d", msg, WSAGetLastError());

	DbgLog((LOG_TRACE, 3, buffer));
}

int CTCPServer::Setup(bool allInterface)
{

	// Create a new socket to receive datagrams on.
	ListenSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ListenSocket == INVALID_SOCKET)
	{
		printf("Server: Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return -1;
	}

	// The IPv4 family
	SOCKADDR_IN        ReceiverAddr;

	ReceiverAddr.sin_family = AF_INET;
	ReceiverAddr.sin_port = htons(m_port);
	
	DWORD ip = inet_addr(m_ipAddress); 
	if (allInterface == false)
		ReceiverAddr.sin_addr.s_addr = htonl(ip);
	else 
		ReceiverAddr.sin_addr.s_addr = htonl(INADDR_ANY);

 
	int res = ::bind(ListenSocket, (SOCKADDR *)&ReceiverAddr, sizeof(ReceiverAddr));
	if (res == SOCKET_ERROR)
	{
 		 
		char string[25];
		itoa(WSAGetLastError(), string, 10);
		char msg[100];
		sprintf(msg, "Server bind error %d", WSAGetLastError());

	 
		::MessageBox(NULL, GetWC(msg), L"TCP Source filter", 0);
		// Close the socket
		closesocket(ListenSocket);
		// Do the clean up
		WSACleanup();
		// and exit with error
		return -1;
	}

	int iResult = listen(ListenSocket, SOMAXCONN);
	if (iResult == SOCKET_ERROR) {
		printf("listen failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return 1;
	}
	 
	m_acceptThread = make_shared<thread>(&CTCPServer::AcceptThread, this);
	 
	return 1;

}

void CTCPServer::AcceptThread()
{

	// Accept a client socket
	ClientSocket = accept(ListenSocket, NULL, NULL);
	if (ClientSocket == INVALID_SOCKET)
	{
		printf("accept failed with error: %d\n", WSAGetLastError());
		closesocket(ListenSocket);
		WSACleanup();
		return;
	}
	 
	// No longer need server socket
	closesocket(ListenSocket);
	 
}

void CTCPServer::StartReceiveThread()
{
	m_running = true;
	m_receiveThread = make_shared<thread>(&CTCPServer::ReceiveThread, this);
}

void CTCPServer::Pause()
{

}
void CTCPServer::Stop()
{
	m_running = false;
	 
		 
}
void CTCPServer::Start()
{
	if (m_running == true)
		return;
	 
	//m_running = true;
	//m_receiveThread = make_shared<thread>(&CTCPServer::ReceiveThread, this);

	StartReceiveThread();

#if USE_FIFO
	m_frameThread = make_shared<thread>(&CTCPServer::FrameThread, this);
#endif 
}