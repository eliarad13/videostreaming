//------------------------------------------------------------------------------
// File: AsyncFlt.cpp
//
// Desc: DirectShow sample code - implementation of CAsyncFilter.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <winsock2.h>
#include <windows.h>
#include <streams.h>
#include <string>
#include <string.h>
#include <memory>
#include <thread>
#include "asyncio.h"
#include "asyncrdr.h"
#pragma warning(disable:4710)  // 'function' not inlined (optimization)
#include "asyncflt.h"


//
// Setup data for filter registration
//
const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{ &MEDIATYPE_Stream     // clsMajorType
, &MEDIASUBTYPE_NULL }; // clsMinorType

const AMOVIESETUP_PIN sudOpPin =
{ L"Output"          // strName
, FALSE              // bRendered
, TRUE               // bOutput
, FALSE              // bZero
, FALSE              // bMany
, &CLSID_NULL        // clsConnectsToFilter
, L"Input"           // strConnectsToPin
, 1                  // nTypes
, &sudOpPinTypes };  // lpTypes

const AMOVIESETUP_FILTER sudAsync =
{ &CLSID_BauotechHLSSource              // clsID
, L"Bauotech HLS Source"  // strName
, MERIT_UNLIKELY                  // dwMerit
, 1                               // nPins
, &sudOpPin };                    // lpPin


//
//  Object creation template
//
CFactoryTemplate g_Templates[1] = {
    { L"Bauotech HLS Source"
	, &CLSID_BauotechHLSSource
    , CAsyncFilter::CreateInstance
    , NULL
    , &sudAsync }
};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}


//* Create a new instance of this class
CUnknown * WINAPI CAsyncFilter::CreateInstance(LPUNKNOWN pUnk, HRESULT *phr)
{
    ASSERT(phr);

    //  DLLEntry does the right thing with the return code and
    //  the returned value on failure

    return new CAsyncFilter(pUnk, phr);
}


SOCKET CAsyncFilter::HTTPConnectToServer()
{
	SOCKADDR_IN serverInfo;
	SOCKET sck;
	WSADATA wsaData;
	LPHOSTENT hostEntry;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	sck = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sck == INVALID_SOCKET){
		WSACleanup();
		puts("Failed to setup socket");
		getchar();
		return 0;
	}
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = inet_addr(ServerIpAddress);
	serverInfo.sin_port = htons(ServerPort);
	int i = connect(sck, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	if (sck == SOCKET_ERROR) return 0;
	if (i != 0) return 0;

	return sck;
}


void CAsyncFilter::HTTPRequestPage(SOCKET s, char *page, char *host)
{
	unsigned int len;
	if (strlen(page)>strlen(host)){
		len = strlen(page);
	}
	else len = strlen(host);

	char message[20 + 100];
	if (strlen(page) <= 0){
		strcpy(message, "GET / HTTP/1.1\r\n");
	}
	else sprintf(message, "GET %s HTTP/1.1\r\n", page);
	send(s, message, strlen(message), 0);

	memset(message, 0, sizeof(message));
	sprintf(message, "Host: %s\r\n\r\n", host);
	send(s, message, strlen(message), 0);
}

BOOL CAsyncFilter::DownloadToBuffer(char * webpage, uint8_t *buffer, unsigned long max)
{
	if (webpage == NULL || buffer == NULL || max == 0) return FALSE;

	unsigned short shift = 0;
	if (_strnicmp(webpage, "http://", strlen("http://")) == 0){
		shift = strlen("http://");
	}
	if (_strnicmp(webpage + shift, "www.", strlen("www.")) == 0){
		shift += strlen("www.");
	}
	char cut[200];
	strcpy(cut, _strdup(webpage + shift));

	char *p;
	char *server = strtok(cut, "/");
	char ipAddress[100];
	char port[20];
	ServerPort = 80;
	strcpy(ipAddress, server);
	p = strchr(ipAddress, ':');
	if (p != NULL)
	{
		*p = 0;
		strcpy(ServerIpAddress, ipAddress);
		strcpy(port, server);
		p = strchr(port, ':') + 1;
		strcpy(port, p);
		ServerPort = atoi(port);
	}
	else
	{
		strcpy(ipAddress, server);
		strcpy(ServerIpAddress, ipAddress);
		ServerPort = 80;
	}


	char *page = _strdup(webpage + shift + strlen(server));

	strcpy(relativeAddress, page);
	p = relativeAddress;
	p = p + strlen(relativeAddress) - 1;
	while (*p != '/')
		p--;
	*p = 0;

	SOCKET s = HTTPConnectToServer();
	HTTPRequestPage(s, page, server);

	int i = recv(s, (char *)buffer, max, 0);
	closesocket(s);

	if (i <= 0) return FALSE;
	return TRUE;
}

bool CAsyncFilter::GetManifest(char *url)
{
	DownloadToBuffer("http://10.0.0.10:8080/1/mp_2020_04_15_12_11_17/hls.m3u8", manifestBuffer, 20000);
	char *p = strchr((char *)manifestBuffer, '\n');
	char response[30];
	memset(response, 0, sizeof(response));
	memcpy(response, manifestBuffer, p - (char *)manifestBuffer);
	p = strchr(response, '\r');
	if (p != NULL)
		*p = NULL;
	if (strcmp(response, "HTTP/1.1 200 OK") == 0)
	{


		char* token = strtok((char *)manifestBuffer, "\n");
		while (token != NULL)
		{
			token = strtok(NULL, "\n");
			if (strstr(token, "EXTINF") != NULL)
			{
				while (token != NULL)
				{
					if (strstr(token, "EXTINF") != NULL)
					{
						strcpy(temp, token);
						p = strchr(temp, ':');
						p++;
						strcpy(temp, p);
						if (strchr(temp, ',') != NULL)
							*p = 0;
						m_hlsTimeLine.push_back(atof(temp));
						if (m_hlsTimeLine.size() == 185)
							printf("f");
					}
					else
					{
						strcpy(temp, token);
						if ((p = strchr(temp, '\r')) != NULL)
							*p = 0;
						m_hlsFileList.push_back(temp);
					}
					token = strtok(NULL, "\n");
				}
				if (m_hlsFileList.size() == 0)
					return false;
				return true;
			}
		}
	}
	else
	{
		return false;
	}

}
void CAsyncFilter::ReadHLSFilesThread()
{
	 
	m_hlsWr = 0;
	 
	m_maxHlsBufferSize = 1024 * 1024 * 188;
	hlsBuffer = (uint8_t *)malloc(m_maxHlsBufferSize);
		
	SOCKET s = HTTPConnectToServer();
	char url[1000];
	sprintf(url, "GET %s/%s HTTP/1.0\r\n\r\n", relativeAddress, m_hlsFileList.front().c_str());
	m_hlsFileList.pop_front();
	send(s, url, strlen(url), 0);
	int r = recv(s, (char *)hlsBuffer, 2000, 0);	 
	if (r == -1)
	{
		closesocket(s);
		return;
	}
	m_hlsWr += r;
	while (r > 0)
	{
		r = recv(s, (char *)hlsBuffer + m_hlsWr, 2000, 0);
		if (r == -1)
		{
			closesocket(s);
			return;
		}
		m_hlsWr += r;
	}
	closesocket(s);
	m_pbData = hlsBuffer;
	m_llSize = m_hlsWr;
	m_fileLoaded = true;

	list<string>::iterator it;
	int count = 0;
	for (it = m_hlsFileList.begin(); it != m_hlsFileList.end(); ++it)
	{
		if (count == 2)
			break;
		sprintf(url, "GET %s/%s HTTP/1.0\r\n\r\n", relativeAddress, (*it).c_str());
		SOCKET s = HTTPConnectToServer();
		send(s, url, strlen(url), 0);
		int r;
		r = recv(s, (char *)(hlsBuffer + m_hlsWr), 2000, 0);
		if (r == -1)
		{
			closesocket(s);
			continue;
		}
		m_hlsWr += r;
		m_llSize = m_hlsWr;
		m_Stream.UpdateSize(m_llSize);
		while (r > 0)
		{
			r = recv(s, (char *)(hlsBuffer + m_hlsWr), 2000, 0);
			if (r == -1)
			{
				break;
			}
			m_hlsWr += r;
			m_llSize = m_hlsWr;
			m_Stream.UpdateSize(m_llSize);
		}
		closesocket(s);
		count++;
	}

	 
}
 

BOOL CAsyncFilter::ReadTheFile(LPCTSTR lpszFileName)
{
    DWORD dwBytesRead;

	size_t size = wcstombs(NULL, lpszFileName, 0);
	char* fileName = new char[size + 1];
	memset(fileName, 0, size + 1);
	wcstombs(fileName, lpszFileName, size + 1);
	m_fileLoaded = false;
#if 0 

    // Open the requested file
    HANDLE hFile = CreateFile(lpszFileName,
                              GENERIC_READ,
                              FILE_SHARE_READ,
                              NULL,
                              OPEN_EXISTING,
                              0,
                              NULL);
    if (hFile == INVALID_HANDLE_VALUE) 
    {
        DbgLog((LOG_TRACE, 2, TEXT("Could not open %s\n"), lpszFileName));
        return FALSE;
    }

    // Determine the file size
    ULARGE_INTEGER uliSize;
    uliSize.LowPart = GetFileSize(hFile, &uliSize.HighPart);

    PBYTE pbMem = new BYTE[uliSize.LowPart];
    if (pbMem == NULL) 
    {
        CloseHandle(hFile);
        return FALSE;
    }

    // Read the data from the file
    if (!ReadFile(hFile,
                  (LPVOID) pbMem,
                  uliSize.LowPart,
                  &dwBytesRead,
                  NULL) ||
        (dwBytesRead != uliSize.LowPart))
    {
        DbgLog((LOG_TRACE, 1, TEXT("Could not read file\n")));

        delete [] pbMem;
        CloseHandle(hFile);
        return FALSE;
    }

    // Save a pointer to the data that was read from the file
    m_pbData = pbMem;
    m_llSize = (LONGLONG)uliSize.QuadPart;

    // Close the file
    CloseHandle(hFile);
#else 
	
	
	if (GetManifest(fileName) == true)
	{
		shared_ptr<thread> t1 = make_shared<thread>(&CAsyncFilter::ReadHLSFilesThread, this);
		t1->detach();

		while (m_fileLoaded == false)
		{
			Sleep(300);
		}
		 
	}
	else
	{
		MessageBoxA(NULL, "Failed to load HLS manifest", "Bauotech HLS Source", 0);
	}  

#endif 

    return TRUE;
}

