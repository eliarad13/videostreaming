#include "Fifo.h"
#include <Windows.h>
#include <stdio.h>
#include <stdlib.h>
#include <memory.h>

CFifo::CFifo()
{

}

CFifo::~CFifo()
{

}

void CFifo::CreateFifo(int size)
{

	fifo = NULL;
	fifo_wr = 0;
	fifo_rd = 0;

	fifo = (uint8_t *)malloc(size);
	fifo_wr = 0;
	fifo_rd = 0;
	fifo_size = size;
	
}

int CFifo::GetFifoFullness()
{
	mutex.Enter();
	uint32_t x;
	if (fifo_wr == fifo_rd)
		x = 0;
	else 
	if (fifo_wr > fifo_rd)
		x = fifo_wr - fifo_rd;
	else 
		x  = fifo_size - fifo_rd + fifo_wr;

	mutex.Leave();
	return x;
}
int CFifo::GetFifoEmptiness()
{
	mutex.Enter();
	uint32_t x;
	if (fifo_wr == fifo_rd)
		x = fifo_size;
	else
	if (fifo_wr > fifo_rd)
		x = fifo_size - (fifo_wr - fifo_rd);
	else
		x = fifo_size - (fifo_size - fifo_rd + fifo_wr);

	mutex.Leave();
	return x;

}
void CFifo::FifoPush(uint8_t *buffer, uint32_t size)
{ 

	uint32_t x;
	while ((x = GetFifoEmptiness()) < size)
	{
		Sleep(50);
	}
	int _size = size;
	mutex.Enter();
	if (fifo_wr >= fifo_rd)
	{
		x = fifo_size - fifo_wr;
		if (x >= size)
			memcpy(fifo + fifo_wr, buffer, size);
		else
		{
			memcpy(fifo + fifo_wr, buffer, x);
			size -= x;
			memcpy(fifo, buffer + x, size);
		}
	}
	else
	{
		memcpy(fifo + fifo_wr, buffer, size);
	}

	
	fifo_wr = (fifo_wr + _size) % fifo_size;
	mutex.Leave();
}


void CFifo::FifoPush(uint32_t size)
{
	uint32_t x;
	while ((x = GetFifoEmptiness()) < size)
	{
		Sleep(50);
	}
	mutex.Enter();
	fifo_wr = (fifo_wr + size) % fifo_size;
	mutex.Leave();
}

void CFifo::FifoGet(uint8_t *buffer)
{
	int size = 1;
	while (GetFifoFullness() < 1)
	{
		Sleep(120);
	}
	mutex.Enter();
	int _size = 1;
	if (fifo_wr > fifo_rd)
	{
		*buffer = fifo[fifo_rd];
	}
	else
	{
		int x = fifo_size - fifo_rd;
		if (size <= x)
		{
			*buffer = fifo[fifo_rd];
		}
		else
		{
			*buffer = fifo[fifo_rd];
		}
	}

	fifo_rd = (fifo_rd + 1) % fifo_size;
	mutex.Leave();
}


void CFifo::FifoPull(uint8_t *buffer, uint32_t size)
{ 
	while (GetFifoFullness() < size)
	{
		Sleep(120);
	}
	mutex.Enter();
	int _size = size;
	if (fifo_wr > fifo_rd)
	{
		memcpy(buffer, fifo + fifo_rd , size);
	}
	else
	{
		int x = fifo_size - fifo_rd;
		if (size <= x)
		{
			memcpy(buffer, fifo + fifo_rd, size);
		}
		else 
		{
			memcpy(buffer, fifo + fifo_rd, x);
			size -= x;
			if (size > 0)
			{
				memcpy(buffer + x, fifo, size);
			}
		}
	}	

	fifo_rd = (fifo_rd + _size) % fifo_size;
	mutex.Leave();
}

void CFifo::FreeFifo()
{
	if (fifo != NULL)
	{
		free(fifo);
		fifo = NULL;
	}
}