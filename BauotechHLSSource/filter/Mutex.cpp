#include "Mutex.h"


CMutex::CMutex()
{
	InitializeCriticalSectionAndSpinCount(&commCriticalSection, 0x00000400);
		
}

CMutex::~CMutex()
{

}

void CMutex::Enter()
{
	EnterCriticalSection(&commCriticalSection);
}

void CMutex::Leave()
{
	LeaveCriticalSection(&commCriticalSection);
}


 

 