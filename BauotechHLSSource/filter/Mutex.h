#ifndef MDS_MUTEX_H
#define MDS_MUTEX_H
#include <Windows.h>


class CMutex
{
	CRITICAL_SECTION commCriticalSection;
public:

	CMutex();
	~CMutex();
	void Enter();
	void Leave();

};

#endif 