#ifndef MDS_FIFO_H
#define MDS_FIFO_H

#include <stdint.h>
#include "Mutex.h"

class CFifo
{

private:
	
	
	uint32_t fifo_size;
	CMutex mutex;
public:

	CFifo();
	~CFifo();
	uint8_t *fifo;
	uint32_t fifo_wr;
	uint32_t fifo_rd;
	void CreateFifo(int size);
	int GetFifoFullness();
	int GetFifoEmptiness();
	void FifoPush(uint8_t *buffer, uint32_t size);
	void FifoPush(uint32_t size);
	void FifoGet(uint8_t *buffer);
	void FifoPull(uint8_t *buffer, uint32_t size);
	void FreeFifo();

};

#endif 
