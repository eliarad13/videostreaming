//BauotechHLSSourceFilter

#include <strsafe.h>
#include <stdio.h>
#include <string.h> 
#include <stdint.h>
#include <list>
#include "fifo.h"

using namespace std;

 

// {8D6DD764-74D5-4077-BB9B-EA2172D34BC8}
DEFINE_GUID(CLSID_BauotechHLSSource,
	0x8d6dd764, 0x74d5, 0x4077, 0xbb, 0x9b, 0xea, 0x21, 0x72, 0xd3, 0x4b, 0xc8);


//  NOTE:  This filter does NOT support AVI format

//
//  Define an internal filter that wraps the base CBaseReader stuff
//

class CMemStream : public CAsyncStream
{
public:
    CMemStream() :
        m_llPosition(0)
    {
		m_running = false;
    }

    /*  Initialization */
    void Init(LPBYTE pbData, LONGLONG llLength, DWORD dwKBPerSec = INFINITE)
    {
        m_pbData = pbData;
        m_llLength = llLength;
        m_dwKBPerSec = dwKBPerSec;
        m_dwTimeStart = timeGetTime();
    }
	void UpdateSize(LONGLONG llLength)
	{		 
		m_llLength = llLength;
		m_llRealLength = m_llLength;
	}

	void UpdateSize2(LONGLONG llLength)
	{
		m_llRealLength = m_llLength;
		m_llLength = llLength;
	}
	 
	 
	HRESULT SetPointer(LONGLONG llPos)
	{
		if (m_running == false)
		{
			if (llPos < 0 || llPos > m_llRealLength /*m_llLength*/)
			{
				return S_FALSE;
			}
			else
			{
				m_llPosition = llPos;
				return S_OK;
			}
		}
		else
		{
			if (llPos < 0 || llPos > m_llRealLength /*m_llLength*/)
			{
				m_llPosition = 1000;
				return S_OK;
			}
			else
			{
				m_llPosition = llPos;
				return S_OK;
			}

		}
 
    }

    HRESULT Read(PBYTE pbBuffer,
                 DWORD dwBytesToRead,
                 BOOL bAlign,
                 LPDWORD pdwBytesRead)
    {
        CAutoLock lck(&m_csLock);
        DWORD dwReadLength;

        /*  Wait until the bytes are here! */
        DWORD dwTime = timeGetTime();

        if (m_llPosition + dwBytesToRead > m_llLength) {
            dwReadLength = (DWORD)(m_llLength - m_llPosition);
        } else {
            dwReadLength = dwBytesToRead;
        }
        DWORD dwTimeToArrive =
            ((DWORD)m_llPosition + dwReadLength) / m_dwKBPerSec;

        if (dwTime - m_dwTimeStart < dwTimeToArrive) {
            Sleep(dwTimeToArrive - dwTime + m_dwTimeStart);
        }

		
        CopyMemory((PVOID)pbBuffer, (PVOID)(m_pbData + m_llPosition), dwReadLength);

        m_llPosition += dwReadLength;
        *pdwBytesRead = dwReadLength;
		m_llPositionNext = m_llPosition;

		//char msgbuf[200];
		//sprintf(msgbuf, "m_llPosition  %d  dwReadLength  %d\n", m_llPosition, dwReadLength);
		//OutputDebugStringA(msgbuf);

        return S_OK;
    }

    LONGLONG Size(LONGLONG *pSizeAvailable)
    {
		LONGLONG llCurrentAvailable =
			static_cast <LONGLONG> (UInt32x32To64((timeGetTime() - m_dwTimeStart), m_dwKBPerSec));
		 
		*pSizeAvailable = min(m_llLength, llCurrentAvailable);
		if (m_running == true)
			*pSizeAvailable = *pSizeAvailable / 10000;
		return m_llLength;
		 

    }

    DWORD Alignment()
    {
        return 1;
    }
	bool		  m_running;
    void Lock()
    {
        m_csLock.Lock();
    }

    void Unlock()
    {
        m_csLock.Unlock();
    }

private:
    CCritSec       m_csLock;
    PBYTE          m_pbData;
    LONGLONG       m_llLength;
	LONGLONG       m_llRealLength;
    LONGLONG       m_llPosition;
	LONGLONG       m_llPositionNext;
    DWORD          m_dwKBPerSec;
    DWORD          m_dwTimeStart;
	
};

class CAsyncFilter : public CAsyncReader, public IFileSourceFilter
{
public:
    CAsyncFilter(LPUNKNOWN pUnk, HRESULT *phr) :
        CAsyncReader(NAME("Mem Reader"), pUnk, &m_Stream, phr),
        m_pFileName(NULL),
        m_pbData(NULL)
    {

		manifestBuffer = NULL;
		manifestBuffer = (uint8_t*)malloc(1024 * 1024 * 1);
		memset(manifestBuffer, 0, 1024 * 1024 * 1);
    }

    ~CAsyncFilter()
    {
        delete [] m_pbData;
        delete [] m_pFileName;

		if (manifestBuffer != NULL)
		{
			free(manifestBuffer);
		}		 
		if (hlsBuffer != NULL)
		{
			free(hlsBuffer);
		}
		
    }
	void UpdateSize(LONGLONG llLength);

	bool GetManifest(char *url);
	void ReadHLSFilesThread();
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN, HRESULT *);

    DECLARE_IUNKNOWN

	 
	bool m_running;
	STDMETHODIMP Run(REFERENCE_TIME tStart)
	{	 
		m_running = true;
		m_Stream.m_running = true;
		m_Stream.UpdateSize2(m_llSize * 10000);
		//HRESULT hr = CBaseFilter::Run(tStart);

		return S_OK;
	}


    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv)
    {
        if (riid == IID_IFileSourceFilter) {
            return GetInterface((IFileSourceFilter *)this, ppv);
        } else {
            return CAsyncReader::NonDelegatingQueryInterface(riid, ppv);
        }
    }

    /*  IFileSourceFilter methods */

    //  Load a (new) file
    STDMETHODIMP Load(LPCOLESTR lpwszFileName, const AM_MEDIA_TYPE *pmt)
    {
        CheckPointer(lpwszFileName, E_POINTER);

        // lstrlenW is one of the few Unicode functions that works on win95
        int cch = lstrlenW(lpwszFileName) + 1;

#ifndef UNICODE
        TCHAR *lpszFileName=0;
        lpszFileName = new char[cch * 2];
        if (!lpszFileName) {
      	    return E_OUTOFMEMORY;
        }
        WideCharToMultiByte(GetACP(), 0, lpwszFileName, -1,
    			lpszFileName, cch, NULL, NULL);
#else
        TCHAR lpszFileName[MAX_PATH]={0};
        (void)StringCchCopy(lpszFileName, NUMELMS(lpszFileName), lpwszFileName);
#endif
        CAutoLock lck(&m_csFilter);

        /*  Check the file type */
        CMediaType cmt;
        if (NULL == pmt) {
            cmt.SetType(&MEDIATYPE_Stream);
            cmt.SetSubtype(&MEDIASUBTYPE_NULL);
        } else {
            cmt = *pmt;
        }

        if (!ReadTheFile(lpszFileName)) {
#ifndef UNICODE
            delete [] lpszFileName;
#endif
            return E_FAIL;
        }

 
        m_Stream.Init(m_pbData, m_llSize);

        m_pFileName = new WCHAR[cch];

        if (m_pFileName!=NULL)
    	    CopyMemory(m_pFileName, lpwszFileName, cch*sizeof(WCHAR));

        // this is not a simple assignment... pointers and format
        // block (if any) are intelligently copied
    	m_mt = cmt;

        /*  Work out file type */
        cmt.bTemporalCompression = TRUE;	       //???
        cmt.lSampleSize = 1;
 

        return S_OK;
    }

    // Modeled on IPersistFile::Load
    // Caller needs to CoTaskMemFree or equivalent.

    STDMETHODIMP GetCurFile(LPOLESTR * ppszFileName, AM_MEDIA_TYPE *pmt)
    {
        CheckPointer(ppszFileName, E_POINTER);
        *ppszFileName = NULL;

        if (m_pFileName!=NULL) {
        	DWORD n = sizeof(WCHAR)*(1+lstrlenW(m_pFileName));

            *ppszFileName = (LPOLESTR) CoTaskMemAlloc( n );
            if (*ppszFileName!=NULL) {
                  CopyMemory(*ppszFileName, m_pFileName, n);
            }
        }

        if (pmt!=NULL) {
            CopyMediaType(pmt, &m_mt);
        }

        return NOERROR;
    }

private:
    BOOL CAsyncFilter::ReadTheFile(LPCTSTR lpszFileName);
	char ServerIpAddress[100];
	int ServerPort;
	char temp[500];
	char relativeAddress[500];

	BOOL DownloadToBuffer(char * webpage, uint8_t *buffer, unsigned long max);
	void HTTPRequestPage(SOCKET s, char *page, char *host);
	SOCKET HTTPConnectToServer();
	uint8_t *manifestBuffer;
	uint8_t *hlsBuffer;
	uint64_t m_hlsWr;
	uint32_t m_maxHlsBufferSize;

	list<string> m_hlsFileList;
	list<float> m_hlsTimeLine;

	bool m_fileLoaded;

private:
    LPWSTR     m_pFileName;
    LONGLONG   m_llSize;
    PBYTE      m_pbData;
    CMemStream m_Stream;
};
