//------------------------------------------------------------------------------
// File: FBall.cpp
//
// Desc: DirectShow sample code - implementation of filter behaviors
//       for the bouncing ball source filter.  For more information,
//       refer to Ball.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <winsock2.h>
#include <ws2ipdef.h>
#include <streams.h>
#include <olectl.h>
#include <initguid.h>
#include "ball.h"
#include "fball.h"
#include <memory>
#include <thread>
#include <time.h>  

SOCKET m_socket;
#define IMAGE_SIZE 188 * 400

#pragma warning(disable:4710)  // 'function': function not inlined (optimzation)

// Setup data

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
    &MEDIATYPE_Video,       // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

const AMOVIESETUP_PIN sudOpPin =
{
    L"Output",              // Pin string name
    FALSE,                  // Is it rendered
    TRUE,                   // Is it an output
    FALSE,                  // Can we have none
    FALSE,                  // Can we have many
    &CLSID_NULL,            // Connects to filter
    NULL,                   // Connects to pin
    1,                      // Number of types
    &sudOpPinTypes };       // Pin details

const AMOVIESETUP_FILTER sudBallax =
{
	&CLSID_BauotechUDPClientReceiver,    // Filter CLSID
    L"Bauotech UDP Client Receiver",       // String name
    MERIT_DO_NOT_USE,       // Filter merit
    1,                      // Number pins
    &sudOpPin               // Pin details
};


// COM global table of objects in this dll

CFactoryTemplate g_Templates[] = {
  { L"Bauotech UDP Client Receiver"
  , &CLSID_BauotechUDPClientReceiver
  , CBouncingBall::CreateInstance
  , NULL
  , &sudBallax }
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterServer
//
// Exported entry points for registration and unregistration
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

//
// CreateInstance
//
// The only allowed way to create Bouncing balls!
//
CUnknown * WINAPI CBouncingBall::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);

    CUnknown *punk = new CBouncingBall(lpunk, phr);
    if(punk == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;
    }
    return punk;

} // CreateInstance


//
// Constructor
//
// Initialise a CBallStream object so that we have a pin.
//
CBouncingBall::CBouncingBall(LPUNKNOWN lpunk, HRESULT *phr) :
CSource(NAME("Bauotech UDP Client Receiver"), lpunk, CLSID_BauotechUDPClientReceiver)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cStateLock);

    m_paStreams = (CSourceStream **) new CBallStream*[1];
    if(m_paStreams == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }

    m_paStreams[0] = new CBallStream(phr, this, L"Out");
    if(m_paStreams[0] == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }
	pOutPin = (CBallStream *)m_paStreams[0];

	pOutPin->pFilter = this;

	 
	//Setup("82.81.105.171" , 7900);
	Setup("10.0.0.10", 7900);

} // (Constructor)

STDMETHODIMP CBouncingBall::Run(REFERENCE_TIME tStart)
{
	  
	return CBaseFilter::Run(tStart);
}
STDMETHODIMP CBouncingBall::Pause()
{
	return CBaseFilter::Pause();

}
STDMETHODIMP CBouncingBall::Stop()
{
	pOutPin->m_running = false;
	return CBaseFilter::Stop();
}

int CBouncingBall::Setup(char *IpAddress, int port)
{
	int iResult;

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		perror("WSAStartup");
		return 0;
	}

	// Create a new socket to receive datagrams on.
	m_ClientSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (m_ClientSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return 0;
	}
	  

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	DWORD ip = inet_addr(IpAddress);
	addr.sin_addr.s_addr = ip;
	addr.sin_port = htons(port);


	DWORD timeout = 1000;
	if (setsockopt(m_ClientSocket, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(DWORD)))
	{
		return 0;
	}


	// bind to receive address
	//
	if (::connect(m_ClientSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("Connect");
		return 0;
	}
	 

	uint8_t buf[4] = { 5, 0xA, 9, 0xD };
	::send(m_ClientSocket, (char *)buf, 4, 0);
	  
	return 1;

}


//
// Constructor
//
CBallStream::CBallStream(HRESULT *phr,
                         CBouncingBall *pParent,
                         LPCWSTR pPinName) :
    CSourceStream(NAME("Bouncing Ball"),phr, pParent, pPinName),
    
    m_iDefaultRepeatTime(20)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cSharedState);

	

	  

} // (Constructor)


//
// Destructor
//
CBallStream::~CBallStream()
{
    CAutoLock cAutoLock(&m_cSharedState);

	if (pFilter->m_ClientSocket != -1)
	{
		uint8_t buf[4] = { 6, 0xB, 10, 0xE };
		::send(pFilter->m_ClientSocket, (char *)buf, 4, 0);
	}
  

} // (Destructor)
  
 
HRESULT CBallStream::FillBuffer(IMediaSample *pms)
{
    CheckPointer(pms,E_POINTER);
    
    BYTE *pData;
    long lDataLen;

    pms->GetPointer(&pData);
    lDataLen = pms->GetSize();

	 
	int size = recv(
		pFilter->m_ClientSocket,
		(char *)pData,
		lDataLen,
		0);
	if (size == -1)
	{
		return NOERROR;
	}
	pms->SetActualDataLength(size);
	 
	 

#if 0 
    // The current time is the sample's start
    CRefTime rtStart = m_rtSampleTime;

    // Increment to find the finish time
    m_rtSampleTime += (LONG)m_iRepeatTime;

    pms->SetTime((REFERENCE_TIME *) &rtStart,(REFERENCE_TIME *) &m_rtSampleTime);
#endif 
    //pms->SetSyncPoint(TRUE);
    return NOERROR;

} 


//
// Notify
//
// Alter the repeat rate according to quality management messages sent from
// the downstream filter (often the renderer).  Wind it up or down according
// to the flooding level - also skip forward if we are notified of Late-ness
//
STDMETHODIMP CBallStream::Notify(IBaseFilter * pSender, Quality q)
{
    // Adjust the repeat rate.
    if(q.Proportion<=0)
    {
        m_iRepeatTime = 1000;        // We don't go slower than 1 per second
    }
    else
    {
        m_iRepeatTime = m_iRepeatTime*1000 / q.Proportion;
        if(m_iRepeatTime>1000)
        {
            m_iRepeatTime = 1000;    // We don't go slower than 1 per second
        }
        else if(m_iRepeatTime<10)
        {
            m_iRepeatTime = 10;      // We don't go faster than 100/sec
        }
    }

    // skip forwards
    if(q.Late > 0)
        m_rtSampleTime += q.Late;

    return NOERROR;

} // Notify


//
// GetMediaType
 
HRESULT CBallStream::GetMediaType(int iPosition, CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);

	if (iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if (iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}
	 
	switch (iPosition)
	{
	case 0:
		pmt->SetType(&MEDIATYPE_Video);
		pmt->SetSubtype(&MEDIASUBTYPE_H264);
		pmt->SetFormatType(&FORMAT_VideoInfo);
		pmt->SetTemporalCompression(TRUE /*FALSE*/);
		pmt->SetSampleSize(IMAGE_SIZE);
		break;
	case 1:
		pmt->SetType(&MEDIATYPE_Stream);
		pmt->SetSubtype(&GUID_NULL);
		pmt->SetFormatType(&GUID_NULL);
		break;
	}

	return S_OK;
 

} // GetMediaType


//
// CheckMediaType
//
// We will accept 8, 16, 24 or 32 bit video formats, in any
// image size that gives room to bounce.
// Returns E_INVALIDARG if the mediatype is not acceptable
//
HRESULT CBallStream::CheckMediaType(const CMediaType *pMediaType)
{
    CheckPointer(pMediaType,E_POINTER);

	return S_OK;

   

} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT CBallStream::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
    CheckPointer(pAlloc,E_POINTER);
    CheckPointer(pProperties,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    HRESULT hr = NOERROR;

    
    pProperties->cBuffers = 1;
	pProperties->cbBuffer = IMAGE_SIZE;

    ASSERT(pProperties->cbBuffer);

    // Ask the allocator to reserve us some sample memory, NOTE the function
    // can succeed (that is return NOERROR) but still not have allocated the
    // memory that we requested, so we must check we got whatever we wanted

    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pProperties,&Actual);
    if(FAILED(hr))
    {
        return hr;
    }

    // Is this allocator unsuitable

    if(Actual.cbBuffer < pProperties->cbBuffer)
    {
        return E_FAIL;
    }

    // Make sure that we have only 1 buffer (we erase the ball in the
    // old buffer to save having to zero a 200k+ buffer every time
    // we draw a frame)

    ASSERT(Actual.cBuffers == 1);
    return NOERROR;

} // DecideBufferSize


/*
HRESULT CBallStream::SetMediaType(const CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    // Pass the call up to my base class

    HRESULT hr = CSourceStream::SetMediaType(pMediaType);

	return NOERROR;    

} // SetMediaType
*/

//
// OnThreadCreate
//
// As we go active reset the stream time to zero
//
HRESULT CBallStream::OnThreadCreate()
{
	m_running = true;
	 
    CAutoLock cAutoLockShared(&m_cSharedState);
    m_rtSampleTime = 0;	
	 
	  
    // we need to also reset the repeat time in case the system
    // clock is turned off after m_iRepeatTime gets very big
    m_iRepeatTime = m_iDefaultRepeatTime;

    return NOERROR;

} // OnThreadCreate


 

 

