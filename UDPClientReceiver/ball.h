//------------------------------------------------------------------------------
// File: Ball.h
//
// Desc: DirectShow sample code - header file for the bouncing ball
//       source filter.  For more information, refer to Ball.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Define GUIDS used in this sample
//------------------------------------------------------------------------------
 
 
// {BB1244DB-EE43-4713-8D4F-21E9240BEBFB}
DEFINE_GUID(CLSID_BauotechUDPClientReceiver,
	0xbb1244db, 0xee43, 0x4713, 0x8d, 0x4f, 0x21, 0xe9, 0x24, 0xb, 0xeb, 0xfb);

