#pragma once

#define LIVE_FILTER_NAME TEXT("Frame Input Source Filter")
#define LIVE_OUTPIN_NAME L"Out"

// {77F36C8D-DAB1-4356-8273-3FBE6063F257}
static const GUID IID_ILiveSource = 
{ 0x77f36c8d, 0xdab1, 0x4356, { 0x82, 0x73, 0x3f, 0xbe, 0x60, 0x63, 0xf2, 0x57 } };

// {63CA2EE3-6460-4098-9B74-079CE740E4B5}
static const GUID CLSID_CLiveSource = 
{ 0x63ca2ee3, 0x6460, 0x4098, { 0x9b, 0x74, 0x7, 0x9c, 0xe7, 0x40, 0xe4, 0xb5 } };


DECLARE_INTERFACE_(ILiveSource, IUnknown)
{
	// Adds bitmap to the video sequence
	STDMETHOD(AddFrame)(HBITMAP hBmp) PURE;

	// Adds pixel data buffer to the video sequence
	STDMETHOD(AddFrame)(BYTE* pBuffer, DWORD size) PURE;

	// Set the video frame info.
	// Default value is width = 704, height = 576 (4CIF) and 32 bits per pixel
	STDMETHOD(SetBitmapInfo)(BITMAPINFOHEADER& bInfo) PURE;

	// Set the expected frame rate of the video.
	// Value should be in range of [0,30]
	// Default value is 0
	STDMETHOD(SetFrameRate)(int frameRate) PURE;
};