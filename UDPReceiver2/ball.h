//------------------------------------------------------------------------------
// File: Ball.h
//
// Desc: DirectShow sample code - header file for the bouncing ball
//       source filter.  For more information, refer to Ball.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Define GUIDS used in this sample
//------------------------------------------------------------------------------
 

// {C9C01DF4-8C73-4BC7-8809-5B898E8E507D}
DEFINE_GUID(CLSID_BouncingBall,
	0xc9c01df4, 0x8c73, 0x4bc7, 0x88, 0x9, 0x5b, 0x89, 0x8e, 0x8e, 0x50, 0x7d);

 