//------------------------------------------------------------------------------
// File: FBall.cpp
//
// Desc: DirectShow sample code - implementation of filter behaviors
//       for the bouncing ball source filter.  For more information,
//       refer to Ball.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <winsock2.h>
#include <ws2ipdef.h>
#include <streams.h>
#include <olectl.h>
#include <initguid.h>
#include "ball.h"
#include "fball.h"
#include <memory>
#include <thread>
#include <time.h>  


SOCKET m_socket;
#define IMAGE_SIZE 188 * 400

#pragma warning(disable:4710)  // 'function': function not inlined (optimzation)

// Setup data

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
    &MEDIATYPE_Video,       // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

const AMOVIESETUP_PIN sudOpPin =
{
    L"Output",              // Pin string name
    FALSE,                  // Is it rendered
    TRUE,                   // Is it an output
    FALSE,                  // Can we have none
    FALSE,                  // Can we have many
    &CLSID_NULL,            // Connects to filter
    NULL,                   // Connects to pin
    1,                      // Number of types
    &sudOpPinTypes };       // Pin details

const AMOVIESETUP_FILTER sudBallax =
{
    &CLSID_BouncingBall,    // Filter CLSID
    L"Bauotech UDP Receiver 2",       // String name
    MERIT_DO_NOT_USE,       // Filter merit
    1,                      // Number pins
    &sudOpPin               // Pin details
};


// COM global table of objects in this dll

CFactoryTemplate g_Templates[] = {
  { L"Bauotech UDP Receiver 2"
  , &CLSID_BouncingBall
  , CBouncingBall::CreateInstance
  , NULL
  , &sudBallax }
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterServer
//
// Exported entry points for registration and unregistration
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

//
// CreateInstance
//
// The only allowed way to create Bouncing balls!
//
CUnknown * WINAPI CBouncingBall::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);

    CUnknown *punk = new CBouncingBall(lpunk, phr);
    if(punk == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;
    }
    return punk;

} // CreateInstance


//
// Constructor
//
// Initialise a CBallStream object so that we have a pin.
//
CBouncingBall::CBouncingBall(LPUNKNOWN lpunk, HRESULT *phr) :
    CSource(NAME("Bauotech UDP Receiver 2"), lpunk, CLSID_BouncingBall)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cStateLock);

    m_paStreams = (CSourceStream **) new CBallStream*[1];
    if(m_paStreams == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }

    m_paStreams[0] = new CBallStream(phr, this, L"Out");
    if(m_paStreams[0] == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }
	pOutPin = (CBallStream *)m_paStreams[0];


	pOutPin->m_hlsRd = 0;
	pOutPin->m_hlsWr = 0;
	Setup(true, "10.0.0.17" , 6000, "234.5.5.5");

} // (Constructor)

STDMETHODIMP CBouncingBall::Run(REFERENCE_TIME tStart)
{
	  
	return CBaseFilter::Run(tStart);
}
STDMETHODIMP CBouncingBall::Pause()
{
	return CBaseFilter::Pause();

}
STDMETHODIMP CBouncingBall::Stop()
{
	pOutPin->m_running = false;
	return CBaseFilter::Stop();
}



int CBouncingBall::Setup(bool multicast,  char *interfaceAddress, int port, char *multicastAddress)
{
	int iResult;

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		perror("WSAStartup");
		return 1;
	}

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return -1;
	}
	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;

	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return 1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	DWORD ip = inet_addr(interfaceAddress);
	addr.sin_addr.s_addr = ip;
	addr.sin_port = htons(port);

	// bind to receive address
	//
	if (::bind(ReceivingSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}
	m_socket = ReceivingSocket;


	if (multicast == true)
	{
		IP_MREQ mreq;
		mreq.imr_multiaddr.s_addr = inet_addr("234.5.5.5");
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if (setsockopt(ReceivingSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
		{
			return -5;
		}
	}

	return 1;

}
void CBallStream::InitTSParser()
{

	show_pcr_time = false;
	show_frame_pos = false;
	show_frame_pts = false;
	show_frame_dts = false;
	show_key_frame = true;


	vc = 0;
	sc = 0;
	offset = 0;
	pts = 0;
	dts = 0;
	vknown_type = false;
	aknown_type = false;


	si.pid_ = 40;
	si.type_ = util::stream_info::stream_video;
	si.stream_type_ = tsParser.stream_type(std::string("H264"));
	streams.push_back(si);
	si.pid_ = 50;
	si.type_ = util::stream_info::stream_audio;
	si.stream_type_ = tsParser.stream_type(std::string("AAC"));
	streams.push_back(si);
	tsParser.init_streams(streams);

	mi.pid_ = 40;
	mi.is_video_ = true;
	mi.is_audio_ = false;


}

//
// Constructor
//
CBallStream::CBallStream(HRESULT *phr,
                         CBouncingBall *pParent,
                         LPCWSTR pPinName) :
    CSourceStream(NAME("Bouncing Ball"),phr, pParent, pPinName),
    
    m_iDefaultRepeatTime(20)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cSharedState);
	InitTSParser();
	m_runCount = 0;
	manifestBuffer = new uint8_t[MANIFEST_BUFFER_SIZE];
	m_maxHlsBufferSize = 1024 * 1024 * 16;
	for (int i = 0; i < MAX_HLS_FILES; i++)
	{
		hlsBuffer[i] = (uint8_t *)malloc(m_maxHlsBufferSize);
		hlsBufferSize[i] = 0;
	}
	

} // (Constructor)


//
// Destructor
//
CBallStream::~CBallStream()
{
    CAutoLock cAutoLock(&m_cSharedState);
	if (manifestBuffer != NULL)
	{
		delete manifestBuffer;
		manifestBuffer = NULL;
	}

	for (int i = 0; i < MAX_HLS_FILES; i++)
	{
		free (hlsBuffer[i]);
	}
    

} // (Destructor)

SOCKET CBallStream::HTTPConnectToServer()
{
	SOCKADDR_IN serverInfo;
	SOCKET sck;
	WSADATA wsaData;
	LPHOSTENT hostEntry;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	sck = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sck == INVALID_SOCKET){
		WSACleanup();
		puts("Failed to setup socket");
		getchar();
		return 0;
	}
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = inet_addr(ServerIpAddress);
	serverInfo.sin_port = htons(ServerPort);
	int i = connect(sck, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	if (sck == SOCKET_ERROR) return 0;
	if (i != 0) return 0;

	return sck;
}


void CBallStream::HTTPRequestPage(SOCKET s, char *page, char *host)
{
	unsigned int len;
	if (strlen(page)>strlen(host)){
		len = strlen(page);
	}
	else len = strlen(host);

	char message[500];
	 
	//sprintf(message, "Get %s HTTP/1.1\r\n Host: %s:%d\r\n \r\n \r\n", page, ServerIpAddress, ServerPort);	 
	//send(s, message, strlen(message), 0);


	sprintf(message, "GET %s HTTP/1.1\r\n", page);
	send(s, message, strlen(message), 0);

	memset(message, 0, sizeof(message));
	sprintf(message, "Host: %s\r\n\r\n", host);
	send(s, message, strlen(message), 0);


	 
}

int CBallStream::GetFifoEmptiness()
{
	//mutex.Enter();
	uint32_t x;
	if (m_hlsWr == m_hlsRd)
		x = MAX_HLS_FILES;
	else
		if (m_hlsWr > m_hlsRd)
			x = MAX_HLS_FILES - (m_hlsWr - m_hlsRd);
		else
			x = MAX_HLS_FILES - (MAX_HLS_FILES - m_hlsRd + m_hlsWr);

	//mutex.Leave();
	return x;

}
int CBallStream::GetFifoFullness()
{
	//mutex.Enter();
	uint32_t x;
	if (m_hlsWr == m_hlsRd)
		x = 0;
	else
		if (m_hlsWr > m_hlsRd)
			x = m_hlsWr - m_hlsRd;
		else
			x = MAX_HLS_FILES - m_hlsRd + m_hlsWr;

	//mutex.Leave();
	return x;
}

void CBallStream::ReadHLSFilesThread()
{

	m_hlsWr = 0;
	
	for (int i = 0; i < MAX_HLS_FILES; i++)
	{		
		hlsBufferSize[i] = 0;
	}
	 
	char buffer[2000];
	char url[1000];	  
	m_fileLoaded = true;
	list<string>::iterator it;

	for (it = m_hlsFileList.begin(); it != m_hlsFileList.end(); ++it)
	{
		int size = 0;
		hlsBufferSize[m_hlsWr] = 0;
		while (GetFifoEmptiness() < 2)
		{
			if (m_running == false)
				return;				
			Sleep(10);
		}
		 
		uint8_t *p = hlsBuffer[m_hlsWr];

		sprintf(url, "GET %s/%s HTTP/1.0\r\n\r\n", relativeAddress, (*it).c_str());
		SOCKET s = HTTPConnectToServer();
		send(s, url, strlen(url), 0);
		int r;
		r = recv(s, (char *)(buffer), 2000, 0);
		if (r == -1)
		{
			closesocket(s);
			continue;
		}

		int i = 235;
		while (buffer[i] != 0x47)
			i++;

		size += (r - i);
		memcpy(p, buffer + i, r - i);
		 
		while (r > 0)
		{
			if (m_running == false)
				return;
			r = recv(s, (char *)(p + size), 2000, 0);
			if (r == -1)
			{
				break;
			}			 
			size += r;
		}
		closesocket(s);		 
		hlsBufferSize[m_hlsWr] = size;
		list<float>::iterator it1;
		it1 = std::next(m_hlsTimeLine.begin(), m_hlsWr);
		hlsBitrate[m_hlsWr] = hlsBufferSize[m_hlsWr] / *it1;
		
		m_hlsWr = (m_hlsWr + 1) % MAX_HLS_FILES;
	}

}
bool CBallStream::DownloadToBuffer(char * webpage, uint8_t *buffer, unsigned long max)
{
	if (webpage == NULL || buffer == NULL || max == 0) return FALSE;

	unsigned short shift = 0;
	if (_strnicmp(webpage, "http://", strlen("http://")) == 0){
		shift = strlen("http://");
	}
	if (_strnicmp(webpage + shift, "www.", strlen("www.")) == 0){
		shift += strlen("www.");
	}
	char cut[200];
	strcpy(cut, _strdup(webpage + shift));

	char *p;
	char *server = strtok(cut, "/");
	char ipAddress[100];
	char port[20];
	ServerPort = 80;
	strcpy(ipAddress, server);
	p = strchr(ipAddress, ':');
	if (p != NULL)
	{
		*p = 0;
		strcpy(ServerIpAddress, ipAddress);
		strcpy(port, server);
		p = strchr(port, ':') + 1;
		strcpy(port, p);
		ServerPort = atoi(port);
	}
	else
	{
		strcpy(ipAddress, server);
		strcpy(ServerIpAddress, ipAddress);
		ServerPort = 80;
	}


	char *page = _strdup(webpage + shift + strlen(server));

	strcpy(relativeAddress, page);
	p = relativeAddress;
	p = p + strlen(relativeAddress) - 1;
	while (*p != '/')
		p--;
	*p = 0;

	SOCKET s = HTTPConnectToServer();
	HTTPRequestPage(s, page, server);

	int i = recv(s, (char *)buffer, max, 0);
	closesocket(s);

	if (i <= 0) return false;
	return true;
}

bool CBallStream::GetManifest(char *url)
{
	memset(manifestBuffer, 0, MANIFEST_BUFFER_SIZE);
	if (DownloadToBuffer(url, manifestBuffer, MANIFEST_BUFFER_SIZE) == false)
		return false;
	char *p = strchr((char *)manifestBuffer, '\n');
	char response[30];
	memset(response, 0, sizeof(response));
	memcpy(response, manifestBuffer, p - (char *)manifestBuffer);
	p = strchr(response, '\r');
	if (p != NULL)
		*p = NULL;
	if (strcmp(response, "HTTP/1.1 200 OK") == 0)
	{

		m_hlsTimeLine.clear();
		m_hlsFileList.clear();
		char* token = strtok((char *)manifestBuffer, "\n");
		while (token != NULL)
		{
			token = strtok(NULL, "\n");
			if (strstr(token, "EXTINF") != NULL)
			{
				while (token != NULL)
				{
					if (strstr(token, "EXTINF") != NULL)
					{
						strcpy(temp, token);
						p = strchr(temp, ':');
						p++;
						strcpy(temp, p);
						if (strchr(temp, ',') != NULL)
							*p = 0;
						m_hlsTimeLine.push_back(atof(temp));
						 
					}
					else
					{
						strcpy(temp, token);
						if ((p = strchr(temp, '\r')) != NULL)
							*p = 0;
						m_hlsFileList.push_back(temp);
					}
					token = strtok(NULL, "\n");
				}
				if (m_hlsFileList.size() == 0)
					return false;
				return true;
			}
		}
	}
	else
	{
		return false;
	}

}

//
// FillBuffer
//
// Plots a ball into the supplied video buffer
//


bool m_hls = true;
HRESULT CBallStream::FillBuffer(IMediaSample *pms)
{
    CheckPointer(pms,E_POINTER);
    
    BYTE *pData;
    long lDataLen;

    pms->GetPointer(&pData);
    lDataLen = pms->GetSize();

	if (m_hls == false)
	{
		int size = recv(
			m_socket,
			(char *)pData,
			lDataLen,
			0);
		if (size == -1)
		{
			return NOERROR;
		}
		pms->SetActualDataLength(size);
		Sleep(0);
	}
	else
	{
		
		uint32_t m_measure = 1;
		
	 
		char msgbuf[100];
		if (hlsBufferSize[m_hlsRd] > 0)
		{
		  
			util::mpegts_info info;
			
			
			memset(&info, 0, sizeof(info));
			uint8_t *p = hlsBuffer[m_hlsRd];
			auto suc = tsParser.do_parser(p + m_bufIndex, info);

			//sprintf(msgbuf, "time: %f  m_byteCount = %d  avg = %f  target bitrate %f\n", diffTime, m_byteCount, avg, hlsBitrate[m_hlsRd]);
			double elapsed = 0;
			double pcrDiff = 0;
			if (info.pcr_ > 0)
			{
				if (m_firstPcr == 0)
				{
					clock_start = clock();
					m_firstPcr = info.pcr_;
				}

				pcrDiff = (info.pcr_ - m_firstPcr) / 90.0;
				sprintf(msgbuf, "PCR Diff: %f \n", pcrDiff);
				OutputDebugStringA(msgbuf);
				 

				clock_t clock_stop = clock();
				elapsed = (double)(clock_stop - clock_start) * 1000 / CLOCKS_PER_SEC;				
				sprintf(msgbuf, "Time elapsed in ms: %f", elapsed);
				OutputDebugStringA(msgbuf);
			}
			if (m_firstPcr == 0)
			{
				m_bufIndex += 188;
				hlsBufferSize[m_hlsRd] -= 188;
				pms->SetActualDataLength(0);
				return NOERROR;
			}

			while ((pcrDiff + 20000) > elapsed)
			{
				if (m_running == false)
				{
					pms->SetActualDataLength(0);
					return NOERROR;
				}
				Sleep(1);
				clock_t clock_stop = clock();
				elapsed = (double)(clock_stop - clock_start) * 1000 / CLOCKS_PER_SEC;
			}
			 
			 
			int s = min(1316, hlsBufferSize[m_hlsRd]);
			 
			memcpy(pData, p + m_bufIndex, s);
			m_bufIndex += s;
			hlsBufferSize[m_hlsRd] -= s;
			pms->SetActualDataLength(s);
			m_byteCount += s;
			if (hlsBufferSize[m_hlsRd] == 0)
			{
				m_hlsRd = (m_hlsRd + 1) % MAX_HLS_FILES;
				m_bufIndex = 0;				 
			}
			Sleep(0);
		}
		else
		{
			pms->SetActualDataLength(0);
			return NOERROR;
		}
	}

#if 0 
    // The current time is the sample's start
    CRefTime rtStart = m_rtSampleTime;

    // Increment to find the finish time
    m_rtSampleTime += (LONG)m_iRepeatTime;

    pms->SetTime((REFERENCE_TIME *) &rtStart,(REFERENCE_TIME *) &m_rtSampleTime);
#endif 
    //pms->SetSyncPoint(TRUE);
    return NOERROR;

} 


//
// Notify
//
// Alter the repeat rate according to quality management messages sent from
// the downstream filter (often the renderer).  Wind it up or down according
// to the flooding level - also skip forward if we are notified of Late-ness
//
STDMETHODIMP CBallStream::Notify(IBaseFilter * pSender, Quality q)
{
    // Adjust the repeat rate.
    if(q.Proportion<=0)
    {
        m_iRepeatTime = 1000;        // We don't go slower than 1 per second
    }
    else
    {
        m_iRepeatTime = m_iRepeatTime*1000 / q.Proportion;
        if(m_iRepeatTime>1000)
        {
            m_iRepeatTime = 1000;    // We don't go slower than 1 per second
        }
        else if(m_iRepeatTime<10)
        {
            m_iRepeatTime = 10;      // We don't go faster than 100/sec
        }
    }

    // skip forwards
    if(q.Late > 0)
        m_rtSampleTime += q.Late;

    return NOERROR;

} // Notify


//
// GetMediaType
 
HRESULT CBallStream::GetMediaType(int iPosition, CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);

	if (iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if (iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}
	 
	switch (iPosition)
	{
	case 0:
		pmt->SetType(&MEDIATYPE_Video);
		pmt->SetSubtype(&MEDIASUBTYPE_H264);
		pmt->SetFormatType(&FORMAT_VideoInfo);
		pmt->SetTemporalCompression(TRUE /*FALSE*/);
		pmt->SetSampleSize(IMAGE_SIZE);
		break;
	case 1:
		pmt->SetType(&MEDIATYPE_Stream);
		pmt->SetSubtype(&GUID_NULL);
		pmt->SetFormatType(&GUID_NULL);
		break;
	}

	return S_OK;
 

} // GetMediaType


//
// CheckMediaType
//
// We will accept 8, 16, 24 or 32 bit video formats, in any
// image size that gives room to bounce.
// Returns E_INVALIDARG if the mediatype is not acceptable
//
HRESULT CBallStream::CheckMediaType(const CMediaType *pMediaType)
{
    CheckPointer(pMediaType,E_POINTER);

	return S_OK;

   

} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT CBallStream::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
    CheckPointer(pAlloc,E_POINTER);
    CheckPointer(pProperties,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    HRESULT hr = NOERROR;

    
    pProperties->cBuffers = 1;
	pProperties->cbBuffer = IMAGE_SIZE;

    ASSERT(pProperties->cbBuffer);

    // Ask the allocator to reserve us some sample memory, NOTE the function
    // can succeed (that is return NOERROR) but still not have allocated the
    // memory that we requested, so we must check we got whatever we wanted

    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pProperties,&Actual);
    if(FAILED(hr))
    {
        return hr;
    }

    // Is this allocator unsuitable

    if(Actual.cbBuffer < pProperties->cbBuffer)
    {
        return E_FAIL;
    }

    // Make sure that we have only 1 buffer (we erase the ball in the
    // old buffer to save having to zero a 200k+ buffer every time
    // we draw a frame)

    ASSERT(Actual.cBuffers == 1);
    return NOERROR;

} // DecideBufferSize


/*
HRESULT CBallStream::SetMediaType(const CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    // Pass the call up to my base class

    HRESULT hr = CSourceStream::SetMediaType(pMediaType);

	return NOERROR;    

} // SetMediaType
*/

//
// OnThreadCreate
//
// As we go active reset the stream time to zero
//
HRESULT CBallStream::OnThreadCreate()
{
	m_running = true;
	m_firstPcr = 0;
	m_runCount++;
    CAutoLock cAutoLockShared(&m_cSharedState);
    m_rtSampleTime = 0;	
	m_byteCount = 0;
	m_bufIndex = 0;
	m_hlsRd = 0;
	m_hlsWr = 0;
	 
	if (m_hls == true)
	{
		char fileName[500];
		strcpy(fileName, "http://10.0.0.10:8080/1/playlist/mpuser/Request_2020430_22238_1.m3u8");
		if (GetManifest(fileName) == true)
		{
			shared_ptr<thread> t1 = make_shared<thread>(&CBallStream::ReadHLSFilesThread, this);
			t1->detach();
		}
		else
		{
			MessageBoxA(NULL, "Failed to load HLS manifest", "Bauotech HLS Source", 0);
		}
	}


    // we need to also reset the repeat time in case the system
    // clock is turned off after m_iRepeatTime gets very big
    m_iRepeatTime = m_iDefaultRepeatTime;

    return NOERROR;

} // OnThreadCreate


 

 

