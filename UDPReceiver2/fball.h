#pragma once 
#include <stdint.h>
#include <list>
#include <string>
#include <time.h>
#include "mpegts.hpp"

using namespace std;
 
//------------------------------------------------------------------------------
// File: FBall.h
//
// Desc: DirectShow sample code - main header file for the bouncing ball
//       source filter.  For more information refer to Ball.cpp
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

//------------------------------------------------------------------------------
// Forward Declarations
//------------------------------------------------------------------------------
// The class managing the output pin
class CBallStream;


//------------------------------------------------------------------------------
// Class CBouncingBall
//
// This is the main class for the bouncing ball filter. It inherits from
// CSource, the DirectShow base class for source filters.
//------------------------------------------------------------------------------
class CBouncingBall : public CSource
{
public:

    // The only allowed way to create Bouncing balls!
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);

	// Open and close the file as necessary
	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();
	 
	


private:

    // It is only allowed to to create these objects with CreateInstance
    CBouncingBall(LPUNKNOWN lpunk, HRESULT *phr);
	int Setup(bool multicast, char *interfaceAddress, int port, char *multicastAddress);

	CBallStream *pOutPin;
	SOCKET ReceivingSocket;
	struct sockaddr_in addr;

}; // CBouncingBall


//------------------------------------------------------------------------------
// Class CBallStream
//
// This class implements the stream which is used to output the bouncing ball
// data from the source filter. It inherits from DirectShows's base
// CSourceStream class.
//------------------------------------------------------------------------------
class CBallStream : public CSourceStream
{

public:

    CBallStream(HRESULT *phr, CBouncingBall *pParent, LPCWSTR pPinName);
    ~CBallStream();

    // plots a ball into the supplied video frame
    HRESULT FillBuffer(IMediaSample *pms);

    // Ask for buffers of the size appropriate to the agreed media type
    HRESULT DecideBufferSize(IMemAllocator *pIMemAlloc,
                             ALLOCATOR_PROPERTIES *pProperties);

    // Set the agreed media type, and set up the necessary ball parameters
    //HRESULT SetMediaType(const CMediaType *pMediaType);

    // Because we calculate the ball there is no reason why we
    // can't calculate it in any one of a set of formats...
    HRESULT CheckMediaType(const CMediaType *pMediaType);
    HRESULT GetMediaType(int iPosition, CMediaType *pmt);

    // Resets the stream time to zero
    HRESULT OnThreadCreate(void);

	void InitTSParser();


	util::mpegts_parser tsParser;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;

    // Quality control notifications sent to us
    STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);
	bool GetManifest(char *url);
	void ReadHLSFilesThread();
	SOCKET HTTPConnectToServer();
	void HTTPRequestPage(SOCKET s, char *page, char *host);
	bool DownloadToBuffer(char * webpage, uint8_t *buffer, unsigned long max);

	double m_firstPcr;
	#define MAX_HLS_FILES 10
	#define MANIFEST_BUFFER_SIZE  5* 1024 * 1024 
	uint8_t *manifestBuffer;
	uint8_t *hlsBuffer[MAX_HLS_FILES];
	uint32_t hlsBufferSize[MAX_HLS_FILES];
	float hlsBitrate[MAX_HLS_FILES];
	uint32_t m_bufIndex = 0;
	uint64_t m_hlsWr;
	uint64_t m_hlsRd;
	uint32_t m_maxHlsBufferSize;
	bool m_fileLoaded;
	list<string> m_hlsFileList;
	list<float> m_hlsTimeLine;
	uint32_t m_byteCount;
	time_t m_lastTime;
	clock_t clock_start;	
	int GetFifoFullness();
	int GetFifoEmptiness();
	bool m_running;
	int m_runCount;
	char ServerIpAddress[100];
	int ServerPort;
	char temp[500];
	char relativeAddress[500];

private:

    int m_iRepeatTime;                  // Time in msec between frames
    const int m_iDefaultRepeatTime;     // Initial m_iRepeatTime

    
    int m_iPixelSize;                   // The pixel size in bytes
     
    CCritSec m_cSharedState;            // Lock on m_rtSampleTime and m_Ball
    CRefTime m_rtSampleTime;            // The time stamp for each sample
	 

}; // CBallStream
    
