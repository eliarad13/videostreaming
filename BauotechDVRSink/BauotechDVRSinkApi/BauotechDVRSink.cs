﻿using BoutechPlayerCSLib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static BoutechPlayerCSLib.BauotechPlayer;

namespace BauotechDVRSinkApi
{
    public class BauotechDVRSink
    {
        BauotechPlayer m_player = new BauotechPlayer();
      

        public BauotechDVRSink()
        {
            BauotechPlayer.UnlockBautechPlayer(362826616);
        }

        public void SetProgressCallback2Host(ProgressCallback2Host p)
        {            
            m_player.SetProgressCallback2Host(p);
        }
        public bool Initialize(string ipAddress, 
                               int port , 
                               string rootDirectory,
                               bool useBoutechReceiver,
                               int LogTime, 
                               int maxFilesPerDirectory,
                               out int result)
        {


             

            if ((result = m_player.InitializeNetwordReceiverToBoutechDVRSink(rootDirectory, 
                                                                             ipAddress, 
                                                                             null , 
                                                                             port, 
                                                                             useBoutechReceiver,
                                                                             LogTime,
                                                                             maxFilesPerDirectory)) < 0)
                return false;

            m_player.Play();
            return true;
        }

        public void Close()
        {
            m_player.Close();
        }

        public int Run()
        {
            return m_player.Run();
        }

        public int Stop()
        {
            return m_player.Stop();
        }
    }
}
