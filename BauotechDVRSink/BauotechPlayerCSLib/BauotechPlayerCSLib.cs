﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.InteropServices;
using System.Windows;
using System.Drawing;

namespace BoutechPlayerCSLib
{

    public class BauotechPlayer
    {
        bool m_videoInitialized = false;
        const long ONE_SEC = 10000000;

        public enum SHAPE_IDs
        {
            LINE1,
            LINE2,
            CIRCLE = 2,
            DISTANCE_TEXT1 = 3,
            DISTANCE_TEXT2 = 4,
            CENTER_CROSS_1 = 5,
            CENTER_CROSS_2 = 6,
        }

        public enum ASPECT_RATIO
        {
            STREACH,
            PRESERVED
        };


        public enum SELECTED_DECODER
        {
            RENDER,
            ELECARD,
            LEADTOOLS,
            IMPAL,
            AVOBJECTS,
            MICROSOFT_DTV
        }

        public enum VIDEO_RENDER
        {
            Try_EVR, Try_VMR9, Try_VMR7, Try_VMR_DEF
        };

        public enum SELECTED_PULLDEMUX
        {
            RENDER_DEMUX,
            ELECARD_PULL_DEMUX,
            IMPAL
        }

#if DEBUG
        const string dsPath = @"C:\Program Files\Bauotech\Dll\BauotechPlayerLib.dll";
#else
        const string dsPath = @"BauotechPlayerLib.dll";
#endif

#if DEBUG
        const string dsDemuxGetDurationPath = @"C:\Program Files\Bauotech\Dll\DemuxFileDurationApi.dll";
#else
        const string dsDemuxGetDurationPath = @"BauotechPlayerLib.dll";
#endif


        [DllImport(dsDemuxGetDurationPath, CallingConvention = CallingConvention.StdCall)]
        public static extern bool DEMUX_GetDuration(string fileName, ref double duration);
         

        //////////////////////////// Bauotech player ///////////////////////////////



        [UnmanagedFunctionPointer(CallingConvention.Cdecl)]
        public delegate void ProgressCallback(string fileName,
                                              double PCR,
                                              int fileState);


        public delegate void ProgressCallback2Host(string fileName,
                                                   double PCR,
                                                   int fileState);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void DSShow_SetDVRSinkCallback([MarshalAs(UnmanagedType.FunctionPtr)] ProgressCallback callbackPointer);


        public delegate void FrameProcessedDelegate(IntPtr pBuffer, int len, int height, int width, int stride);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_AddTextOverlay(int graphIndex, [MarshalAs(UnmanagedType.LPWStr)]String text,
                                                         int id,
                                                         int left,
                                                         int top,
                                                         int right,
                                                         int bottom,
                                                         int color,
                                                         float fontSize,
                                                         int fontStyle);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_AddTextOverlay2(int graphIndex, [MarshalAs(UnmanagedType.LPWStr)]String text, int id,
                                      int left,
                                      int top,
                                      int right,
                                      int bottom,
                                      int color,
                                      float fontSize);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Clear(int graphIndex);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Visible(int graphIndex, int id, bool visible);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_AddLine(int graphIndex, int id,
                                                int x1,
                                                int y1,
                                                int x2,
                                                int y2,
                                                int color,
                                                int width);



        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_AddCircle(int graphIndex, int id,
                                                  int x1,
                                                  int y1,
                                                  int radios_w,
                                                  int radios_h,
                                                  int color,
                                                  int width);



        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern void DSShow_UnlockBautechPlayer(long code);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_ApplyOverlay(int graphIndex, float alpha_opacity);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Repaint(int graphIndex, IntPtr hdc);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SelectPullDemux(int graphIndex, int demux);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SelectDecoder(int graphIndex, int selectedDecoder);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SetAspectRatio(int graphIndex, int ratio, bool applyNow);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SelectRenderer(int graphIndex, int renderer);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_UpdateVideoWindow(int graphIndex, int x, int y, int width, int height);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SetWindowHandle(int graphIndex, IntPtr handle);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SetEventWindow(int graphIndex, IntPtr handle, int eventId);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_GetGraphEvent(int graphIndex, ref long param1, ref long param2, ref long evCode);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SetFileName(int graphIndex, [MarshalAs(UnmanagedType.LPWStr)]String sFileName);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializePlayer(int graphIndex,
                                                         [MarshalAs(UnmanagedType.SysInt)]IntPtr handle,
                                                         int klvPID = 0);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitilizeNetworkMulticastPlayer(int graphIndex,
                                                        [MarshalAs(UnmanagedType.SysInt)]IntPtr handle,
                                                        bool useBoutechReceiver,
                                                        [MarshalAs(UnmanagedType.LPWStr)]String MulticastAddress,
                                                        int MulticastPort,
                                                        bool useInterface,
                                                        float demuxLatency = 1,
                                                        int klvPID = 0);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializeBoutechReceiverToWebRTC(int graphIndex,
                                                                          [MarshalAs(UnmanagedType.LPWStr)]String serverAddress,
                                                                          bool useTransport,
                                                                          int cameraPort,
                                                                          [MarshalAs(UnmanagedType.LPWStr)]String webRtcURL);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializeBoutechReceiverToWebRTCAndHLS(int graphIndex,
                                                                                [MarshalAs(UnmanagedType.LPWStr)]String serverAddress,
                                                                                bool useTransport,
                                                                                int cameraPort,
                                                                                [MarshalAs(UnmanagedType.LPWStr)]String webRtcURL,
                                                                                [MarshalAs(UnmanagedType.LPWStr)]String HLSOutputPath,
                                                                                [MarshalAs(UnmanagedType.LPWStr)]String HLSName,
                                                                                int HLSRelativeUrl,
                                                                                int HLSLive,
                                                                                int HLSPlaylistDuration,
                                                                                int HLSSegmentDuration,
                                                                                [MarshalAs(UnmanagedType.LPWStr)]String HLSStreamName);


         


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializeScreenCaptureToDump(int graphIndex,
                                                                      [MarshalAs(UnmanagedType.LPWStr)]String fileName,
                                                                      int screenWidth,
                                                                      int screenHeight,
                                                                      bool includeAudio,
                                                                      int bitrate);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitilizeNetworkVideoEsPlayer(int graphIndex,
                                                        [MarshalAs(UnmanagedType.SysInt)]IntPtr handle,
                                                        bool useBoutechReceiver,
                                                        [MarshalAs(UnmanagedType.LPWStr)]String ServerAddress,
                                                        int ServerPort);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SetRate(int graphIndex, double rate);
        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializeNetwordReceiverToBoutechDVRSink(int graphIndex,
                                                                                  [MarshalAs(UnmanagedType.LPWStr)]String rootDirectory,
                                                                                  [MarshalAs(UnmanagedType.LPWStr)]String serverAddress,
                                                                                  [MarshalAs(UnmanagedType.LPWStr)]String interfaceAddress,
                                                                                  int serverPort,
                                                                                  bool useBoutechReceiver,
                                                                                  int LogTime,
                                                                                  int maxFilesPerDirectory);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializeBauotechDVRPlayer(int graphIndex,
                                                                    IntPtr hwnd,
                                                                    [MarshalAs(UnmanagedType.LPWStr)] String DvrFileName,
                                                                    float startTime,
                                                                    bool loop);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_InitializeBauotechDVRPlayerAsync(int graphIndex,
                                                                   IntPtr hwnd,
                                                                   [MarshalAs(UnmanagedType.LPWStr)] String DvrFileName,
                                                                   float startTime,
                                                                   bool loop);



        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Snapshot(int graphIndex,
                                                 [MarshalAs(UnmanagedType.LPWStr)]String url);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_StepForward(int graphIndex, long lFrames, bool bSec = false);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_StepBackward(int graphIndex, long lFrames, bool bSec = false);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Play(int graphIndex);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Pause(int graphIndex);
        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Stop(int graphIndex);


        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_GetDuration(int graphIndex, ref long duration);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_SetPosition(int graphIndex, long pos);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern long DSShow_GetCurrentPosition(int graphIndex);



        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_Close(int graphIndex);

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_CloseAll();

        [DllImport(dsPath, CallingConvention = CallingConvention.Cdecl)]
        public static extern int DSShow_StopAll();

        int m_graphIndex = -1;

        ProgressCallback pDvrCallback;
        public BauotechPlayer(int graphIndex = 0)
        {
            m_graphIndex = graphIndex;
            pDvrCallback = new ProgressCallback(ProgressCallbackMsg);
        }

        public static void CloseAll()
        {
            DSShow_CloseAll();
        }

        public static void StopAll()
        {
            DSShow_StopAll();
        }

        bool m_playerClosed = true;
        public void Close()
        {
            DSShow_Close(m_graphIndex);
            m_playerClosed = true;
        }

        public bool IsPlayerClosed()
        {
            return m_playerClosed;
        }

        public int Play()
        {
            lock (this)
            {
                return DSShow_Play(m_graphIndex);
            }
        }

        public int Run()
        {
            lock (this)
            {
                return DSShow_Play(m_graphIndex);
            }
        }

        public int Start()
        {
            lock (this)
            {
                return DSShow_Play(m_graphIndex);
            }
        }


        public int Pause()
        {
            lock (this)
            {
                return DSShow_Pause(m_graphIndex);
            }
        }

        public int Stop()
        {
            lock (this)
            {
                return DSShow_Stop(m_graphIndex);
            }
        }

        public int Repaint(IntPtr hdc)
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return 0;
                return DSShow_Repaint(m_graphIndex, hdc);
            }
        }
        SELECTED_PULLDEMUX m_selectedDemux = SELECTED_PULLDEMUX.RENDER_DEMUX;
        public void SelectPullDemux(SELECTED_PULLDEMUX demux)
        {
            lock (this)
            {
                DSShow_SelectPullDemux(m_graphIndex, (int)demux);
                m_selectedDemux = demux;
            }
        }
        public void SelectRenderer(VIDEO_RENDER rend)
        {
            lock (this)
            {
                DSShow_SelectRenderer(m_graphIndex, (int)rend);
            }
        }

        public void SetAspectRatio(ASPECT_RATIO ratio, bool applyNow)
        {
            lock (this)
            {
                DSShow_SetAspectRatio(m_graphIndex, (int)ratio, applyNow);
            }
        }
        public void SelectDecoder(SELECTED_DECODER decoder)
        {
            lock (this)
            {
                DSShow_SelectDecoder(m_graphIndex, (int)decoder);
            }
        }
        public void SetWindowHandle(IntPtr handle)
        {
            lock (this)
            {
                try
                {
                    if (m_videoInitialized == false)
                        return;
                    DSShow_SetWindowHandle(m_graphIndex, handle);
                }
                catch (Exception err)
                {

                }
            }
        }

        public void UpdateVideoWindow(Rect prc)
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return;
                DSShow_UpdateVideoWindow(m_graphIndex, (int)prc.X, (int)prc.Y, (int)prc.Width, (int)prc.Height);
            }
        }

        public int SetFileName(string sFileName)
        {
            lock (this)
            {
                return DSShow_SetFileName(m_graphIndex, sFileName);
            }
        }


        public int StepForward(long lFrames = 1, bool bSec = false)
        {
            lock (this)
            {
                int r = DSShow_StepForward(m_graphIndex, lFrames, bSec);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public int StepBackward(long lFrames = 1, bool bSec = false)
        {
            lock (this)
            {
                int r = DSShow_StepBackward(m_graphIndex, lFrames, bSec);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public int Snapshot(string fileName)
        {
            lock (this)
            {
                int r = DSShow_Snapshot(m_graphIndex, fileName);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public long GetCurrentPosition()
        {
            lock (this)
            {
                return DSShow_GetCurrentPosition(m_graphIndex);
            }
        }

        public void GetCurrentPosition(ref TimeSpan time)
        {
            long curPos = DSShow_GetCurrentPosition(m_graphIndex);
            string s = null;
            TimeFormat(curPos, ref s, ref time);
        }

        void TimeFormat(long rtTime, ref string timeFormat, ref TimeSpan time)
        {
            long rtT = rtTime / (ONE_SEC / 100);

            uint msec = (uint)(rtT % 100); rtT = rtT / 100;
            uint sec = (uint)(rtT % 60); rtT = rtT / 60;
            uint minute = (uint)(rtT % 60); rtT = rtT / 60;
            uint hour = (uint)rtT;
            if (timeFormat != null)
                timeFormat = string.Format("{0}:{1}:{2}:{3}", hour.ToString("00"), minute.ToString("00"), sec.ToString("00"), msec.ToString("00"));
            if (time != null)
                time = new TimeSpan(0, (int)hour, (int)minute, (int)sec, (int)msec);
        }



        public int SetPosition(long pos)
        {
            lock (this)
            {
                int r = DSShow_SetPosition(m_graphIndex, pos);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public int SetPosition(double pos)
        {
            lock (this)
            {
                int r = DSShow_SetPosition(m_graphIndex, (long)(pos));
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public int SetPosition(TimeSpan time)
        {
            lock (this)
            {
                int r = DSShow_SetPosition(m_graphIndex, (long)(time.TotalSeconds * ONE_SEC));
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }


        public int GetDuration(ref long pDuration)
        {
            lock (this)
            {
                int r = DSShow_GetDuration(m_graphIndex, ref pDuration);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }

        }

        public int InitializePlayer(IntPtr handle, out string outMessage, int klvPID = 0)
        {
            lock (this)
            {
                outMessage = string.Empty;
                if (klvPID > 0 && m_selectedDemux != SELECTED_PULLDEMUX.IMPAL)
                {
                    outMessage = "KLV is not supported in this mode";
                    return -1;
                }
                int r = DSShow_InitializePlayer(m_graphIndex, handle, klvPID);
                if (r == 0)
                {
                    m_playerClosed = false;
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public int SetEventWindow(IntPtr handle, int eventId)
        {
            lock (this)
            {
                int r = DSShow_SetEventWindow(m_graphIndex, handle, eventId);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }

        public int GetGraphEvent(ref long param1, ref long param2, ref long evCode)
        {
            lock (this)
            {
                int r = DSShow_GetGraphEvent(m_graphIndex, ref param1, ref param2, ref evCode);
                if (r == 0)
                {
                    m_videoInitialized = true;
                }
                return r;
            }
        }
        public int AddTextOverlay(string text,
                                  int id,
                                  int left,
                                  int top,
                                  int right,
                                  int bottom,
                                  Color color,
                                  float fontSize,
                                  int fontStyle)
        {



            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                int c = color.B << 16 | color.G << 8 | color.R;

                return DSShow_AddTextOverlay(m_graphIndex,
                                             text,
                                             id,
                                             left,
                                             top,
                                             right,
                                             bottom,
                                             c,
                                             fontSize,
                                             fontStyle);
            }
        }



        public int AddTextOverlay2(string text, int id,
                                       int left,
                                       int top,
                                       int right,
                                       int bottom,
                                       Color color,
                                       float fontSize)
        {

            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;

                int c = color.B << 16 | color.G << 8 | color.R;

                return DSShow_AddTextOverlay2(m_graphIndex,
                                        text,
                                        id,
                                        left,
                                        top,
                                        right,
                                        bottom,
                                        c,
                                        fontSize);
            }
        }


        public int ApplyOverlay(float alpha_opacity = 1f)
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                return DSShow_ApplyOverlay(m_graphIndex, alpha_opacity);
            }
        }

        public static void UnlockBautechPlayer(long code)
        {
            DSShow_UnlockBautechPlayer(code);
        }

        public int Clear()
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                return DSShow_Clear(m_graphIndex);
            }
        }


        public int Visible(SHAPE_IDs id, bool visible)
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                return DSShow_Visible(m_graphIndex, (int)id, visible);
            }
        }

        public int Visible(int id, bool visible)
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                return DSShow_Visible(m_graphIndex, id, visible);
            }
        }

        public int AddLine(int id,
                            int x1,
                            int y1,
                            int x2,
                            int y2,
                            Color color,
                            int width)
        {
            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                int c = color.B << 16 | color.G << 8 | color.R;
                return DSShow_AddLine(m_graphIndex,
                                       id,
                                       x1,
                                       y1,
                                       x2,
                                       y2,
                                       c,
                                       width);
            }

        }

        public int AddCircle(int id,
                             int x1,
                             int y1,
                             int radios_w,
                             int radios_h,
                             Color color,
                             int width)
        {

            lock (this)
            {
                if (m_videoInitialized == false)
                    return 1;
                int c = color.B << 16 | color.G << 8 | color.R;
                return DSShow_AddCircle(m_graphIndex,
                                        id,
                                        x1,
                                        y1,
                                        radios_w,
                                        radios_h,
                                        c,
                                        width);
            }
        }

        public int InitilizeNetworkMulticastPlayer(IntPtr hwnd,
                                                   bool useBoutechReceiver,
                                                   string MulticastAddress,
                                                   int MulticastPort,
                                                   bool useInterface,
                                                   float demuxLatency = 1,
                                                   int klvPID = 0)
        {

            lock (this)
            {
                int hr; 
                if ((hr = DSShow_InitilizeNetworkMulticastPlayer(m_graphIndex, hwnd, useBoutechReceiver, MulticastAddress, 
                                                                MulticastPort, useInterface,
                                                                demuxLatency, klvPID)) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return hr;
            }
        }

        public int InitializeBoutechReceiverToWebRTC(string serverAddress,
                                                     bool useTransport,
                                                     int cameraPort,
                                                     string webRtcURL)
        {

            lock (this)
            {

                if (DSShow_InitializeBoutechReceiverToWebRTC(m_graphIndex, serverAddress, useTransport, cameraPort, webRtcURL) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }
        }


        public int InitializeBoutechReceiverToWebRTCAndHLS(string serverAddress,
                                                           bool useTransport,
                                                           int cameraPort,
                                                           string webRtcURL,
                                                           string HLSOutputPath,
                                                           string HLSName,
                                                           int HLSRelativeUrl,
                                                           int HLSLive,
                                                           int HLSPlaylistDuration,
                                                           int HLSSegmentDuration,
                                                           string HLSStreamName)
        {

            lock (this)
            {

                if (DSShow_InitializeBoutechReceiverToWebRTCAndHLS(m_graphIndex,
                                                                   serverAddress,
                                                                   useTransport,
                                                                   cameraPort,
                                                                   webRtcURL,
                                                                   HLSOutputPath,
                                                                   HLSName,
                                                                   HLSRelativeUrl,
                                                                   HLSLive,
                                                                   HLSPlaylistDuration,
                                                                   HLSSegmentDuration,
                                                                   HLSStreamName) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }
        }

        public int SetRate(double rate)
        {
            lock (this)
            {

                if (DSShow_SetRate(m_graphIndex, rate) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }
        }

        public int InitializeScreenCaptureToDump(string fileName,
                                                 int screenWidth,
                                                 int screenHeight,
                                                 bool includeAudio,
                                                 int bitrate)
        {
            lock (this)
            {

                int res;
                if ((res = DSShow_InitializeScreenCaptureToDump(m_graphIndex, 
                                                                fileName , 
                                                                screenWidth, 
                                                                screenHeight, 
                                                                includeAudio,
                                                                bitrate)) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return res;
            }
        }

        ProgressCallback2Host pProgressCallback2Host;
        public void SetProgressCallback2Host(ProgressCallback2Host p)
        {
            pProgressCallback2Host = p;
        }


        void ProgressCallbackMsg(string fileName,
                                double PCR,
                                int fileState)
        {

            pProgressCallback2Host(fileName, PCR, fileState);
        }

        public int InitializeNetwordReceiverToBoutechDVRSink(string rootDirectory,
                                                             string serverAddress,
                                                             string interfaceAddress,
                                                             int serverPort,
                                                             bool useBoutechReceiver,
                                                             int LogTime,
                                                             int maxFilesPerDirectory)
        {


           
             

            /*
            // define a progress callback delegate
            ProgressCallback callback =
                (fileName, PCR, fileState) =>
                {
                   
                    if (pProgressCallback2Host != null)
                    {
                        
                    }
                };
              */         

            lock (this)
            {

                if (DSShow_InitializeNetwordReceiverToBoutechDVRSink(m_graphIndex, 
                                                                     rootDirectory, 
                                                                     serverAddress,
                                                                     interfaceAddress,
                                                                     serverPort,
                                                                     useBoutechReceiver,
                                                                     LogTime,
                                                                     maxFilesPerDirectory) == 0)
                {
                    DSShow_SetDVRSinkCallback(pDvrCallback);
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }


        }

        public int InitializeBauotechDVRPlayer(IntPtr hwnd,
                                                string DvrFileName,
                                                float startTime,
                                                bool loop)
        {

            lock (this)
            {

                if (DSShow_InitializeBauotechDVRPlayer(m_graphIndex, hwnd, DvrFileName, startTime, loop) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }

        } 
        public int InitializeBauotechDVRPlayerAsync(IntPtr hwnd,
                                                string DvrFileName,
                                                float startTime,
                                                bool loop)
        {

            lock (this)
            {

                if (DSShow_InitializeBauotechDVRPlayerAsync(m_graphIndex, hwnd, DvrFileName, startTime, loop) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }

        }

        public int InitilizeNetworkVideoEsPlayer(IntPtr hwnd,
                                                 bool useBoutechReceiver,
                                                 string ServerAddress,
                                                 int ServerPort)
        {

            lock (this)
            {

                if (DSShow_InitilizeNetworkVideoEsPlayer(m_graphIndex, hwnd, useBoutechReceiver,  ServerAddress, ServerPort) == 0)
                {
                    m_videoInitialized = true;
                    return 1;
                }
                return 0;
            }
        }
        /////// //////////////////////////////////////

        public bool GetDuration(string fileName, ref double duration)
        {
            lock (this)
            {

                return DEMUX_GetDuration(fileName, ref duration);
            }

        }
    }
}