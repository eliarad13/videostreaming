// DSC2CSLib.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "BauotechPlayerIface.h"
#include "BauotechPlayer.h"

#define MAX_GRAPHS  1000
DShowPlayer *m_dsShowPlayer[MAX_GRAPHS];
 

DSC2CSLIB_API HRESULT DSShow_Play(int graphIndex)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->Play();
}

DSC2CSLIB_API HRESULT DSShow_Pause(int graphIndex)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->Pause();
}

DSC2CSLIB_API HRESULT DSShow_Stop(int graphIndex)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->Stop();
}
  
 

DSC2CSLIB_API void DSShow_Close(int graphIndex)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return;
	m_dsShowPlayer[graphIndex]->Close();

	delete m_dsShowPlayer[graphIndex];
	m_dsShowPlayer[graphIndex] = NULL;
	 
	return;
}

DSC2CSLIB_API void DSShow_StopAll()
{
	for (int i = 0; i < MAX_GRAPHS; i++)
	{
		if (m_dsShowPlayer[i] == NULL)
			continue;
		m_dsShowPlayer[i]->Stop();
	}
}

DSC2CSLIB_API void DSShow_CloseAll()
{
	DSShow_StopAll();

	for (int i = 0; i < MAX_GRAPHS; i++)
	{
		if (m_dsShowPlayer[i] == NULL) 
			continue;
		m_dsShowPlayer[i]->Close();

		delete m_dsShowPlayer[i];
		m_dsShowPlayer[i] = NULL;
	}
 
}
  
DSC2CSLIB_API void DSShow_SetWindowHandle(int graphIndex, HWND hwnd)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return;
	m_dsShowPlayer[graphIndex]->SetWindowHandle(hwnd);
}

DSC2CSLIB_API HRESULT DSShow_InitializePlayer(int graphIndex, HWND hwnd, int klvPID)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	return m_dsShowPlayer[graphIndex]->InitializePlayer(hwnd, klvPID);
}
 
DSC2CSLIB_API  void DSShow_UpdateVideoWindow(int graphIndex, int x, int y, int width, int height)
{
	RECT r;
	r.left = x;
	r.top = y;
	r.right = width;
	r.bottom = height;

	if (m_dsShowPlayer[graphIndex] == NULL) return;
	m_dsShowPlayer[graphIndex]->UpdateVideoWindow((LPRECT)&r);
}

DSC2CSLIB_API void DSShow_SetAspectRatio(int graphIndex, int ratio, bool applyNow)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	m_dsShowPlayer[graphIndex]->SetAspectRatio((ASPECT_RATIO)ratio, applyNow);

}

DSC2CSLIB_API  void DSShow_SelectPullDemux(int graphIndex, int selectedPullDemux)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	m_dsShowPlayer[graphIndex]->SelectPullDemux((SELECTED_PULLDEMUX)selectedPullDemux);
} 

DSC2CSLIB_API  void  DSShow_SelectRenderer(int graphIndex, int renderer)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	m_dsShowPlayer[graphIndex]->SelectRenderer((VIDEO_RENDER)renderer);
}

DSC2CSLIB_API  void DSShow_SelectDecoder(int graphIndex, int selectedDecoder)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	m_dsShowPlayer[graphIndex]->SelectDecoder((SELECTED_DECODER)selectedDecoder);
}

DSC2CSLIB_API void DSShow_SetFileName(int graphIndex, const WCHAR* sFileName)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return;
	m_dsShowPlayer[graphIndex]->SetFileName(sFileName);

}
 

DSC2CSLIB_API int DSShow_Snapshot(int graphIndex, const WCHAR *fileName)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	return m_dsShowPlayer[graphIndex]->Snapshot(fileName);
}
 

DSC2CSLIB_API HRESULT DSShow_GetDuration(int graphIndex, LONGLONG *pDuration)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		return -1;
	}

	return m_dsShowPlayer[graphIndex]->GetDuration(pDuration);
}

DSC2CSLIB_API HRESULT DSShow_SetPosition(int graphIndex, LONGLONG pos)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		return -1;
	}

	return m_dsShowPlayer[graphIndex]->SetPosition(pos);
}
 

DSC2CSLIB_API LONGLONG DSShow_GetCurrentPosition(int graphIndex)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		return -1;
	}
	LONGLONG pTimeNow;
	m_dsShowPlayer[graphIndex]->GetCurrentPosition(&pTimeNow);
	return pTimeNow;
}

DSC2CSLIB_API HRESULT DSShow_GetGraphEvent(int graphIndex, long *param1, long *param2, long *evCode)
{
	 
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		return -1;
	}
	return m_dsShowPlayer[graphIndex]->GetGraphEvent(param1, param2, evCode);	
}


DSC2CSLIB_API HRESULT DSShow_StepForward(int graphIndex, long lFrames , BOOL bSec)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		return -1;
	}
	return m_dsShowPlayer[graphIndex]->StepForward();
}

DSC2CSLIB_API HRESULT DSShow_StepBackward(int graphIndex, long lFrames, BOOL bSec)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		return -1;
	}
	return m_dsShowPlayer[graphIndex]->StepBackward();
}

DSC2CSLIB_API HRESULT DSShow_AddCircle(int graphIndex,
									   int id,
										int x1,
										int y1,
										int radios_w,
										int radios_h,
										COLORREF color,
										int width)
{

	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;

	return m_dsShowPlayer[graphIndex]->AddCircle(id,
		x1,
		y1,
		radios_w,
		radios_h,
		color,
		width);

}

DSC2CSLIB_API HRESULT DSShow_AddLine(int graphIndex, int id,
	int x1,
	int y1,
	int x2,
	int y2,
	int color,
	int width)
{

	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->AddLine(id,
		x1,
		y1,
		x2,
		y2,
		color,
		width);
}


DSC2CSLIB_API HRESULT	DSShow_Visible(int graphIndex, int id, bool visible)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->Visible(id, visible);
}
DSC2CSLIB_API HRESULT	DSShow_Clear(int graphIndex)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->Clear();
}

HRESULT	DSShow_AddTextOverlay2(int graphIndex, WCHAR *text, int id,
	int left,
	int top,
	int right,
	int bottom,
	int color,
	float fontSize)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->AddTextOverlay2(text, id,
		left,
		top,
		right,
		bottom,
		color,
		fontSize);

}

DSC2CSLIB_API HRESULT	DSShow_AddTextOverlay(int graphIndex, WCHAR *text,
	int id,
	int left,
	int top,
	int right,
	int bottom,
	int color,
	float fontSize,
	int fontStyle)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;

	return m_dsShowPlayer[graphIndex]->AddTextOverlay(text,
		id,
		left,
		top,
		right,
		bottom,
		color,
		fontSize,
		fontStyle);

}


DSC2CSLIB_API HRESULT DSShow_Repaint(int graphIndex, HDC hdc)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->Repaint(hdc);

}

DSC2CSLIB_API  HRESULT DSShow_ApplyOverlay(int graphIndex, float alpha_opacity)
{
	if (m_dsShowPlayer[graphIndex] == NULL) return S_FALSE;
	return m_dsShowPlayer[graphIndex]->ApplyOverlay(alpha_opacity);
}

DSC2CSLIB_API HRESULT DSShow_InitilizeNetworkMulticastPlayer(int graphIndex, HWND hwnd, 
															 bool useBoutechReceover,
															 const WCHAR *MulticastAddress, 															 
															 int MulticastPort, 
															 bool useInterface,
															 float demuxLatency, int klvPID)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeNetworkMulticastPlayer(hwnd, 
																		useBoutechReceover, 
																	    MulticastAddress, 
																		MulticastPort, 
																	    useInterface,
																		demuxLatency, klvPID);
}

DSC2CSLIB_API HRESULT DSShow_InitilizeNetworkVideoEsPlayer(int graphIndex,
															HWND hwnd,
															bool useBoutechReceiver,
															const WCHAR *ServerAddress,
															int ServerPort)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitilizeNetworkVideoEsPlayer(hwnd, useBoutechReceiver, ServerAddress, ServerPort);
}

DSC2CSLIB_API HRESULT DSShow_InitializeBoutechReceiverToWebRTC(int graphIndex,
	const WCHAR * serverAddress,
	bool useTransport,
	int cameraPort,
	const WCHAR * webRtcURL)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeBoutechReceiverToWebRTC(serverAddress, useTransport, cameraPort, webRtcURL);

}

DSC2CSLIB_API HRESULT DSShow_InitializeBoutechReceiverToWebRTCAndHLS(int graphIndex,
																	 const WCHAR * serverAddress,
																	 bool useTransport,
																	 int cameraPort,
																	 const WCHAR * webRtcURL,
																	 const WCHAR * HLSOutputPath,
																	 const WCHAR * HLSName,
																	 int HLSRelativeUrl,
																	 int HLSLive,
																	 int HLSPlaylistDuration,
																	 int HLSSegmentDuration,
																	 const WCHAR * HLSStreamName)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}
 
	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeBoutechReceiverToWebRTCAndHLS(serverAddress, useTransport, cameraPort, webRtcURL,
																				HLSOutputPath, HLSName, HLSRelativeUrl, 
																				HLSLive, 
																			    HLSPlaylistDuration, 
																				HLSSegmentDuration, 
																				HLSStreamName);

}

DSC2CSLIB_API  HRESULT DSShow_SetRate(int graphIndex, double rate)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->SetRate(rate);
}

DSC2CSLIB_API HRESULT  DSShow_InitializeScreenCaptureToDump(int graphIndex, const WCHAR* fileName,
												int screenWidth,
												int screenHeight,
										        bool includeAudio,
											    int bitrate)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeScreenCaptureToDump(fileName, screenWidth, screenHeight, includeAudio, bitrate);

}


DSC2CSLIB_API HRESULT DSShow_InitializeBauotechDVRPlayerAsync(int graphIndex,
										HWND hwnd,
										const WCHAR *DvrFileName,
										float startTime,
										bool loop)
{

	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeBauotechDVRPlayerAsync(hwnd, DvrFileName, startTime, loop);

}

DSC2CSLIB_API HRESULT DSShow_InitializeBauotechDVRPlayer(int graphIndex,
											HWND hwnd,
											const WCHAR *DvrFileName,
											float startTime,
											bool loop)
{
	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeBauotechDVRPlayer(hwnd, DvrFileName, startTime, loop);

}
DSC2CSLIB_API HRESULT DSShow_InitializeNetwordReceiverToBoutechDVRSink(int graphIndex, 
																		const WCHAR * rootDirectory,
																		const WCHAR * serverAddress,
																	    const WCHAR * interfaceAddress,
																		int serverPort,
																		bool useBoutechReceiver,
																		int LogTime,
																		int maxFilesPerDirectory)

{


	if (m_dsShowPlayer[graphIndex] == NULL)
	{
		m_dsShowPlayer[graphIndex] = new DShowPlayer();
	}

	if (m_dsShowPlayer[graphIndex] == NULL)
		return S_FALSE;
	return m_dsShowPlayer[graphIndex]->InitializeNetwordReceiverToBoutechDVRSink(rootDirectory, 
												serverAddress, interfaceAddress, 
												serverPort, 
												useBoutechReceiver, LogTime, maxFilesPerDirectory);


}

DSC2CSLIB_API void DSShow_SetDVRSinkCallback(ProgressCallback progressCallback)
{
	pProgressCallback = progressCallback;

	return m_dsShowPlayer[0]->SetDVRSinkCallback(progressCallback);

}
 
DSC2CSLIB_API void DSShow_UnlockBautechPlayer(const long code)
{
	  
	return DShowPlayer::UnlockBautechPlayer(code);
}
