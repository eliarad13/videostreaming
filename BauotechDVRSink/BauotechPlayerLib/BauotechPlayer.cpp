//////////////////////////////////////////////////////////////////////////
// DShowPlayer.cpp: Implements DirectShow playback functionality.
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
//////////////////////////////////////////////////////////////////////////

#include "BauotechPlayer.h"
#include "DshowUtil.h"
#include <gdiplus.h>
#include <ctime>
#include <iostream>
#include <algorithm>
#include "NWPlayerEng_config.h"
#include "WRtpServer.h"
#include "ModuleConfig_h.h"
#include <comutil.h>
#include "enwr.h"
#include <Dvdmedia.h>
#include "ehlssink.h"
#include "edc.h"
#include "ecommon.h"
#include <atlsafe.h>
 
 
 
extern IVMRMixerBitmap9  *pBmp;
extern IVMRWindowlessControl9 *m_pWC;
//-----------------------------------------------------------------------------
// DShowPlayer constructor.
//-----------------------------------------------------------------------------

long DShowPlayer::m_bauotechLockKey = 0;
DShowPlayer::DShowPlayer() :
	m_state(STATE_CLOSED),	
	m_hwndEvent(NULL),
	m_EventMsg(0),
	m_pGraph(NULL),
	m_pControl(NULL),
	m_pEvent(NULL),
	m_pSeek(NULL),
	m_pAudio(NULL),
    m_pVideo(NULL),
	m_seekCaps(0),
	m_bMute(FALSE),
	m_lVolume(MAX_VOLUME)
{
	 
	pShapeFilterInterface = NULL;
	m_selectedRenderer = VIDEO_RENDER::Try_VMR9;
	m_selectedDecoder = SELECTED_DECODER::ELECARD;
	m_selectedPullDemux = SELECTED_PULLDEMUX::RENDER_DEMUX;
	m_vmrAspectRatio = ASPECT_RATIO::PRESERVE;
	

}
 

//-----------------------------------------------------------------------------
// DShowPlayer destructor.
//-----------------------------------------------------------------------------

DShowPlayer::~DShowPlayer()
{
	TearDownGraph();
}


void GetDesktopResolution(int& horizontal, int& vertical)
{
	RECT desktop;
	// Get a handle to the desktop window
	const HWND hDesktop = GetDesktopWindow();
	// Get the size of screen to the variable desktop
	GetWindowRect(hDesktop, &desktop);
	// The top left corner will have coordinates (0,0)
	// and the bottom right corner will have coordinates
	// (horizontal, vertical)
	horizontal = desktop.right;
	vertical = desktop.bottom;
}

template <typename T> int sign(T val) {
	return (T(0) < val) - (val < T(0));
}
   

IPin *DShowPlayer::GetPin(IBaseFilter *Filter, CString PinName, PIN_DIRECTION  direction)
{

	IEnumPins     *pEnumPins;
	ULONG          ul;
	PIN_DIRECTION  pd;
	CString pin_name = "";
	int counter = 0;
	HRESULT hr = Filter->EnumPins(&pEnumPins);
	PIN_INFO    pin_info;
	IPin *requestedPin = NULL;
	ASSERT(SUCCEEDED(hr));
	while ((pin_name != PinName) && (counter != 50)) {
		// Get the Sigma Splitter video output pin
		hr = pEnumPins->Next(1, &requestedPin, &ul);
		if ((S_OK != hr) && (0 == ul))
			return NULL;
		requestedPin->QueryDirection(&pd);
		if (pd == direction) {
			const_cast<IPin*>(requestedPin)->QueryPinInfo(&pin_info);
			pin_name = pin_info.achName;
		}
		counter++;
	}
	pEnumPins->Release();

	return requestedPin;
}


size_t findCaseInsensitive(std::string data, std::string toSearch, size_t pos = 0)
{
	// Convert complete given String to lower case
	std::transform(data.begin(), data.end(), data.begin(), ::tolower);
	// Convert complete given Sub String to lower case
	std::transform(toSearch.begin(), toSearch.end(), toSearch.begin(), ::tolower);
	// Find sub string in given string
	return data.find(toSearch, pos);
}

IPin *DShowPlayer::GetPinContainsName(IBaseFilter *Filter, CString PinName, PIN_DIRECTION  direction)
{

	IEnumPins     *pEnumPins;
	ULONG          ul;
	PIN_DIRECTION  pd;
	CString pin_name = "";
	int counter = 0;
	HRESULT hr = Filter->EnumPins(&pEnumPins);
	PIN_INFO    pin_info;
	IPin *requestedPin = NULL;
	ASSERT(SUCCEEDED(hr));

	
	PinName = PinName.MakeLower();
	 
	while ((pin_name.Find(PinName) != 0) && (counter != 50)) {
		// Get the Sigma Splitter video output pin
		hr = pEnumPins->Next(1, &requestedPin, &ul);
		if ((S_OK != hr) && (0 == ul))
			return NULL;
		requestedPin->QueryDirection(&pd);
		if (pd == direction) {
			const_cast<IPin*>(requestedPin)->QueryPinInfo(&pin_info);
			pin_name = pin_info.achName;
			pin_name = pin_name.MakeLower();
		}
		counter++;
	}
	pEnumPins->Release();

	return requestedPin;
}

 
HRESULT DShowPlayer::GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin, int num)
{
	CComPtr<IEnumPins>  pEnum = NULL;
	IPin       *pPin = NULL;
	HRESULT    hr;

	if (ppPin == NULL)
	{
		return E_POINTER;
	}

	int count = 0;

	hr = pFilter->EnumPins(&pEnum);
	if (FAILED(hr))
	{
		return hr;
	}
	while (pEnum->Next(1, &pPin, 0) == S_OK)
	{
		PIN_DIRECTION PinDirThis;
		hr = pPin->QueryDirection(&PinDirThis);
		if (FAILED(hr))
		{
			//pPin->Release();
			//pEnum->Release();
			return hr;
		}
		if (PinDir == PinDirThis && count == num)
		{
			// Found a match. Return the IPin pointer to the caller.
			*ppPin = pPin;
			//pPin->Release();
		    //pEnum->Release();
			return S_OK;
		}
		if (PinDir == PinDirThis)
			count++;
		// Release the pin for the next time through the loop.
		//pPin->Release();
	}
	// No more pins. We did not find a match.
	//pEnum->Release();
	return E_FAIL;
}
 

HRESULT DShowPlayer::EnumerateDevices(REFGUID category, IEnumMoniker **ppEnum)
{
	// Create the System Device Enumerator.
	ICreateDevEnum *pDevEnum;
	HRESULT hr = CoCreateInstance(CLSID_SystemDeviceEnum, NULL,
		CLSCTX_INPROC_SERVER, IID_PPV_ARGS(&pDevEnum));

	if (SUCCEEDED(hr))
	{
		// Create an enumerator for the category.
		hr = pDevEnum->CreateClassEnumerator(category, ppEnum, 0);
		if (hr == S_FALSE)
		{
			hr = VFW_E_NOT_FOUND;  // The category is empty. Treat as an error.
		}
		pDevEnum->Release();
	}
	return hr;
}
 
std::string BSTR2string(BSTR bstr)
{
	USES_CONVERSION;
	return std::string(W2A(bstr));
}
 
 
int DShowPlayer::Snapshot(const WCHAR *fileName)
{
	HRESULT hr;

	return m_pVideo->SaveFrame(fileName);

}
 
HRESULT DShowPlayer::ClearDecoderBufferForFastLatency()
{ 
	HRESULT hr = E_FAIL;
	// {9A1044D4-5017-4888-8ECF-9A8F2553942F}
	static const GUID EAVCDEC_DecFrameBuffering = { 0x9a1044d4, 0x5017, 0x4888,{ 0x8e, 0xcf, 0x9a, 0x8f, 0x25, 0x53, 0x94, 0x2f } };

	// {294C64BC-ECB1-4cae-98A9-258C291E9489}
	static const GUID EAVCDEC_FullThreadLoad = { 0x294c64bc, 0xecb1, 0x4cae,{ 0x98, 0xa9, 0x25, 0x8c, 0x29, 0x1e, 0x94, 0x89 } };

	 
	CComQIPtr<IModuleConfig> mc_demuxer(pVideoDecoder);
	if (pVideoDecoder)
	{
		CComVariant var = 0;
		hr = mc_demuxer->SetValue(&EAVCDEC_DecFrameBuffering, &var);
		hr = mc_demuxer->CommitChanges(NULL);
		 
		hr = mc_demuxer->SetValue(&EAVCDEC_FullThreadLoad, &var);
		hr = mc_demuxer->CommitChanges(NULL);
	}


	return hr;

}

HRESULT DShowPlayer::SetDemuxerLatency(float nLatency)
{

	// {6AFFB5BE-C0E1-4644-A78A-F2B0F9BAE5B0}
	static const GUID EMPGPDMX_LATENCY_VALUE = { 0x6affb5be, 0xc0e1, 0x4644,{ 0xa7, 0x8a, 0xf2, 0xb0, 0xf9, 0xba, 0xe5, 0xb0 } };


	HRESULT hr = E_FAIL;
	CComQIPtr<IModuleConfig> mc_demuxer(pElecardMpegPushDemux);
	if (mc_demuxer)
	{
		CComVariant var = nLatency;
		hr = mc_demuxer->SetValue(&EMPGPDMX_LATENCY_VALUE, &var);
		hr = mc_demuxer->CommitChanges(NULL);
	}

	return hr;
}
 
 
 
// retrive announces from NWSourcePlus filter
HRESULT DShowPlayer::get_announces(ENWSource_Plus::announces_descr_ext **announces_descr)
{
	if (m_nwsource_config == NULL)
		return E_UNEXPECTED;

	CComVariant val;
	HRESULT hr = m_nwsource_config->GetValue(&ENS_announces_bstr, &val);
	if (FAILED(hr))
		return hr;

	*announces_descr = (ENWSource_Plus::announces_descr_ext*)val.ullVal;
	return S_OK;
}
  
  
HRESULT DShowPlayer::LogError(char *error)
{
	FILE *handle = fopen("c:\\DS.csv", "a+t");


	std::time_t t = std::time(0);   // get time now
	std::tm* now = std::localtime(&t);
	string date = (now->tm_year + 1900) + "-" + (now->tm_mon + 1) + '-' + now->tm_mday;
	
	fprintf(handle, "%s,%s\n" , date, error);
	if (handle != NULL)
		fclose(handle);

	return S_OK;
}

void LogError(char *err)
{


}


//-----------------------------------------------------------------------------
// DShowPlayer::SetEventWindow
// Description: Set the window to receive graph events.
//
// hwnd: Window to receive the events.
// msg: Private window message that window will receive whenever a 
//      graph event occurs. (Must be in the range WM_APP through 0xBFFF.)
//-----------------------------------------------------------------------------

HRESULT DShowPlayer::SetEventWindow(HWND hwnd, UINT msg)
{
	m_hwndEvent = hwnd;
	m_EventMsg = msg;
	return S_OK;
}
 

//-----------------------------------------------------------------------------
// DShowPlayer::HandleGraphEvent
// Description: Respond to a graph event.
//
// The owning window should call this method when it receives the window
// message that the application specified when it called SetEventWindow.
//
// pCB: Pointer to the GraphEventCallback callback, implemented by 
//      the application. This callback is invoked once for each event
//      in the queue. 
//
// Caution: Do not tear down the graph from inside the callback.
//-----------------------------------------------------------------------------

HRESULT DShowPlayer::GetGraphEvent(long *param1, long *param2 , long *evCode)
{

	m_pEvent->GetEvent(evCode, (LONG_PTR *)param1, (LONG_PTR *)param2, 0);
	HRESULT hr = m_pEvent->FreeEventParams(*evCode, *(LONG_PTR *)param1, *(LONG_PTR *)param2);

	return hr;

}


HRESULT DShowPlayer::HandleGraphEvent(GraphEventCallback *pCB)
{
	if (pCB == NULL)
	{
		return E_POINTER;
	}

	if (!m_pEvent)
	{
		return E_UNEXPECTED;
	}

	long evCode = 0;
	LONG_PTR param1 = 0, param2 = 0;

	HRESULT hr = S_OK;

    // Get the events from the queue.
	while (SUCCEEDED(m_pEvent->GetEvent(&evCode, &param1, &param2, 0)))
	{
		if (m_abort == true)
		{
			return -999919;
		}

        // Invoke the callback.
		pCB->OnGraphEvent(evCode, param1, param2);

        // Free the event data.
		hr = m_pEvent->FreeEventParams(evCode, param1, param2);
		if (FAILED(hr))
		{
			break;
		}
	}

	return hr;
}


// state changes

HRESULT DShowPlayer::Play()
{
	if (m_state != STATE_PAUSED && m_state != STATE_STOPPED)
	{
		return VFW_E_WRONG_STATE;
	}

 	 
	HRESULT hr = m_pControl->Run();

	if (SUCCEEDED(hr))
	{
		m_state = STATE_RUNNING;
	}

	return hr;
}

HRESULT DShowPlayer::Pause()
{
	if (m_state != STATE_RUNNING)
	{
		return VFW_E_WRONG_STATE;
	}

 
	HRESULT hr = m_pControl->Pause();

	if (SUCCEEDED(hr))
	{
		m_state = STATE_PAUSED;
	}

	return hr;
}


HRESULT DShowPlayer::Stop()
{
	if (m_state != STATE_RUNNING && m_state != STATE_PAUSED)
	{
		return VFW_E_WRONG_STATE;
	}
 
	HRESULT hr = m_pControl->Stop();

	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}

	return hr;
}


// EVR/VMR functionality


BOOL DShowPlayer::HasVideo() const
{
    return (m_pVideo && m_pVideo->HasVideo());
}
 
 
BOOL DShowPlayer::CanSeek() const
{
	const DWORD caps = AM_SEEKING_CanSeekAbsolute | AM_SEEKING_CanGetDuration;
	return ((m_seekCaps & caps) == caps);
}
 
//-----------------------------------------------------------------------------
// DShowPlayer::SetPosition
// Description: Seeks to a new position.
//-----------------------------------------------------------------------------

HRESULT DShowPlayer::SetPosition(REFERENCE_TIME pos)
{
	if (m_pControl == NULL || m_pSeek == NULL)
	{
		return E_UNEXPECTED;
	}

	HRESULT hr = S_OK;

	hr = m_pSeek->SetPositions(&pos, AM_SEEKING_AbsolutePositioning,
		NULL, AM_SEEKING_NoPositioning);

	if (SUCCEEDED(hr))
	{
		// If playback is stopped, we need to put the graph into the paused
		// state to update the video renderer with the new frame, and then stop 
		// the graph again. The IMediaControl::StopWhenReady does this.
		if (m_state == STATE_STOPPED)
		{
			hr = m_pControl->StopWhenReady();
		}
	}

	return hr;
}

//-----------------------------------------------------------------------------
// DShowPlayer::GetDuration
// Description: Gets the duration of the current file.
//-----------------------------------------------------------------------------
 
HRESULT DShowPlayer::GetDuration(LONGLONG *pDuration)
{
	if (m_pSeek == NULL)
	{
		return E_UNEXPECTED;
	}

	return m_pSeek->GetDuration(pDuration);
}

//-----------------------------------------------------------------------------
// DShowPlayer::GetCurrentPosition
// Description: Gets the current playback position.
//-----------------------------------------------------------------------------

HRESULT DShowPlayer::GetCurrentPosition(LONGLONG *pTimeNow)
{
	if (m_pSeek == NULL)
	{
		return E_UNEXPECTED;
	}

	return m_pSeek->GetCurrentPosition(pTimeNow);
}


// Audio

//-----------------------------------------------------------------------------
// DShowPlayer::Mute
// Description: Mutes or unmutes the audio.
//-----------------------------------------------------------------------------

HRESULT	DShowPlayer::Mute(BOOL bMute)
{
	m_bMute = bMute;
	return UpdateVolume();
}

//-----------------------------------------------------------------------------
// DShowPlayer::SetVolume
// Description: Sets the volume. 
//-----------------------------------------------------------------------------

HRESULT	DShowPlayer::SetVolume(long lVolume)
{
	m_lVolume = lVolume;
	return UpdateVolume();
}


//-----------------------------------------------------------------------------
// DShowPlayer::UpdateVolume
// Description: Update the volume after a call to Mute() or SetVolume().
//-----------------------------------------------------------------------------

HRESULT DShowPlayer::UpdateVolume()
{
	HRESULT hr = S_OK;

	if (m_bAudioStream && m_pAudio)
	{
        // If the audio is muted, set the minimum volume. 
		if (m_bMute)
		{
			hr = m_pAudio->put_Volume(MIN_VOLUME);
		}
		else
		{
			// Restore previous volume setting
			hr = m_pAudio->put_Volume(m_lVolume);
		}
	}

	return hr;
}
 
// Graph building
 
 
HRESULT DShowPlayer::InitializeGraph()
{
	HRESULT hr = S_OK;

	TearDownGraph();
	m_abort = false;


	if (m_bauotechLockKey != BAUTECH_LOCK_KEY)
	{
		return -98765;
	}

	// Use its member function CoCreateInstance to
	// create the COM object and obtain the IGraphBuilder pointer.
	hr = m_pGraph.CoCreateInstance(CLSID_FilterGraph);

	// Query for graph interfaces.
	if (SUCCEEDED(hr))
	{
		hr = m_pGraph->QueryInterface(IID_IMediaControl, (void**)&m_pControl);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_pGraph->QueryInterface(IID_IMediaEventEx, (void**)&m_pEvent);
	}

	if (SUCCEEDED(hr))
	{
		hr = m_pGraph->QueryInterface(IID_IMediaSeeking, (void**)&m_pSeek);
	}

#if 0 
	if (SUCCEEDED(hr))
	{
		hr = m_pGraph->QueryInterface(IID_IBasicAudio, (void**)&m_pAudio);
	}
#endif 


	// Set up event notification.
	if (SUCCEEDED(hr))
	{
		hr = m_pEvent->SetNotifyWindow((OAHWND)m_hwndEvent, m_EventMsg, NULL);
	}

	return hr;
}

//-----------------------------------------------------------------------------
// DShowPlayer::TearDownGraph
// Description: Tear down the filter graph and release resources. 
//-----------------------------------------------------------------------------


void DShowPlayer::Close()
{
	m_abort = true;
	Sleep(1000);

	if (pSinkFilter != NULL)
		m_pGraph->RemoveFilter(pSinkFilter);

	if (pInfTeeFilter != NULL)
		m_pGraph->RemoveFilter(pInfTeeFilter);

	if (pAudioRenderer != NULL)
		m_pGraph->RemoveFilter(pAudioRenderer);

	if (pVideoDecoder != NULL)
		m_pGraph->RemoveFilter(pVideoDecoder);


	//if (pLeadToolsRTSPSource != NULL)
		//m_pGraph->RemoveFilter(pLeadToolsRTSPSource);

	if (m_pVideo != NULL)
		m_pVideo->FinalizeGraph(m_pGraph);
	
  
	/*
	// Enumerate the filters in the graph.
	CComPtr<IEnumFilters> pEnum = NULL;
	HRESULT hr = m_pGraph->EnumFilters(&pEnum);
	if (SUCCEEDED(hr))
	{
		IBaseFilter *pFilter = NULL;
		while (S_OK == pEnum->Next(1, &pFilter, NULL))
		{
			FILTER_INFO FilterInfo;
			hr = pFilter->QueryFilterInfo(&FilterInfo);
			// Remove the filter.
			m_pGraph->RemoveFilter(pFilter);
			// Reset the enumerator.
			pEnum->Reset();
			pFilter->Release();
		}
		//pEnum->Release();
	}
	*/
	
	TearDownGraph();

	CoUninitialize();

}

void DShowPlayer::TearDownGraph()
{
	// Stop sending event messages
	if (m_pEvent)
	{
		m_pEvent->SetNotifyWindow((OAHWND)NULL, NULL, NULL);
	}
	 

    SAFE_DELETE(m_pVideo);

	m_state = STATE_CLOSED;
	m_seekCaps = 0;

    m_bAudioStream = FALSE;
}
  


//-----------------------------------------------------------------------------
// DShowPlayer::RenderStreams
// Description: Render the streams from a source filter. 
//-----------------------------------------------------------------------------

HRESULT	DShowPlayer::RenderStreams(IBaseFilter *pSource)
{
	HRESULT hr = S_OK;

	BOOL bRenderedAnyPin = FALSE;

	IFilterGraph2 *pGraph2 = NULL;
	IEnumPins *pEnum = NULL;
	IBaseFilter *pAudioRenderer = NULL;
	IBaseFilter *pElecardVideoRenderer = NULL;

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);

    // Add the video renderer to the graph
    if (SUCCEEDED(hr))
    {
        
    }

#if 0 
	// Add the DSound Renderer to the graph.
	if (SUCCEEDED(hr))
	{
		hr = AddFilterByCLSID(m_pGraph, CLSID_DSoundRender, &pAudioRenderer, L"Audio Renderer");
	}
#endif 

	  

    // Enumerate the pins on the source filter.
	if (SUCCEEDED(hr))
	{
		hr = pSource->EnumPins(&pEnum);
	}
	 
	if (SUCCEEDED(hr))
	{
		// Loop through all the pins
		IPin *pPin = NULL;

		while (S_OK == pEnum->Next(1, &pPin, NULL))
		{			
			// Try to render this pin. 
			// It's OK if we fail some pins, if at least one pin renders.
			HRESULT hr2 = pGraph2->RenderEx(pPin, AM_RENDEREX_RENDERTOEXISTINGRENDERERS, NULL);

			pPin->Release();

			if (SUCCEEDED(hr2))
			{
				bRenderedAnyPin = TRUE;
			}
		}
	}


	// Remove un-used renderers.

    // Try to remove the VMR.
	if (SUCCEEDED(hr))
	{
        hr = m_pVideo->FinalizeGraph(m_pGraph);
	}

#if 0 
    // Try to remove the audio renderer.
	if (SUCCEEDED(hr))
	{
    	BOOL bRemoved = FALSE;
		hr = RemoveUnconnectedRenderer(m_pGraph, pAudioRenderer, &bRemoved);

        if (bRemoved)
        {
            m_bAudioStream = FALSE;
        }
        else
        {
            m_bAudioStream = TRUE;
        }
	}
#endif 

	SAFE_RELEASE(pEnum);
	//SAFE_RELEASE(pVMR);
	SAFE_RELEASE(pAudioRenderer);
	SAFE_RELEASE(pGraph2);

	// If we succeeded to this point, make sure we rendered at least one 
	// stream.
	if (SUCCEEDED(hr))
	{
		if (!bRenderedAnyPin)
		{
			hr = VFW_E_CANNOT_RENDER;
		}
	}

	return hr;
}


//-----------------------------------------------------------------------------
// DShowPlayer::RemoveUnconnectedRenderer
// Description: Remove a renderer filter from the graph if the filter is
//              not connected. 
//-----------------------------------------------------------------------------

HRESULT RemoveUnconnectedRenderer(IGraphBuilder *pGraph, IBaseFilter *pRenderer, BOOL *pbRemoved)
{
	IPin *pPin = NULL;

	BOOL bRemoved = FALSE;

	// Look for a connected input pin on the renderer.

	HRESULT hr = FindConnectedPin(pRenderer, PINDIR_INPUT, &pPin);
	SAFE_RELEASE(pPin);

	// If this function succeeds, the renderer is connected, so we don't remove it.
	// If it fails, it means the renderer is not connected to anything, so
	// we remove it.

	if (FAILED(hr))
	{
		hr = pGraph->RemoveFilter(pRenderer);
		bRemoved = TRUE;
	}

	if (SUCCEEDED(hr))
	{
		*pbRemoved = bRemoved;
	}

	return hr;
}
  
HRESULT DShowPlayer::UpdateVideoWindow(const LPRECT prc)
{
	HRESULT hr = S_OK;

	if (m_pVideo)
	{
		hr = m_pVideo->UpdateVideoWindow(m_hwndVideo, prc);
		m_pVideo->Repaint(m_hwndVideo, 0);
	}

	return hr;
}
void DShowPlayer::SetAspectRatio(ASPECT_RATIO ratio, bool applyNow)
{
	m_vmrAspectRatio = ratio;
	if (m_pVideo != NULL && applyNow == true)
		m_pVideo->SetAspectRatioMode(ratio);
}

void DShowPlayer::SelectPullDemux(SELECTED_PULLDEMUX selectedPullDemux)
{
	m_selectedPullDemux = selectedPullDemux;
}

void DShowPlayer::SelectRenderer(VIDEO_RENDER rend)
{
	m_selectedRenderer = rend;
}
void DShowPlayer::SelectDecoder(SELECTED_DECODER selectedDecoder)
{

	m_selectedDecoder = selectedDecoder;
}

HRESULT DShowPlayer::Repaint(HDC hdc)
{
	HRESULT hr = S_OK;

	if (m_pVideo)
	{
		hr = m_pVideo->Repaint(m_hwndVideo, hdc);
	}

	return hr;
}

HRESULT DShowPlayer::SaveFrame(LPCTSTR tcsFileName)
{
	HRESULT hr = S_OK;
	if (m_pVideo)
	{
		hr = m_pVideo->SaveFrame(tcsFileName);
	}

	return hr;

}
 
 
HRESULT DShowPlayer::CreateVideoRenderer()
{
	HRESULT hr = E_FAIL;

	enum { Try_EVR, Try_VMR9, Try_VMR7 };

	for (DWORD i = Try_EVR; i <= Try_VMR7; i++)
	{
		switch (i)
		{
		case Try_EVR:
			m_pVideo = new EVR();
			break;

		case Try_VMR9:
			m_pVideo = new VMR9();
			break;

		case Try_VMR7:
			m_pVideo = new VMR7();
			break;
		}

		if (m_pVideo == NULL)
		{
			hr = E_OUTOFMEMORY;
			break;
		}

		hr = m_pVideo->AddToGraph(m_pGraph, m_hwndVideo , m_vmrAspectRatio);
		if (SUCCEEDED(hr))
		{
			break;
		}

		SAFE_DELETE(m_pVideo);
	}

	if (FAILED(hr))
	{
		SAFE_DELETE(m_pVideo);
	}
	return hr;
}

HRESULT DShowPlayer::CreateVideoRenderer(VIDEO_RENDER render)
{
	HRESULT hr = E_FAIL;


	switch (render)
	{
		 
	case Try_VMR_DEF:
		m_pVideo = new VMR_DEF();
	break;

	case Try_EVR:
		m_pVideo = new EVR();
		break;

	case Try_VMR9:
		m_pVideo = new VMR9();
		break;

	case Try_VMR7:
		m_pVideo = new VMR7();
		break;
	}

	if (m_pVideo == NULL)
	{
		hr = E_OUTOFMEMORY;
		return hr;
	}

	hr = m_pVideo->AddToGraph(m_pGraph, m_hwndVideo , m_vmrAspectRatio);
	if (SUCCEEDED(hr))
	{
		return hr;
	}

	SAFE_DELETE(m_pVideo);


	return hr;
}

void DShowPlayer::SetWindowHandle(HWND hwnd)
{
	m_hwndVideo = hwnd;
	m_pVideo->SetVideoWindow(hwnd);

}

void DShowPlayer::SetVideoWindow(HWND hwnd)
{
	m_pVideo->SetVideoWindow(hwnd);
	m_pVideo->Repaint(hwnd, 0);
}
 

WCHAR* m_sFileName;
void DShowPlayer::SetFileName(const WCHAR* sFileName)
{

	m_sFileName = new wchar_t[500];
	wcscpy(m_sFileName, sFileName);

}


HRESULT DShowPlayer::ActivateFilter(IBaseFilter *filter)
{
	//b9d44b32-d34a-11e8-86ff-003048592e16
	static const GUID KEY_GUID = { 0xb9d44b32, 0xd34a, 0x11e8, { 0x86, 0xFF,  0x00 , 0x30 ,0x48 ,0x59 ,0x2e, 0x16 } };
	  
	HRESULT hr = S_OK;
	 
	IModuleConfig* IMC;
	hr = filter->QueryInterface(&IMC);
	if (SUCCEEDED(hr))
	{	 
		hr = IMC->SetValue(&KEY_GUID, NULL);
		IMC->Release();
	}
	return hr;
	  
}


HRESULT DShowPlayer::InitializeBoutechReceiverToWebRTCAndHLS(const WCHAR * serverAddress,
															bool useTransport,
															int cameraPort,
															const WCHAR * webRtcURL,
															const WCHAR * HLSOutputPath,
															const WCHAR * HLSName,
															int HLSRelativeUrl,
															int HLSLive,
															int HLSPlaylistDuration,
															int HLSSegmentDuration,
															const WCHAR * HLSStreamName)

{
	bool useBoutechReceiver = true;
	HRESULT hr = S_OK;
	PIN_INFO pininfo;


	 
	bool usHls = true;

	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

	if (useBoutechReceiver == false)
	{
		static const GUID CLSID_NWSource =
		{ 0x62341545, 0x9318, 0x4671,{ 0x9d, 0x62, 0x9c, 0xaa, 0xcd, 0xd5, 0xd2, 0xa } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_NWSource, &pNetworkSource, L"Elecard network Source");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard network source");
			return -5;
		}

		static const GUID IID_IModuleConfig =
		{ 0x486F726E, 0x4D43, 0x49b9,{ 0x8A, 0x0C,  0xC2, 0x2A, 0x2B, 0x05, 0x24, 0xE8 } };

		hr = pNetworkSource->QueryInterface(IID_IModuleConfig, (void**)&m_nwsource_config);
		if (FAILED(hr))
			return -6;


		ENWSource_Plus::announces_descr_ext *announce_descr = NULL;
		wstring ws(serverAddress);
		string serverURL(ws.begin(), ws.end());

		string port = std::to_string(cameraPort);


		get_announces(&announce_descr);
		// set SDP configuration
		string sdp = "v=0\no=- 1364663709 8 IN IP4 0.0.0.0\ns=Truck\nc=IN IP4 " + serverURL + " " + "/64\nb=CT:0\nt=0 0\nm=video " + port + " " + "udp 33\na = rtpmap:33 MP2T / 90000\na = control:trackID = 0\n";
		CComVariant val(sdp.c_str());
		hr = m_nwsource_config->SetValue(&ENS_announce_sdp_data, &val);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val1(port.c_str());
		hr = m_nwsource_config->SetValue(&ENS_port, &val1);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val2(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_mcast_address, &val2);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val3(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_interface, &val3);
		if (FAILED(hr))
		{
			return -7;
		}

		hr = m_nwsource_config->SetValue(&ENS_server, &val3);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val4(INT_MAX);
		hr = m_nwsource_config->SetValue(&ENS_timeout, &val4);
		if (FAILED(hr))
		{
			return -7;
		}

		m_nwsource_config->CommitChanges(NULL);
	}
	else
	{
		static const GUID CLSID_CBoutechLiveSourceReceiver =
		{ 0xc21d1b3d, 0x648d, 0x4099, { 0xac, 0xeb, 0x5, 0x9, 0x82, 0x42, 0x7e, 0x8b } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_CBoutechLiveSourceReceiver, &pNetworkSource, L"Boutech Live Source Receiver");
		if (FAILED(hr))
		{
			LogError("Failed to add Boutech Live Source Receiver");
			return -5;
		}


		// {CF24BB08-37C8-44BD-9A12-683B118F290A}
		static const GUID IID_IBoutechLiveSource =
		{ 0xcf24bb08, 0x37c8, 0x44bd, { 0x9a, 0x12, 0x68, 0x3b, 0x11, 0x8f, 0x29, 0xa } };

		CComPtr<IBoutechLiveSource> pBoutechReceiverInterface;
		hr = pNetworkSource->QueryInterface(IID_IBoutechLiveSource, (void **)&pBoutechReceiverInterface);
		pBoutechReceiverInterface->SetIpAddress((WCHAR *)serverAddress);
		pBoutechReceiverInterface->SetPort(cameraPort);
		pBoutechReceiverInterface->SetPinType(useTransport == true ? 0 : 1);
	}


	// {668EE184-FD2D-4c72-8E79-439A35B438DE} 
	static const GUID CLSID_EMPGPDMX =
	{ 0x668ee184, 0xfd2d, 0x4c72,{ 0x8e, 0x79, 0x43, 0x9a, 0x35, 0xb4, 0x38, 0xde } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGPDMX, &pElecardMpegPushDemux, L"Elecard Push Demux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Push Demux");
		return -10;
	}
	SetDemuxerLatency(0);
	CComPtr<IPin> ppDemuxIn;
	GetPin(pElecardMpegPushDemux, PIN_DIRECTION::PINDIR_INPUT, &ppDemuxIn);
	hr = ppDemuxIn->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}
	CComPtr<IPin> pSourceOutPin;
	GetPin(pNetworkSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}
	 

	// add boutech inifite PIE Filter 
	static const GUID CLSID_BoutechInfiniteTee =
	{ 0xc71c1d37, 0xc867, 0x4318,{ 0xaf, 0x2, 0x67, 0xe4, 0xe5, 0xce, 0xc1, 0x10} };

	hr = AddFilterByCLSID(m_pGraph, CLSID_BoutechInfiniteTee, &pBoutechInfinitePieFilter, L"Boutech Infinite Tee Filter");
	if (FAILED(hr))
	{
		LogError("Failed to add Boutech Infinite Tee Filter");
		return -10;
	}
	 
	CComPtr<IPin> pInfiniteInPin;
	GetPin(pBoutechInfinitePieFilter, PIN_DIRECTION::PINDIR_INPUT, &pInfiniteInPin);
	hr = pInfiniteInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}



	hr = m_pGraph->Connect(pSourceOutPin, pInfiniteInPin);
	if (FAILED(hr))
	{
		return -13;
	}

	CComPtr<IPin> pInfiniteOut1Pin;
	GetPin(pBoutechInfinitePieFilter, PIN_DIRECTION::PINDIR_OUTPUT, &pInfiniteOut1Pin, 0);
	hr = pInfiniteOut1Pin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -1233;
	} 

	hr = m_pGraph->Connect(pInfiniteOut1Pin, ppDemuxIn);
	if (FAILED(hr))
	{
		return -13;
	}


	CComPtr<IPin> pDemuxOut;
	hr = S_FALSE;
	int timeOut = 20;
	while (pDemuxOut == NULL && hr == S_FALSE)
	{
		if (m_abort == true)
		{
			return -999919;
		}
		if (useTransport == true)
			pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "H264", PIN_DIRECTION::PINDIR_OUTPUT);
		else
			pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "AVC", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pDemuxOut == NULL)
		{
			timeOut--;
			Sleep(1000);
			continue;
		}
		hr = pDemuxOut->QueryPinInfo(&pininfo);
		if (hr == S_OK)
			break;
	}
	 

	/// Add HLS Sink
	CComPtr<IPin> pHlsSinkInPin;
	if (usHls == true)
	{

		// {3CC57738-6371-4362-9298-7F124C4F4896}
		static const GUID CLSID_EHlsSink =
		{ 0x3cc57738, 0x6371, 0x4362, { 0x92, 0x98, 0x7f, 0x12, 0x4c, 0x4f, 0x48, 0x96 } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EHlsSink, &pElecardHLSSink, L"HLS Sink Filter");
		if (FAILED(hr))
		{
			LogError("Failed to add AVObject Web RTC Filter");
			return -10;
		}

		
		GetPin(pElecardHLSSink, PIN_DIRECTION::PINDIR_INPUT, &pHlsSinkInPin, 0);
		hr = pHlsSinkInPin->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -1233;
		}


		CComPtr<IModuleConfig>	pElecardModuleConfig;
		hr = pElecardHLSSink->QueryInterface(IID_IModuleConfig, (void**)&pElecardModuleConfig);
		if (FAILED(hr))
			return -6;



		wstring ws(HLSName);
		string strSink_Name(ws.begin(), ws.end());
		CComVariant valHlsSinkName(strSink_Name.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_Name, &valHlsSinkName);
		if (FAILED(hr))
		{
			return -7;
		}



		CComPtr<IModuleConfig>	pElecardModuleConfigInPin;
		hr = pHlsSinkInPin->QueryInterface(IID_IModuleConfig, (void**)&pElecardModuleConfigInPin);
		if (FAILED(hr))
			return -6;


		wstring ws11(HLSStreamName);
		string strHlsSink_HLSStreamName(ws11.begin(), ws11.end());
		CComVariant valSink_HLSStreamName(strHlsSink_HLSStreamName.c_str());
		hr = pElecardModuleConfigInPin->SetValue(&EHlsSink_StreamName, &valSink_HLSStreamName);
		if (FAILED(hr))
		{
			return -765;
		}



		wstring ws1(HLSOutputPath);
		string strHlsSink_OutputPath(ws1.begin(), ws1.end());
		CComVariant valSink_OutputPath(strHlsSink_OutputPath.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_OutputPath, &valSink_OutputPath);
		if (FAILED(hr))
		{
			return -7;
		}

		string HLSMasterPlaylistFileName = "$Name$.m3u8";
		CComVariant valHlsSink_MasterPlaylistFilename(HLSMasterPlaylistFileName.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_MasterPlaylistFilename, &valHlsSink_MasterPlaylistFilename);
		if (FAILED(hr))
		{
			return -7;
		}
		string HLSSegmentFileName = "$Name$/$StreamName$/$Number$.ts";
		CComVariant var100(HLSSegmentFileName.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_SegmentFilename, &var100);
		if (FAILED(hr))
		{
			return -7;
		}

		string HLSMediaPlaylistFileName = "$Name$/$StreamName$.m3u8";
		CComVariant var101(HLSMediaPlaylistFileName.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_MediaPlaylistFilename, &var101);
		if (FAILED(hr))
		{
			return -7;
		}



		string strHLSRelativeUrl = std::to_string(HLSRelativeUrl);
		CComVariant valHLSRelativeUrl(strHLSRelativeUrl.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_UseRelativeUrls, &valHLSRelativeUrl);
		if (FAILED(hr))
		{
			return -707;
		}


		string Sink_Live = std::to_string(HLSLive);
		CComVariant valSinkLive(Sink_Live.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_Live, &valSinkLive);
		if (FAILED(hr))
		{
			return -707;
		}



		string Sink_SegmentDuration = std::to_string(HLSSegmentDuration);
		CComVariant valSegmentDuration(Sink_SegmentDuration.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_SegmentDuration, &valSegmentDuration);
		if (FAILED(hr))
		{
			return -745;
		}


		string strHLSPlaylistDuration = std::to_string(HLSPlaylistDuration);
		CComVariant valHLSPlaylistDuration(strHLSPlaylistDuration.c_str());
		hr = pElecardModuleConfig->SetValue(&EHlsSink_PlaylistDuration, &valHLSPlaylistDuration);
		if (FAILED(hr))
		{
			return -745;
		}

		pElecardModuleConfig->CommitChanges(0);
		pElecardModuleConfigInPin->CommitChanges(0);
	}

	 

	 
	 
	// Add web rtc filter 
	static const GUID CLSID_CoWRtpServer =
	{ 0xBC3EAA01, 0x2C36 , 0x4FED,{ 0x88, 0xF5, 0x4F ,0x2B ,0xD0, 0xA8 ,0xE8, 0x48 } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_CoWRtpServer, &pAvObjectFilter, L"AVObject Web RTC Filter");
	if (FAILED(hr))
	{
		LogError("Failed to add AVObject Web RTC Filter");
		return -10;
	}

	static const GUID IID_IAVOWRtpServer =
	{ 0x6AC926BE, 0x81D7, 0x49C4,{ 0x8F, 0x73, 0x46, 0x0D, 0x76, 0x79, 0x21, 0xB1 } };


	CComPtr<IAVOWRtpServer>	m_iavObjectWebRTC;
	hr = pAvObjectFilter->QueryInterface(IID_IAVOWRtpServer, (void**)&m_iavObjectWebRTC);
	if (FAILED(hr))
		return -7;

	CComBSTR cbs;
	m_iavObjectWebRTC->GetUrl(&cbs);

	BSTR bstr = SysAllocString(webRtcURL);
	m_iavObjectWebRTC->SetUrl(bstr);
	SysFreeString(bstr);

	CComPtr<IPin> ppWebRtcInPin;
	GetPin(pAvObjectFilter, PIN_DIRECTION::PINDIR_INPUT, &ppWebRtcInPin);
	hr = ppWebRtcInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -1233;
	}

	hr = m_pGraph->Connect(pDemuxOut, ppWebRtcInPin);
	if (FAILED(hr))
	{
		return -13;
	}
	 
	if (usHls == true)
	{
		CComPtr<IPin> pInfiniteOut2Pin;
		GetPin(pBoutechInfinitePieFilter, PIN_DIRECTION::PINDIR_OUTPUT, &pInfiniteOut2Pin, 1);
		hr = pInfiniteOut2Pin->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -1233;
		}

		hr = m_pGraph->Connect(pInfiniteOut2Pin, pHlsSinkInPin);
		if (FAILED(hr))
		{
			return -13;
		}
	}


	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}
	return hr;
  
}

HRESULT DShowPlayer::InitializeBoutechReceiverToWebRTC(const WCHAR * serverAddress,
	bool useTransport,
	int cameraPort,
	const WCHAR * webRtcURL)
{

	bool useBoutechReceiver = true;
	HRESULT hr = S_OK;
	PIN_INFO pininfo;

	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

	if (useBoutechReceiver == false)
	{
		static const GUID CLSID_NWSource =
		{ 0x62341545, 0x9318, 0x4671,{ 0x9d, 0x62, 0x9c, 0xaa, 0xcd, 0xd5, 0xd2, 0xa } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_NWSource, &pNetworkSource, L"Elecard network Source");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard network source");
			return -5;
		}

		static const GUID IID_IModuleConfig =
		{ 0x486F726E, 0x4D43, 0x49b9,{ 0x8A, 0x0C,  0xC2, 0x2A, 0x2B, 0x05, 0x24, 0xE8 } };

		hr = pNetworkSource->QueryInterface(IID_IModuleConfig, (void**)&m_nwsource_config);
		if (FAILED(hr))
			return -6;


		ENWSource_Plus::announces_descr_ext *announce_descr = NULL;
		wstring ws(serverAddress);
		string serverURL(ws.begin(), ws.end());

		string port = std::to_string(cameraPort);


		get_announces(&announce_descr);
		// set SDP configuration
		string sdp = "v=0\no=- 1364663709 8 IN IP4 0.0.0.0\ns=Truck\nc=IN IP4 " + serverURL + " " + "/64\nb=CT:0\nt=0 0\nm=video " + port + " " + "udp 33\na = rtpmap:33 MP2T / 90000\na = control:trackID = 0\n";
		CComVariant val(sdp.c_str());
		hr = m_nwsource_config->SetValue(&ENS_announce_sdp_data, &val);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val1(port.c_str());
		hr = m_nwsource_config->SetValue(&ENS_port, &val1);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val2(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_mcast_address, &val2);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val3(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_interface, &val3);
		if (FAILED(hr))
		{
			return -7;
		}

		hr = m_nwsource_config->SetValue(&ENS_server, &val3);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val4(INT_MAX);
		hr = m_nwsource_config->SetValue(&ENS_timeout, &val4);
		if (FAILED(hr))
		{
			return -7;
		}

		m_nwsource_config->CommitChanges(NULL);
	}
	else
	{
		static const GUID CLSID_CBoutechLiveSourceReceiver =
		{ 0xc21d1b3d, 0x648d, 0x4099, { 0xac, 0xeb, 0x5, 0x9, 0x82, 0x42, 0x7e, 0x8b } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_CBoutechLiveSourceReceiver, &pNetworkSource, L"Boutech Live Source Receiver");
		if (FAILED(hr))
		{
			LogError("Failed to add Boutech Live Source Receiver");
			return -5;
		}


		// {CF24BB08-37C8-44BD-9A12-683B118F290A}
		static const GUID IID_IBoutechLiveSource =
		{ 0xcf24bb08, 0x37c8, 0x44bd, { 0x9a, 0x12, 0x68, 0x3b, 0x11, 0x8f, 0x29, 0xa } };

		CComPtr<IBoutechLiveSource> pBoutechReceiverInterface;
		hr = pNetworkSource->QueryInterface(IID_IBoutechLiveSource, (void **)&pBoutechReceiverInterface);
		pBoutechReceiverInterface->SetIpAddress((WCHAR *)serverAddress);
		pBoutechReceiverInterface->SetPort(cameraPort);
		pBoutechReceiverInterface->SetPinType(useTransport == true ? 0 : 1);
	}


	// {668EE184-FD2D-4c72-8E79-439A35B438DE} 
	static const GUID CLSID_EMPGPDMX =
	{ 0x668ee184, 0xfd2d, 0x4c72,{ 0x8e, 0x79, 0x43, 0x9a, 0x35, 0xb4, 0x38, 0xde } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGPDMX, &pElecardMpegPushDemux, L"Elecard Push Demux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Push Demux");
		return -10;
	}
	SetDemuxerLatency(0);
	CComPtr<IPin> ppDemuxIn;
	GetPin(pElecardMpegPushDemux, PIN_DIRECTION::PINDIR_INPUT, &ppDemuxIn);
	hr = ppDemuxIn->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}
	CComPtr<IPin> pSourceOutPin;
	GetPin(pNetworkSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}


	hr = m_pGraph->Connect(pSourceOutPin, ppDemuxIn);
	if (FAILED(hr))
	{
		return -13;
	}
	 
	CComPtr<IPin> pDemuxOut;
	hr = S_FALSE;
	int timeOut = 20;
	while (pDemuxOut == NULL && hr == S_FALSE)
	{
		if (m_abort == true)
		{
			return -999919;
		}
		if (useTransport == true)
			pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "H264", PIN_DIRECTION::PINDIR_OUTPUT);
		else 
			pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "AVC", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pDemuxOut == NULL)
		{
			timeOut--;
			Sleep(1000);
			continue;
		}
		hr = pDemuxOut->QueryPinInfo(&pininfo);
		if (hr == S_OK)
			break;
	}
	 
	static const GUID CLSID_CoWRtpServer =
	{ 0xBC3EAA01, 0x2C36 , 0x4FED,{ 0x88, 0xF5, 0x4F ,0x2B ,0xD0, 0xA8 ,0xE8, 0x48 } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_CoWRtpServer, &pAvObjectFilter, L"AVObject Web RTC Filter");
	if (FAILED(hr))
	{
		LogError("Failed to add AVObject Web RTC Filter");
		return -10;
	}

	static const GUID IID_IAVOWRtpServer =
	{ 0x6AC926BE, 0x81D7, 0x49C4,{ 0x8F, 0x73, 0x46, 0x0D, 0x76, 0x79, 0x21, 0xB1 } };


	CComPtr<IAVOWRtpServer>	m_iavObjectWebRTC;
	hr = pAvObjectFilter->QueryInterface(IID_IAVOWRtpServer, (void**)&m_iavObjectWebRTC);
	if (FAILED(hr))
		return -7;

	CComBSTR cbs;
	m_iavObjectWebRTC->GetUrl(&cbs);

	BSTR bstr = SysAllocString(webRtcURL);
	m_iavObjectWebRTC->SetUrl(bstr);
	SysFreeString(bstr);

	CComPtr<IPin> ppWebRtcInPin;
	GetPin(pAvObjectFilter, PIN_DIRECTION::PINDIR_INPUT, &ppWebRtcInPin);
	hr = ppWebRtcInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -1233;
	}


	hr = m_pGraph->Connect(pDemuxOut, ppWebRtcInPin);
	if (FAILED(hr))
	{
		return -13;
	}

	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}
	return hr;
}

HRESULT DShowPlayer::InitilizeNetworkVideoEsPlayer(HWND hwnd,
												   bool useBoutechReceiver,
												   const WCHAR *ServerAddress,
												   int ServerPort)
{

	HRESULT hr = S_OK;
	m_hwndVideo = hwnd;
	PIN_INFO pininfo;

	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

	if (useBoutechReceiver == false)
	{
		static const GUID CLSID_NWSource =
		{ 0x62341545, 0x9318, 0x4671,{ 0x9d, 0x62, 0x9c, 0xaa, 0xcd, 0xd5, 0xd2, 0xa } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_NWSource, &pNetworkSource, L"Elecard network Source");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard network source");
			return -5;
		}

		static const GUID IID_IModuleConfig =
		{ 0x486F726E, 0x4D43, 0x49b9,{ 0x8A, 0x0C,  0xC2, 0x2A, 0x2B, 0x05, 0x24, 0xE8 } };

		hr = pNetworkSource->QueryInterface(IID_IModuleConfig, (void**)&m_nwsource_config);
		if (FAILED(hr))
			return -6;


		ENWSource_Plus::announces_descr_ext *announce_descr = NULL;
		wstring ws(ServerAddress);
		string serverURL(ws.begin(), ws.end());

		string port = std::to_string(ServerPort);


		get_announces(&announce_descr);
		// set SDP configuration
		string sdp = "v=0\no=- 1364663709 8 IN IP4 0.0.0.0\ns=Truck\nc=IN IP4 " + serverURL + " " + "/64\nb=CT:0\nt=0 0\nm=video " + port + " " + "udp 33\na = rtpmap:33 MP2T / 90000\na = control:trackID = 0\n";
		CComVariant val(sdp.c_str());
		hr = m_nwsource_config->SetValue(&ENS_announce_sdp_data, &val);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val1(port.c_str());
		hr = m_nwsource_config->SetValue(&ENS_port, &val1);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val2(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_mcast_address, &val2);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val3(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_interface, &val3);
		if (FAILED(hr))
		{
			return -7;
		}

		hr = m_nwsource_config->SetValue(&ENS_server, &val3);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val4(INT_MAX);
		hr = m_nwsource_config->SetValue(&ENS_timeout, &val4);
		if (FAILED(hr))
		{
			return -7;
		}

		m_nwsource_config->CommitChanges(NULL);
	}
	else
	{
		static const GUID CLSID_CBoutechLiveSourceReceiver =
		{ 0xc21d1b3d, 0x648d, 0x4099, { 0xac, 0xeb, 0x5, 0x9, 0x82, 0x42, 0x7e, 0x8b } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_CBoutechLiveSourceReceiver, &pNetworkSource, L"Boutech Live Source Receiver");
		if (FAILED(hr))
		{
			LogError("Failed to add Boutech Live Source Receiver");
			return -5;
		}


		// {CF24BB08-37C8-44BD-9A12-683B118F290A}
		static const GUID IID_IBoutechLiveSource =
		{ 0xcf24bb08, 0x37c8, 0x44bd, { 0x9a, 0x12, 0x68, 0x3b, 0x11, 0x8f, 0x29, 0xa } };

		CComPtr<IBoutechLiveSource> pBoutechReceiverInterface;
		hr = pNetworkSource->QueryInterface(IID_IBoutechLiveSource, (void **)&pBoutechReceiverInterface);
		pBoutechReceiverInterface->SetIpAddress((WCHAR *)ServerAddress);
		pBoutechReceiverInterface->SetPort(ServerPort);
		pBoutechReceiverInterface->SetPinType(1);
	}
	  

	// {668EE184-FD2D-4c72-8E79-439A35B438DE} 
	static const GUID CLSID_EMPGPDMX =
	{ 0x668ee184, 0xfd2d, 0x4c72,{ 0x8e, 0x79, 0x43, 0x9a, 0x35, 0xb4, 0x38, 0xde } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGPDMX, &pElecardMpegPushDemux, L"Elecard Push Demux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Push Demux");
		return -10;
	}
	SetDemuxerLatency(0);


	static const GUID CLSID_EAVCDEC =
	{ 0x5c122c6D, 0x8fcc, 0x46f9,{ 0xaa, 0xb7, 0xdc, 0xfb, 0x8, 0x41, 0xe0, 0x4d } };

	hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Elecard AVC Video Decoder");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard AVC Video Decoder");
		return -8;
	}
	ActivateFilter(pVideoDecoder);
	ClearDecoderBufferForFastLatency();

	CComPtr<IPin> pSourceOutPin;
	GetPin(pNetworkSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}


	CComPtr<IPin> ppDemuxIn;
	GetPin(pElecardMpegPushDemux, PIN_DIRECTION::PINDIR_INPUT, &ppDemuxIn);
	hr = ppDemuxIn->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}



	hr = m_pGraph->Connect(pSourceOutPin, ppDemuxIn);
	if (FAILED(hr))
	{
		return -13;
	}

	 

	CComPtr<IPin> pDemuxOut;
	hr = S_FALSE;
	int timeOut = 20;
	while (pDemuxOut == NULL && hr == S_FALSE)
	{
		if (m_abort == true)
		{
			return -999919;
		}
		pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "AVC", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pDemuxOut == NULL)
		{
			timeOut--;
			Sleep(1000);
			continue;
		}
		hr = pDemuxOut->QueryPinInfo(&pininfo);
		if (hr == S_OK)
			break;
	}
	 


	CComPtr<IPin> pDecoderInPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_INPUT, &pDecoderInPin);
	hr = pDecoderInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -15;
	}


	CComPtr<IPin> pDecoderOutPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_OUTPUT, &pDecoderOutPin);
	hr = pDecoderOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}


	hr = m_pGraph->Connect(pDemuxOut, pDecoderInPin);
	if (FAILED(hr))
	{
		return -19;
	}

	hr = CreateVideoRenderer(m_selectedRenderer);
	if (FAILED(hr))
	{
		LogError("Failed to create VMR9");
		return hr;
	}

	hr = pGraph2->RenderEx(pDecoderOutPin, AM_RENDEREX_RENDERTOEXISTINGRENDERERS, NULL);

	CComPtr<IMediaFilter> media_filter;
	hr = m_pGraph->QueryInterface(&media_filter);
	if (FAILED(hr))
		return -3484;

	media_filter->SetSyncSource(NULL);
	 
	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}
	return hr;
}



HRESULT DShowPlayer::InitializeNetworkMulticastPlayer(HWND hwnd,
													 bool useBoutechReceover,
													 const WCHAR *MulticastAddress,
													 int MulticastPort,
												     bool useInterface,
													 float demuxLatency,
													 int klvPID)
												     
{
	HRESULT hr = S_OK;
	m_hwndVideo = hwnd;
	PIN_INFO pininfo;

	 

	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}


	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

	if (useBoutechReceover == false)
	{

		static const GUID CLSID_NWSource =
		{ 0x62341545, 0x9318, 0x4671,{ 0x9d, 0x62, 0x9c, 0xaa, 0xcd, 0xd5, 0xd2, 0xa } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_NWSource, &pNetworkSource, L"Elecard network Source");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard network source");
			return -5;
		}

		static const GUID IID_IModuleConfig =
		{ 0x486F726E, 0x4D43, 0x49b9,{ 0x8A, 0x0C,  0xC2, 0x2A, 0x2B, 0x05, 0x24, 0xE8 } };

		hr = pNetworkSource->QueryInterface(IID_IModuleConfig, (void**)&m_nwsource_config);
		if (FAILED(hr))
			return -6;


		ENWSource_Plus::announces_descr_ext *announce_descr = NULL;
		wstring ws(MulticastAddress);
		string serverURL(ws.begin(), ws.end());

		string port = std::to_string(MulticastPort);


		get_announces(&announce_descr);
		// set SDP configuration
		string sdp = "v=0\no=- 1364663709 8 IN IP4 0.0.0.0\ns=Truck\nc=IN IP4 " + serverURL + " " + "/64\nb=CT:0\nt=0 0\nm=video " + port + " " + "udp 33\na = rtpmap:33 MP2T / 90000\na = control:trackID = 0\n";
		CComVariant val(sdp.c_str());
		hr = m_nwsource_config->SetValue(&ENS_announce_sdp_data, &val);
		if (FAILED(hr))
		{
			return -7;
		}


		CComVariant val1(port.c_str());
		hr = m_nwsource_config->SetValue(&ENS_port, &val1);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val2(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_mcast_address, &val2);
		if (FAILED(hr))
		{
			return -7;
		}
		
		CComVariant val3(serverURL.c_str());
		if (useInterface != NULL)
		{			
			hr = m_nwsource_config->SetValue(&ENS_interface, &val3);
			if (FAILED(hr))
			{
				return -7;
			}
		}

		hr = m_nwsource_config->SetValue(&ENS_server, &val3);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val4(INT_MAX);
		hr = m_nwsource_config->SetValue(&ENS_timeout, &val4);
		if (FAILED(hr))
		{
			return -7;
		}



		m_nwsource_config->CommitChanges(NULL);

	}
	else	
	{
			
		static const GUID CLSID_CBoutechLiveSourceReceiver =
		{ 0xc21d1b3d, 0x648d, 0x4099, { 0xac, 0xeb, 0x5, 0x9, 0x82, 0x42, 0x7e, 0x8b } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_CBoutechLiveSourceReceiver, &pNetworkSource, L"Boutech Live Source Receiver");
		if (FAILED(hr))
		{
			LogError("Failed to add Boutech Live Source Receiver");
			return -5;
		}


		// {CF24BB08-37C8-44BD-9A12-683B118F290A}
		static const GUID IID_IBoutechLiveSource =
		{ 0xcf24bb08, 0x37c8, 0x44bd, { 0x9a, 0x12, 0x68, 0x3b, 0x11, 0x8f, 0x29, 0xa } };

		CComPtr<IBoutechLiveSource> pBoutechReceiverInterface;
		hr = pNetworkSource->QueryInterface(IID_IBoutechLiveSource, (void **)&pBoutechReceiverInterface);
		pBoutechReceiverInterface->SetIpAddress((WCHAR *)MulticastAddress);
		pBoutechReceiverInterface->SetPort(MulticastPort);
		pBoutechReceiverInterface->SetPinType(0);

	}

	// {668EE184-FD2D-4c72-8E79-439A35B438DE} 
	static const GUID CLSID_EMPGPDMX =
	{ 0x668ee184, 0xfd2d, 0x4c72,{ 0x8e, 0x79, 0x43, 0x9a, 0x35, 0xb4, 0x38, 0xde } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGPDMX, &pElecardMpegPushDemux, L"Elecard Push Demux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Push Demux");
		return -10;
	}
	SetDemuxerLatency(demuxLatency);

	 
	if (m_selectedDecoder == SELECTED_DECODER::LEADTOOLS && SUCCEEDED(hr))
	{
		static const GUID CLSID_LEADTOOLS_H264_DECODER =
		{ 0xE2B7DF25,0x38C5,0x11D5,{ 0x91,0xF6, 0x00, 0x10, 0x4B , 0xDB ,0x8F , 0xF9 } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_LEADTOOLS_H264_DECODER, &pVideoDecoder, L"LeadTools AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add LeadTools AVC Video Decoder");
			return hr;
		}
	}
	else
		if (m_selectedDecoder == SELECTED_DECODER::ELECARD)
		{
			// {5C122C6D-8FCC-46f9-AAB7-DCFB0841E04D}
			static const GUID CLSID_EAVCDEC =
			{ 0x5c122c6D, 0x8fcc, 0x46f9,{ 0xaa, 0xb7, 0xdc, 0xfb, 0x8, 0x41, 0xe0, 0x4d } };

			hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Elecard AVC Video Decoder");
			if (FAILED(hr))
			{
				LogError("Failed to add Elecard AVC Video Decoder");
				return hr;
			}
			ActivateFilter(pVideoDecoder);
			ClearDecoderBufferForFastLatency();

		}
	if (m_selectedDecoder == SELECTED_DECODER::IMPAL)
	{

		static const GUID CLSID_EAVCDEC =
		{ 0x2379e92b , 0x3ffa , 0x4ce5 ,{ 0x85 , 0x91 , 0xf6 , 0xa4 , 0x8c , 0x57 , 0xef, 0x6a } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Impleo AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Impleo AVC Video Decoder");
			return hr;
		}
	}
	else
	{
		if (m_selectedDecoder == SELECTED_DECODER::AVOBJECTS)
		{

			static const GUID CLSID_EAVCDEC =
			{ 0x4BD50AA0 , 0x6CE6 , 0x4CD2 ,{ 0x96 , 0xB6 , 0xFB, 0x5F, 0xE7, 0xDE, 0x0C, 0xF2} };
		 

			hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"AVObjects AVC Video Decoder");
			if (FAILED(hr))
			{
				LogError("Failed to add AVObjects AVC Video Decoder");
				return hr;
			}
		}
	}
	 

	CComPtr<IPin> pSourceOutPin;
	GetPin(pNetworkSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}


	CComPtr<IPin> ppDemuxIn;
	GetPin(pElecardMpegPushDemux, PIN_DIRECTION::PINDIR_INPUT, &ppDemuxIn);
	hr = ppDemuxIn->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}
	 


	hr = m_pGraph->Connect(pSourceOutPin, ppDemuxIn);
	if (FAILED(hr))
	{
		return -13;
	}

	CComPtr<IPin> pDemuxOut;
	hr = S_FALSE;
	int timeOut = 20;
	while (pDemuxOut == NULL && hr == S_FALSE)
	{
		if (m_abort == true)
		{
			return -999919;
		}
		pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "H264", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pDemuxOut == NULL)
		{
			timeOut--;
			Sleep(400);
			continue;
		}
		hr = pDemuxOut->QueryPinInfo(&pininfo);
		if (hr == S_OK)
			break;
	}

	if (klvPID > 0)
	{

		AM_MEDIA_TYPE pMediaType;

		MPEG2VIDEOINFO    video_info;
		interface IMPEG2StreamIdMap    *StreamIdMapVideo;

		memset(&pMediaType, 0x0, sizeof(pMediaType));
		memset(&video_info, 0x0, sizeof(MPEG2VIDEOINFO));

		//Type settings for the output stream
		pMediaType.majortype = MEDIATYPE_Video;
		pMediaType.subtype = MEDIASUBTYPE_MPEG2_VIDEO;
		pMediaType.formattype = FORMAT_MPEG2Video;

		//not sure exactly what I need these for
		pMediaType.bFixedSizeSamples = FALSE;
		pMediaType.bTemporalCompression = TRUE;
		pMediaType.lSampleSize = 0;
		pMediaType.cbFormat = sizeof(MPEG2VIDEOINFO);

		pMediaType.pbFormat = (BYTE *)&video_info;

		CComPtr<IMpeg2Demultiplexer> pDemuxInterface;
		hr = pElecardMpegPushDemux->QueryInterface(IID_IMpeg2Demultiplexer, (void **)&pDemuxInterface);
		IPin    *pDemuxKlvOutPin;
		hr = pDemuxInterface->CreateOutputPin(&pMediaType, L"KLV", &pDemuxKlvOutPin);
		if (FAILED(hr))
		{
			return hr;
		}
		hr = pDemuxKlvOutPin->QueryInterface(IID_IMPEG2StreamIdMap,
			(void **)&StreamIdMapVideo);
		if (FAILED(hr))
		{
			return hr;
		}
		hr = StreamIdMapVideo->MapStreamId(klvPID, MPEG2_PROGRAM_PES_STREAM, 0, 0);
		if (FAILED(hr))
		{
			return hr;
		}
		PIN_INFO pininfo;
		CComPtr<IPin> pKlvOutPin;
		pKlvOutPin = GetPinContainsName(pElecardMpegPushDemux, "KLV", PIN_DIRECTION::PINDIR_OUTPUT);
		hr = AddDumpFilter(true);
		if (FAILED(hr))
		{
			return hr;
		}
		hr = m_pGraph->Connect(pKlvOutPin, pDumpInPin);
		if (FAILED(hr))
		{
			return hr;
		}
		CComPtr<IFileSinkFilter>  iSink;
		hr = pDumpFilter->QueryInterface(IID_IFileSinkFilter, (void **)&iSink);
		if (FAILED(hr))
		{
			return hr;
		}

		WCHAR dumpFileName[100] = L"c:\\klv.bin";
		// Load the source file.
		hr = iSink->SetFileName(dumpFileName, NULL);
		if (FAILED(hr))
		{
			return hr;
		}
	}
	  
	 
	CComPtr<IPin> pDecoderInPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_INPUT, &pDecoderInPin);
	hr = pDecoderInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -15;
	}


	CComPtr<IPin> pDecoderOutPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_OUTPUT, &pDecoderOutPin);
	hr = pDecoderOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}


	hr = m_pGraph->Connect(pDemuxOut, pDecoderInPin);
	if (FAILED(hr))
	{
		return -19;
	}


	hr = CreateVideoRenderer(m_selectedRenderer);
	if (FAILED(hr))
	{
		LogError("Failed to create VMR9");
		return hr;
	}

	hr = pGraph2->RenderEx(pDecoderOutPin, AM_RENDEREX_RENDERTOEXISTINGRENDERERS, NULL);
	 

	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}

	return hr;

}
 
HRESULT DShowPlayer::InitializePlayer(HWND hwnd, int klvPID)
{
	HRESULT hr = S_OK;
	m_hwndVideo = hwnd;


	IBaseFilter *pSource = NULL;

	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}


	// Add the source filter to the graph.
	CComPtr<IPin> pSourceOutPin;
	if (SUCCEEDED(hr))
	{
		hr = m_pGraph->AddSourceFilter(m_sFileName, NULL, &pSource);

		PIN_INFO pininfo;
		
		GetPin(pSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
		if (pSourceOutPin == NULL)
		{
			return -811;
		}
	}

	if (m_selectedPullDemux == SELECTED_PULLDEMUX::ELECARD_PULL_DEMUX)
	{ 
		// {136DCBF5-3874-4b70-AE3E-15997D6334F7} 
		static const GUID CLSID_EMPGDMX =
		{ 0x136dcbf5, 0x3874, 0x4b70, { 0xae, 0x3e, 0x15, 0x99, 0x7d, 0x63, 0x34, 0xf7 } };
		 
		hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGDMX, &pElecardPullDemux, L"Elecard Pull Demux");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard Pull Demux");
			return hr;
		}

		PIN_INFO pininfo;
		CComPtr<IPin> pDemuxInPin;
		GetPin(pElecardPullDemux, PIN_DIRECTION::PINDIR_INPUT, &pDemuxInPin);
		if (pSourceOutPin == NULL)
		{
			return -811;
		}

		hr = m_pGraph->Connect(pSourceOutPin, pDemuxInPin);
		if (pSourceOutPin == NULL)
		{
			return -841;
		}
	} else 

	if (m_selectedPullDemux == SELECTED_PULLDEMUX::LEADTOOLD_TS_DEMUX)
	{
		// {136DCBF5-3874-4b70-AE3E-15997D6334F7} 
		static const GUID CLSID_EMPGDMX =

		{ 0xE2B7DF1A, 0x38C5, 0x11D5, { 0x91, 0xF6, 0x00, 0x10, 0x4B, 0xDb, 0x8F, 0xF9} };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGDMX, &pElecardPullDemux, L"Elecard Pull Demux");
		if (FAILED(hr))
		{
			LogError("Failed to add Leadtoold transport Demux");
			return hr;
		}
		 
		PIN_INFO pininfo;
		CComPtr<IPin> pDemuxInPin;
		GetPin(pElecardPullDemux, PIN_DIRECTION::PINDIR_INPUT, &pDemuxInPin);
		if (pSourceOutPin == NULL)
		{
			return -811;
		}

		hr = m_pGraph->Connect(pSourceOutPin, pDemuxInPin);
		if (pSourceOutPin == NULL)
		{
			return -841;
		}
	}

	if (m_selectedPullDemux == SELECTED_PULLDEMUX::IMPAL_PULL_DEMUX)
	{
		// {136DCBF5-3874-4b70-AE3E-15997D6334F7} 
		static const GUID CLSID_IMPLEO_PULL_DEMUX =
		{ 0x49BC1CC5, 0x591C, 0x48DE, { 0xB4, 0x41, 0x02, 0x56, 0xF4, 0xC3, 0x96, 0x52 } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_IMPLEO_PULL_DEMUX, &pElecardPullDemux, L"Impleo Pull Demux");
		if (FAILED(hr))
		{
			LogError("Failed to add Impleo Pull Demux");
			return hr;
		}

		PIN_INFO pininfo;
		CComPtr<IPin> pDemuxInPin;
		GetPin(pElecardPullDemux, PIN_DIRECTION::PINDIR_INPUT, &pDemuxInPin);
		if (pSourceOutPin == NULL)
		{
			return -811;
		}

		hr = m_pGraph->Connect(pSourceOutPin, pDemuxInPin);
		if (pSourceOutPin == NULL)
		{
			return -841;
		}
	}
  
	
	if (klvPID > 0)
	{
		 
		PIN_INFO pininfo;
		CComPtr<IPin> pKlvOutPin;
		pKlvOutPin = GetPinContainsName(pElecardPullDemux, "sync klv", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pKlvOutPin == NULL)
		{
			return -881;
		}
		hr = AddDumpFilter(true);
		if (FAILED(hr))
		{
			return hr;
		}
		hr = m_pGraph->Connect(pKlvOutPin, pDumpInPin);
		if (FAILED(hr))
		{
			return hr;
		}
		CComPtr<IFileSinkFilter>  iSink;
		hr = pDumpFilter->QueryInterface(IID_IFileSinkFilter, (void **)&iSink);
		if (FAILED(hr))
		{
			return hr;
		}

		WCHAR dumpFileName[100] = L"c:\\klv.bin";
		// Load the source file.
		hr = iSink->SetFileName(dumpFileName, NULL);
		if (FAILED(hr))
		{
			return hr;
		}
	}	 

	hr = CreateVideoRenderer(m_selectedRenderer);
	if (FAILED(hr))
	{
		LogError("Failed to create VMR9");
		return hr;
	}

	if (m_selectedDecoder == SELECTED_DECODER::LEADTOOLS && SUCCEEDED(hr))
	{
		static const GUID CLSID_LEADTOOLS_H264_DECODER =
		{ 0xE2B7DF25,0x38C5,0x11D5,{ 0x91,0xF6, 0x00, 0x10, 0x4B , 0xDB ,0x8F , 0xF9 } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_LEADTOOLS_H264_DECODER, &pVideoDecoder, L"LeadTools AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add LeadTools AVC Video Decoder");
			return hr;
		}
	} else 
	if (m_selectedDecoder == SELECTED_DECODER::ELECARD)
	{
		// {5C122C6D-8FCC-46f9-AAB7-DCFB0841E04D}
		static const GUID CLSID_EAVCDEC =
		{ 0x5c122c6D, 0x8fcc, 0x46f9,{ 0xaa, 0xb7, 0xdc, 0xfb, 0x8, 0x41, 0xe0, 0x4d } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Elecard AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard AVC Video Decoder");
			return hr; 
		}
		ActivateFilter(pVideoDecoder);		 
	} 
	if (m_selectedDecoder == SELECTED_DECODER::IMPAL)
	{
		 
		static const GUID CLSID_EAVCDEC =
		{ 0x2379e92b , 0x3ffa , 0x4ce5 ,{ 0x85 , 0x91 , 0xf6 , 0xa4 , 0x8c , 0x57 , 0xef, 0x6a } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Impleo AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Impleo AVC Video Decoder");
			return hr;
		}
	}
	else if (m_selectedDecoder == SELECTED_DECODER::AVOBJECTS)
	{

		static const GUID CLSID_EAVCDEC =
		{ 0x4BD50AA0 , 0x6CE6 , 0x4CD2 ,{ 0x96 , 0xB6 , 0xFB, 0x5F, 0xE7, 0xDE, 0x0C, 0xF2} };


		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"AVObjects AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add AVObjects AVC Video Decoder");
			return hr;
		}
	}	
	else if (m_selectedDecoder == SELECTED_DECODER::MICROSOFT_DTV)
	{

		static const GUID CLSID_EAVCDEC =
		{ 0x212690FB , 0x83E5 , 0x4526 ,{ 0x8F , 0xD7 , 0x74, 0x47, 0x9B, 0x79, 0x39, 0xCD} };


		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Microsoft DTV Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Microsoft DTV Video Decoder");
			return hr;
		}
	}
	 
  

	// Try to render the streams.
	if (m_selectedPullDemux == SELECTED_PULLDEMUX::RENDER_DEMUX)
	{
		if (SUCCEEDED(hr))
		{
			hr = RenderStreams(pSource);
		}
	}
	else
	{
		PIN_INFO pininfo;
		CComPtr<IPin> ph264OutPin;
		ph264OutPin = GetPinContainsName(pElecardPullDemux, "h264", PIN_DIRECTION::PINDIR_OUTPUT);
		if (ph264OutPin == NULL)
		{
			ph264OutPin = GetPinContainsName(pElecardPullDemux, "h.264", PIN_DIRECTION::PINDIR_OUTPUT);
			if (ph264OutPin == NULL)
			{
				return -8121;
			}
		}
		 
		CComPtr<IPin> pDecoderInPin;
		GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_INPUT, &pDecoderInPin);
		if (pDecoderInPin == NULL)
		{
			return -8112;
		}
		 
		hr = m_pGraph->Connect(ph264OutPin, pDecoderInPin);
		if (FAILED(hr))
		{
			return -841;
		}

		CComPtr<IPin> pDecoderOutPin;
		GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_OUTPUT, &pDecoderOutPin);
		hr = pDecoderOutPin->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -16;
		}
		hr = m_pGraph->Render(pDecoderOutPin);
   
	}

	// Get the seeking capabilities.
	if (SUCCEEDED(hr))
	{
		hr = m_pSeek->GetCapabilities(&m_seekCaps);
	}

	// Set the volume.
	if (SUCCEEDED(hr))
	{
		hr = UpdateVolume();
	}

	// Update our state.
	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}

	SAFE_RELEASE(pSource);

	return hr;
}

const long ONE_SEC = 10000000;

HRESULT DShowPlayer::SetRate(double rate)
{
	if (CanSeek() == true)
	{
		return m_pSeek->SetRate(rate);
	}
	return S_FALSE;
}

HRESULT	 DShowPlayer::Step(long lFrames, BOOL bSec /* = FALSE */)
{
	//if (m_pStateThread->CurrentGraphState() == graphStopped)
		//Pause();

	REFERENCE_TIME rtPos;
	GetCurrentPosition(&rtPos);

	if (!bSec && m_rt > 0)
		rtPos += lFrames * m_rt;
	else
		rtPos += lFrames * ONE_SEC;

	return SetPosition(rtPos);
}
//////////////////////////////////////////////////////////////////////////
HRESULT	 DShowPlayer::StepByStep(long lFrames)
{
	HRESULT hr = S_OK;
	
	 
	CComPtr<IVideoFrameStep> pIVFStep;
	if (SUCCEEDED(hr = m_pGraph->QueryInterface(IID_IVideoFrameStep, (void **)&pIVFStep)))
	{
		if (SUCCEEDED(hr = pIVFStep->CanStep(FALSE, NULL)))
			hr = pIVFStep->Step(lFrames, NULL);
	}
	 
	return hr;
}
//////////////////////////////////////////////////////////////////////////
HRESULT DShowPlayer::StepBackward(long lFrames /* = -1 */, BOOL bSec /* = FALSE */)
{
	HRESULT m_hrError = Step(lFrames, bSec);

	GetCurrentPosition(&m_rtFastPosition);
	m_playbackMode = modeFastBckwrd;

	if (!bSec)
		m_rtFastStep = lFrames * m_rt;
	else
		m_rtFastStep = lFrames * ONE_SEC;

	return m_hrError;
}
//////////////////////////////////////////////////////////////////////////
HRESULT DShowPlayer::StepForward(long lFrames /* = 1 */, BOOL bSec /* = FALSE */)
{
	HRESULT m_hrError = Step(lFrames, bSec);

	GetCurrentPosition(&m_rtFastPosition);
	m_playbackMode = modeFastFrwrd;

	if (!bSec)
		m_rtFastStep = lFrames * m_rt;
	else
		m_rtFastStep = lFrames * ONE_SEC;

	return m_hrError;
}

void DShowPlayer::DrawString(HDC hdc)
{
	Graphics graphics(hdc);

	// Create a string.
	WCHAR string[] = L"Sample Text";

	// Initialize arguments.
	Font myFont(L"Arial", 16);
	RectF layoutRect(0.0f, 0.0f, 200.0f, 50.0f);
	StringFormat format;
	format.SetAlignment(StringAlignmentCenter);
	SolidBrush blackBrush(Color(255, 0, 0, 0));

	// Draw string.
	graphics.DrawString(
		string,
		11,
		&myFont,
		layoutRect,
		&format,
		&blackBrush);

	// Draw layoutRect.
	graphics.DrawRectangle(&Pen(Color::Black, 3), layoutRect);
}
  
HRESULT DShowPlayer::ApplyOverlay(float alpha_opacity)
{
	int cx, cy;
	HRESULT hr;
	HBITMAP hbm;
	RECT rcClient;

	GetDesktopResolution(cx, cy);

	GetClientRect(m_hwndVideo, &rcClient);

	HDC hdc = GetDC(m_hwndVideo);

	if (hdc == NULL)
	{
		return E_FAIL;
	}
	HDC hdcBmp = CreateCompatibleDC(hdc);
	if (hdcBmp == NULL)
	{
		return E_FAIL;
	}
	hbm = CreateCompatibleBitmap(hdc, rcClient.right, rcClient.bottom);
	BITMAP bm;
	if (0 == GetObject(hbm, sizeof(bm), &bm))
	{
		DeleteDC(hdcBmp);
		return E_FAIL;
	}

	HBITMAP hbmOld = (HBITMAP)SelectObject(hdcBmp, hbm);
	if (hbmOld == 0)
	{
		DeleteDC(hdcBmp);
		return E_FAIL;
	}
	//To draw line
	//DrawLine1(100, 100, 200, 200, hdcBmp, 12);
	map<int, Overlay*>::iterator it;
	for (it = m_overlays.begin(); it != m_overlays.end(); it++)
	{
		Overlay* over = (*it).second;
		if (over->visible == 0)
			continue;

		if (over->type == 1) // line
		{

			HPEN hPen;
			//RGB(255, 0, 0)
			hPen = CreatePen(PS_SOLID, over->lineWidth, over->color.ToCOLORREF());
			SelectObject(hdcBmp, hPen);

			MoveToEx(hdcBmp, over->x1, over->y1, NULL);
			LineTo(hdcBmp, over->x2, over->y2);
			DeletePen(hPen);
		}
		else
			if (over->type == 2) // circle
			{
				HPEN hPen = CreatePen(PS_SOLID, over->lineWidth, over->color.ToCOLORREF());
				SelectObject(hdcBmp, hPen);
				SelectObject(hdcBmp, GetStockObject(HOLLOW_BRUSH));
				//Ellipse(hdcBmp , 1000 , 800 ,1400, 1000);
				Ellipse(hdcBmp, over->x1, over->y1, over->radios_w, over->radios_h);
				  
				DeletePen(hPen);
			}
			else
				if (over->type == 0) // text
				{
					RECT rect;
					HBRUSH hBrush;
					HFONT hFont;
					 
#if 0

					hFont = CreateFont(over->fontSize, 0, 0, 0,
						FW_NORMAL, FALSE, FALSE, FALSE,
						ANSI_CHARSET, OUT_DEFAULT_PRECIS,
						CLIP_DEFAULT_PRECIS, DEFAULT_QUALITY,
						DEFAULT_PITCH | FF_ROMAN,
						L"Times New Roman");
#else

					hFont = CreateFont(over->fontSize, 
									   0, 
									   0,
									   0, 
									   FW_BOLD, 
									   0, 
									   0, 
									   0,
									   DEFAULT_CHARSET, 
									   OUT_OUTLINE_PRECIS,
									   CLIP_DEFAULT_PRECIS, 
									   ANTIALIASED_QUALITY, 
									   FF_MODERN, 
									   __T("Ariel"));
#endif 
  
					 
					SelectObject(hdcBmp, hFont); 
					 
					//Sets the coordinates for the rectangle in which the text is to be formatted.
					SetRect(&rect, over->pos.X, over->pos.Y, over->pos.Width, over->pos.Height);
					SetTextColor(hdcBmp, over->color.ToCOLORREF());
					SetBkColor(hdcBmp, TRANSPARENT);
					DrawText(hdcBmp, over->text, -1, &rect, DT_NOCLIP);
					 
					DeleteObject(hFont);
   
				}
	}
	 

	VMR9AlphaBitmap bmpInfo;
	ZeroMemory(&bmpInfo, sizeof(bmpInfo));

	bmpInfo.dwFlags = VMRBITMAP_HDC | VMRBITMAP_SRCCOLORKEY;

	bmpInfo.hdc = hdcBmp;

	SetRect(&bmpInfo.rSrc, 0, 0, bm.bmWidth, bm.bmHeight);
	bmpInfo.rDest.left = 0.f;
	bmpInfo.rDest.top = 0.f;
	bmpInfo.rDest.right = 1.0f;
	bmpInfo.rDest.bottom = 1.0f;

	// Set the transparency value (1.0 is opaque, 0.0 is transparent).
	bmpInfo.fAlpha = alpha_opacity;  // 1;// 0.5f;
	bmpInfo.clrSrcKey = RGB(0, 0, 0);

	if (m_pVideo != NULL)
	{
		hr = pBmp->SetAlphaBitmap(&bmpInfo);
		if (FAILED(hr))
		{
			return E_FAIL;
		}
		//pBmp->Release();
		//SAFE_RELEASE(pBmp);	 
	}
	// Clean up.
	ReleaseDC(m_hwndVideo, hdc);
	DeleteBitmap(hbm);
	DeleteObject(SelectObject(hdcBmp, hbmOld));
	DeleteDC(hdcBmp);

	return S_OK;
}
 

HRESULT	DShowPlayer::AddTextOverlay(WCHAR *text,
									int id,
									int left,
									int top,
									int right,
									int bottom,
									int color,
									float fontSize,
									int fontStyle)
{

	HRESULT hr;

	map<int, Overlay*>::iterator it;
	if ((it = m_overlays.find(id)) != m_overlays.end()) // id already in the map
	{

		Overlay* over = (*it).second;
		over->type = 0;

		int size = wcslen(text);
		wmemcpy(over->text, text, size);
		over->text[size] = '\0';

		over->visible = 1;
		over->fontStyle = fontStyle;
		over->color.SetFromCOLORREF(color);
		over->fontSize = fontSize;
		RectF rcBounds(left, top, right, bottom);
		over->pos = rcBounds;
		m_overlays[id] = over;

	}
	else {

		Overlay* overlay = new Overlay(text);
		overlay->type = 0;
		overlay->visible = 1;
		overlay->fontStyle = fontStyle;
		overlay->color.SetFromCOLORREF(color);
		overlay->fontSize = fontSize;
		RectF rcBounds(left, top, right, bottom);
		overlay->pos = rcBounds;
		m_overlays[id] = overlay;

	}

	return S_OK;
 
}
 
HRESULT	DShowPlayer::AddCircle(int id,
	int x,
	int y,
	int radios_w,
	int radios_h,
	int color,
	int width)
{
	HRESULT hr;

	map<int, Overlay*>::iterator it;
	if ((it = m_overlays.find(id)) != m_overlays.end()) // id already in the map
	{
		Overlay* over = (*it).second;
		over->type = 2;
		//over->visible = 1;
		over->color.SetFromCOLORREF(color);
		over->fontSize = 0;

		over->x1 = x;
		over->y1 = y;
		over->radios_w = radios_w;
		over->radios_h = radios_h;
		over->lineWidth = width;

		m_overlays[id] = over;

		return S_OK;
	}
	else {

		Overlay* overlay = new Overlay();
		overlay->type = 2;
		overlay->visible = 1;
		overlay->color.SetFromCOLORREF(color);
		overlay->fontSize = 0;

		overlay->x1 = x;
		overlay->y1 = y;
		overlay->radios_w = radios_w;
		overlay->radios_h = radios_h;
		overlay->lineWidth = width;

		m_overlays[id] = overlay;

		return S_OK;
	}
}

HRESULT	DShowPlayer::AddTextOverlay2(WCHAR *text, int id,
	int left,
	int top,
	int right,
	int bottom,
	int color,
	float fontSize)
{


	map<int, Overlay*>::iterator it;
	if ((it = m_overlays.find(id)) != m_overlays.end()) // id already in the map
	{
		Overlay* over = (*it).second;
		over->type = 0;

		int size = wcslen(text);
		wmemcpy(over->text, text, size);
		over->text[size] = '\0';

		over->visible = 1;
		over->fontStyle = FontStyle::FontStyleRegular;
		over->color.SetFromCOLORREF(color);
		over->fontSize = fontSize;
		RectF rcBounds(left, top, right, bottom);
		over->pos = rcBounds;
		m_overlays[id] = over;

	}
	else
	{

		Overlay* overlay = new Overlay(text);
		overlay->type = 0;
		overlay->visible = 1;
		overlay->fontStyle = FontStyle::FontStyleRegular;
		overlay->color.SetFromCOLORREF(color);
		overlay->fontSize = fontSize;
		RectF rcBounds(left, top, right, bottom);
		overlay->pos = rcBounds;
		m_overlays[id] = overlay;
	}

	return S_OK;
	 
}
 
HRESULT	DShowPlayer::Clear()
{

	map<int, Overlay*>::iterator it;
	for (it = m_overlays.begin(); it != m_overlays.end(); it++)
	{
		Overlay* over = (*it).second;
		over->visible = 0;
	}
	return S_OK;
  
}
HRESULT	DShowPlayer::Visible(int id, bool visible)
{

	if (m_overlays[id] != NULL)
	{
		m_overlays[id]->visible = visible;

	}
	return S_OK;
  
}

HRESULT	DShowPlayer::AddLine(int id,
	int x1,
	int y1,
	int x2,
	int y2,
	int color,
	int width)
{
	HRESULT hr;


	map<int, Overlay*>::iterator it;
	if ((it = m_overlays.find(id)) != m_overlays.end()) // id already in the map
	{
		Overlay* over = (*it).second;
		over->type = 1;
		//over->visible = 1;
		over->color.SetFromCOLORREF(color);
		over->fontSize = 0;

		over->x1 = x1;
		over->x2 = x2;
		over->y1 = y1;
		over->y2 = y2;
		over->lineWidth = width;

		m_overlays[id] = over;

		return S_OK;
	}
	else {

		Overlay* overlay = new Overlay();
		overlay->type = 1;
		overlay->visible = 1;
		overlay->color.SetFromCOLORREF(color);
		overlay->fontSize = 0;

		overlay->x1 = x1;
		overlay->x2 = x2;
		overlay->y1 = y1;
		overlay->y2 = y2;
		overlay->lineWidth = width;

		m_overlays[id] = overlay;

		return S_OK;
	}
	 
}

void SetColorRef(VMR9AlphaBitmap& bmpInfo)
{
	// Set the COLORREF so that the bitmap outline will be transparent
	bmpInfo.clrSrcKey = RGB(255, 255, 255);  // Pure white
	bmpInfo.dwFlags |= VMRBITMAP_SRCCOLORKEY;
}


COLORREF g_rgbColors = DEFAULT_FONT_COLOR;
// Destination rectangle used for alpha-blended text
VMR9NormalizedRect  g_rDest = { 0 };
float g_fBitmapCompWidth = 0;  // Width of bitmap in composition space units
int g_nImageWidth = 0;         // Width of text bitmap
const float EDGE_BUFFER = 0.04f; // Pixel buffer between bitmap and window edge
							   // (represented in composition space [0 - 1.0f])

HRESULT DShowPlayer::BlendApplicationText(TCHAR *szNewText)
{
	LONG cx, cy;
	HRESULT hr;
	 
	RECT rcClient;

	// Read the default video size
	hr = m_pWC->GetNativeVideoSize(&cx, &cy, NULL, NULL);
	if (FAILED(hr))
		return hr;

	// Create a device context compatible with the current window
	HDC hdc = GetDC(m_hwndVideo);
	HDC hdcBmp = CreateCompatibleDC(hdc);

	// Write with a known font by selecting it into our HDC
	HFONT hOldFont = (HFONT)SelectObject(hdcBmp, g_hFont);

	// Determine the length of the string, then determine the
	// dimensions (in pixels) of the character string using the
	// currently selected font.  These dimensions are used to create
	// a bitmap below.
	int nLength, nTextBmpWidth, nTextBmpHeight;
	SIZE sz = { 0 };
	nLength = (int)_tcslen(szNewText);
	GetTextExtentPoint32(hdcBmp, szNewText, nLength, &sz);
	nTextBmpHeight = sz.cy;
	nTextBmpWidth = sz.cx;

	// Create a new bitmap that is compatible with the current window
	HBITMAP hbm = CreateCompatibleBitmap(hdc, nTextBmpWidth, nTextBmpHeight);
	ReleaseDC(m_hwndVideo, hdc);

	// Select our bitmap into the device context and save the old one
	BITMAP bm;
	HBITMAP hbmOld;
	GetObject(hbm, sizeof(bm), &bm);
	hbmOld = (HBITMAP)SelectObject(hdcBmp, hbm);

	// Set initial bitmap settings
	RECT rcText;
	SetRect(&rcText, 0, 0, nTextBmpWidth, nTextBmpHeight);
	SetBkColor(hdcBmp, RGB(255, 255, 255)); // Pure white background
	SetTextColor(hdcBmp, g_rgbColors);      // Write text with requested color

	// Draw the requested text string onto the bitmap
	TextOut(hdcBmp, 0, 0, szNewText, nLength);

	// Configure the VMR's bitmap structure
	VMR9AlphaBitmap bmpInfo;
	ZeroMemory(&bmpInfo, sizeof(bmpInfo));
	bmpInfo.dwFlags = VMRBITMAP_HDC;
	bmpInfo.hdc = hdcBmp;  // DC which has selected our bitmap

	// Remember the width of this new bitmap
	g_nImageWidth = bm.bmWidth;

	// Save the ratio of the bitmap's width to the width of the video file.
	// This value is used to reposition the bitmap in composition space.
	g_fBitmapCompWidth = (float)g_nImageWidth / (float)cx;

	// Display the bitmap in the bottom right corner.
	// rSrc specifies the source rectangle in the GDI device context 
	// rDest specifies the destination rectangle in composition space (0.0f to 1.0f)
	bmpInfo.rDest.left = 1.0f;
	bmpInfo.rDest.right = 1.0f + g_fBitmapCompWidth;
	bmpInfo.rDest.top = (float)(cy - bm.bmHeight) / (float)cy - EDGE_BUFFER;
	bmpInfo.rDest.bottom = 1.0f - EDGE_BUFFER;
	bmpInfo.rSrc = rcText;

	// Copy initial settings to global memory for later modification
	g_rDest = bmpInfo.rDest;

	// Transparency value 1.0 is opaque, 0.0 is transparent.
	bmpInfo.fAlpha = TRANSPARENCY_VALUE;

	// Set the COLORREF so that the bitmap outline will be transparent
	SetColorRef(bmpInfo);

	// Give the bitmap to the VMR for display
	hr = pBmp->SetAlphaBitmap(&bmpInfo);
	if (FAILED(hr))
		return hr;

	// Select the initial objects back into our device context
	DeleteObject(SelectObject(hdcBmp, hbmOld));
	SelectObject(hdc, hOldFont);

	// Clean up resources
	DeleteObject(hbm);
	DeleteDC(hdcBmp);

	return hr;
}
HRESULT DShowPlayer::AddDumpFilter(bool elecardDump)
{
	HRESULT hr;
	PIN_INFO pininfo;
	  
	static const GUID CLSID_ESinkFilter =
	{ 0xcf2521a7,0x4029,0x4cc1,{ 0x8C,0x6E, 0xf8, 0x2b, 0xd8 ,0x2b, 0xb3 ,0x43 } };


	static const GUID CLSID_SinkFilter =
	{ 0x36A5F770,0xFE4C,0x11CE,{ 0xA8,0xED, 0x00, 0xAA, 0x00, 0x2F, 0xEA, 0xB5 } };
  
	if (elecardDump == false)
	{
		hr = AddFilterByCLSID(m_pGraph, CLSID_SinkFilter, &pDumpFilter, L"Dump Filter");
		if (FAILED(hr))
		{
			return -120;
		}
	}
	else
	{
		hr = AddFilterByCLSID(m_pGraph, CLSID_ESinkFilter, &pDumpFilter, L"Elecard sink Filter");
		if (FAILED(hr))
		{
			return -120;
		}
	}

	 
	GetPin(pDumpFilter, PIN_DIRECTION::PINDIR_INPUT, &pDumpInPin);
	hr = pDumpInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}

	return hr;
}
 
HRESULT DShowPlayer::InitializeScreenCaptureToDump(const WCHAR* fileName,
													int screenWidth,
													int screenHeight,
												    bool includeAudio,
												    int bitrate)
{

	HRESULT hr;
	PIN_INFO pininfo;

	bool useBoutechSender = true;

	 

	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

	// {0C07B3AC-BA59-48ca-B2EC-6D4F9BC7928E}
	static const GUID CLSID_ElecardDesktopCapture =
	{ 0xc07b3ac, 0xba59, 0x48ca, {0xb2, 0xec, 0x6d, 0x4f, 0x9b, 0xc7, 0x92, 0x8e} };

	hr = AddFilterByCLSID(m_pGraph, CLSID_ElecardDesktopCapture, &pDesktopCapture, L"Elecard Descktop Capture");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Descktop Capture");
		return -5;
	} 

	CComPtr<IPin> pDesktopVideoOutPin;
	GetPin(pDesktopCapture, PIN_DIRECTION::PINDIR_OUTPUT, &pDesktopVideoOutPin);
	hr = pDesktopVideoOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -181;
	}
	
	CComVariant var;
	CComQIPtr<IModuleConfig> mc_capture(pDesktopCapture);
	var.vt = VT_INT;
	var.intVal = 1;
	mc_capture->SetValue(&EDC_CaptureVideo, &var);
	var.intVal = includeAudio;
	mc_capture->SetValue(&EDC_CaptureAudio, &var);
	mc_capture->CommitChanges(0);

	//set video settings
	CComQIPtr<IModuleConfig> mc_capture_video(pDesktopVideoOutPin);
	if (mc_capture_video)
	{
		mc_capture_video->SetDefState();

		//Set init capture rect
		CComSafeArray <int> sa(4);
		sa.SetAt(2, screenWidth, 0);
		sa.SetAt(3, screenHeight, 0);

		var.vt = VT_I4 | VT_ARRAY;
		var.parray = sa.Detach();

		HRESULT hr = mc_capture_video->SetValue(&EDC_VideoCaptureRect, &var);
		mc_capture_video->CommitChanges(0);
		var.Clear();

		//Set capture mode
		var.vt = VT_INT;
		var.intVal = 0; //set GDI mode (the most compatible mode)
		hr = mc_capture_video->SetValue(&EDC_VideoCaptureMode, &var);
		mc_capture_video->CommitChanges(0);
	}



	static const GUID CLSID_AVCEncoder =
	{ 0xe09edec9, 0x5e3, 0x4aaa,{ 0x95, 0x54, 0x14, 0x9f, 0x94, 0xc2, 0x47, 0x80 } };

	hr = AddFilterByCLSID(m_pGraph, CLSID_AVCEncoder, &pVideoEncoder, L"Elecard AVC Video Encoder");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard AVC Video Encoder");
		return -5;
	}


	 
	CComQIPtr<IModuleConfig> mc_video_encoder(pVideoEncoder);	 
	CComVariant varEncBitrate;
	varEncBitrate.vt = VT_INT;
	varEncBitrate.intVal = bitrate;
	hr = mc_video_encoder->SetValue(&EMC_BITRATE_AVG, &varEncBitrate);


	int maxBitrate = bitrate * 3 / 2;

  
	CComVariant varEncMaxBitrate;
	varEncMaxBitrate.vt = VT_INT;
	varEncMaxBitrate.intVal = maxBitrate;
	hr = mc_video_encoder->SetValue(&EMC_BITRATE_MAX, &varEncMaxBitrate);
	   
	hr = mc_video_encoder->CommitChanges(NULL);
	  

	CComPtr<IPin> pEncoderInPin;
	GetPin(pVideoEncoder, PIN_DIRECTION::PINDIR_INPUT, &pEncoderInPin);
	hr = pEncoderInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -18;
	}

	hr = pGraph2->Connect(pDesktopVideoOutPin, pEncoderInPin);
	if (FAILED(hr))
	{
		return -173;
	}
	 

	CComPtr<IPin> pEncoderOutPin;
	GetPin(pVideoEncoder, PIN_DIRECTION::PINDIR_OUTPUT, &pEncoderOutPin);
	hr = pEncoderOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -18;
	}
	 
	// {0FD7F9F6-747D-46cf-AC0B-FA9DAE4F6299}
	static const GUID CLSID_EMPEGMUXER = {
		0xfd7f9f6, 0x747d, 0x46cf,{ 0xac, 0xb, 0xfa, 0x9d, 0xae, 0x4f, 0x62, 0x99 } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPEGMUXER, &pElecardMpegMux, L"Elecard MPEG2 Mux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard MPEG2 Mux");
		return -512;
	}
	CComPtr<IPin> pMuxInPin;
	GetPin(pElecardMpegMux, PIN_DIRECTION::PINDIR_INPUT, &pMuxInPin);
	hr = pMuxInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -17;
	}


	hr = pGraph2->Connect(pEncoderOutPin, pMuxInPin);
	if (FAILED(hr))
	{
		return -172;
	}

	CComPtr<IPin> pMuxOutPin;
	GetPin(pElecardMpegMux, PIN_DIRECTION::PINDIR_OUTPUT, &pMuxOutPin);
	hr = pMuxOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -17;
	}


	static const GUID CLSID_SinkFilter =
	{ 0x36A5F770,0xFE4C,0x11CE,{ 0xA8,0xED, 0x00, 0xAA, 0x00, 0x2F, 0xEA, 0xB5 } };

	static const GUID CLSID_ESinkFilter =
	{ 0xcf2521a7,0x4029,0x4cc1,{ 0x8C,0x6E, 0xf8, 0x2b, 0xd8 ,0x2b, 0xb3 ,0x43 } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_ESinkFilter, &pSinkFilter, L"Dump Filter");
	//hr = AddFilterByCLSID(m_pGraph, CLSID_SinkFilter, &pSinkFilter, L"Dump Filter");
	if (FAILED(hr))
	{
		LogError("Failed to Sink Filter ");
		return hr;
	}

	CComPtr<IFileSinkFilter>  iSink;
	hr = pSinkFilter->QueryInterface(IID_IFileSinkFilter, (void **)&iSink);
	if (FAILED(hr))
	{
		LogError("Failed to query interface of IFileSinkFilter sinkFilter");
		return hr;
	}

	// Load the source file.
	hr = iSink->SetFileName(fileName, NULL);
	if (FAILED(hr))
	{
		LogError("File to set fileName:");
		return hr;
	}

	CComPtr<IPin> pDumpPin;
	GetPin(pSinkFilter, PIN_DIRECTION::PINDIR_INPUT, &pDumpPin);
	hr = pDumpPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -17;
	}

	if (includeAudio)
	{

		// {55131B92-196E-496e-9B9C-8F417D040ADD}
		static const GUID CLSID_ElecardAACAudioEncoder = 
		{0x55131b92, 0x196e, 0x496e, {0x9b, 0x9c, 0x8f, 0x41, 0x7d, 0x4, 0xa, 0xdd}};


		hr = AddFilterByCLSID(m_pGraph, CLSID_ElecardAACAudioEncoder, &pAudioEncoder, L"Elecard AAC Audio Encoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard  AAC Audio Encoder");
			return -5;
		}

		CComPtr<IPin> pDesktopAudioOutPin;
		GetPin(pDesktopCapture, PIN_DIRECTION::PINDIR_OUTPUT, &pDesktopAudioOutPin, 1);
		hr = pDesktopAudioOutPin->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -181;
		}
		

		CComPtr<IPin> pAudioEncoderInPin;
		GetPin(pAudioEncoder, PIN_DIRECTION::PINDIR_INPUT, &pAudioEncoderInPin);
		hr = pAudioEncoderInPin->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -317;
		}

		hr = pGraph2->Connect(pDesktopAudioOutPin, pAudioEncoderInPin);
		if (FAILED(hr))
		{
			return -1272;
		}

		CComPtr<IPin> pAudioEncoderOutPin;
		GetPin(pAudioEncoder, PIN_DIRECTION::PINDIR_OUTPUT, &pAudioEncoderOutPin);
		hr = pAudioEncoderOutPin->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -127;
		}

		CComPtr<IPin> pMuxInPin2;
		GetPin(pElecardMpegMux, PIN_DIRECTION::PINDIR_INPUT, &pMuxInPin2, 1);
		hr = pMuxInPin2->QueryPinInfo(&pininfo);
		if (FAILED(hr))
		{
			return -165;
		}

		hr = pGraph2->Connect(pAudioEncoderOutPin, pMuxInPin2);
		if (FAILED(hr))
		{
			return -172;
		}
	}
	 

	hr = pGraph2->Connect(pMuxOutPin, pDumpPin);
	if (FAILED(hr))
	{
		return -172;
	}


	m_state = STATE_STOPPED;
	return hr;

}

/// interface can be null in case of UDP
// interface is the ip address of this computer
HRESULT DShowPlayer::InitializeNetwordReceiverToBoutechDVRSink(const WCHAR * rootDirectory,															   
															   const WCHAR * serverAddress,
															   const WCHAR * interfaceAddress,
															   int serverPort,
															   bool useBoutechReceiver,
															   int LogTime,
															   int maxFilesPerDirectory)

{
	
	HRESULT hr = S_OK;
	PIN_INFO pininfo;

	bool useTransport = true;

 
	 
	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

	if (useBoutechReceiver == false)
	{
		static const GUID CLSID_NWSource =
		{ 0x62341545, 0x9318, 0x4671,{ 0x9d, 0x62, 0x9c, 0xaa, 0xcd, 0xd5, 0xd2, 0xa } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_NWSource, &pNetworkSource, L"Elecard network Source");
		if (FAILED(hr))
		{
			LogError("Failed to add Elecard network source");
			return -5;
		}

		static const GUID IID_IModuleConfig =
		{ 0x486F726E, 0x4D43, 0x49b9,{ 0x8A, 0x0C,  0xC2, 0x2A, 0x2B, 0x05, 0x24, 0xE8 } };

		hr = pNetworkSource->QueryInterface(IID_IModuleConfig, (void**)&m_nwsource_config);
		if (FAILED(hr))
			return -6;


		ENWSource_Plus::announces_descr_ext *announce_descr = NULL;
		wstring ws(serverAddress);
		string serverURL(ws.begin(), ws.end());

		string port = std::to_string(serverPort);


		get_announces(&announce_descr);
		// set SDP configuration
		string sdp = "v=0\no=- 1364663709 8 IN IP4 0.0.0.0\ns=Truck\nc=IN IP4 " + serverURL + " " + "/64\nb=CT:0\nt=0 0\nm=video " + port + " " + "udp 33\na = rtpmap:33 MP2T / 90000\na = control:trackID = 0\n";
		CComVariant val(sdp.c_str());
		hr = m_nwsource_config->SetValue(&ENS_announce_sdp_data, &val);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val1(port.c_str());
		hr = m_nwsource_config->SetValue(&ENS_port, &val1);
		if (FAILED(hr))
		{
			return -7;
		}

		CComVariant val2(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_mcast_address, &val2);
		if (FAILED(hr))
		{
			return -7;
		}
		 
		if (interfaceAddress != NULL)
		{
			wstring wsInterface(interfaceAddress);
			string strInterface(wsInterface.begin(), wsInterface.end());

			CComVariant val3(strInterface.c_str());
			hr = m_nwsource_config->SetValue(&ENS_interface, &val3);
			if (FAILED(hr))
			{
				return -7;
			}
		}
 
		CComVariant val4(serverURL.c_str());
		hr = m_nwsource_config->SetValue(&ENS_server, &val4);
		if (FAILED(hr))
		{
			return -7;
		}
		CComVariant val5(INT_MAX);
		hr = m_nwsource_config->SetValue(&ENS_timeout, &val5);
		if (FAILED(hr))
		{
			return -7;
		}

		m_nwsource_config->CommitChanges(NULL);
	}
	else
	{
		static const GUID CLSID_CBoutechLiveSourceReceiver =
		{ 0xc21d1b3d, 0x648d, 0x4099, { 0xac, 0xeb, 0x5, 0x9, 0x82, 0x42, 0x7e, 0x8b } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_CBoutechLiveSourceReceiver, &pNetworkSource, L"Boutech Live Source Receiver");
		if (FAILED(hr))
		{
			LogError("Failed to add Boutech Live Source Receiver");
			return -5;
		}


		// {CF24BB08-37C8-44BD-9A12-683B118F290A}
		static const GUID IID_IBoutechLiveSource =
		{ 0xcf24bb08, 0x37c8, 0x44bd, { 0x9a, 0x12, 0x68, 0x3b, 0x11, 0x8f, 0x29, 0xa } };

		CComPtr<IBoutechLiveSource> pBoutechReceiverInterface;
		hr = pNetworkSource->QueryInterface(IID_IBoutechLiveSource, (void **)&pBoutechReceiverInterface);
		pBoutechReceiverInterface->SetIpAddress((WCHAR *)serverAddress);
		pBoutechReceiverInterface->SetPort(serverPort);
		pBoutechReceiverInterface->SetPinType(useTransport == true ? 0 : 1);
	}
	 
	CComPtr<IPin> pSourceOutPin;
	GetPin(pNetworkSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}

 
	static const GUID CLSID_BauotechDVRSink =
	{ 0x2839e900, 0x2ccf, 0x4227,{ 0x9b, 0x3b, 0x81, 0x2c, 0xac, 0xb0, 0x61, 0x3a } };

	 

	hr = AddFilterByCLSID(m_pGraph, CLSID_BauotechDVRSink, &pBauotechDVRSink, L"Bauotech DVR Sink");
	if (FAILED(hr))
	{
		LogError("Failed to add auotech DVR Sink");
		return -5234;
	}

	CComPtr<IPin> pDvrSinkInPin;
	GetPin(pBauotechDVRSink, PIN_DIRECTION::PINDIR_INPUT, &pDvrSinkInPin);
	hr = pDvrSinkInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}
	 
	 
	static const GUID IID_IBoutechDVRSink =
	{ 0xacb60a35, 0x4a4e, 0x49b8, { 0x8f, 0xc1, 0x27, 0x1b, 0x34, 0xe7, 0xaa, 0xd8 } };
	 
	 

	CComPtr<IBoutechDVRSink> pDvrSinkIface;
	hr = pBauotechDVRSink->QueryInterface(IID_IBoutechDVRSink, (void**)&pDvrSinkIface);
	if (FAILED(hr))
		return -6;

	 
	hr = pDvrSinkIface->ConfigDvrSink(rootDirectory, LogTime, maxFilesPerDirectory);
	if (FAILED(hr))
	{
		return -1172;
	}


	hr = pGraph2->Connect(pSourceOutPin, pDvrSinkInPin);
	if (FAILED(hr))
	{
		return -172;
	}

	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}
	return hr;

}
 
void DShowPlayer::UnlockBautechPlayer(const long code)
{
	m_bauotechLockKey = code;
}

void DShowPlayer::SetDVRSinkCallback(ProgressCallback progressCallback)
{
	static const GUID IID_IBoutechDVRSink =
	{ 0xacb60a35, 0x4a4e, 0x49b8, { 0x8f, 0xc1, 0x27, 0x1b, 0x34, 0xe7, 0xaa, 0xd8 } };


	CComPtr<IBoutechDVRSink> pDvrSinkIface;
	pBauotechDVRSink->QueryInterface(IID_IBoutechDVRSink, (void**)&pDvrSinkIface);
	 


	 pDvrSinkIface->SetDVRSinkCallback(progressCallback);
	 
}

HRESULT DShowPlayer::InitializeBauotechDVRPlayerAsync(HWND hwnd,
	const WCHAR *DvrFileName,
	float startTime,
	bool loop)
{



	HRESULT hr = S_OK;
	m_hwndVideo = hwnd;
	PIN_INFO pininfo;

	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;



	// {E3F87BBC-C056-447E-8DDD-7ED90CC1E436}
	static const GUID CLSID_BauotectDVRSourceAsync =
	{ 0xe3f87bbc, 0xc056, 0x447e, { 0x8d, 0xdd, 0x7e, 0xd9, 0xc, 0xc1, 0xe4, 0x36 } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_BauotectDVRSourceAsync, &pBauotechDVRSource, L"Bauotech DVR Source Async");
	if (FAILED(hr))
	{
		LogError("Failed to add Bauotech DVR Source Async");
		return -10;
	}

	CComPtr<IFileSourceFilter>  iSourceFilter;
	hr = pBauotechDVRSource->QueryInterface(IID_IFileSourceFilter, (void **)&iSourceFilter);
	if (FAILED(hr))
	{
		return hr;
	}
	iSourceFilter->Load(DvrFileName, NULL);
	 
	CComPtr<IPin> pDvrSourceOutPin;
	GetPin(pBauotechDVRSource, PIN_DIRECTION::PINDIR_OUTPUT, &pDvrSourceOutPin);
	hr = pDvrSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}

	// {136DCBF5-3874-4b70-AE3E-15997D6334F7} 
	static const GUID CLSID_EMPGDMX =
	{ 0x136dcbf5, 0x3874, 0x4b70, { 0xae, 0x3e, 0x15, 0x99, 0x7d, 0x63, 0x34, 0xf7 } };



	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGDMX, &pElecardPullDemux, L"Elecard pull Demux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Push Demux");
		return -10;
	}
	SetDemuxerLatency(1.5);


	if (m_selectedDecoder == SELECTED_DECODER::LEADTOOLS && SUCCEEDED(hr))
	{
		static const GUID CLSID_LEADTOOLS_H264_DECODER =
		{ 0xE2B7DF25,0x38C5,0x11D5,{ 0x91,0xF6, 0x00, 0x10, 0x4B , 0xDB ,0x8F , 0xF9 } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_LEADTOOLS_H264_DECODER, &pVideoDecoder, L"LeadTools AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add LeadTools AVC Video Decoder");
			return hr;
		}
	}
	else
		if (m_selectedDecoder == SELECTED_DECODER::ELECARD)
		{
			// {5C122C6D-8FCC-46f9-AAB7-DCFB0841E04D}
			static const GUID CLSID_EAVCDEC =
			{ 0x5c122c6D, 0x8fcc, 0x46f9,{ 0xaa, 0xb7, 0xdc, 0xfb, 0x8, 0x41, 0xe0, 0x4d } };

			hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Elecard AVC Video Decoder");
			if (FAILED(hr))
			{
				LogError("Failed to add Elecard AVC Video Decoder");
				return hr;
			}
			ActivateFilter(pVideoDecoder);
			ClearDecoderBufferForFastLatency();

		}
	if (m_selectedDecoder == SELECTED_DECODER::IMPAL)
	{

		static const GUID CLSID_EAVCDEC =
		{ 0x2379e92b , 0x3ffa , 0x4ce5 ,{ 0x85 , 0x91 , 0xf6 , 0xa4 , 0x8c , 0x57 , 0xef, 0x6a } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Impleo AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Impleo AVC Video Decoder");
			return hr;
		}
	}
	else
	{
		if (m_selectedDecoder == SELECTED_DECODER::AVOBJECTS)
		{

			static const GUID CLSID_EAVCDEC =
			{ 0x4BD50AA0 , 0x6CE6 , 0x4CD2 ,{ 0x96 , 0xB6 , 0xFB, 0x5F, 0xE7, 0xDE, 0x0C, 0xF2} };


			hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"AVObjects AVC Video Decoder");
			if (FAILED(hr))
			{
				LogError("Failed to add AVObjects AVC Video Decoder");
				return hr;
			}
		}
	}


	CComPtr<IPin> pSourceOutPin;
	GetPin(pBauotechDVRSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}


	CComPtr<IPin> ppDemuxIn;
	GetPin(pElecardPullDemux, PIN_DIRECTION::PINDIR_INPUT, &ppDemuxIn);
	hr = ppDemuxIn->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}


	hr = m_pGraph->Connect(pDvrSourceOutPin, ppDemuxIn);
	if (FAILED(hr))
	{
		return -13;
	}

	CComPtr<IPin> pDemuxOut;
	hr = S_FALSE;
	int timeOut = 20;
	while (pDemuxOut == NULL && hr == S_FALSE)
	{
		if (timeOut == 0)
		{
			return -81343;
		}
		if (m_abort == true)
		{
			return -999919;
		}
		pDemuxOut = GetPinContainsName(pElecardPullDemux, "H264", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pDemuxOut == NULL)
		{
			timeOut--;
			Sleep(400);
			continue;
		}
		hr = pDemuxOut->QueryPinInfo(&pininfo);
		if (hr == S_OK)
			break;
	}



	CComPtr<IPin> pDecoderInPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_INPUT, &pDecoderInPin);
	hr = pDecoderInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -15;
	}


	CComPtr<IPin> pDecoderOutPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_OUTPUT, &pDecoderOutPin);
	hr = pDecoderOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}


	hr = m_pGraph->Connect(pDemuxOut, pDecoderInPin);
	if (FAILED(hr))
	{
		return -19;
	}


	hr = CreateVideoRenderer(m_selectedRenderer);
	if (FAILED(hr))
	{
		LogError("Failed to create VMR9");
		return hr;
	}

	hr = pGraph2->RenderEx(pDecoderOutPin, AM_RENDEREX_RENDERTOEXISTINGRENDERERS, NULL);


	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}

	return hr;



}

HRESULT DShowPlayer::InitializeBauotechDVRPlayer(HWND hwnd,
												const WCHAR *DvrFileName,
												float startTime,
												bool loop)
	 
{
	HRESULT hr = S_OK;
	m_hwndVideo = hwnd;
	PIN_INFO pininfo;


	// Create a new filter graph. (This also closes the old one, if any.)
	hr = InitializeGraph();
	if (hr != S_OK)
	{
		return -987654;
	}

	hr = m_pGraph->QueryInterface(IID_IFilterGraph2, (void**)&pGraph2);
	if (hr != S_OK)
		return -4;

 

	// {35525B8F-A479-4350-ACB8-3B39994C1A0C}
	static const GUID CLSID_CBoutechPushDVRSource =
	{ 0x35525b8f, 0xa479, 0x4350, { 0xac, 0xb8, 0x3b, 0x39, 0x99, 0x4c, 0x1a, 0xc } };

	 
	   
	hr = AddFilterByCLSID(m_pGraph, CLSID_CBoutechPushDVRSource, &pBauotechDVRSource, L"Bauotech DVR Source");
	if (FAILED(hr))
	{
		LogError("Failed to add Bauotech DVR Source");
		return -10;
	}

	// {E85FEC76-7949-4F63-B76A-46AD90FF384C}
	static const GUID IID_IBoutechPushDVRSource =
	{ 0xe85fec76, 0x7949, 0x4f63, { 0xb7, 0x6a, 0x46, 0xad, 0x90, 0xff, 0x38, 0x4c } };


	CComPtr<IBoutechPushDVRSource> pDvrSourceInterface;	 
	hr = pBauotechDVRSource->QueryInterface(IID_IBoutechPushDVRSource, (void**)&pDvrSourceInterface);
	if (FAILED(hr))
		return -634;

	pDvrSourceInterface->SetDVRListFileName((WCHAR *)DvrFileName);
	pDvrSourceInterface->SetLoop(loop);
	pDvrSourceInterface->SetStartTime(startTime);


	CComPtr<IPin> pDvrSourceOutPin;
	GetPin(pBauotechDVRSource, PIN_DIRECTION::PINDIR_OUTPUT, &pDvrSourceOutPin);
	hr = pDvrSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}

	// {668EE184-FD2D-4c72-8E79-439A35B438DE} 
	static const GUID CLSID_EMPGPDMX =
	{ 0x668ee184, 0xfd2d, 0x4c72,{ 0x8e, 0x79, 0x43, 0x9a, 0x35, 0xb4, 0x38, 0xde } };


	hr = AddFilterByCLSID(m_pGraph, CLSID_EMPGPDMX, &pElecardMpegPushDemux, L"Elecard Push Demux");
	if (FAILED(hr))
	{
		LogError("Failed to add Elecard Push Demux");
		return -10;
	}
	SetDemuxerLatency(1.5);


	if (m_selectedDecoder == SELECTED_DECODER::LEADTOOLS && SUCCEEDED(hr))
	{
		static const GUID CLSID_LEADTOOLS_H264_DECODER =
		{ 0xE2B7DF25,0x38C5,0x11D5,{ 0x91,0xF6, 0x00, 0x10, 0x4B , 0xDB ,0x8F , 0xF9 } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_LEADTOOLS_H264_DECODER, &pVideoDecoder, L"LeadTools AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add LeadTools AVC Video Decoder");
			return hr;
		}
	}
	else
		if (m_selectedDecoder == SELECTED_DECODER::ELECARD)
		{
			// {5C122C6D-8FCC-46f9-AAB7-DCFB0841E04D}
			static const GUID CLSID_EAVCDEC =
			{ 0x5c122c6D, 0x8fcc, 0x46f9,{ 0xaa, 0xb7, 0xdc, 0xfb, 0x8, 0x41, 0xe0, 0x4d } };

			hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Elecard AVC Video Decoder");
			if (FAILED(hr))
			{
				LogError("Failed to add Elecard AVC Video Decoder");
				return hr;
			}
			ActivateFilter(pVideoDecoder);
			ClearDecoderBufferForFastLatency();

		}
	if (m_selectedDecoder == SELECTED_DECODER::IMPAL)
	{

		static const GUID CLSID_EAVCDEC =
		{ 0x2379e92b , 0x3ffa , 0x4ce5 ,{ 0x85 , 0x91 , 0xf6 , 0xa4 , 0x8c , 0x57 , 0xef, 0x6a } };

		hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"Impleo AVC Video Decoder");
		if (FAILED(hr))
		{
			LogError("Failed to add Impleo AVC Video Decoder");
			return hr;
		}
	}
	else
	{
		if (m_selectedDecoder == SELECTED_DECODER::AVOBJECTS)
		{

			static const GUID CLSID_EAVCDEC =
			{ 0x4BD50AA0 , 0x6CE6 , 0x4CD2 ,{ 0x96 , 0xB6 , 0xFB, 0x5F, 0xE7, 0xDE, 0x0C, 0xF2} };


			hr = AddFilterByCLSID(m_pGraph, CLSID_EAVCDEC, &pVideoDecoder, L"AVObjects AVC Video Decoder");
			if (FAILED(hr))
			{
				LogError("Failed to add AVObjects AVC Video Decoder");
				return hr;
			}
		}
	}


	CComPtr<IPin> pSourceOutPin;
	GetPin(pBauotechDVRSource, PIN_DIRECTION::PINDIR_OUTPUT, &pSourceOutPin);
	hr = pSourceOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}


	CComPtr<IPin> ppDemuxIn;
	GetPin(pElecardMpegPushDemux, PIN_DIRECTION::PINDIR_INPUT, &ppDemuxIn);
	hr = ppDemuxIn->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -13;
	}
	 

	hr = m_pGraph->Connect(pDvrSourceOutPin, ppDemuxIn);
	if (FAILED(hr))
	{
		return -13;
	}

	CComPtr<IPin> pDemuxOut;
	hr = S_FALSE;
	int timeOut = 20;
	while (pDemuxOut == NULL && hr == S_FALSE)
	{
		if (m_abort == true)
		{
			return -999919;
		}
		pDemuxOut = GetPinContainsName(pElecardMpegPushDemux, "H264", PIN_DIRECTION::PINDIR_OUTPUT);
		if (pDemuxOut == NULL)
		{
			timeOut--;
			Sleep(400);
			continue;
		}
		hr = pDemuxOut->QueryPinInfo(&pininfo);
		if (hr == S_OK)
			break;
	}

	  

	CComPtr<IPin> pDecoderInPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_INPUT, &pDecoderInPin);
	hr = pDecoderInPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -15;
	}


	CComPtr<IPin> pDecoderOutPin;
	GetPin(pVideoDecoder, PIN_DIRECTION::PINDIR_OUTPUT, &pDecoderOutPin);
	hr = pDecoderOutPin->QueryPinInfo(&pininfo);
	if (FAILED(hr))
	{
		return -16;
	}


	hr = m_pGraph->Connect(pDemuxOut, pDecoderInPin);
	if (FAILED(hr))
	{
		return -19;
	}


	hr = CreateVideoRenderer(m_selectedRenderer);
	if (FAILED(hr))
	{
		LogError("Failed to create VMR9");
		return hr;
	}

	hr = pGraph2->RenderEx(pDecoderOutPin, AM_RENDEREX_RENDERTOEXISTINGRENDERERS, NULL);


	if (SUCCEEDED(hr))
	{
		m_state = STATE_STOPPED;
	}

	return hr;

}
