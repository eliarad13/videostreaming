#pragma once

// callback definition
typedef void (CALLBACK *MANAGED_SAMPLEPROCESSEDPROC)(BYTE* pData, long len, int height, int width, int stride);
typedef void (CALLBACK *MANAGED_INFORMMEDIATYPEPROC)(int height, int width, int size, int stride);
