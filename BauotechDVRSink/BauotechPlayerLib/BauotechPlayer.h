//////////////////////////////////////////////////////////////////////////
// DShowPlayer.h: Implements DirectShow playback functionality.
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
//////////////////////////////////////////////////////////////////////////

#pragma once

#ifndef WINVER				// Allow use of features specific to Windows 95 and Windows NT 4 or later.
#define WINVER 0x0A00		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif

#ifndef _WIN32_WINNT		// Allow use of features specific to Windows NT 4 or later.
#define _WIN32_WINNT 0x0600		// Change this to the appropriate value to target Windows 98 and Windows 2000 or later.
#endif						

#ifndef _WIN32_WINDOWS		// Allow use of features specific to Windows 98 or later.
#define _WIN32_WINDOWS 0x0410 // Change this to the appropriate value to target Windows Me or later.
#endif

#ifndef _WIN32_IE			// Allow use of features specific to IE 4.0 or later.
#define _WIN32_IE 0x0501	// Change this to the appropriate value to target IE 5.0 or later.
#endif

 
#define BAUTECH_LOCK_KEY   362826616

// Windows Header Files:
#include <windows.h>
#include <strsafe.h>
#include <dshow.h>
#include <atlbase.h>
#include "ModuleConfig_h.h"
#include "enwsplus.h" 
#include <string>
#include "AvCommon.h"
#include <atlstr.h>
#include <map>
#include "Overlay.h"
#include "video.h"
using namespace std;
typedef void(__stdcall * ProgressCallback)(char *rootDirectory, char *startDateTime, char *tsDir, char *tsFile, char *fileName, double lastPCR);

#define DEFAULT_FONT_NAME   TEXT("Impact\0")
#define DEFAULT_FONT_STYLE  TEXT("Regular\0")
#define DEFAULT_FONT_SIZE   20
#define DEFAULT_FONT_COLOR  RGB(255,0,0)
#define MAX_FONT_SIZE		30
#define TRANSPARENCY_VALUE   (0.6f)

MIDL_INTERFACE("3645E33E-25DE-4B6B-A377-B9486659C560")
IBoutechUDPMulticastSender: public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE ReconnectSocket(const WCHAR *IpAddress, const int port, bool any, const WCHAR *IpInterfaceAddress) PURE;
};

// ISampleGrabber
#ifdef __cplusplus
extern "C" {
#endif 
	

	DECLARE_INTERFACE_(ISampleGrabber, IUnknown) {
		STDMETHOD(RegisterCallback)(MANAGED_SAMPLEPROCESSEDPROC callback) PURE;
		STDMETHOD(Snapshot)(const WCHAR *fileName) PURE;
		STDMETHOD(GetSnapshot)(bool *snapshot) PURE;
	};
#ifdef __cplusplus
}
#endif 

 

MIDL_INTERFACE("9839D73E-9292-4F72-ABAD-27F657C7FCC8")
IBoutechUDPMulticastClientSourceEx : public IUnknown
{
	   
	virtual HRESULT STDMETHODCALLTYPE SetMulticastIpAddress(const WCHAR *IpAddress) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetMulticastPort(const int port) PURE;
	   
};


MIDL_INTERFACE("CF24BB08-37C8-44BD-9A12-683B118F290A")
IBoutechLiveSource: public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE SetIpAddress(WCHAR *IpAddress) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetPort(int port) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetPinType(int pinType) PURE;
};

////////

MIDL_INTERFACE("ACB60A35-4A4E-49B8-8FC1-271B34E7AAD8")
IBoutechDVRSink: public IUnknown
{
	virtual HRESULT STDMETHODCALLTYPE ConfigDvrSink(const WCHAR *rootDirectory, int LogTime, int maxFilesPerDirectory) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetDVRSinkCallback(ProgressCallback progressCallback) PURE;

};

 
MIDL_INTERFACE("E85FEC76-7949-4F63-B76A-46AD90FF384C")
IBoutechPushDVRSource : public IUnknown
{

	virtual HRESULT STDMETHODCALLTYPE SetDVRListFileName(WCHAR *fileName) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetPcrAheadTime(int time) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetLoop(bool loop) PURE;
	virtual HRESULT STDMETHODCALLTYPE SetStartTime(float time) PURE;

};




MIDL_INTERFACE("B6F36855-D861-4ADB-B76F-5F3CF52410AC")
IShapeSourceFilter : public IUnknown
{
public:

	 
#if 0 
	virtual HRESULT STDMETHODCALLTYPE Clear(void) PURE;
	virtual HRESULT STDMETHODCALLTYPE Visible(int id, bool visible) PURE;
	virtual HRESULT STDMETHODCALLTYPE AddLine(int id, int    x1, int   y1, int    x2, int    y2, COLORREF color, int width) PURE;
	virtual HRESULT STDMETHODCALLTYPE AddCircle(int id, int    x1, int   y1, int radios_w, int, COLORREF color, int width) PURE;
	virtual HRESULT STDMETHODCALLTYPE Close();
#endif 

};

// Include the v6 common controls in the manifest
#pragma comment(linker, \
    "\"/manifestdependency:type='Win32' "\
    "name='Microsoft.Windows.Common-Controls' "\
    "version='6.0.0.0' "\
    "processorArchitecture='*' "\
    "publicKeyToken='6595b64144ccf1df' "\
    "language='*'\"")

#ifndef SAFE_RELEASE
#define SAFE_RELEASE(x) { if (x) { x->Release(); x = NULL; } }
#endif

#ifndef SAFE_DELETE
#define SAFE_DELETE(x) { delete x; x = NULL; }
#endif




const long MIN_VOLUME = -10000;
const long MAX_VOLUME = 0;

enum PlaybackState
{
	STATE_RUNNING,
	STATE_PAUSED,
	STATE_STOPPED,
	STATE_CLOSED
};

typedef enum SELECTED_PULLDEMUX
{
	RENDER_DEMUX,
	ELECARD_PULL_DEMUX,
	IMPAL_PULL_DEMUX,
	LEADTOOLD_TS_DEMUX
} SELECTED_PULLDEMUX;

typedef enum SELECTED_DECODER
{
	RENDER,
	ELECARD,
	LEADTOOLS,
	IMPAL,
	AVOBJECTS,
	MICROSOFT_DTV
} SELECTED_DECODER;

enum  VIDEO_RENDER
{
	Try_EVR, Try_VMR9, Try_VMR7, Try_VMR_DEF
};

typedef enum ASPECT_RATIO_
{
	STREACH,
	PRESERVE

} ASPECT_RATIO;

struct GraphEventCallback
{
	virtual void OnGraphEvent(long eventCode, LONG_PTR param1, LONG_PTR param2) = 0;
};
 

class DShowPlayer
{
public:

	DShowPlayer();
	~DShowPlayer();
	 
	PlaybackState State() const { return m_state; }

	HRESULT InitializePlayer(HWND hwnd, int klvPID);
	HRESULT InitializeNetworkMulticastPlayer(HWND hwnd,
										    bool useBoutechReceover,
											const WCHAR *MulticastAddress,
											int MulticastPort,
											bool useInterface,
											float demuxLatency,
											int klvPID);

	HRESULT InitializeBoutechReceiverToWebRTC(const WCHAR * serverAddress,
											bool useTransport,
											int cameraPort,
											const WCHAR * webRtcURL);


	static void UnlockBautechPlayer(const long code);

	HRESULT InitializeBoutechReceiverToWebRTCAndHLS(const WCHAR * serverAddress,
													bool useTransport,
													int cameraPort,
													const WCHAR * webRtcURL,
													const WCHAR * HLSOutputPath,
													const WCHAR * HLSName,
													int HLSRelativeUrl,
													int HLSLive,
													int HLSPlaylistDuration,
													int HLSSegmentDuration,
													const WCHAR * HLSStreamName);



	HRESULT InitializeScreenCaptureToDump(const WCHAR* fileName,
		int screenWidth,
		int screenHeight,
		bool includeAudio,
		int bitrate);



	void SetDVRSinkCallback(ProgressCallback progressCallback);

	HRESULT InitializeNetwordReceiverToBoutechDVRSink(const WCHAR * rootDirectory,
													  const WCHAR * IpAddress,
												      const WCHAR * interfaceAddress,
													  int port,
												      bool useBoutechReceiver,
													  int maxTSChunks,
													  int maxFilesPerDirectory);

	HRESULT LoadPlayerFileSource(const WCHAR* sFileName);


	HRESULT InitializeBauotechDVRPlayer(HWND hwnd,
		const WCHAR *DvrFileName,
		float startTime,
		bool loop);

	HRESULT InitializeBauotechDVRPlayerAsync(HWND hwnd,
											const WCHAR *DvrFileName,
											float startTime,
											bool loop);

	HRESULT InitilizeNetworkVideoEsPlayer(HWND hwnd,
										  bool useBoutechReceiver,
										  const WCHAR *ServerAddress,
										  int ServerPort);



	int Snapshot(const WCHAR *fileName);

  
	void SetFileName(const WCHAR* sFileName);
	void SetWindowHandle(HWND hwnd);
	void SelectDecoder(SELECTED_DECODER selectedDecoder);
	void SelectRenderer(VIDEO_RENDER rend);
	void SelectPullDemux(SELECTED_PULLDEMUX selectedPullDemux);
	void SetAspectRatio(ASPECT_RATIO ratio, bool applyNow);

	// Streaming
	HRESULT Play();
	HRESULT Pause();
	HRESULT Stop();

	void Close();

	// VMR functionality
	BOOL    HasVideo() const;
	HRESULT UpdateVideoWindow(const LPRECT prc);
	HRESULT Repaint(HDC hdc);
	//HRESULT DisplayModeChanged();
	// events
	HRESULT HandleGraphEvent(GraphEventCallback *pCB);
	HRESULT SetRate(double rate);
 
	// seeking
	BOOL	CanSeek() const;
	HRESULT SetPosition(REFERENCE_TIME pos);
	HRESULT GetDuration(LONGLONG *pDuration);
	HRESULT GetCurrentPosition(LONGLONG *pTimeNow);
	void SetVideoWindow(HWND hwnd);
	HRESULT CreateVideoRenderer(VIDEO_RENDER render);
	HRESULT CreateVideoRenderer();
	HRESULT ActivateFilter(IBaseFilter *filter);
 
	HRESULT	StepByStep(long lFrames);
	HRESULT StepBackward(long lFrames = -1 , BOOL bSec  = FALSE );
	HRESULT StepForward(long lFrames  = 1 , BOOL bSec = FALSE);
	HRESULT	Step(long lFrames, BOOL bSec = FALSE);
	HRESULT SaveFrame(LPCTSTR tcsFileName);

	// Audio
	HRESULT	Mute(BOOL bMute);
	BOOL	IsMuted() const { return m_bMute; }
	HRESULT	SetVolume(long lVolume);
	long	GetVolume() const { return m_lVolume; }
	HRESULT SetEventWindow(HWND hwnd, UINT msg);
	HRESULT GetGraphEvent(long *param1, long *param2, long *evCode);
	HRESULT ApplyOverlay(float alpha_opacity);


	HRESULT AddTextOverlay(WCHAR* text, 
							int id,
							int    left,
							int    top,
							int    right,
							int    bottom,
							int	   color,
							float fontSize,
							int fontStyle) ;

  
	 HRESULT  AddTextOverlay2(WCHAR* text,
							  int id,
							  int    left,
							  int    top,
							  int    right,
							  int    bottom,
							  int color,
							  float fontSize);	


	 HRESULT Clear(void);
	 HRESULT Visible(int id, bool visible);
	 HRESULT AddLine(int id, int x1, int y1, int x2, int    y2, int color, int width);
	 HRESULT AddCircle(int id, int    x1, int   y1, int radios_w, int, int color, int width);
	 void DrawString(HDC hdc);
	 HRESULT BlendApplicationText(TCHAR *szNewText);


private:

	typedef enum
	{
		modeFastBckwrd = -1,
		modeNormal = 0,
		modeFastFrwrd = 1,

	} PlaybackModes;

	VIDEO_RENDER m_selectedRenderer;
	bool m_abort;
	HFONT g_hFont;
	HRESULT EnumerateDevices(REFGUID category, IEnumMoniker **ppEnum);
	HRESULT get_announces(ENWSource_Plus::announces_descr_ext **announces_descr);
	void DisplayDeviceInformation(IEnumMoniker *pEnum,  string CaptureDeviceName);
	HRESULT GetPin(IBaseFilter *pFilter, PIN_DIRECTION PinDir, IPin **ppPin, int num = 0);
	IPin *GetPinContainsName(IBaseFilter *Filter, CString PinName, PIN_DIRECTION  direction);
	IPin *GetPin(IBaseFilter *Filter, CString PinName, PIN_DIRECTION  direction);
	HRESULT InitializeGraph();
	void	TearDownGraph();
	HRESULT	RenderStreams(IBaseFilter *pSource);
	HRESULT UpdateVolume();
	void ConfigNWReneder(const WCHAR* IpAddress, int port, const WCHAR*interfaceIpAddress);

	HRESULT LogError(char *error);
	static long m_bauotechLockKey;
	CComPtr<IBaseFilter> pCap;
	CComPtr<IFileSourceFilter> pFS;
	CComPtr<IFilterGraph2> pGraph2;
	map<int, Overlay*> m_overlays;
	PlaybackState	m_state;
	HWND			m_hwndEvent;	// Window to receive events
	UINT			m_EventMsg;		// Windows message for graph events

	DWORD			m_seekCaps;		// Caps bits for IMediaSeeking
	SELECTED_DECODER m_selectedDecoder;
	HRESULT SetDemuxerLatency(float nLatency);
	HWND			m_hwndVideo;	// Video clipping window
	// Audio
    BOOL            m_bAudioStream; // Is there an audio stream?
	long			m_lVolume;		// Current volume (unless muted)
	BOOL			m_bMute;		// Volume muted?		
	REFERENCE_TIME  m_rt;
	REFERENCE_TIME			m_rtFastStep;
	REFERENCE_TIME			m_rtFastPosition;
	CComPtr<IMediaControl> pControl;

	CComPtr<IGraphBuilder> m_pGraph;
	CComPtr<IMediaControl> m_pControl;
	CComPtr<IMediaEventEx> m_pEvent;
	CComPtr<IMediaSeeking> m_pSeek;
	CComPtr<IBasicAudio> m_pAudio;

	CComPtr<IBaseFilter> pLeadToolsRTSPSource;
	
	CComPtr <IBaseFilter> pElecardHLSSink;
	
	CComPtr <IBaseFilter> pTCPServerPushSource;
	CComPtr <IBaseFilter> pTCPNetwordSender;
	CComPtr <IBaseFilter> pVideoEncoder;
	CComPtr <IBaseFilter> pDesktopCapture;
	CComPtr <IBaseFilter> pElecardMpegMux;
	CComPtr<IBaseFilter> pAudioRenderer;
	CComPtr<IBaseFilter> pVideoDecoder;
	CComPtr<IBaseFilter> pElecardMpegPushDemux;
	CComPtr<IBaseFilter> pBauotechDVRSource;
	 
	CComPtr<IBaseFilter> pAvObjectFilter;
	CComPtr<IModuleConfig>	m_nwsource_config;
	CComPtr<IBaseFilter> pSampleGrabberFilter;
	CComPtr<IBaseFilter> pDumpFilter;

	CComPtr<IBaseFilter> pBoutechInfinitePieFilter;
	 
	CComPtr<ISampleGrabber> m_pISampleGrabber;
	
	CComPtr<IBaseFilter> pMulticastSender;
	CComPtr<IBaseFilter> pNWRenderer;

	 
	CComPtr<IBaseFilter> pElecardPullDemux;
	
	CComPtr<IBaseFilter> pBauotechDVRSink;
	CComPtr<IBaseFilter> pNetworkSource;
	CComPtr<IBaseFilter> pInfTeeFilter;
	CComPtr<IBaseFilter> pSinkFilter;
	CComPtr<IBaseFilter> pAudioEncoder;
	CComPtr<IPin> pDumpInPin;
	BaseVideoRenderer *m_pVideo;
	CComPtr<IShapeSourceFilter>  pShapeFilterInterface;

	HRESULT AddDumpFilter(bool elecardDump);
	HRESULT ClearDecoderBufferForFastLatency();
	PlaybackModes			m_playbackMode;
	MANAGED_SAMPLEPROCESSEDPROC m_ManagedCallback;
	SELECTED_PULLDEMUX  m_selectedPullDemux;
	ASPECT_RATIO m_vmrAspectRatio;
};

HRESULT RemoveUnconnectedRenderer(IGraphBuilder *pGraph, IBaseFilter *pRenderer, BOOL *pbRemoved);
