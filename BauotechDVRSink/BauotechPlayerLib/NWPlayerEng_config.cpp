#include "StdAfx.h"
#include ".\NWPlayerEng_config.h"

//////////////////////////////////////////////////////////////////////////
//
//
//
//////////////////////////////////////////////////////////////////////////
CNWPlayerEng_config::CNWPlayerEng_config()
					: config_loaded(false)
					, start_config_from_reg(false)
{
	use_cmd_line_config = false;

	source_type			= source_NWSPlus;

	config_sdp			= L"";
	clock_mode			= clock_auto;
	m_split_window		= false;

	// SinkFilter settings
	sink_config.output_file_name = L"";
	sink_config.dump_mode	= 0;
	sink_config.crop_mode	= 0;
	sink_config.crop_size	= 1024;
	sink_config.create_index= false;
	sink_config.mpeg_ves	= true;
	sink_config.stream_id	= 224;
	sink_config.crete_index_start_from = 0;

	// RTSP NetSource filter configuration
	rtsp_config.server_url		= L"rtsp://localhost:554/sample.mpg";
	rtsp_config.compat			= 0;
	rtsp_config.lower_tranp		= 1;
	rtsp_config.deliver_transp	= 0;
	rtsp_config.client_port_from= 10202;
	rtsp_config.client_port_to	= 10203;

	// HLS Source filter configuration
	hls_config.url					= L"http://localhost:554/sample.m3u8";
	hls_config.connection_timeout	= 10;
	hls_config.data_timeout			= 10;
	hls_config.download_rate		= 0;
}

CNWPlayerEng_config::~CNWPlayerEng_config()
{
}

//
CNWPlayerEng_config &CNWPlayerEng_config::operator = (const CNWPlayerEng_config &_config)
{
	use_cmd_line_config = _config.use_cmd_line_config;

	start_config_from_reg = _config.start_config_from_reg;

	source_type			= _config.source_type;

	config_sdp			= _config.config_sdp;
	clock_mode			= _config.clock_mode;
	m_split_window		= _config.m_split_window;

	sink_config.output_file_name = _config.sink_config.output_file_name;
	sink_config.dump_mode	= _config.sink_config.dump_mode;
	sink_config.crop_mode	= _config.sink_config.crop_mode;
	sink_config.crop_size	= _config.sink_config.crop_size;
	sink_config.create_index= _config.sink_config.create_index;
	sink_config.mpeg_ves	= _config.sink_config.mpeg_ves;
	sink_config.stream_id	= _config.sink_config.stream_id;
	sink_config.crete_index_start_from = _config.sink_config.crete_index_start_from;

	rtsp_config.server_url		= _config.rtsp_config.server_url;
	rtsp_config.compat			= _config.rtsp_config.compat;
	rtsp_config.lower_tranp		= _config.rtsp_config.lower_tranp;
	rtsp_config.deliver_transp	= _config.rtsp_config.deliver_transp;
	rtsp_config.client_port_from= _config.rtsp_config.client_port_from;
	rtsp_config.client_port_to	= _config.rtsp_config.client_port_to;

	hls_config.url					= _config.hls_config.url;
	hls_config.connection_timeout	= _config.hls_config.connection_timeout;
	hls_config.data_timeout			= _config.hls_config.data_timeout;
	hls_config.download_rate		= _config.hls_config.download_rate;

	forward_config.a1			= _config.forward_config.a1;
	forward_config.a2			= _config.forward_config.a2;
	forward_config.a3			= _config.forward_config.a3;
	forward_config.v1			= _config.forward_config.v1;
	forward_config.v2			= _config.forward_config.v2;
	forward_config.use_forward	= _config.forward_config.use_forward;

	return *this;
}

//
HRESULT CNWPlayerEng_config::load_registry_config(CString key_name)
{
	config_loaded = false;
	ElUtils::CRegKey reg_edit;
	if (reg_edit.Open((HKEY)HKEY_CURRENT_USER, key_name) != S_OK)
		return E_FAIL;

	start_config_from_reg = (bool)reg_edit.GetDWORD(_T("start_config_from_reg"), false);

	source_type	= (_source_type)reg_edit.GetDWORD(_T("source_type"), source_NWSPlus);

	USES_CONVERSION;
    TCHAR tmp_sdp_buff[1024 * 50]; int buf_size = 1024 * 50;
	reg_edit.GetString(_T("config_sdp"), _T(""), tmp_sdp_buff, buf_size);
	config_sdp = L"";
    if (_tcslen(tmp_sdp_buff) > 0)
		config_sdp = tmp_sdp_buff;

	clock_mode		 = clock_auto;//(_clock_mode)reg_edit.GetDWORD("clock_mode", clock_auto);
	m_split_window	 = (bool)reg_edit.GetDWORD(_T("split_window"), 0);

	buf_size = 1024*50;
    reg_edit.GetString(_T("sink_config.output_file_name"), _T(""), tmp_sdp_buff, buf_size);
	sink_config.output_file_name = L"";
    if (_tcslen(tmp_sdp_buff) > 0)
		sink_config.output_file_name = tmp_sdp_buff;

	sink_config.dump_mode = reg_edit.GetDWORD(_T("sink_config.dump_mode"), 0);
	sink_config.crop_mode = reg_edit.GetDWORD(_T("sink_config.crop_mode"), 0);
	sink_config.crop_size = reg_edit.GetDWORD(_T("sink_config.crop_size"), 1024);
	sink_config.create_index= (bool)reg_edit.GetDWORD(_T("sink_config.create_index"), 0);
	sink_config.mpeg_ves	= (bool)reg_edit.GetDWORD(_T("sink_config.mpeg_ves"), 1);
	sink_config.stream_id	= reg_edit.GetDWORD(_T("sink_config.stream_id"), 224);
	sink_config.crete_index_start_from = reg_edit.GetDWORD(_T("sink_config.crete_index_start_from"), 0);

	buf_size = 1024*50;
    reg_edit.GetString(_T("rtsp_config.server_url"), _T(""), tmp_sdp_buff, buf_size);
	rtsp_config.server_url = L"";
    if (_tcslen(tmp_sdp_buff) > 0)
		rtsp_config.server_url = tmp_sdp_buff;
	rtsp_config.compat			= reg_edit.GetDWORD(_T("rtsp_config.compat"), 0);
	rtsp_config.lower_tranp		= reg_edit.GetDWORD(_T("rtsp_config.lower_tranp"), 1);
	rtsp_config.deliver_transp	= reg_edit.GetDWORD(_T("rtsp_config.deliver_transp"), 0);
	rtsp_config.client_port_from= reg_edit.GetDWORD(_T("rtsp_config.client_port_from"), 10202);
	rtsp_config.client_port_to	= reg_edit.GetDWORD(_T("rtsp_config.client_port_to"), 10203);

	buf_size = 1024*50;
	reg_edit.GetString(_T("hls_config.url"), _T(""), tmp_sdp_buff, buf_size);
	hls_config.url = L"";
    if (_tcslen(tmp_sdp_buff) > 0)
		hls_config.url = tmp_sdp_buff;
	hls_config.connection_timeout	= reg_edit.GetDWORD(_T("hls_config.connection_timeout"), 10);
	hls_config.data_timeout			= reg_edit.GetDWORD(_T("hls_config.data_timeout"), 10);
	hls_config.download_rate		= reg_edit.GetDWORD(_T("hls_config.download_rate"), 0);

	forward_config.use_forward = false;

	config_loaded = true;
	return S_OK;
}

//
HRESULT CNWPlayerEng_config::save_registry_config(CString key_name)
{
	ElUtils::CRegKey reg_edit;
	if (reg_edit.Create((HKEY)HKEY_CURRENT_USER, key_name) != S_OK)
		return E_FAIL;

    reg_edit.SetDWORD(_T("start_config_from_reg"), start_config_from_reg);

    reg_edit.SetDWORD(_T("source_type"), source_type);

	USES_CONVERSION;
	TCHAR tch[MAX_PATH*2];
    reg_edit.SetString(_T("config_sdp"), config_sdp.c_str());
    reg_edit.SetDWORD(_T("clock_mode"), clock_mode);
    reg_edit.SetDWORD(_T("split_window"), m_split_window);

    reg_edit.SetString(_T("sink_config.output_file_name"), sink_config.output_file_name.c_str());
    reg_edit.SetDWORD(_T("sink_config.dump_mode"), sink_config.dump_mode);
    reg_edit.SetDWORD(_T("sink_config.crop_mode"), sink_config.crop_mode);
    reg_edit.SetDWORD(_T("sink_config.crop_size"), sink_config.crop_size);
    reg_edit.SetDWORD(_T("sink_config.create_index"), sink_config.create_index);
    reg_edit.SetDWORD(_T("sink_config.mpeg_ves"), sink_config.mpeg_ves);
    reg_edit.SetDWORD(_T("sink_config.stream_id"), sink_config.stream_id);
    reg_edit.SetDWORD(_T("sink_config.crete_index_start_from"), sink_config.crete_index_start_from);

    reg_edit.SetString(_T("rtsp_config.server_url"), GetTCHAR(rtsp_config.server_url.c_str(), tch));
    reg_edit.SetDWORD(_T("rtsp_config.compat"), rtsp_config.compat);
    reg_edit.SetDWORD(_T("rtsp_config.lower_tranp"), rtsp_config.lower_tranp);
    reg_edit.SetDWORD(_T("rtsp_config.deliver_transp"), rtsp_config.deliver_transp);
    reg_edit.SetDWORD(_T("rtsp_config.client_port_from"), rtsp_config.client_port_from);
    reg_edit.SetDWORD(_T("rtsp_config.client_port_to"), rtsp_config.client_port_to);

    reg_edit.SetString(_T("hls_config.server_url"), GetTCHAR(hls_config.url.c_str(), tch));
    reg_edit.SetDWORD(_T("hls_config.connection_timeout"), hls_config.connection_timeout);
    reg_edit.SetDWORD(_T("hls_config.data_timeout"), hls_config.data_timeout);
    reg_edit.SetDWORD(_T("hls_config.download_rate"), hls_config.download_rate);

	return S_OK;
}