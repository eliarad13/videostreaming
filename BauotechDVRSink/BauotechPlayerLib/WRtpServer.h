

/* this ALWAYS GENERATED file contains the definitions for the interfaces */


 /* File created by MIDL compiler version 7.00.0555 */
/* at Thu Dec 20 12:35:02 2018
 */
/* Compiler settings for WRtpServer.idl:
    Oicf, W1, Zp8, env=Win64 (32b run), target_arch=AMD64 7.00.0555 
    protocol : dce , ms_ext, c_ext, robust
    error checks: allocation ref bounds_check enum stub_data 
    VC __declspec() decoration level: 
         __declspec(uuid()), __declspec(selectany), __declspec(novtable)
         DECLSPEC_UUID(), MIDL_INTERFACE()
*/
/* @@MIDL_FILE_HEADING(  ) */

#pragma warning( disable: 4049 )  /* more than 64k source lines */


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 475
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __WRtpServer_h__
#define __WRtpServer_h__

#if defined(_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif

/* Forward Declarations */ 

#ifndef __IAVOWRtpServer_FWD_DEFINED__
#define __IAVOWRtpServer_FWD_DEFINED__
typedef interface IAVOWRtpServer IAVOWRtpServer;
#endif 	/* __IAVOWRtpServer_FWD_DEFINED__ */


#ifndef __IAVVersion_FWD_DEFINED__
#define __IAVVersion_FWD_DEFINED__
typedef interface IAVVersion IAVVersion;
#endif 	/* __IAVVersion_FWD_DEFINED__ */


#ifndef __CoWRtpServer_FWD_DEFINED__
#define __CoWRtpServer_FWD_DEFINED__

#ifdef __cplusplus
typedef class CoWRtpServer CoWRtpServer;
#else
typedef struct CoWRtpServer CoWRtpServer;
#endif /* __cplusplus */

#endif 	/* __CoWRtpServer_FWD_DEFINED__ */


#ifndef __PPWRtpServer_FWD_DEFINED__
#define __PPWRtpServer_FWD_DEFINED__

#ifdef __cplusplus
typedef class PPWRtpServer PPWRtpServer;
#else
typedef struct PPWRtpServer PPWRtpServer;
#endif /* __cplusplus */

#endif 	/* __PPWRtpServer_FWD_DEFINED__ */


#ifndef __AboutWRtpServer_FWD_DEFINED__
#define __AboutWRtpServer_FWD_DEFINED__

#ifdef __cplusplus
typedef class AboutWRtpServer AboutWRtpServer;
#else
typedef struct AboutWRtpServer AboutWRtpServer;
#endif /* __cplusplus */

#endif 	/* __AboutWRtpServer_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

#ifdef __cplusplus
extern "C"{
#endif 


/* interface __MIDL_itf_WRtpServer_0000_0000 */
/* [local] */ 

#pragma once
#pragma once
EXTERN_C void CALLBACK SetMeritW(HWND hWnd, HINSTANCE hInstance, LPWSTR lpwCmdLine, int nCmdShow);
typedef void (CALLBACK SETMERITW)(HWND hWnd, HINSTANCE hInstance, LPWSTR lpwCmdLine, int nCmdShow);
#define EC_KEYFRAME_NEEDED 0x8000
#define EC_CONNECTIONS_CHANGED 0x8001
typedef struct tagRtpStats
    {
    DWORD cConnections;
    LONGLONG llPacks;
    LONGLONG llBytes;
    } 	RTP_STATS;



extern RPC_IF_HANDLE __MIDL_itf_WRtpServer_0000_0000_v0_0_c_ifspec;
extern RPC_IF_HANDLE __MIDL_itf_WRtpServer_0000_0000_v0_0_s_ifspec;

#ifndef __IAVOWRtpServer_INTERFACE_DEFINED__
#define __IAVOWRtpServer_INTERFACE_DEFINED__

/* interface IAVOWRtpServer */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IAVOWRtpServer;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("6AC926BE-81D7-49C4-8F73-460D767921B1")
    IAVOWRtpServer : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetUrl( 
            /* [in] */ BSTR bsUrlPrefix) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetUrl( 
            /* [out] */ BSTR *pbsUrlPrefix) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetICEServersCount( 
            /* [out] */ DWORD *pnCount) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE AddICEServer( 
            /* [in] */ BSTR bsURI,
            /* [in] */ BSTR bsUser,
            /* [in] */ BSTR bsPassword) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetICEServer( 
            /* [in] */ DWORD nIdx,
            /* [out] */ BSTR *pbsURI,
            /* [out] */ BSTR *pbsUser,
            /* [out] */ BSTR *pbsPassword) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE RemoveICEServer( 
            /* [in] */ DWORD nIdx) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE GetRtpStats( 
            /* [out] */ RTP_STATS *pStats) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAVOWRtpServerVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAVOWRtpServer * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAVOWRtpServer * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAVOWRtpServer * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *SetUrl )( 
            IAVOWRtpServer * This,
            /* [in] */ BSTR bsUrlPrefix);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetUrl )( 
            IAVOWRtpServer * This,
            /* [out] */ BSTR *pbsUrlPrefix);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetICEServersCount )( 
            IAVOWRtpServer * This,
            /* [out] */ DWORD *pnCount);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *AddICEServer )( 
            IAVOWRtpServer * This,
            /* [in] */ BSTR bsURI,
            /* [in] */ BSTR bsUser,
            /* [in] */ BSTR bsPassword);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetICEServer )( 
            IAVOWRtpServer * This,
            /* [in] */ DWORD nIdx,
            /* [out] */ BSTR *pbsURI,
            /* [out] */ BSTR *pbsUser,
            /* [out] */ BSTR *pbsPassword);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *RemoveICEServer )( 
            IAVOWRtpServer * This,
            /* [in] */ DWORD nIdx);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *GetRtpStats )( 
            IAVOWRtpServer * This,
            /* [out] */ RTP_STATS *pStats);
        
        END_INTERFACE
    } IAVOWRtpServerVtbl;

    interface IAVOWRtpServer
    {
        CONST_VTBL struct IAVOWRtpServerVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAVOWRtpServer_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAVOWRtpServer_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAVOWRtpServer_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAVOWRtpServer_SetUrl(This,bsUrlPrefix)	\
    ( (This)->lpVtbl -> SetUrl(This,bsUrlPrefix) ) 

#define IAVOWRtpServer_GetUrl(This,pbsUrlPrefix)	\
    ( (This)->lpVtbl -> GetUrl(This,pbsUrlPrefix) ) 

#define IAVOWRtpServer_GetICEServersCount(This,pnCount)	\
    ( (This)->lpVtbl -> GetICEServersCount(This,pnCount) ) 

#define IAVOWRtpServer_AddICEServer(This,bsURI,bsUser,bsPassword)	\
    ( (This)->lpVtbl -> AddICEServer(This,bsURI,bsUser,bsPassword) ) 

#define IAVOWRtpServer_GetICEServer(This,nIdx,pbsURI,pbsUser,pbsPassword)	\
    ( (This)->lpVtbl -> GetICEServer(This,nIdx,pbsURI,pbsUser,pbsPassword) ) 

#define IAVOWRtpServer_RemoveICEServer(This,nIdx)	\
    ( (This)->lpVtbl -> RemoveICEServer(This,nIdx) ) 

#define IAVOWRtpServer_GetRtpStats(This,pStats)	\
    ( (This)->lpVtbl -> GetRtpStats(This,pStats) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAVOWRtpServer_INTERFACE_DEFINED__ */


#ifndef __IAVVersion_INTERFACE_DEFINED__
#define __IAVVersion_INTERFACE_DEFINED__

/* interface IAVVersion */
/* [object][helpstring][unique][version][uuid] */ 

typedef 
enum tagAV_Ver_Type
    {	eAV_Ver_Debug	= 0,
	eAV_Ver_Release	= ( eAV_Ver_Debug + 1 ) 
    } 	AV_Ver_Type;

typedef 
enum tagAV_Ver_OS
    {	eAV_Ver_win32	= 0,
	eAV_Ver_x64	= ( eAV_Ver_win32 + 1 ) 
    } 	AV_Ver_OS;


EXTERN_C const IID IID_IAVVersion;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("44B8DE04-B24C-4523-899D-295F013E5958")
    IAVVersion : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VerType( 
            /* [out] */ AV_Ver_Type *peType) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VerOS( 
            /* [out] */ AV_Ver_OS *peOS) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VerName( 
            /* [out] */ BSTR *pbsName) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VerNumber( 
            /* [out] */ BSTR *pbsNumber) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE VerCreated( 
            /* [out] */ BSTR *pbsDateTime) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IAVVersionVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE *QueryInterface )( 
            IAVVersion * This,
            /* [in] */ REFIID riid,
            /* [annotation][iid_is][out] */ 
            __RPC__deref_out  void **ppvObject);
        
        ULONG ( STDMETHODCALLTYPE *AddRef )( 
            IAVVersion * This);
        
        ULONG ( STDMETHODCALLTYPE *Release )( 
            IAVVersion * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VerType )( 
            IAVVersion * This,
            /* [out] */ AV_Ver_Type *peType);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VerOS )( 
            IAVVersion * This,
            /* [out] */ AV_Ver_OS *peOS);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VerName )( 
            IAVVersion * This,
            /* [out] */ BSTR *pbsName);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VerNumber )( 
            IAVVersion * This,
            /* [out] */ BSTR *pbsNumber);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE *VerCreated )( 
            IAVVersion * This,
            /* [out] */ BSTR *pbsDateTime);
        
        END_INTERFACE
    } IAVVersionVtbl;

    interface IAVVersion
    {
        CONST_VTBL struct IAVVersionVtbl *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IAVVersion_QueryInterface(This,riid,ppvObject)	\
    ( (This)->lpVtbl -> QueryInterface(This,riid,ppvObject) ) 

#define IAVVersion_AddRef(This)	\
    ( (This)->lpVtbl -> AddRef(This) ) 

#define IAVVersion_Release(This)	\
    ( (This)->lpVtbl -> Release(This) ) 


#define IAVVersion_VerType(This,peType)	\
    ( (This)->lpVtbl -> VerType(This,peType) ) 

#define IAVVersion_VerOS(This,peOS)	\
    ( (This)->lpVtbl -> VerOS(This,peOS) ) 

#define IAVVersion_VerName(This,pbsName)	\
    ( (This)->lpVtbl -> VerName(This,pbsName) ) 

#define IAVVersion_VerNumber(This,pbsNumber)	\
    ( (This)->lpVtbl -> VerNumber(This,pbsNumber) ) 

#define IAVVersion_VerCreated(This,pbsDateTime)	\
    ( (This)->lpVtbl -> VerCreated(This,pbsDateTime) ) 

#endif /* COBJMACROS */


#endif 	/* C style interface */




#endif 	/* __IAVVersion_INTERFACE_DEFINED__ */



#ifndef __WRtpServerLib_LIBRARY_DEFINED__
#define __WRtpServerLib_LIBRARY_DEFINED__

/* library WRtpServerLib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_WRtpServerLib;

EXTERN_C const CLSID CLSID_CoWRtpServer;

#ifdef __cplusplus

class DECLSPEC_UUID("BC3EAA01-2C36-4FED-88F5-4F2BD0A8E848")
CoWRtpServer;
#endif

EXTERN_C const CLSID CLSID_PPWRtpServer;

#ifdef __cplusplus

class DECLSPEC_UUID("BC3EAA02-2C36-4FED-88F5-4F2BD0A8E848")
PPWRtpServer;
#endif

EXTERN_C const CLSID CLSID_AboutWRtpServer;

#ifdef __cplusplus

class DECLSPEC_UUID("BC3EAA03-2C36-4FED-88F5-4F2BD0A8E848")
AboutWRtpServer;
#endif
#endif /* __WRtpServerLib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long *, unsigned long            , BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserMarshal(  unsigned long *, unsigned char *, BSTR * ); 
unsigned char * __RPC_USER  BSTR_UserUnmarshal(unsigned long *, unsigned char *, BSTR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long *, BSTR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif


