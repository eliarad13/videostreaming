//////////////////////////////////////////////////////////////////////////
// 
// THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF
// ANY KIND, EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO
// THE IMPLIED WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A
// PARTICULAR PURPOSE.
//
// Copyright (c) Microsoft Corporation. All rights reserved.
//
//////////////////////////////////////////////////////////////////////////

#include "BauotechPlayer.h"
#include "DshowUtil.h"
#include <Mfidl.h>

HRESULT InitializeEVR(IBaseFilter *pEVR, HWND hwnd, int vmrAspectRatio, IMFVideoDisplayControl ** ppWc);
HRESULT InitWindowlessVMR9(CComPtr<IBaseFilter>, HWND hwnd, int vmrAspectRatio, IVMRWindowlessControl9 ** ppWc);
HRESULT InitWindowlessVMR(IBaseFilter *pVMR, HWND hwnd, int vmrAspectRatio, IVMRWindowlessControl** ppWc);

/// VMR-7 Wrapper

VMR7::VMR7() : m_pWindowless(NULL)
{

}
 
VMR7::~VMR7()
{
	SAFE_RELEASE(m_pWindowless);
}

HRESULT VMR7::SetAspectRatioMode(int vmrAspectRatio)
{
	return m_pWindowless->SetAspectRatioMode(vmrAspectRatio);
}

HRESULT VMR7::SaveFrame(LPCTSTR tcsFileName)
{
	if (m_pWindowless == NULL)
		return E_FAIL;

	HRESULT hr = S_OK;
	BYTE* pDib = NULL;

	if (SUCCEEDED(hr = m_pWindowless->GetCurrentImage(&pDib)))
	{
		BITMAPINFOHEADER* bmiHeader = (BITMAPINFOHEADER*)pDib;
		long ulSize = bmiHeader->biSize + bmiHeader->biSizeImage;

		//Save to file
		TCHAR drive[_MAX_DRIVE];
		TCHAR dir[_MAX_DIR];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];

		_tsplitpath_s(tcsFileName, drive, dir, fname, ext);

		TCHAR* pDirName = new TCHAR[_MAX_PATH];
		_tcscpy_s(pDirName, _MAX_PATH, drive);
		_tcscat_s(pDirName, _MAX_PATH, dir);

		if (_tccmp(pDirName, L"") != 0)
		{
			if (!::PathFileExists(pDirName))
			{
				if (!::CreateDirectory(pDirName, NULL))
				{
					CoTaskMemFree(pDib);
					return E_FAIL;
				}
			}
			delete[] pDirName;
		}

		HANDLE hFile = ::CreateFile(tcsFileName, GENERIC_WRITE,              // open for writing 
			FILE_SHARE_WRITE,
			NULL,                      // default security 
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,     // normal file 
			NULL);                     // no attr. template 

		if (hFile == INVALID_HANDLE_VALUE)
		{
			CoTaskMemFree(pDib);
			return E_FAIL;
		}

		// Write file header
		BITMAPFILEHEADER bmpfilehdr = { 0x4D42,
			(DWORD)(sizeof(BITMAPFILEHEADER) +
				bmiHeader->biSize + bmiHeader->biClrUsed
				* sizeof(RGBQUAD) + bmiHeader->biSizeImage),
			0, 0,
			(DWORD) sizeof(BITMAPFILEHEADER) +
			bmiHeader->biSize + bmiHeader->biClrUsed
			* sizeof(RGBQUAD) };

		DWORD dwWritten = 0;
		WriteFile(hFile, &bmpfilehdr, sizeof(BITMAPFILEHEADER), &dwWritten, 0);
		WriteFile(hFile, pDib, ulSize, &dwWritten, 0);
		CloseHandle(hFile);
		CoTaskMemFree(pDib);
	}

	return hr;
}

HRESULT VMR7::AddToGraph(IGraphBuilder *pGraph, HWND hwnd, int vmrAspectRatio)
{
	HRESULT hr = S_OK;

	IBaseFilter *pVMR = NULL;

	hr = AddFilterByCLSID(pGraph, CLSID_VideoMixingRenderer, &pVMR, L"VMR-7");

	// Set windowless mode on the VMR. This must be done before the VMR is connected.
	if (SUCCEEDED(hr))
	{
		hr = InitWindowlessVMR(pVMR, hwnd, vmrAspectRatio, &m_pWindowless);
	}

	return hr;
}


HRESULT VMR7::FinalizeGraph(IGraphBuilder *pGraph)
{
	if (m_pWindowless == NULL)
	{
		return S_OK;
	}

	HRESULT hr = S_OK;
	BOOL bRemoved = FALSE;

	IBaseFilter *pFilter = NULL;

	hr = m_pWindowless->QueryInterface(__uuidof(IBaseFilter), (void**)&pFilter);

	if (SUCCEEDED(hr))
	{
		hr = RemoveUnconnectedRenderer(pGraph, pFilter, &bRemoved);

		// If we removed the VMR, then we also need to release our 
		// pointer to the VMR's windowless control interface.
		if (bRemoved)
		{
			SAFE_RELEASE(m_pWindowless);
		}
	}

	SAFE_RELEASE(pFilter);
	return hr;
}

HRESULT VMR7::UpdateVideoWindow(HWND hwnd, const LPRECT prc)
{
	HRESULT hr = S_OK;

	if (m_pWindowless == NULL)
	{
		return S_OK; // no-op
	}

	if (prc)
	{
		hr = m_pWindowless->SetVideoPosition(NULL, prc);
	}
	else
	{

		RECT rc;
		GetClientRect(hwnd, &rc);
		hr = m_pWindowless->SetVideoPosition(NULL, &rc);
	}
	return hr;
}

HRESULT VMR7::Repaint(HWND hwnd, HDC hdc)
{
	HRESULT hr = S_OK;

	if (m_pWindowless)
	{
		hr = m_pWindowless->RepaintVideo(hwnd, hdc);
	}
	return hr;
}

HRESULT VMR7::DisplayModeChanged()
{
	HRESULT hr = S_OK;

	if (m_pWindowless)
	{
		hr = m_pWindowless->DisplayModeChanged();
	}
	return hr;


}
void VMR7::SetVideoWindow(HWND hwnd)
{
	HRESULT hr = S_OK;
	//hr = m_pVideoDisplay->SetVideoWindow(hwnd);

}

IVMRMixerBitmap9  *pBmp = NULL;
//-----------------------------------------------------------------------------
// InitWindowlessVMR
// Initialize the VMR-7 for windowless mode. 
//-----------------------------------------------------------------------------

HRESULT InitWindowlessVMR(
	IBaseFilter *pVMR,				// Pointer to the VMR
	HWND hwnd,						// Clipping window
	int vmrAspectRatio,
	IVMRWindowlessControl** ppWC	// Receives a pointer to the VMR.
)
{

	IVMRFilterConfig* pConfig = NULL;
	IVMRWindowlessControl *pWC = NULL;

	HRESULT hr = S_OK;

	// Set the rendering mode.  
	hr = pVMR->QueryInterface(IID_IVMRFilterConfig, (void**)&pConfig);
	if (SUCCEEDED(hr))
	{
		hr = pConfig->SetRenderingMode(VMRMode_Windowless);
	}
	 

	// Query for the windowless control interface.
	if (SUCCEEDED(hr))
	{
		hr = pVMR->QueryInterface(IID_IVMRWindowlessControl, (void**)&pWC);
	}

	// Set the clipping window.
	if (SUCCEEDED(hr))
	{
		hr = pWC->SetVideoClippingWindow(hwnd);
	}

	// Preserve aspect ratio by letter-boxing
	if (SUCCEEDED(hr))
	{
		//hr = pWC->SetAspectRatioMode(VMR_ARMODE_LETTER_BOX);
		hr = pWC->SetAspectRatioMode(vmrAspectRatio);
	}

	// Return the IVMRWindowlessControl pointer to the caller.
	if (SUCCEEDED(hr))
	{
		*ppWC = pWC;
		(*ppWC)->AddRef();
	}

	SAFE_RELEASE(pConfig);
	SAFE_RELEASE(pWC);

	return hr;
}


/// VMR-9 Wrapper

VMR9::VMR9() : m_pWindowless(NULL)
{

}


VMR9::~VMR9()
{
	SAFE_RELEASE(m_pWindowless);
}

HRESULT VMR9::SetAspectRatioMode(int vmrAspectRatio)
{
	return m_pWindowless->SetAspectRatioMode(vmrAspectRatio);
}

HRESULT VMR9::AddToGraph(IGraphBuilder *pGraph, HWND hwnd, int vmrAspectRatio)
{
	HRESULT hr = S_OK;

	CComPtr<IBaseFilter> pVMR = NULL;

	hr = AddFilterByCLSID(pGraph, CLSID_VideoMixingRenderer9, &pVMR, L"VMR-9");

	// Set windowless mode on the VMR. This must be done before the VMR is connected.
	if (SUCCEEDED(hr))
	{
		hr = InitWindowlessVMR9(pVMR, hwnd, vmrAspectRatio, &m_pWindowless);
	}
	 

	return hr;
}

HRESULT VMR9::SaveFrame(LPCTSTR tcsFileName)
{
	if (m_pWindowless == NULL)
		return E_FAIL;

	HRESULT hr = S_OK;
	BYTE* pDib = NULL;

	if (SUCCEEDED(hr = m_pWindowless->GetCurrentImage(&pDib)))
	{
		BITMAPINFOHEADER* bmiHeader = (BITMAPINFOHEADER*)pDib;
		long ulSize = bmiHeader->biSize + bmiHeader->biSizeImage;

		//Save to file
		TCHAR drive[_MAX_DRIVE];
		TCHAR dir[_MAX_DIR];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];

		_tsplitpath_s(tcsFileName, drive, dir, fname, ext);

		TCHAR* pDirName = new TCHAR[_MAX_PATH];
		_tcscpy_s(pDirName, _MAX_PATH, drive);
		_tcscat_s(pDirName, _MAX_PATH, dir);

		if (_tccmp(pDirName, L"") != 0)
		{
			if (!::PathFileExists(pDirName))
			{
				if (!::CreateDirectory(pDirName, NULL))
				{
					CoTaskMemFree(pDib);
					return E_FAIL;
				}
			}
			delete[] pDirName;
		}

		HANDLE hFile = ::CreateFile(tcsFileName, GENERIC_WRITE,              // open for writing 
			FILE_SHARE_WRITE,
			NULL,                      // default security 
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,     // normal file 
			NULL);                     // no attr. template 

		if (hFile == INVALID_HANDLE_VALUE)
		{
			CoTaskMemFree(pDib);
			return E_FAIL;
		}

		// Write file header
		BITMAPFILEHEADER bmpfilehdr = { 0x4D42,
			(DWORD)(sizeof(BITMAPFILEHEADER) +
				bmiHeader->biSize + bmiHeader->biClrUsed
				* sizeof(RGBQUAD) + bmiHeader->biSizeImage),
			0, 0,
			(DWORD) sizeof(BITMAPFILEHEADER) +
			bmiHeader->biSize + bmiHeader->biClrUsed
			* sizeof(RGBQUAD) };

		DWORD dwWritten = 0;
		WriteFile(hFile, &bmpfilehdr, sizeof(BITMAPFILEHEADER), &dwWritten, 0);
		WriteFile(hFile, pDib, ulSize, &dwWritten, 0);
		CloseHandle(hFile);
		CoTaskMemFree(pDib);
	}

	return hr;
}


HRESULT VMR9::FinalizeGraph(IGraphBuilder *pGraph)
{
	if (m_pWindowless == NULL)
	{
		return S_OK;
	}

	HRESULT hr = S_OK;
	BOOL bRemoved = FALSE;

	IBaseFilter *pFilter = NULL;

	hr = m_pWindowless->QueryInterface(__uuidof(IBaseFilter), (void**)&pFilter);

	if (SUCCEEDED(hr))
	{
		hr = RemoveUnconnectedRenderer(pGraph, pFilter, &bRemoved);

		// If we removed the VMR, then we also need to release our 
		// pointer to the VMR's windowless control interface.
		if (bRemoved)
		{
			SAFE_RELEASE(m_pWindowless);
		}
	}

	SAFE_RELEASE(pFilter);
	return hr;
}




HRESULT VMR9::UpdateVideoWindow(HWND hwnd, const LPRECT prc)
{
	HRESULT hr = S_OK;

	if (m_pWindowless == NULL)
	{
		return S_OK; // no-op
	}

	if (prc)
	{
		hr = m_pWindowless->SetVideoPosition(NULL, prc);
	}
	else
	{

		RECT rc;
		GetClientRect(hwnd, &rc);
		hr = m_pWindowless->SetVideoPosition(NULL, &rc);
	}
	return hr;
}
void VMR9::SetVideoWindow(HWND hwnd)
{
	HRESULT hr = S_OK;
	m_pWindowless->SetVideoClippingWindow(hwnd);

}

HRESULT VMR9::Repaint(HWND hwnd, HDC hdc)
{
	HRESULT hr = S_OK;

	if (m_pWindowless)
	{
		hr = m_pWindowless->RepaintVideo(hwnd, hdc);
	}
	return hr;
}

HRESULT VMR9::DisplayModeChanged()
{
	HRESULT hr = S_OK;

	if (m_pWindowless)
	{
		hr = m_pWindowless->DisplayModeChanged();
	}
	return hr;


}
IVMRWindowlessControl9 *m_pWC = NULL;

//-----------------------------------------------------------------------------
// InitWindowlessVMR9
// Initialize the VMR-9 for windowless mode. 
//-----------------------------------------------------------------------------



HRESULT InitWindowlessVMR9(
	CComPtr<IBaseFilter> pVMR,		// Pointer to the VMR
	HWND hwnd,						// Clipping window
	int vmrAspectRatio,
	IVMRWindowlessControl9** ppWC	// Receives a pointer to the VMR.
)
{

	CComPtr<IVMRFilterConfig9>  pConfig = NULL;
	IVMRWindowlessControl9 *pWC = NULL;

	HRESULT hr = S_OK;

	// Set the rendering mode.  
	hr = pVMR->QueryInterface(IID_IVMRFilterConfig9, (void**)&pConfig);
	if (SUCCEEDED(hr))
	{
		hr = pConfig->SetRenderingMode(VMR9Mode_Windowless);
	}


	// Query for the windowless control interface.
	if (SUCCEEDED(hr))
	{
		hr = pVMR->QueryInterface(IID_IVMRWindowlessControl9, (void**)&pWC);
	}
	m_pWC = pWC;

	hr = pVMR->QueryInterface(IID_IVMRMixerBitmap9, (void**)&pBmp);	 

	// Set the clipping window.
	if (SUCCEEDED(hr))
	{
		hr = pWC->SetVideoClippingWindow(hwnd);
	}

	// Preserve aspect ratio by letter-boxing
	if (SUCCEEDED(hr))
	{
		hr = pWC->SetAspectRatioMode(vmrAspectRatio);
	}

	// Return the IVMRWindowlessControl pointer to the caller.
	if (SUCCEEDED(hr))
	{
		*ppWC = pWC;
		(*ppWC)->AddRef();
	}

	//SAFE_RELEASE(pConfig);
	SAFE_RELEASE(pWC);

	return hr;
}
 

/// EVR Wrapper

EVR::EVR() : m_pEVR(NULL), m_pVideoDisplay(NULL)
{

}
HRESULT EVR::SetAspectRatioMode(int vmrAspectRatio)
{
	return m_pVideoDisplay->SetAspectRatioMode(vmrAspectRatio);
}

EVR::~EVR()
{
	SAFE_RELEASE(m_pEVR);
	SAFE_RELEASE(m_pVideoDisplay);
}

void EVR::SetVideoWindow(HWND hwnd)
{
	HRESULT hr = S_OK;
	hr = m_pVideoDisplay->SetVideoWindow(hwnd);
}

HRESULT EVR::SaveFrame(LPCTSTR tcsFileName)
{
	if (m_pVideoDisplay == NULL)
		return E_FAIL;

	HRESULT hr = S_OK;

	PVOID pData = NULL;
	BITMAPINFOHEADER bmiHeader = { 0 };
	bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	BYTE* pDib = NULL;
	DWORD cbDib = 0;

	if (SUCCEEDED(hr = m_pVideoDisplay->GetCurrentImage(&bmiHeader, &pDib, &cbDib, NULL)))
	{
		long ulSize = bmiHeader.biSize + cbDib;

		pData = CoTaskMemAlloc(ulSize);
		ZeroMemory(pData, ulSize);
		memcpy(pData, &bmiHeader, bmiHeader.biSize);
		memcpy((PBYTE)pData + bmiHeader.biSize, pDib, cbDib);

		CoTaskMemFree(pDib);

		//Save to file
		TCHAR drive[_MAX_DRIVE];
		TCHAR dir[_MAX_DIR];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];

		_tsplitpath_s(tcsFileName, drive, dir, fname, ext);

		TCHAR* pDirName = new TCHAR[_MAX_PATH];
		_tcscpy_s(pDirName, _MAX_PATH, drive);
		_tcscat_s(pDirName, _MAX_PATH, dir);

		if (_tccmp(pDirName, L"") != 0)
		{
			if (!::PathFileExists(pDirName))
			{
				if (!::CreateDirectory(pDirName, NULL))
				{
					CoTaskMemFree(pData);
					return E_FAIL;
				}
			}
			delete[] pDirName;
		}

		HANDLE hFile = ::CreateFile(tcsFileName, GENERIC_WRITE,              // open for writing 
			FILE_SHARE_WRITE,
			NULL,                      // default security 
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,     // normal file 
			NULL);                     // no attr. template 

		if (hFile == INVALID_HANDLE_VALUE)
		{
			CoTaskMemFree(pData);
			return E_FAIL;
		}

		// Write file header
		BITMAPFILEHEADER bmpfilehdr = { 0x4D42,
			(DWORD)(sizeof(BITMAPFILEHEADER) +
				bmiHeader.biSize + bmiHeader.biClrUsed
				* sizeof(RGBQUAD) + bmiHeader.biSizeImage),
			0, 0,
			(DWORD) sizeof(BITMAPFILEHEADER) +
			bmiHeader.biSize + bmiHeader.biClrUsed
			* sizeof(RGBQUAD) };

		DWORD dwWritten = 0;
		WriteFile(hFile, &bmpfilehdr, sizeof(BITMAPFILEHEADER), &dwWritten, 0);
		WriteFile(hFile, pData, ulSize, &dwWritten, 0);
		CloseHandle(hFile);
		CoTaskMemFree(pData);
	}

	return hr;
}

HRESULT EVR::AddToGraph(IGraphBuilder *pGraph, HWND hwnd, int vmrAspectRatio)
{
	HRESULT hr = S_OK;

	IBaseFilter *pEVR = NULL;

	hr = AddFilterByCLSID(pGraph, CLSID_EnhancedVideoRenderer, &pEVR, L"EVR");

	if (SUCCEEDED(hr))
	{
		hr = InitializeEVR(pEVR, hwnd, vmrAspectRatio, &m_pVideoDisplay);
	}

	if (SUCCEEDED(hr))
	{
		// Note: Because IMFVideoDisplayControl is a service interface,
		// you cannot QI the pointer to get back the IBaseFilter pointer.
		// Therefore, we need to cache the IBaseFilter pointer.

		m_pEVR = pEVR;
		m_pEVR->AddRef();
	}

	SAFE_RELEASE(pEVR);

	return hr;
}


HRESULT EVR::FinalizeGraph(IGraphBuilder *pGraph)
{
	if (m_pEVR == NULL)
	{
		return S_OK;
	}

	HRESULT hr = S_OK;
	BOOL bRemoved = FALSE;

	hr = RemoveUnconnectedRenderer(pGraph, m_pEVR, &bRemoved);

	if (bRemoved)
	{
		SAFE_RELEASE(m_pEVR);
		SAFE_RELEASE(m_pVideoDisplay);
	}

	return hr;
}


HRESULT EVR::UpdateVideoWindow(HWND hwnd, const LPRECT prc)
{
	HRESULT hr = S_OK;

	if (m_pVideoDisplay == NULL)
	{
		return S_OK; // no-op
	}

	if (prc)
	{
		hr = m_pVideoDisplay->SetVideoPosition(NULL, prc);
	}
	else
	{

		RECT rc;
		GetClientRect(hwnd, &rc);
		hr = m_pVideoDisplay->SetVideoPosition(NULL, &rc);
	}
	return hr;
}

HRESULT EVR::Repaint(HWND hwnd, HDC hdc)
{
	HRESULT hr = S_OK;

	if (m_pVideoDisplay)
	{
		hr = m_pVideoDisplay->RepaintVideo();
	}
	return hr;
}

HRESULT EVR::DisplayModeChanged()
{
	return S_OK; // No-op
}

 
//-----------------------------------------------------------------------------
// InitializeEVR
// Initialize the EVR filter. 
//-----------------------------------------------------------------------------

HRESULT InitializeEVR(
	IBaseFilter *pEVR,				// Pointer to the EVR
	HWND hwnd,						// Clipping window
	int vmrAspectRatio,
	IMFVideoDisplayControl** ppDisplayControl
)
{

	HRESULT hr = S_OK;

	IMFGetService *pService = NULL;
	IMFVideoDisplayControl *pDisplay = NULL;


	hr = pEVR->QueryInterface(__uuidof(IMFGetService), (void**)&pService);
	if (SUCCEEDED(hr))
	{
		hr = pService->GetService(
			MR_VIDEO_RENDER_SERVICE,
			__uuidof(IMFVideoDisplayControl),
			(void**)&pDisplay
		);
	}

	// Set the clipping window.
	if (SUCCEEDED(hr))
	{
		hr = pDisplay->SetVideoWindow(hwnd);
	}

	// Preserve aspect ratio by letter-boxing
	if (SUCCEEDED(hr))
	{
		hr = pDisplay->SetAspectRatioMode(vmrAspectRatio);
	}

	// Return the IMFVideoDisplayControl pointer to the caller.
	if (SUCCEEDED(hr))
	{
		*ppDisplayControl = pDisplay;
		(*ppDisplayControl)->AddRef();
	}

	SAFE_RELEASE(pService);
	SAFE_RELEASE(pDisplay);

	return hr;
}

//// DEFAULT VMR
VMR_DEF::VMR_DEF() : m_pWindowless(NULL)
{

}

VMR_DEF::~VMR_DEF()
{
	SAFE_RELEASE(m_pWindowless);
}

HRESULT VMR_DEF::SetAspectRatioMode(int vmrAspectRatio)
{
	return m_pWindowless->SetAspectRatioMode(vmrAspectRatio);
}

HRESULT VMR_DEF::SaveFrame(LPCTSTR tcsFileName)
{
	if (m_pWindowless == NULL)
		return E_FAIL;

	HRESULT hr = S_OK;
	BYTE* pDib = NULL;

	if (SUCCEEDED(hr = m_pWindowless->GetCurrentImage(&pDib)))
	{
		BITMAPINFOHEADER* bmiHeader = (BITMAPINFOHEADER*)pDib;
		long ulSize = bmiHeader->biSize + bmiHeader->biSizeImage;

		//Save to file
		TCHAR drive[_MAX_DRIVE];
		TCHAR dir[_MAX_DIR];
		TCHAR fname[_MAX_FNAME];
		TCHAR ext[_MAX_EXT];

		_tsplitpath_s(tcsFileName, drive, dir, fname, ext);

		TCHAR* pDirName = new TCHAR[_MAX_PATH];
		_tcscpy_s(pDirName, _MAX_PATH, drive);
		_tcscat_s(pDirName, _MAX_PATH, dir);

		if (_tccmp(pDirName, L"") != 0)
		{
			if (!::PathFileExists(pDirName))
			{
				if (!::CreateDirectory(pDirName, NULL))
				{
					CoTaskMemFree(pDib);
					return E_FAIL;
				}
			}
			delete[] pDirName;
		}

		HANDLE hFile = ::CreateFile(tcsFileName, GENERIC_WRITE,              // open for writing 
			FILE_SHARE_WRITE,
			NULL,                      // default security 
			OPEN_ALWAYS,
			FILE_ATTRIBUTE_NORMAL,     // normal file 
			NULL);                     // no attr. template 

		if (hFile == INVALID_HANDLE_VALUE)
		{
			CoTaskMemFree(pDib);
			return E_FAIL;
		}

		// Write file header
		BITMAPFILEHEADER bmpfilehdr = { 0x4D42,
			(DWORD)(sizeof(BITMAPFILEHEADER) +
				bmiHeader->biSize + bmiHeader->biClrUsed
				* sizeof(RGBQUAD) + bmiHeader->biSizeImage),
			0, 0,
			(DWORD) sizeof(BITMAPFILEHEADER) +
			bmiHeader->biSize + bmiHeader->biClrUsed
			* sizeof(RGBQUAD) };

		DWORD dwWritten = 0;
		WriteFile(hFile, &bmpfilehdr, sizeof(BITMAPFILEHEADER), &dwWritten, 0);
		WriteFile(hFile, pDib, ulSize, &dwWritten, 0);
		CloseHandle(hFile);
		CoTaskMemFree(pDib);
	}

	return hr;
}

HRESULT VMR_DEF::AddToGraph(IGraphBuilder *pGraph, HWND hwnd, int vmrAspectRatio)
{
	HRESULT hr = S_OK;

	IBaseFilter *pVMR = NULL;

	hr = AddFilterByCLSID(pGraph, CLSID_VideoRendererDefault, &pVMR, L"Default Video Renderer");

	// Set windowless mode on the VMR. This must be done before the VMR is connected.
	if (SUCCEEDED(hr))
	{
		hr = InitWindowlessVMR(pVMR, hwnd, vmrAspectRatio, &m_pWindowless);
	}

	return hr;
}


HRESULT VMR_DEF::FinalizeGraph(IGraphBuilder *pGraph)
{
	if (m_pWindowless == NULL)
	{
		return S_OK;
	}

	HRESULT hr = S_OK;
	BOOL bRemoved = FALSE;

	IBaseFilter *pFilter = NULL;

	hr = m_pWindowless->QueryInterface(__uuidof(IBaseFilter), (void**)&pFilter);

	if (SUCCEEDED(hr))
	{
		hr = RemoveUnconnectedRenderer(pGraph, pFilter, &bRemoved);

		// If we removed the VMR, then we also need to release our 
		// pointer to the VMR's windowless control interface.
		if (bRemoved)
		{
			SAFE_RELEASE(m_pWindowless);
		}
	}

	SAFE_RELEASE(pFilter);
	return hr;
}

HRESULT VMR_DEF::UpdateVideoWindow(HWND hwnd, const LPRECT prc)
{
	HRESULT hr = S_OK;

	if (m_pWindowless == NULL)
	{
		return S_OK; // no-op
	}

	if (prc)
	{
		hr = m_pWindowless->SetVideoPosition(NULL, prc);
	}
	else
	{

		RECT rc;
		GetClientRect(hwnd, &rc);
		hr = m_pWindowless->SetVideoPosition(NULL, &rc);
	}
	return hr;
}

HRESULT VMR_DEF::Repaint(HWND hwnd, HDC hdc)
{
	HRESULT hr = S_OK;

	if (m_pWindowless)
	{
		hr = m_pWindowless->RepaintVideo(hwnd, hdc);
	}
	return hr;
}

HRESULT VMR_DEF::DisplayModeChanged()
{
	HRESULT hr = S_OK;

	if (m_pWindowless)
	{
		hr = m_pWindowless->DisplayModeChanged();
	}
	return hr;


}
void VMR_DEF::SetVideoWindow(HWND hwnd)
{
	HRESULT hr = S_OK;
	m_pWindowless->SetVideoClippingWindow(hwnd);

}
