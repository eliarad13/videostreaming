#pragma once
#pragma warning (disable : 4800)

//////////////////////////////////////////////////////////////////////////
//
struct open_url_config
{
	std::wstring	m_mcast_group;
	std::wstring	m_interface;
	std::wstring	m_server_addr;

	UINT			m_port;
	UINT			m_payload;
	UINT			m_transport;
};

//////////////////////////////////////////////////////////////////////////
//
enum _clock_mode
{
	clock_auto		= 0 //automatically chooses a reference clock
	,clock_src_dmx	= 1 //source (for elementary) or push demultiplexer filter is clock holder
	,clock_render	= 2 //push audio renderer filter is clock holder
	,clock_off		= 3 //graph does not use a reference clock
};

//////////////////////////////////////////////////////////////////////////
//
enum _source_type
{
	 source_NWSPlus = 0
	,source_RTSP	= 1
	,source_HLS		= 2
};

//////////////////////////////////////////////////////////////////////////
//
typedef struct _player_config_
{
	bool use_cmd_line_config;

	// type of source filter
	_source_type source_type;

	// NWSourcePlus config SDP - if empty then will use DVB-ASI source
	std::wstring config_sdp;

	// clock mode
	_clock_mode clock_mode;

	// use split window flag
	bool m_split_window;

	// RTSP NetSource filter configuration
	struct _rtsp_config
	{
		// server URL
		std::wstring server_url;

		// compatibility
		UINT		compat;

		// lower transport
		UINT		lower_tranp;

		// delivery transport
		UINT		deliver_transp;

		// client port range
		ULONG		client_port_from;
		ULONG		client_port_to;
	} rtsp_config;

	// HLS Source filter configuration
	struct _hls_config
	{
		// URL
		std::wstring url;

		// connection timeout
		UINT		connection_timeout;

		// data timeout
		UINT		data_timeout;

		// max download rate
		UINT		download_rate;
	} hls_config;

	struct _forward_config_
	{
		// Audio PIDs
		UINT	a1;
		UINT	a2;
		UINT	a3;

		//Video PIDs
		UINT	v1;
		UINT	v2;

		bool	use_forward;
	} forward_config;

	

	//////////////////////////////////////////////////////////////////////////
	// SinkFilter settings
	struct _sink_config_
	{
		// output file name
		std::wstring output_file_name;
		// dump mode
		UINT dump_mode;

		// crop mode
		UINT crop_mode;
		// crop size
		ULONG crop_size;

		// create index file flag
		bool create_index;
		// mpeg-ves stream type flag
		bool mpeg_ves;
		// stream id (for non mpeg-ves stream type)
		UINT stream_id;
		// create index start from time
		ULONG crete_index_start_from;

	} sink_config;

} player_config;
 