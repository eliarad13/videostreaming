#pragma once

#include "gdiplus.h"
#include <wchar.h>

using namespace Gdiplus;

struct Overlay
{
	int type;
	int visible;
	int fontStyle;
	WCHAR* text;
	RectF pos;
	Color color;
	float fontSize;
	int x1;
	int y1;
	int radios_w;
	int radios_h;
	int x2;
	int y2;
	int lineWidth;

	Overlay(WCHAR* overlayText)
	{
		int size = wcslen(overlayText);
		text = new WCHAR[size + 10];
		wmemcpy(this->text, overlayText, size);
		this->text[size] = '\0';
	}

	Overlay()
	{
		text = NULL;
	}

	~Overlay()
	{
		if (text != NULL)
			delete text;
	}
};