// The following ifdef block is the standard way of creating macros which make exporting 
// from a DLL simpler. All files within this DLL are compiled with the DSC2CSLIB_EXPORTS
// symbol defined on the command line. This symbol should not be defined on any project
// that uses this DLL. This way any other project whose source files include this file see 
// DSC2CSLIB_API functions as being imported from a DLL, whereas this DLL sees symbols
// defined with this macro as being exported.
#ifdef DSC2CSLIB_EXPORTS
#define DSC2CSLIB_API __declspec(dllexport)
#else
#define DSC2CSLIB_API __declspec(dllimport)
#endif

#include "AvCommon.h"

typedef void(__stdcall * ProgressCallback)(char *rootDirectory, char *startDateTime, char *tsDir, char *tsFile, char *fileName, double lastPCR);

extern "C"
{
	ProgressCallback pProgressCallback;
	DSC2CSLIB_API void DSShow_SetDVRSinkCallback(ProgressCallback progressCallback);
	DSC2CSLIB_API void DSShow_UnlockBautechPlayer(const long code);

	DSC2CSLIB_API HRESULT DSShow_Play(int graphIndex);
	DSC2CSLIB_API HRESULT DSShow_Stop(int graphIndex);
	DSC2CSLIB_API HRESULT DSShow_Pause(int graphIndex);

	DSC2CSLIB_API HRESULT DSShow_GetDuration(int graphIndex, LONGLONG *pDuration);
	DSC2CSLIB_API HRESULT DSShow_SetPosition(int graphIndex, LONGLONG pos);
	DSC2CSLIB_API LONGLONG DSShow_GetCurrentPosition(int graphIndex);
	DSC2CSLIB_API HRESULT DSShow_GetGraphEvent(int graphIndex, long *param1, long *param2, long *evCode);
	DSC2CSLIB_API int DSShow_Snapshot(int graphIndex, const WCHAR *fileName);

	DSC2CSLIB_API HRESULT DSShow_InitializePlayer(int graphIndex, HWND hwnd, int klvPID);
	DSC2CSLIB_API HRESULT DSShow_InitilizeNetworkMulticastPlayer(int graphIndex, 
																 HWND hwnd, 
																 bool useBoutechReceover,
																 const WCHAR *MulticastAddress, 
																 int MulticastPort, 
															     bool useInterface,
															     float demuxLatency, int klvPID);

	DSC2CSLIB_API HRESULT DSShow_InitilizeNetworkVideoEsPlayer(int graphIndex,
															   HWND hwnd ,
															   bool useBoutechReceiver,
															   const WCHAR *ServerAddress,
															   int ServerPort);


	DSC2CSLIB_API HRESULT  DSShow_InitializeScreenCaptureToDump(int graphIndex,
												  const WCHAR* fileName,
												  int screenWidth,
											      int screenHeight,
												  bool includeAudio,
											      int bitrate);


	DSC2CSLIB_API HRESULT DSShow_InitializeNetwordReceiverToBoutechDVRSink(int graphIndex,
		const WCHAR * rootDirectory,
		const WCHAR * serverAddress,
		const WCHAR * interfaceAddress,
		int serverPort, 
		bool useBoutechReceiver,
		int maxTSChunks,
		int maxFilesPerDirectory);


	DSC2CSLIB_API HRESULT DSShow_InitializeBoutechReceiverToWebRTCAndHLS(int graphIndex,
		const WCHAR * serverAddress,
		bool useTransport,
		int cameraPort,
		const WCHAR * webRtcURL,
		const WCHAR * HLSOutputPath,
		const WCHAR * HLSName,
		int HLSRelativeUrl,
		int HLSLive,
		int HLSPlaylistDuration,
		int HLSSegmentDuration,
		const WCHAR * HLSStreamName);

	DSC2CSLIB_API HRESULT DSShow_InitializeBauotechDVRPlayerAsync(int graphIndex, 
																  HWND hwnd,
																  const WCHAR *DvrFileName,
																  float startTime,
																  bool loop);

	DSC2CSLIB_API HRESULT DSShow_InitializeBauotechDVRPlayer(int graphIndex,
		HWND hwnd,
		const WCHAR *DvrFileName,
		float startTime,
		bool loop);


	DSC2CSLIB_API void DSShow_SelectRenderer(int graphIndex, int renderer);
	DSC2CSLIB_API void DSShow_SelectDecoder(int graphIndex, int selectedDecoder);
	DSC2CSLIB_API void DSShow_SelectPullDemux(int graphIndex, int selectedPullDemux);
	DSC2CSLIB_API void DSShow_SetAspectRatio(int graphIndex, int ratio, bool applyNow);
	DSC2CSLIB_API void DSShow_UpdateVideoWindow(int graphIndex, int x, int y, int width, int height);
	DSC2CSLIB_API void DSShow_SetWindowHandle(int graphIndex, HWND hwnd);
	DSC2CSLIB_API void DSShow_SetFileName(int graphIndex, const WCHAR* sFileName);

	DSC2CSLIB_API  void DSShow_CloseAll();
	DSC2CSLIB_API  void DSShow_StopAll();
	DSC2CSLIB_API  void DSShow_Close(int graphIndex);

	DSC2CSLIB_API  HRESULT DSShow_SetRate(int graphIndex, double rate);

	DSC2CSLIB_API HRESULT DSShow_InitializeBoutechReceiverToWebRTC(int graphIndex,
		const WCHAR * serverAddress,
		bool useTransport,
		int cameraPort,
		const WCHAR * webRtcURL);

	DSC2CSLIB_API HRESULT DSShow_StepForward(int graphIndex, long lFrames = 1, BOOL bSec = FALSE);
	DSC2CSLIB_API HRESULT DSShow_StepBackward(int graphIndex, long lFrames = 1, BOOL bSec = FALSE);
	
	DSC2CSLIB_API  HRESULT DSShow_ApplyOverlay(int graphIndex, float alpha_opacity);

	DSC2CSLIB_API HRESULT DSShow_AddCircle(int graphIndex, int id,
		int x1,
		int y1,
		int radios_w,
		int radios_h,
		COLORREF color,
		int width);
	 

	DSC2CSLIB_API HRESULT DSShow_AddLine(int graphIndex, int id,
		int x1,
		int y1,
		int x2,
		int y2,
		int color,
		int width);


	DSC2CSLIB_API HRESULT	DSShow_Repaint(int graphIndex, HDC hdc);
	DSC2CSLIB_API HRESULT	DSShow_Visible(int graphIndex, int id, bool visible);
	DSC2CSLIB_API HRESULT	DSShow_Clear(int graphIndex);


	DSC2CSLIB_API HRESULT	DSShow_AddTextOverlay2(int graphIndex, WCHAR *text, int id,
		int left,
		int top,
		int right,
		int bottom,
		int color,
		float fontSize);;

	DSC2CSLIB_API HRESULT	DSShow_AddTextOverlay(int graphIndex, WCHAR *text,
		int id,
		int left,
		int top,
		int right,
		int bottom,
		int color,
		float fontSize,
		int fontStyle);

	  
}
