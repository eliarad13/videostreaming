﻿using System;
 

namespace BauotechStreamerClientApp
{
    public class PlayListItem
    {
     
        public DateTime StartDate { get; set; }
        public DateTime StopDate { get; set; }
        public string fileName { get; set; }
        public float PCR { get; set; }
        public int fileState { get; set; }
    }
}
