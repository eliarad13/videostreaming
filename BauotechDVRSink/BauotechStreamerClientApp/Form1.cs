﻿using BauotechDVRStreamerCSLib;
using DVRDomainModel;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BauotechDVRStreamerCSLib.BauotechDVRStreamer;

namespace BauotechStreamerClientApp
{
    

    public partial class Form1 : Form
    {
        //string m_connectionString = @"Data Source=.; Initial Catalog=HLS_SEGMENTATIONS; Integrated Security=True";
        string serverInstance = string.Empty;
        List<PlayListItem> playListItems = new List<PlayListItem>();
        BauotechDVRStreamer m_streamer = new BauotechDVRStreamer();
        DateTime dtStart = DateTime.Now.AddMinutes(-2);
        DateTime dtEnd = DateTime.Now.AddMinutes(5);

        public Form1()
        {
            InitializeComponent();
            label5.Text = string.Empty;
            dtpStart.Format = DateTimePickerFormat.Custom;
            dtpStart.CustomFormat = "yyyy.MM.dd HH:mm:ss";
            dtpStart.Value = dtStart;

            dtpEnd.Format = DateTimePickerFormat.Custom;
            dtpEnd.CustomFormat = "yyyy.MM.dd HH:mm:ss";
            dtpEnd.Value = dtEnd;
        }

        private void btnGetQuery_Click(object sender, EventArgs e)
        {
            playListItems.RemoveRange(0, playListItems.Count);
            dataGridView1.DataSource = null;


            string s = dtpStart.Value.ToString("yyyy.MM.dd HH:mm:ss");
            DateTime startTime = DateTime.Parse(s);

            s = dtpEnd.Value.ToString("yyyy.MM.dd HH:mm:ss");
            DateTime stopTime = DateTime.Parse(s);

           
            if (GetSegmentInfo(startTime, stopTime, int.Parse(txtStreamIndex.Text), 
                            ref playListItems, out string error) == false)
            {
                MessageBox.Show(error);
            }
            else
            {
                dataGridView1.DataSource = playListItems;
                GeneratePlaylist();
            }
        }

        void GeneratePlaylist()
        {
            string s = dtpStart.Value.ToString("yyyy.MM.dd HH:mm:ss");
            DateTime startTime = DateTime.Parse(s);
            //GeneratePlaylist(int.Parse(txtStreamIndex.Text), startTime);
        }
        private void btnGeneratePlaylist_Click(object sender, EventArgs e)
        {
            label5.Text = string.Empty;
            if (StartStreamer(out string outMesssage) == false)
            {
                MessageBox.Show(outMesssage);
                return;
            }
            label5.Text = "Streamer started";
        }
        bool StartStreamer(out string outMesssage)
        {
            outMesssage = string.Empty;
            long totalFileSize = 0;

            BuildDVRList(out int count);
            if (count == 0)
            {
                outMesssage = "No files to stream";
                return false;
            }

            m_streamer.Init();
            bool isMulticast;
            string[] p = txtIpAddress.Text.Split('.');
            if ((int.Parse(p[0]) >= 0xE0))
            {
                isMulticast = true;
            }
            else
            {
                isMulticast = false;
            }
            DVRStreamerCallback p1 = new DVRStreamerCallback(DVRStreamerCallbackMsg);
            m_streamer.SetDVRStreamerCallback(p1);


            if (m_streamer.AddClient(0,
                                    txtIpAddress.Text,
                                    int.Parse(txtPort.Text),
                                    isMulticast,
                                    int.Parse(txtTTL.Text)) == false)
            {
                outMesssage = "Failed to add client to streamer";
                return false;
            }

            //m_streamer.Start(@"D:\tempBauotechDVRStorage\2019_07_03_10_13_39\2019_07_03_10_13_43\dvrlist.txt", 
            if (m_streamer.Start("dvrlist.txt",
                               ref totalFileSize,
                               0,
                               out int numFiles) == false)
            {
                MessageBox.Show("Failed to start " + numFiles);
            }
            return true;
        }
        void DVRStreamerCallbackMsg(int streamerId, string fileName, long fileSize, double AvgBitrate, double fileDuration)
        {
            listBox1.Items.Add(fileSize + " | " + AvgBitrate + "  | " + fileDuration + " | ");
        }
        void BuildDVRList(out int count)
        {
            count = 0;
            using (StreamWriter sw = new StreamWriter("dvrlist.txt"))
            {
                foreach (PlayListItem p in playListItems)
                {
                    sw.WriteLine(p.fileName);
                    count++;
                }
            }
        }

        public bool GetSegmentInfo(DateTime startDateTime,
                                   DateTime endDateTime,
                                   int streamIndex,
                                   ref List<PlayListItem> playListItems,
                                   out string error)
        {
            error = string.Empty;
            try
            {
                bool found1 = false;
                TimeSpan FIVE_MINUTES = new TimeSpan(0, 5, 0);
                DVRSegmentDBContext context = new DVRSegmentDBContext();

                PlayListItem s1 = new PlayListItem(); // save place for the first query , right?
                playListItems.Add(s1);

                string connectionString = System.Configuration.ConfigurationManager.ConnectionStrings["DVRSegmentDBContext"].ConnectionString;

                string queryString = string.Empty;
                queryString = string.Format("select * from DVRSegment{0} where StartDate >= @StartDate and StopDate <= @StopDate and fileState = 2", streamIndex);

                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@StartDate", startDateTime);
                    command.Parameters.AddWithValue("@StopDate", endDateTime);
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            found1 = true;
                            PlayListItem s = new PlayListItem()
                            {
                                StartDate = DateTime.Parse(reader["StartDate"].ToString()),
                                StopDate = DateTime.Parse(reader["StopDate"].ToString()),
                                fileName = reader["fileName"].ToString(),
                                PCR = float.Parse(reader["PCR"].ToString()),
                                fileState = int.Parse(reader["fileState"].ToString()),
                            };
                            playListItems.Add(s);
                        }
                    }
                    catch (Exception err)
                    {
                        Console.WriteLine(err.Message);
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                 
                if (found1 == false)
                {
                    error = "Search found nothing";
                    return false;
                }

                // second query - take one before 
                //queryString = "select top 1 * from hlssegmentinfo1 where FileCreatingTime <= @startDateTime and FileCreatingTime >= @endDateTime2 order by FileCreatingTime desc";
                queryString = string.Format("select top 1 * from DVRSegment{0} where StartDate <= @StartDate  and startDate > @StopDate and fileState = 2  order by StartDate desc", streamIndex);
                found1 = false;
                using (SqlConnection connection = new SqlConnection(connectionString))
                {
                    SqlCommand command = new SqlCommand(queryString, connection);
                    command.Parameters.AddWithValue("@StartDate", startDateTime);
                    command.Parameters.AddWithValue("@StopDate", startDateTime.Subtract(FIVE_MINUTES));
                    //command.Parameters.AddWithValue("@endDateTime2", startDateTime.Subtract(FIVE_MINUTES));
                    connection.Open();
                    SqlDataReader reader = command.ExecuteReader();
                    try
                    {
                        while (reader.Read())
                        {
                            found1 = true;
                            PlayListItem s = new PlayListItem()
                            {
                                StartDate = DateTime.Parse(reader["StartDate"].ToString()),
                                StopDate = DateTime.Parse(reader["StopDate"].ToString()),
                                fileName = reader["fileName"].ToString(),
                                PCR = float.Parse(reader["PCR"].ToString()),
                                fileState = int.Parse(reader["fileState"].ToString()),
                            };
                            playListItems[0] = s;
                        }
                    }
                    finally
                    {
                        // Always call Close when done reading.
                        reader.Close();
                    }
                }
                if (found1 == false)
                {
                    error = "Search found nothing";
                    return false;
                }






                return true;

            }
            catch (Exception ex)
            {
                error = ex.Message;
                return false;
            }
        }
         

        private void TextChanger(string filePathWithName, string textToBeFound, string newText)
        {
            string[] lines = File.ReadAllLines(filePathWithName);

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(textToBeFound))
                {
                    lines[i] = lines[i].Replace(textToBeFound, newText);
                }
            }
            File.WriteAllLines(filePathWithName, lines);
        }

        private void LineChanger(string filePathWithName, string textToBeFound, string newText)
        {
            string[] lines = File.ReadAllLines(filePathWithName);

            for (int i = 0; i < lines.Length; i++)
            {
                if (lines[i].Contains(textToBeFound))
                {
                    lines[i] = newText;
                    break;
                }
            }
            File.WriteAllLines(filePathWithName, lines);
        }

        private void button1_Click(object sender, EventArgs e)
        {
            m_streamer.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            BauotechDVRStreamer.BSTREAMER_CloseAll();
        }
    }

    public static class MyClass
    {
        public static IEnumerable<TSource> DistinctBy<TSource, TKey>(this IEnumerable<TSource> source, Func<TSource, TKey> keySelector)
        {
            HashSet<TKey> seenKeys = new HashSet<TKey>();
            foreach (TSource element in source)
            {
                if (seenKeys.Add(keySelector(element)))
                {
                    yield return element;
                }
            }
        }
    }
}
