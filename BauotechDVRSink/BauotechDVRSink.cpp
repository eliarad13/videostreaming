#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include "BauotechDVRSinkuids.h"
#include "BauotechDVRSink.h"
#include "myInterface.h"
#include "Shlwapi.h"




// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
	&CLSID_BauotechDVRSink,                // Filter CLSID
    L"Bauotech DVR Sink",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
	L"Bauotech DVR Sink", &CLSID_BauotechDVRSink, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;


// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
						 CBaseFilter(NAME("CDumpFilter"), pUnk, pLock, CLSID_BauotechDVRSink),
    m_pDump(pDump)
{
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}


//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	m_pDump->m_running = 0;

	if (m_pDump->hFile != NULL)
		CloseHandle(m_pDump->hFile);
	m_pDump->hFile = NULL;
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);
	 

    return CBaseFilter::Pause();
}


//
// Run
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
    CAutoLock cObjectLock(m_pLock);
 
	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	m_pDump->m_lastTime = time(NULL);

	m_pDump->m_writeSize = 0;
	m_pDump->m_flushSize = 0;
	m_pDump->hFile = NULL;
	m_pDump->m_chunkCount = 0;
	m_pDump->m_fileCounter = 0;
	m_pDump->m_curFileCounter = -1;
	m_pDump->m_canStart = false;
	 
	m_pDump->m_running = 1;
  
    return CBaseFilter::Run(tStart);
}


//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{
	 
	m_timeToLogInSec = 600;
	InitTSParser();
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}


//
// BreakConnect
//
// Break a connection
//
HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}

void CDumpInputPin::InitTSParser()
{
	
	show_pcr_time = false;
	show_frame_pos = false;
	show_frame_pts = false;
	show_frame_dts = false;
	show_key_frame = true;


	vc = 0;
	sc = 0;
	offset = 0;
	pts = 0;
	dts = 0;
	vknown_type = false;
	aknown_type = false;
	 
	
	si.pid_ = 40;
	si.type_ = util::stream_info::stream_video;
	si.stream_type_ = tsParser.stream_type(std::string("H264"));
	streams.push_back(si);
	si.pid_ = 50;
	si.type_ = util::stream_info::stream_audio;
	si.stream_type_ = tsParser.stream_type(std::string("AAC"));
	streams.push_back(si);
	tsParser.init_streams(streams);
	
	mi.pid_ = 40;
	mi.is_video_ = true;
	mi.is_audio_ = false;

	 
}

STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
    CheckPointer(pSample,E_POINTER);

	if (m_pDump->m_running == false)
	{
		m_pFilter->Stop();
		return S_OK;
	}

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
	  
    REFERENCE_TIME tStart, tStop;
    pSample->GetTime(&tStart, &tStop);
	 
   // pSample->GetActualDataLength()));

    m_tLast = tStart;
	 
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    }
	  
	double startPCR;
	double lastPCR;
	int receivedSize = pSample->GetActualDataLength();
	 
	char msgbuf[200];
	
	util::mpegts_info info;

	int tsChunkIndex = 0;
 
	while (receivedSize > 0)
	{
		memset(&info, 0, sizeof(info));
		auto suc = tsParser.do_parser(pbData + tsChunkIndex, info);
		
		//if (info.type_ == util::mpegts_info::idr)
		if (info.pict_type_ == util::av_picture_type_i)
		{
			

			if (m_pDump->m_canStart == false)
			{				
				m_pDump->m_canStart = true;
				m_pDump->CreateNewFile(info.pcr_);
			}
			else 
			{
				if (m_pDump->m_writeSize >= m_pDump->m_maxChunkSize)
				{
					m_pDump->CreateNewFile(info.pcr_);
				}
			}	
			if (m_pDump->pProgressCallback != NULL)
				m_pDump->pProgressCallback(m_pDump->m_curFileCounter, m_pDump->m_writeSize + tsChunkIndex);
		}
		 

		tsChunkIndex += 188;
		receivedSize -= 188;
	}

 
	DWORD dwBytesWritten = 0;
	if (m_pDump->hFile != NULL)
	{
		WriteFile(m_pDump->hFile, pbData, tsChunkIndex, &dwBytesWritten, NULL);
		m_pDump->m_writeSize += dwBytesWritten;

		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		struct tm tmLast = *localtime(&m_pDump->m_lastTime);
		double diff = difftime(t, m_pDump->m_lastTime);
		if ((diff >= m_pDump->m_cacheTime))
		{
			FlushFileBuffers(m_pDump->hFile);
			m_pDump->m_flushSize = 0;
			m_pDump->m_lastTime = time(NULL);
		}	 
	}
	  
	   
	return S_OK;// m_pDump->Write(pbData, pSample->GetActualDataLength());
}
 
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
    CUnknown(NAME("CDump"), pUnk),
    m_pFilter(NULL),
    m_pPin(NULL),
    m_pPosition(NULL),    
    m_pFileName(0) 
    
{
    ASSERT(phr);
    
    m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
    if (m_pFilter == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }
	pProgressCallback = NULL;
	hFile = NULL;
	m_chunkCount = 0;
	m_fileCounter = 0;
	m_running = 0;
	m_cacheTime = 5;
	m_maxChunkSize = 1316 * 1000000;
	m_numberOfFiles = 3;
	strcpy(m_rootDirectoryName, "c:\\temp\\BauotechDVRStorage");
	CreateDirectoryA(m_rootDirectoryName, 0);

    m_pPin = new CDumpInputPin(this,GetOwner(),
                               m_pFilter,
                               &m_Lock,
                               &m_ReceiveLock,
                               phr);
    if (m_pPin == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

}

  

CDump::~CDump()
{
	if (m_pPin != NULL)
		delete m_pPin;
		if (m_pFilter != NULL)
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance


//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);

    // Do we have this interface

	if (riid == IID_IBoutechDVRSink) {
		return GetInterface((IBoutechDVRSink *) this, ppv);
	}
	else
    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Dump Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

} // NonDelegatingQueryInterface

  
//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}
  
bool CDump::CreateNewFile(double PCR)
{
	if (hFile != NULL)
	{
		CloseHandle(hFile);
		hFile = NULL;		
	}
		 		
	sprintf(m_fileName, "%s\\dvrsink_%.5d.ts", m_rootDirectoryName, m_fileCounter);		
	if (PathFileExistsA(m_fileName) == TRUE)
	{
		DeleteFileA(m_fileName);
	}

	m_curFileCounter = m_fileCounter;

	hFile = CreateFileA(m_fileName,                // name of the write
		GENERIC_WRITE,          // open for writing
		FILE_SHARE_READ,                      // do not share
		NULL,                   // default security
		CREATE_ALWAYS,             // create new file only
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, 
		//FILE_FLAG_WRITE_THROUGH ,
		 //FILE_FLAG_NO_BUFFERING,
		NULL);                  // no attr. template
	if (hFile == INVALID_HANDLE_VALUE)
		return false;


	m_fileCounter = (m_fileCounter + 1) % m_numberOfFiles;


	m_writeSize = 0;
	m_flushSize = 0;

	return true;
	

}
 
STDMETHODIMP CDump::ConfigDvrSink(const WCHAR *rootDirectory, int cacheTime, long maxChunkSize, int numberOfFiles)
{
	if (wcscmp(rootDirectory, L"") != 0)
	{
		wcstombs(m_rootDirectoryName, rootDirectory, sizeof(m_rootDirectoryName));
	}
	m_cacheTime = cacheTime;
	m_maxChunkSize = maxChunkSize;
	m_numberOfFiles = numberOfFiles;
	return S_OK;
}

STDMETHODIMP CDump::SetDVRSinkCallback(DVRSinkCallback progressCallback)
{
	pProgressCallback = progressCallback;
	return S_OK;
}