#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>

 
// {ACB60A35-4A4E-49B8-8FC1-271B34E7AAD8}
static const GUID IID_IBoutechDVRSink =
{ 0xacb60a35, 0x4a4e, 0x49b8, { 0x8f, 0xc1, 0x27, 0x1b, 0x34, 0xe7, 0xaa, 0xd8 } };

typedef void(__stdcall * DVRSinkCallback)(int fileNumber, long writeIndex);

DECLARE_INTERFACE_(IBoutechDVRSink, IUnknown)
{	
	 
	STDMETHOD(ConfigDvrSink)(const WCHAR *rootDirectory, int cacheTime, long maxChunkSize, int numberOfFiles) PURE;
	STDMETHOD(SetDVRSinkCallback)(DVRSinkCallback progressCallback) PURE;
	
};