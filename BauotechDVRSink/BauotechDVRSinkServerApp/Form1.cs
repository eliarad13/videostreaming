﻿using BauotechDVRSinkApi;
using BoutechPlayerCSLib;
using DVRDomainModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BauotechDVRSinkServerApp
{
    public partial class Form1 : Form
    {
        DVRSegmentDBContext context = new DVRSegmentDBContext();
        BauotechDVRSink m_dvr = new BauotechDVRSink();
        Thread m_recordTimeThread;
        TimeSpan m_timeToStop;

        AutoResetEvent m_logEvent = new AutoResetEvent(false);
        bool m_running = true;
        Thread m_dvrLogThread;

        string connectionString;
        public Form1()
        {
            InitializeComponent();
            connectionString = ConfigurationManager.ConnectionStrings["DVRSegmentDBContext"].ConnectionString;
            Control.CheckForIllegalCrossThreadCalls = false;
            BauotechPlayer.ProgressCallback2Host p = new BauotechPlayer.ProgressCallback2Host(P2Host);
            m_dvr.SetProgressCallback2Host(p);

            // this os for the create of the EF first time
            using (DVRSegmentDBContext ctx = new DVRSegmentDBContext())
            {
                try
                {
                    var x = (from s in ctx.dvrSegments1
                             select s).First();
                }
                catch (Exception err)
                {

                }
            }

            m_dvrLogThread = new Thread(() => DVRLogThread(2));
            m_dvrLogThread.Start();

        }
        int m_filesCounter = 0;

        struct DVRInfo
        {          
            public DateTime StartDate;
            public DateTime StopDate;
            public string fileName;
            public double PCR;
            public int fileState;
        }

        ConcurrentQueue<DVRInfo> m_dvrinfoQueue = new ConcurrentQueue<DVRInfo>();


        void P2Host(string fileName,
                    double PCR,
                    int fileState)
        {

            m_filesCounter++;

            DVRInfo d = new DVRInfo();
            d.fileName = fileName;
            d.fileState = fileState;
            d.PCR = PCR;
            d.StartDate = DateTime.Now;
            d.StopDate = DateTime.Now;
            m_dvrinfoQueue.Enqueue(d);
            m_logEvent.Set();

        }
       
        void DVRLogThread(int segmentId)
        {
            while (m_running)
            {
                m_logEvent.WaitOne();
                if (m_running == false)
                    return;
                if (m_dvrinfoQueue.TryDequeue(out DVRInfo d) == false)
                    continue;
                this.BeginInvoke((MethodInvoker)delegate ()
                {
                     

                    if (d.fileState == 1)
                    {

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            string queryString = @" 
                             

                        INSERT INTO  DVRSegment" + segmentId.ToString() + @" 
                                   (StartDate
                                   ,StopDate
                                   ,fileName
                                   ,PCR
                                   ,fileState)
                             VALUES
                                   (@StartDate,
                                    @StopDate,
                                    @fileName,
                                    @PCR,
                                    @fileState)";
                            queryString = queryString.Replace('\n', ' ');
                            queryString = queryString.Replace('\r', ' ');

                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@StartDate", DateTime.Now);
                            command.Parameters.AddWithValue("@StopDate", DateTime.Now);
                            command.Parameters.AddWithValue("@fileName", d.fileName);
                            command.Parameters.AddWithValue("@PCR", d.PCR);
                            command.Parameters.AddWithValue("@fileState", d.fileState);

                            connection.Open();
                            int x = command.ExecuteNonQuery();
                            connection.Close();

                            label7.Text = m_filesCounter.ToString();
                            string s = string.Format("{0} | {1} | {2} | {3} | {4}",                                                                   
                                                                    d.StartDate,
                                                                    d.StopDate,
                                                                    d.fileName,
                                                                    d.PCR,
                                                                    d.fileState);
                            listBox1.Items.Add(s.ToString());
                        }

                         
                    }
                    else
                    {

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            string queryString = "UPDATE DVRSegment" + segmentId.ToString() + @" SET fileState=@fileState , StopDate=@StopDate  where fileName =@fileName";
                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@fileName", d.fileName);
                            command.Parameters.AddWithValue("@fileState", d.fileState);
                            command.Parameters.AddWithValue("@StopDate", d.StopDate);
                            connection.Open();
                            int x = command.ExecuteNonQuery();
                            connection.Close();

                            string s = string.Format("{0} | {1} | {2} | {3} | {4}",
                                                                   d.StartDate,
                                                                   d.StopDate,
                                                                   d.fileName,
                                                                   d.PCR,
                                                                   d.fileState);
                            listBox1.Items.Add(s.ToString());
                        }
                    }
                });
            }
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            m_dvr.Close();
            if (m_dvr.Initialize(txtIpAddress.Text, 
                                 int.Parse(txtPort.Text), 
                                 txtRootDirectory.Text, 
                                 false, 
                                 int.Parse(txtTimeToLogInSec.Text),
                                 int.Parse(txtmaxFilesPerDirectory.Text),
                                 out int result) == false)
            {
                MessageBox.Show("Failed to initialized " + result);
                return;
            }
            if (txtRecordTime.Text != "00:00:00")
            {
                m_timeToStop = TimeSpan.Parse(txtRecordTime.Text);
                m_recordTimeThread = new Thread(StopRecordThread);
                m_recordTimeThread.Start();
            }
        }
        AutoResetEvent m_sleepRecord = new AutoResetEvent(false);
        bool m_abort = false;
        void StopRecordThread()
        {
            m_sleepRecord.WaitOne(m_timeToStop);
            if (m_abort == true)
            {
                m_abort = false;
                return;
            }
            m_dvr.Close();
            MessageBox.Show("Record finished");
            
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            m_abort = true;
            m_sleepRecord.Set();
            m_dvr.Close();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_abort = true;
            m_running = false;
            m_logEvent.Set();            
            m_sleepRecord.Set();
            m_dvr.Close();
        }
    }
}
