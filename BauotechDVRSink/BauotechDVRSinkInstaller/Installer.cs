﻿using System;
using System.Threading;

namespace BauotechDVRSinkInstaller
{
    public class Installer
    {

        public static string InstallServiceAndStart(string serviceName, 
                                                    string nameInServices, 
                                                    string serviceFullPath)
        {
            if (ServiceTools.ServiceInstaller.ServiceIsInstalled(serviceName) == false)
            {
                ServiceTools.ServiceInstaller.InstallAndStart(serviceName, nameInServices, serviceFullPath, true);
                for (int i = 0; i < 50; i++)
                {
                    if (ServiceTools.ServiceInstaller.getStatus(serviceName) != "Running")
                    {
                        Thread.Sleep(200);
                    }
                    else
                    {
                        return "ok";
                    }
                }
            }
            else
            {
                ServiceTools.ServiceInstaller.StartService(serviceName);
                for (int i = 0; i < 50; i++)
                {
                    if (ServiceTools.ServiceInstaller.getStatus(serviceName) != "Running")
                    {
                        Thread.Sleep(700);
                    }
                    else
                    {
                        return "ok";
                    }
                }
            }
            return "failed";
        }

        public static string InstallServiceDontStart(string serviceName, string nameInServices, string serviceFullPath)
        {
            if (ServiceTools.ServiceInstaller.ServiceIsInstalled(serviceName) == false)
            {
                ServiceTools.ServiceInstaller.InstallAndStart(serviceName, nameInServices, serviceFullPath, false);
                for (int i = 0; i < 50; i++)
                {
                    if (ServiceTools.ServiceInstaller.getStatus(serviceName) != "Running")
                    {
                        Thread.Sleep(200);
                    }
                    else
                    {
                        return "ok";
                    }
                }
            }
            return "failed";
        }
        public static string StopServiceAndUnInstall(string serviceName)
        {

            if (ServiceTools.ServiceInstaller.ServiceIsInstalled(serviceName) == true)
            {
                try
                {
                    ServiceTools.ServiceInstaller.StopService(serviceName);
                    int count = 15;
                    while (ServiceTools.ServiceInstaller.getStatus(serviceName) != "Stopped")
                    {
                        Thread.Sleep(700);
                        count--;
                        if (count == 0)
                        {
                            return "Failed to stop service";
                        }
                    }
                }
                catch (Exception err)
                {
                    return err.Message;
                }
                try
                {
                    ServiceTools.ServiceInstaller.Uninstall(serviceName);
                    return "ok";
                }
                catch (Exception err)
                {
                    return err.Message;
                }
            }
            else
            {
                return "ok";
            }
        }
    }
}
