﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BauotechDVRSinkInstaller
{
    public partial class Form1 : Form
    {
        

        public Form1()
        {
            InitializeComponent();
 
        }

        private void btnInstall_Click(object sender, EventArgs e)
        {
            string servicePath = @"C:\Program Files\Bauotech\Services\DVRSink\BauotechDVRSinkService.exe ";
            //Usage  IpAddress port rootDir MaxIrFrame maxFilesPerDirectory 
            string args = string.Format(" {0} {1} {2} {3} {4} {5} " , txtIpAddress.Text , int.Parse(txtPort.Text) , txtRootDir.Text, 
                int.Parse(txtIframeCount.Text) , int.Parse(txtMaxFilesInDirectory.Text) , txtCameraNumber.Text);
            servicePath += args;
            string m;
            string serviceName = "BOUTECH_DVRSINK_" + txtCameraNumber.Text + "_" + txtSerialNumber.Text;
            m = Installer.InstallServiceAndStart(serviceName, serviceName, servicePath);
            MessageBox.Show(m);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            
            string serviceName = "BOUTECH_DVRSINK_" + txtCameraNumber.Text + "_" + txtSerialNumber.Text;
            string m = Installer.StopServiceAndUnInstall(serviceName);
            MessageBox.Show(m);
        }
    }
}
