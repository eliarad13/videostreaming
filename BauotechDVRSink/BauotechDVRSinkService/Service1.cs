﻿using BauotechDVRSinkApi;
using BoutechPlayerCSLib;
using DVRDomainModel;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BauotechDVRSinkService
{
    public partial class Service1 : ServiceBase
    {
        const string logFile = "c:\\5.txt";
        Thread m_dvrLogThread;
        int m_segmendId = 0;
        struct DVRInfo
        {
            public DateTime StartDate;
            public DateTime StopDate;
            public string fileName;
            public double PCR;
            public int fileState;
        }

        DVRSegmentDBContext context;
        BauotechDVRSink m_dvr;
        ConcurrentQueue<DVRInfo> m_dvrinfoQueue;

        AutoResetEvent m_logEvent;
        bool m_running = true;
        string connectionString;
        int m_segementId = 0;
        public Service1()
        {
            InitializeComponent();
            
        }
        bool m_debug = false;
        public void DebugCode()
        {
            string[] args = new string[7];
            args[1] = "227.1.1.1";
            args[2] = "10200";
            args[3] = @"D:\tempBauotechDVRStorage";
            args[4] = "60";
            args[5] = "1000";
            args[6] = "1";
            OnStart(args);
        }

        protected override void OnStart(string[] args)
        {

            
            try
            {

                m_logEvent = new AutoResetEvent(false);
                context = new DVRSegmentDBContext();
                m_dvr = new BauotechDVRSink();
                m_dvrinfoQueue = new ConcurrentQueue<DVRInfo>();

                //Usage  IpAddress port rootDir MaxIrFrame maxFilesPerDirectory 

                String[] arguments;
                if (m_debug == false)
                    arguments = Environment.GetCommandLineArgs();
                else
                    arguments = args;
                connectionString = ConfigurationManager.ConnectionStrings["DVRSegmentDBContext"].ConnectionString;

                BauotechPlayer.ProgressCallback2Host p = new BauotechPlayer.ProgressCallback2Host(P2Host);
                m_dvr.SetProgressCallback2Host(p);

                // this os for the create of the EF first time
                using (DVRSegmentDBContext ctx = new DVRSegmentDBContext())
                {
                    try
                    {
                        var x = (from s in ctx.dvrSegments1
                                 select s).First();
                    }
                    catch (Exception err)
                    {

                    }
                }

                m_segmendId = int.Parse(arguments[6]);
                LogToFile("segmendId:" + m_segmendId.ToString());
                
                m_dvrLogThread = new Thread(() => DVRLogThread(m_segmendId));
                m_dvrLogThread.Start();

                LogToFile("ipAddress " + arguments[1]);
                LogToFile("Port " + arguments[2]);
                LogToFile("RootDir " + arguments[3]);
                LogToFile("LogTime " + arguments[4]);
                LogToFile("MaxFilePerDir " + arguments[5]);


                if (m_dvr.Initialize(arguments[1],
                                    int.Parse(arguments[2]),
                                    arguments[3],
                                    false,
                                    int.Parse(arguments[4]),
                                    int.Parse(arguments[5]),
                                    out int result) == false)
                {
                    LogToFile("Failed to intialize DVR" + result);
                    return;
                }
            }
            catch (Exception err)
            {
                LogToFile(err.Message);
            }
        }

        void P2Host(string fileName,
                   double PCR,
                   int fileState)
        {

            LogToFile(": Message arrived " + fileName);

            DVRInfo d = new DVRInfo();
            d.fileName = fileName;
            d.fileState = fileState;
            d.PCR = PCR;
            d.StartDate = DateTime.Now;
            d.StopDate = DateTime.Now;
            m_dvrinfoQueue.Enqueue(d);
            m_logEvent.Set();

            LogToFile("Queueing.. ");

        }

        void LogToFile( string msg)
        {
            lock (this)
            {
                File.AppendAllText(logFile, m_segementId + ": " + msg + Environment.NewLine);
            }
        }

        void DVRLogThread(int segmentId)
        {
            LogToFile("Thread started");

            try
            {
                while (m_running)
                {
                    m_logEvent.WaitOne();
                    LogToFile("Event triggered ");
                    if (m_running == false)
                        return;
                    if (m_dvrinfoQueue.TryDequeue(out DVRInfo d) == false)
                        continue;

                    LogToFile("here 111 ");
                    if (d.fileState == 1)
                    {

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            string queryString = @" 

                    INSERT INTO  DVRSegment" + segmentId.ToString() + @"
                                (StartDate
                                ,StopDate
                                ,fileName
                                ,PCR
                                ,fileState)
                            VALUES
                                (@StartDate,
                                @StopDate,
                                @fileName,
                                @PCR,
                                @fileState)";

                            queryString = queryString.Replace('\n', ' ');
                            queryString = queryString.Replace('\r', ' ');

                            LogToFile(queryString);

                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@StartDate", DateTime.Now);
                            command.Parameters.AddWithValue("@StopDate", DateTime.Now);
                            command.Parameters.AddWithValue("@fileName", d.fileName);
                            command.Parameters.AddWithValue("@PCR", d.PCR);
                            command.Parameters.AddWithValue("@fileState", d.fileState);

                            connection.Open();
                            int x = command.ExecuteNonQuery();
                            connection.Close();

                        }

                    }
                    else
                    {

                        using (SqlConnection connection = new SqlConnection(connectionString))
                        {
                            string queryString = "UPDATE DVRSegment" + segmentId.ToString() + @" SET fileState=@fileState , StopDate=@StopDate  where fileName =@fileName";
                            SqlCommand command = new SqlCommand(queryString, connection);
                            command.Parameters.AddWithValue("@fileName", d.fileName);
                            command.Parameters.AddWithValue("@fileState", d.fileState);
                            command.Parameters.AddWithValue("@StopDate", d.StopDate);
                            connection.Open();
                            int x = command.ExecuteNonQuery();
                            connection.Close();
                        }
                    }
                }
            }
            catch (Exception err)
            {
                LogToFile("Error:" + segmentId.ToString() + err.Message);
            }
            
        }
         
        protected override void OnStop()
        { 
            
            m_running = false;
            if (m_logEvent != null)
                m_logEvent.Set();
            if (m_dvr != null)
                m_dvr.Close();
            
        }
    }
}
