﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AvCommon
{

    public enum AV_GRAPH_MODE
    {
        FROM_UDP_MULTICAST,
        FROM_CAMERA_TO_AV_OBJ_SERVER,
        FROM_CAMERA_TO_TCP_SENDER,
        FROM_CAMERA_TO_TS_TO_TCP_SENDER,
        FROM_CAMERA_TO_TS_DUMP,
        FROM_TCP_PUSH_SERVER_TO_AVSERVER,
        SPARE,
        FROM_WEB_CAM_TO_DUMP,
        FROM_MULTICAST_DUMP,
        FROM_MULTICAST_SCREEN,
        FROM_CAMERA_TO_TS_MULTICAST

    }; 

    public class AppConfig
    {
        public string mcastAddress;
        public int mcastPort;
        public string avObjectServerUrl;
        public AV_GRAPH_MODE avGraphMode;

    }

    public class AppSettings
    {

        string m_fileName;
        public AppSettings(string fileName)
        {
            m_fileName = fileName;
        }

        AppConfig m_config;

        void Default()
        {
            m_config.mcastAddress = "227.1.1.1";
            m_config.mcastPort = 10200;
            m_config.avObjectServerUrl = "http://10.0.0.0:8051/";
        }

        public AppConfig Config
        {
            get
            {
                return m_config;
            }
        }

        public string Save()
        {
            try
            {

                string json = JsonConvert.SerializeObject(m_config);
                string jsonFormatted = JValue.Parse(json).ToString(Formatting.Indented);
                File.WriteAllText(m_fileName, jsonFormatted);

                return "ok";
            }
            catch (Exception err)
            {
                return err.Message;
            }
        }

        public string Load()
        {
            try
            {
             
                if (File.Exists(m_fileName) == false)
                {
                    m_config = new AppConfig();
                    Default();
                    Save();
                    return "file not found";
                }
                string text = File.ReadAllText(m_fileName);
                m_config = JsonConvert.DeserializeObject<AppConfig>(text);
                if (m_config == null)
                {
                    m_config = new AppConfig();
                    Default();
                    Save();
                    return "failed";
                }
                return "ok";
            }
            catch (Exception err)
            {
                m_config = new AppConfig();
                return err.Message;
            }
        }

    }
    
}
