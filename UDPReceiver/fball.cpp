//------------------------------------------------------------------------------
// File: FBall.cpp
//
// Desc: DirectShow sample code - implementation of filter behaviors
//       for the bouncing ball source filter.  For more information,
//       refer to Ball.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <winsock2.h>
#include <ws2ipdef.h>
#include <streams.h>
#include <olectl.h>
#include <initguid.h>
#include "ball.h"
#include "fball.h"
#include <memory>
#include <thread>
#include <time.h>  

SOCKET m_socket;
#define IMAGE_SIZE 188 * 400

#pragma warning(disable:4710)  // 'function': function not inlined (optimzation)

// Setup data

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
    &MEDIATYPE_Video,       // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

const AMOVIESETUP_PIN sudOpPin =
{
    L"Output",              // Pin string name
    FALSE,                  // Is it rendered
    TRUE,                   // Is it an output
    FALSE,                  // Can we have none
    FALSE,                  // Can we have many
    &CLSID_NULL,            // Connects to filter
    NULL,                   // Connects to pin
    1,                      // Number of types
    &sudOpPinTypes };       // Pin details

const AMOVIESETUP_FILTER sudBallax =
{
    &CLSID_BouncingBall,    // Filter CLSID
    L"Bauotech UDP Receiver",       // String name
    MERIT_DO_NOT_USE,       // Filter merit
    1,                      // Number pins
    &sudOpPin               // Pin details
};


// COM global table of objects in this dll

CFactoryTemplate g_Templates[] = {
  { L"Bauotech UDP Receiver"
  , &CLSID_BouncingBall
  , CBouncingBall::CreateInstance
  , NULL
  , &sudBallax }
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterServer
//
// Exported entry points for registration and unregistration
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);

} // DllRegisterServer
uint8_t *videoBuffer = NULL;

//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

//
// CreateInstance
//
// The only allowed way to create Bouncing balls!
//
CUnknown * WINAPI CBouncingBall::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);

    CUnknown *punk = new CBouncingBall(lpunk, phr);
    if(punk == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;
    }
    return punk;

} // CreateInstance


//
// Constructor
//
// Initialise a CBallStream object so that we have a pin.
//
CBouncingBall::CBouncingBall(LPUNKNOWN lpunk, HRESULT *phr) :
    CSource(NAME("Bauotech UDP Receiver"), lpunk, CLSID_BouncingBall)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cStateLock);

    m_paStreams = (CSourceStream **) new CBallStream*[1];
    if(m_paStreams == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }

    m_paStreams[0] = new CBallStream(phr, this, L"Out");
    if(m_paStreams[0] == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }
	pOutPin = (CBallStream *)m_paStreams[0];


	pOutPin->m_hlsRd = 0;
	pOutPin->m_hlsWr = 0;
	//Setup(true, "10.0.0.17" , 6000, "234.5.5.5");
	Setup(false, "127.0.0.1", 5004, "");

} // (Constructor)

STDMETHODIMP CBouncingBall::Run(REFERENCE_TIME tStart)
{
	  
	return CBaseFilter::Run(tStart);
}
STDMETHODIMP CBouncingBall::Pause()
{
	return CBaseFilter::Pause();

}
STDMETHODIMP CBouncingBall::Stop()
{
	pOutPin->m_running = false;
	return CBaseFilter::Stop();
}

int CBouncingBall::Setup(bool multicast,  char *interfaceAddress, int port, char *multicastAddress)
{
	int iResult;

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		perror("WSAStartup");
		return 1;
	}

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return -1;
	}
	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;

	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return 1;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	DWORD ip = inet_addr(interfaceAddress);
	addr.sin_addr.s_addr = ip;
	addr.sin_port = htons(port);

	// bind to receive address
	//
	if (::bind(ReceivingSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}
	m_socket = ReceivingSocket;


	if (multicast == true)
	{
		IP_MREQ mreq;
		mreq.imr_multiaddr.s_addr = inet_addr("234.5.5.5");
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if (setsockopt(ReceivingSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
		{
			return -5;
		}
	}

	return 1;

}


//
// Constructor
//
CBallStream::CBallStream(HRESULT *phr,
                         CBouncingBall *pParent,
                         LPCWSTR pPinName) :
    CSourceStream(NAME("Bouncing Ball"),phr, pParent, pPinName),
    
    m_iDefaultRepeatTime(20)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cSharedState);

	m_runCount = 0;
	  
} // (Constructor)
  
CBallStream::~CBallStream()
{
    CAutoLock cAutoLock(&m_cSharedState);
	 
    

}  
 

int CBallStream::GetFifoEmptiness()
{
	//mutex.Enter();
	uint32_t x;
	if (m_hlsWr == m_hlsRd)
		x = MAX_HLS_FILES;
	else
		if (m_hlsWr > m_hlsRd)
			x = MAX_HLS_FILES - (m_hlsWr - m_hlsRd);
		else
			x = MAX_HLS_FILES - (MAX_HLS_FILES - m_hlsRd + m_hlsWr);

	//mutex.Leave();
	return x;

}
int CBallStream::GetFifoFullness()
{
	//mutex.Enter();
	uint32_t x;
	if (m_hlsWr == m_hlsRd)
		x = 0;
	else
		if (m_hlsWr > m_hlsRd)
			x = m_hlsWr - m_hlsRd;
		else
			x = MAX_HLS_FILES - m_hlsRd + m_hlsWr;

	//mutex.Leave();
	return x;
}
  

int m_index = 0;
uint8_t buffer[10000];
HRESULT CBallStream::FillBuffer(IMediaSample *pms)
{
    CheckPointer(pms,E_POINTER);
    
    BYTE *pData;
    long lDataLen;

    pms->GetPointer(&pData);
    lDataLen = pms->GetSize();

	 
	int size = recv(
		m_socket,
		(char *)(buffer),
		10000,
		0);
	if (size == -1)
	{
		return NOERROR;
	}
	memcpy(videoBuffer + m_index, buffer, size);
	m_index += size;
	if (m_index >= lDataLen)
	{
		memcpy(pData, videoBuffer, lDataLen);
		pms->SetActualDataLength(lDataLen);
		m_index = 0;
	}
	else {
		pms->SetActualDataLength(0);
	}
	//Sleep(0);
	 
#if 0 
    // The current time is the sample's start
    CRefTime rtStart = m_rtSampleTime;

    // Increment to find the finish time
    m_rtSampleTime += (LONG)m_iRepeatTime;

    pms->SetTime((REFERENCE_TIME *) &rtStart,(REFERENCE_TIME *) &m_rtSampleTime);
#endif 
    //pms->SetSyncPoint(TRUE);
    return NOERROR;

} 

 
STDMETHODIMP CBallStream::Notify(IBaseFilter * pSender, Quality q)
{
    // Adjust the repeat rate.
    if(q.Proportion<=0)
    {
        m_iRepeatTime = 1000;        // We don't go slower than 1 per second
    }
    else
    {
        m_iRepeatTime = m_iRepeatTime*1000 / q.Proportion;
        if(m_iRepeatTime>1000)
        {
            m_iRepeatTime = 1000;    // We don't go slower than 1 per second
        }
        else if(m_iRepeatTime<10)
        {
            m_iRepeatTime = 10;      // We don't go faster than 100/sec
        }
    }

    // skip forwards
    if(q.Late > 0)
        m_rtSampleTime += q.Late;

    return NOERROR;

} // Notify

int m_iImageWidth = 640;
int m_iImageHeight = 480;

int m_pixelDepth = BI_RGB;
int m_bitCount = 32;

//
// GetMediaType
 
HRESULT CBallStream::GetMediaType(int iPosition, CMediaType *pmt)
{
	CheckPointer(pmt, E_POINTER);

	CAutoLock cAutoLock(m_pFilter->pStateLock());
	if (iPosition < 0)
	{
		return E_INVALIDARG;
	}

	// Have we run off the end of types?

	if (iPosition > 1)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	VIDEOINFO *pvi = (VIDEOINFO *)pmt->AllocFormatBuffer(sizeof(VIDEOINFO));
	if (NULL == pvi)
		return(E_OUTOFMEMORY);

	ZeroMemory(pvi, sizeof(VIDEOINFO));


	if (m_pixelDepth == BI_RGB && m_bitCount == 32)
	{
		// Return our highest quality 32bit format

		// since we use RGB888 (the default for 32 bit), there is
		// no reason to use BI_BITFIELDS to specify the RGB
		// masks. Also, not everything supports BI_BITFIELDS

		 
		pvi->bmiHeader.biCompression = BI_RGB;
		pvi->bmiHeader.biBitCount = 32;
		
	}

	if (m_pixelDepth == BI_RGB && m_bitCount == 24)
	{   // Return our 24bit format

		 
		pvi->bmiHeader.biCompression = BI_RGB;
		pvi->bmiHeader.biBitCount = 24;

	}

	if (m_pixelDepth == BI_BITFIELDS && m_bitCount == 16)
	{
		// 16 bit per pixel RGB565

		// Place the RGB masks as the first 3 doublewords in the palette area
		for (int i = 0; i < 3; i++)
			pvi->TrueColorInfo.dwBitMasks[i] = bits565[i];

	 
		pvi->bmiHeader.biCompression = BI_BITFIELDS;
		pvi->bmiHeader.biBitCount = 16;

	}

	if (m_pixelDepth == BI_BITFIELDS && m_bitCount == 555)
	{   // 16 bits per pixel RGB555

		// Place the RGB masks as the first 3 doublewords in the palette area
		for (int i = 0; i < 3; i++)
			pvi->TrueColorInfo.dwBitMasks[i] = bits555[i];

	 
		pvi->bmiHeader.biCompression = BI_BITFIELDS;
		pvi->bmiHeader.biBitCount = 16;

	}

	if (m_pixelDepth == BI_RGB && m_bitCount == 8)
	{   // 8 bit palettised

		 
		pvi->bmiHeader.biCompression = BI_RGB;
		pvi->bmiHeader.biBitCount = 8;
		pvi->bmiHeader.biClrUsed = iPALETTE_COLORS;

	}

	 

	pvi->bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	pvi->bmiHeader.biWidth = m_iImageWidth;
	pvi->bmiHeader.biHeight = m_iImageHeight;
	pvi->bmiHeader.biPlanes = 1;
	pvi->bmiHeader.biSizeImage = GetBitmapSize(&pvi->bmiHeader);
	pvi->bmiHeader.biClrImportant = 0;

	SetRectEmpty(&(pvi->rcSource)); // we want the whole image area rendered.
	SetRectEmpty(&(pvi->rcTarget)); // no particular destination rectangle

	pmt->SetType(&MEDIATYPE_Video);
	pmt->SetFormatType(&FORMAT_VideoInfo);
	pmt->SetTemporalCompression(FALSE);

	// Work out the GUID for the subtype from the header info.
	const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
	pmt->SetSubtype(&SubTypeGUID);
	pmt->SetSampleSize(pvi->bmiHeader.biSizeImage);

	return NOERROR;
 

} // GetMediaType

 
HRESULT CBallStream::CheckMediaType(const CMediaType *pMediaType)
{
	CheckPointer(pMediaType, E_POINTER);

	if ((*(pMediaType->Type()) != MEDIATYPE_Video) ||   // we only output video
		!(pMediaType->IsFixedSize()))                   // in fixed size samples
	{
		return E_INVALIDARG;
	}

	// Check for the subtypes we support
	const GUID *SubType = pMediaType->Subtype();
	if (SubType == NULL)
		return E_INVALIDARG;

	if ((*SubType != MEDIASUBTYPE_RGB8)
		&& (*SubType != MEDIASUBTYPE_RGB565)
		&& (*SubType != MEDIASUBTYPE_RGB555)
		&& (*SubType != MEDIASUBTYPE_RGB24)
		&& (*SubType != MEDIASUBTYPE_RGB32))
	{
		return E_INVALIDARG;
	}

	// Get the format area of the media type
	VIDEOINFO *pvi = (VIDEOINFO *)pMediaType->Format();

	if (pvi == NULL)
		return E_INVALIDARG;

	// Check the image size. As my default ball is 10 pixels big
	// look for at least a 20x20 image. This is an arbitary size constraint,
	// but it avoids balls that are bigger than the picture...

	if ((pvi->bmiHeader.biWidth < 20) || (abs(pvi->bmiHeader.biHeight) < 20))
	{
		return E_INVALIDARG;
	}
	 

	return S_OK;  // This format is acceptable.
	 

} // CheckMediaType

 
HRESULT CBallStream::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
    CheckPointer(pAlloc,E_POINTER);
    CheckPointer(pProperties,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    HRESULT hr = NOERROR;

    
    pProperties->cBuffers = 1;
	pProperties->cbBuffer = IMAGE_SIZE;

    ASSERT(pProperties->cbBuffer);

    // Ask the allocator to reserve us some sample memory, NOTE the function
    // can succeed (that is return NOERROR) but still not have allocated the
    // memory that we requested, so we must check we got whatever we wanted

    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pProperties,&Actual);
    if(FAILED(hr))
    {
        return hr;
    }

    // Is this allocator unsuitable

    if(Actual.cbBuffer < pProperties->cbBuffer)
    {
        return E_FAIL;
    }

    // Make sure that we have only 1 buffer (we erase the ball in the
    // old buffer to save having to zero a 200k+ buffer every time
    // we draw a frame)

    ASSERT(Actual.cBuffers == 1);
    return NOERROR;

} // DecideBufferSize


/*
HRESULT CBallStream::SetMediaType(const CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    // Pass the call up to my base class

    HRESULT hr = CSourceStream::SetMediaType(pMediaType);

	return NOERROR;    

} // SetMediaType
*/

//
// OnThreadCreate
//
// As we go active reset the stream time to zero
//
HRESULT CBallStream::OnThreadCreate()
{
	m_running = true;
	m_runCount++;
    CAutoLock cAutoLockShared(&m_cSharedState);
    m_rtSampleTime = 0;	
	 
	m_hlsRd = 0;
	m_hlsWr = 0;
	 
	if (videoBuffer == NULL)
	{
		videoBuffer = (uint8_t *)malloc(m_iImageWidth * m_iImageHeight * 4);
	}

 

    // we need to also reset the repeat time in case the system
    // clock is turned off after m_iRepeatTime gets very big
    m_iRepeatTime = m_iDefaultRepeatTime;

    return NOERROR;

} // OnThreadCreate


 

 

