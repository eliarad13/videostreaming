//------------------------------------------------------------------------------
// File: FBall.cpp
//
// Desc: DirectShow sample code - implementation of filter behaviors
//       for the bouncing ball source filter.  For more information,
//       refer to Ball.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <streams.h>
#include <olectl.h>
#include <initguid.h>
#include "ball.h"
#include "fball.h"
#include "ArenaCApi.h"

// timeout for detecting camera devices (in milliseconds).
#define SYSTEM_TIMEOUT 100
acSystem hSystem = NULL;
acDevice hDevice = NULL;


// Image timeout
//    Timeout for grabbing images (in milliseconds). If no image is available at
//    the end of the timeout, an exception is thrown. The timeout is the maximum
//    time to wait for an image; however, getting an image will return as soon as
//    an image is available, not waiting the full extent of the timeout.
#define IMAGE_TIMEOUT 2000

// maximum buffer length
#define MAX_BUF 1024


#pragma warning(disable:4710)  // 'function': function not inlined (optimzation)

// Setup data

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
    &MEDIATYPE_Video,       // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

const AMOVIESETUP_PIN sudOpPin =
{
    L"Output",              // Pin string name
    FALSE,                  // Is it rendered
    TRUE,                   // Is it an output
    FALSE,                  // Can we have none
    FALSE,                  // Can we have many
    &CLSID_NULL,            // Connects to filter
    NULL,                   // Connects to pin
    1,                      // Number of types
    &sudOpPinTypes };       // Pin details

const AMOVIESETUP_FILTER sudBallax =
{
	&CLSID_BauotechLucidSwirCamera,    // Filter CLSID
    L"Bauotech Lucid Swir Camera",       // String name
    MERIT_DO_NOT_USE,       // Filter merit
    1,                      // Number pins
    &sudOpPin               // Pin details
};


// COM global table of objects in this dll

CFactoryTemplate g_Templates[] = {
  { L"Bauotech Lucid Swir Camera"
  , &CLSID_BauotechLucidSwirCamera
  , CBouncingBall::CreateInstance
  , NULL
  , &sudBallax }
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterServer
//
// Exported entry points for registration and unregistration
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

//
// CreateInstance
//
// The only allowed way to create Bouncing balls!
//
CUnknown * WINAPI CBouncingBall::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);

    CUnknown *punk = new CBouncingBall(lpunk, phr);
    if(punk == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;
    }
    return punk;

} // CreateInstance


//
// Constructor
//
// Initialise a CBallStream object so that we have a pin.
//
CBouncingBall::CBouncingBall(LPUNKNOWN lpunk, HRESULT *phr) :
CSource(NAME("Bouncing ball"), lpunk, CLSID_BauotechLucidSwirCamera)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cStateLock);

    m_paStreams = (CSourceStream **) new CBallStream*[1];
    if(m_paStreams == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }

    m_paStreams[0] = new CBallStream(phr, this, L"Out");
    if(m_paStreams[0] == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;

        return;
    }

} // (Constructor)



STDMETHODIMP CBouncingBall::GetState(DWORD dwMSecs, FILTER_STATE* State)
{
	HRESULT hr = CSource::GetState(dwMSecs, State);
	if (FAILED(hr))
		return hr;
	if (*State == State_Paused)
	{
		return VFW_S_CANT_CUE;
	}
	return hr;
}

bool m_running = false;
STDMETHODIMP CBouncingBall::Run(REFERENCE_TIME tStart)
{
	CAutoLock cObjectLock(m_pLock);

	AC_ERROR err = AC_ERR_SUCCESS;

	m_running = false;

	err = acDeviceStartStream(hDevice);
	if (err != AC_ERR_SUCCESS)
	{
		//::MessageBox(NULL, L"Swir Camera filter: Failed Start Stream", L"Lucid camera ds filter", 0);
		return err;
	}
	
	m_running = true;

	return CBaseFilter::Run(tStart);
}

STDMETHODIMP CBouncingBall::Stop()
{
	CAutoLock cObjectLock(m_pLock);

	if (hDevice != NULL)
	{
		AC_ERROR err = AC_ERR_SUCCESS;
		err = acDeviceStopStream(hDevice);
		m_running = false;
	}

	return CBaseFilter::Stop();
}

 
STDMETHODIMP CBouncingBall::Pause()
{
	CAutoLock cObjectLock(m_pLock);
	 

	return CBaseFilter::Pause();
}



AC_ERROR GetNodeValue(acNodeMap hNodeMap, const char* nodeName, char* pValue, size_t* pLen)
{
	AC_ERROR err = AC_ERR_SUCCESS;

	// get node
	acNode hNode = NULL;
	AC_ACCESS_MODE accessMode = 0;

	err = acNodeMapGetNodeAndAccessMode(hNodeMap, nodeName, &hNode, &accessMode);
	if (err != AC_ERR_SUCCESS)
		return err;

	// check access mode
	if (accessMode != AC_ACCESS_MODE_RO && accessMode != AC_ACCESS_MODE_RW)
		return AC_ERR_ERROR;

	// get value
	err = acValueToString(hNode, pValue, pLen);
	return err;
}

// sets node value
// (1) gets node
// (2) checks access mode
// (3) gets value
AC_ERROR SetNodeValue(acNodeMap hNodeMap, const char* nodeName, const char* pValue)
{
	AC_ERROR err = AC_ERR_SUCCESS;

	// get node
	acNode hNode = NULL;
	AC_ACCESS_MODE accessMode = 0;

	err = acNodeMapGetNodeAndAccessMode(hNodeMap, nodeName, &hNode, &accessMode);
	if (err != AC_ERR_SUCCESS)
		return err;

	// check access mode
	if (accessMode != AC_ACCESS_MODE_WO && accessMode != AC_ACCESS_MODE_RW)
		return AC_ERR_ERROR;

	// get value
	err = acValueFromString(hNode, pValue);
	return err;
}


//
// Constructor
//
CBallStream::CBallStream(HRESULT *phr,
                         CBouncingBall *pParent,
                         LPCWSTR pPinName) :
    CSourceStream(NAME("Bouncing Ball"),phr, pParent, pPinName),
    m_iImageWidth(1280),
    m_iImageHeight(1024),
    m_iDefaultRepeatTime(20)
{
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cSharedState);


	AC_ERROR err = AC_ERR_SUCCESS;

	// prepare example
	
	err = acOpenSystem(&hSystem);
	if (err != 0)
	{
		//::MessageBox(NULL, L"Failed to open swir camera system", L"Lucid camera ds filter", 0);
		return;			
	}
	
	err = acSystemUpdateDevices(hSystem, SYSTEM_TIMEOUT);
	if (err != 0)
	{
		//::MessageBox(NULL, L"Failed to get system updated devices", L"Lucid camera ds filter", 0);
		return;
	}
	
	size_t numDevices = 0;
	err = acSystemGetNumDevices(hSystem, &numDevices);
 
	if (numDevices == 0)
	{	
		//::MessageBox(NULL, L"No swir camera devices", L"Lucid camera ds filter", 0);
		return;
	}
	
	err = acSystemCreateDevice(hSystem, 0, &hDevice);
	if (err != 0)
	{
		//::MessageBox(NULL, L"Failed to create swir camera device", L"Lucid camera ds filter", 0);
		return;
	}

	 
	// get node map
	acNodeMap hNodeMap = NULL;

	err = acDeviceGetNodeMap(hDevice, &hNodeMap);
	if (err != AC_ERR_SUCCESS)
		return;

	// get node values that will be changed in order to return their values at
	// the end of the example
	char pAcquisitionModeInitial[MAX_BUF];
	size_t len = MAX_BUF;

	err = GetNodeValue(hNodeMap, "AcquisitionMode", pAcquisitionModeInitial, &len);
	if (err != AC_ERR_SUCCESS)
		return;

	// Set acquisition mode
	//    Set acquisition mode before starting the stream. Starting the stream
	//    requires the acquisition mode to be set beforehand. The acquisition
	//    mode controls the number of images a device acquires once the stream
	//    has been started. Setting the acquisition mode to 'Continuous' keeps
	//    the stream from stopping. This example returns the camera to its
	//    initial acquisition mode near the end of the example.
	 
	err = SetNodeValue(
		hNodeMap,
		"AcquisitionMode",
		"Continuous");

	if (err != AC_ERR_SUCCESS)
		return;

	// Set buffer handling mode
	//    Set buffer handling mode before starting the stream. Starting the
	//    stream requires the buffer handling mode to be set beforehand. The
	//    buffer handling mode determines the order and behavior of buffers in
	//    the underlying stream engine. Setting the buffer handling mode to
	//    'NewestOnly' ensures the most recent image is delivered, even if it
	//    means skipping frames.
 

	// get stream node map
	acNodeMap hTLStreamNodeMap = NULL;

	err = acDeviceGetTLStreamNodeMap(hDevice, &hTLStreamNodeMap);
	if (err != AC_ERR_SUCCESS)
		return;

	err = SetNodeValue(
		hTLStreamNodeMap,
		"StreamBufferHandlingMode",
		"NewestOnly");

	// Enable stream auto negotiate packet size
	//    Setting the stream packet size is done before starting the stream.
	//    Setting the stream to automatically negotiate packet size instructs
	//    the camera to receive the largest packet size that the system will
	//    allow. This generally increases frame rate and results in fewer
	//    interrupts per image, thereby reducing CPU load on the host system.
	//    Ethernet settings may also be manually changed to allow for a
	//    larger packet size.
	 

	err = acNodeMapSetBooleanValue(hTLStreamNodeMap, "StreamAutoNegotiatePacketSize", true);
	if (err != AC_ERR_SUCCESS)
		return;

	// Enable stream packet resend
	//    Enable stream packet resend before starting the stream. Images are
	//    sent from the camera to the host in packets using UDP protocol,
	//    which includes a header image number, packet number, and timestamp
	//    information. If a packet is missed while receiving an image, a
	//    packet resend is requested and this information is used to retrieve
	//    and redeliver the missing packet in the correct order.
	 

	err = acNodeMapSetBooleanValue(hTLStreamNodeMap, "StreamPacketResendEnable", true);
	if (err != AC_ERR_SUCCESS)
		return;

	// Start stream
	//    Start the stream before grabbing any images. Starting the stream
	//    allocates buffers, which can be passed in as an argument (default: 10),
	//    and begins filling them with data. Starting the stream blocks write
	//    access to many features such as width, height, and pixel format, as
	//    well as acquisition and buffer handling modes. The stream needs
	//    to be stopped later.
	  

} // (Constructor)


//
// Destructor
//
CBallStream::~CBallStream()
{
    CAutoLock cAutoLock(&m_cSharedState);
	AC_ERROR err = AC_ERR_SUCCESS;

	// clean up example
	err = acSystemDestroyDevice(hSystem, hDevice);
	 
	err = acCloseSystem(hSystem);
 
 

} // (Destructor)


//
// FillBuffer
//
// Plots a ball into the supplied video buffer
//
HRESULT CBallStream::FillBuffer(IMediaSample *pms)
{
    CheckPointer(pms,E_POINTER);
     
	if (m_running == false)
	{
		pms->SetActualDataLength(0);
		pms->SetSyncPoint(TRUE);
		return S_OK;
	}

    BYTE *pData;
    long lDataLen;
	acBuffer hBuffer = NULL;
    pms->GetPointer(&pData);
    lDataLen = pms->GetSize(); 
	size_t sizeFilled = 0;

	AC_ERROR err = AC_ERR_SUCCESS;

#if 1 
	err = acDeviceGetBuffer(hDevice, IMAGE_TIMEOUT, &hBuffer);
	if (err != AC_ERR_SUCCESS)
	{
		pms->SetActualDataLength(0);
		pms->SetSyncPoint(TRUE);
		//::MessageBox(NULL, L"Failed accuire buffer", L"Lucid camera ds filter", 0);
		return S_OK;
	}
	  
	  
	acBuffer hConverted = NULL;

	// pixel format
	#define PIXEL_FORMAT PFNC_BGRa8 // BGR8

	err = acImageFactoryConvert(hBuffer, PIXEL_FORMAT, &hConverted);
	if (err != AC_ERR_SUCCESS)
	{
		//::MessageBox(NULL, L"Failed to convert buffer", L"Lucid camera ds filter", 0);
		return err;
	}
	// get image
	uint8_t* pImageData = NULL;

	err = acImageGetData(hConverted, &pImageData);
	if (err != AC_ERR_SUCCESS)
		return err;

	err = acBufferGetSizeFilled(hConverted, &sizeFilled);
	if (err != AC_ERR_SUCCESS)
	{
		//::MessageBox(NULL, L"Failed get buffer size", L"Lucid camera ds filter", 0);
		return err;
	}

	memcpy(pData, pImageData, sizeFilled);
	

#endif 




#if 0 

	#define WIDTH  1280
	#define HEIGHT 1024
	#define CHANNELS 4 // 32-bit: 8 bits for each channel (RGBA)

	sizeFilled = WIDTH* HEIGHT * CHANNELS;
	// Fill the buffer with stripe color bars (alternating red, green, blue)
	for (int y = 0; y < HEIGHT; y++) {
		for (int x = 0; x < WIDTH; x++) {
			int index = (y * WIDTH + x) * CHANNELS;
			if (x < WIDTH / 3) {
				// Red bar
				pData[index + 0] = 255; // R
				pData[index + 1] = 0;   // G
				pData[index + 2] = 0;   // B
				pData[index + 3] = 255; // A (alpha, fully opaque)
			}
			else if (x < 2 * WIDTH / 3) {
				// Green bar
				pData[index + 0] = 0;   // R
				pData[index + 1] = 255; // G
				pData[index + 2] = 0;   // B
				pData[index + 3] = 255; // A
			}
			else {
				// Blue bar
				pData[index + 0] = 0;   // R
				pData[index + 1] = 0;   // G
				pData[index + 2] = 255; // B
				pData[index + 3] = 255; // A
			}
		}
	}
#endif 	 
	    
    CAutoLock cAutoLockShared(&m_cSharedState);

    // If we haven't just cleared the buffer delete the old
    // ball and move the ball on

    // The current time is the sample's start
    CRefTime rtStart = m_rtSampleTime;

    // Increment to find the finish time
    m_rtSampleTime += (LONG)m_iRepeatTime;

    pms->SetTime((REFERENCE_TIME *) &rtStart,(REFERENCE_TIME *) &m_rtSampleTime);
    

	pms->SetActualDataLength(sizeFilled);
    pms->SetSyncPoint(TRUE);



	// Requeue image buffer
	//    Requeue an image buffer when access to it is no longer needed.
	//    Notice that failing to requeue buffers may cause memory to leak and
	//    may also result in the stream engine being starved due to a lack
	//    of available buffers.

#if 1 
	err = acDeviceRequeueBuffer(hDevice, hBuffer);
	 

	// destroy converted image
	err = acImageFactoryDestroy(hConverted);
	 
#endif 
    return NOERROR;

} // FillBuffer


//
// Notify
//
// Alter the repeat rate according to quality management messages sent from
// the downstream filter (often the renderer).  Wind it up or down according
// to the flooding level - also skip forward if we are notified of Late-ness
//
STDMETHODIMP CBallStream::Notify(IBaseFilter * pSender, Quality q)
{
    // Adjust the repeat rate.
    if(q.Proportion<=0)
    {
        m_iRepeatTime = 1000;        // We don't go slower than 1 per second
    }
    else
    {
        m_iRepeatTime = m_iRepeatTime*1000 / q.Proportion;
        if(m_iRepeatTime>1000)
        {
            m_iRepeatTime = 1000;    // We don't go slower than 1 per second
        }
        else if(m_iRepeatTime<10)
        {
            m_iRepeatTime = 10;      // We don't go faster than 100/sec
        }
    }

    // skip forwards
    if(q.Late > 0)
        m_rtSampleTime += q.Late;

    return NOERROR;

} // Notify


//
// GetMediaType
//
 
int m_pixelDepth = BI_RGB;
int m_bitCount = 32;

HRESULT CBallStream::GetMediaType(int iPosition, CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    if(iPosition < 0)
    {
        return E_INVALIDARG;
    }

    // Have we run off the end of types?

    if(iPosition > 1)
    {
        return VFW_S_NO_MORE_ITEMS;
    }

    VIDEOINFO *pvi = (VIDEOINFO *) pmt->AllocFormatBuffer(sizeof(VIDEOINFO));
    if(NULL == pvi)
        return(E_OUTOFMEMORY);

    ZeroMemory(pvi, sizeof(VIDEOINFO));

    
	if (m_pixelDepth == BI_RGB && m_bitCount == 32)
	{
		// Return our highest quality 32bit format

        // since we use RGB888 (the default for 32 bit), there is
        // no reason to use BI_BITFIELDS to specify the RGB
        // masks. Also, not everything supports BI_BITFIELDS

        //SetPaletteEntries(Yellow);
        pvi->bmiHeader.biCompression = BI_RGB;
        pvi->bmiHeader.biBitCount    = 32;        
    }

	if (m_pixelDepth == BI_RGB && m_bitCount == 24)
    {   // Return our 24bit format

        //SetPaletteEntries(Green);
        pvi->bmiHeader.biCompression = BI_RGB;
        pvi->bmiHeader.biBitCount    = 24;
        
    }

	if (m_pixelDepth == BI_BITFIELDS && m_bitCount == 16)
    {       
        // 16 bit per pixel RGB565

        // Place the RGB masks as the first 3 doublewords in the palette area
        for(int i = 0; i < 3; i++)
            pvi->TrueColorInfo.dwBitMasks[i] = bits565[i];

        //SetPaletteEntries(Blue);
        pvi->bmiHeader.biCompression = BI_BITFIELDS;
        pvi->bmiHeader.biBitCount    = 16;
            
    }

	if (m_pixelDepth == BI_BITFIELDS && m_bitCount == 555)
    {   // 16 bits per pixel RGB555

        // Place the RGB masks as the first 3 doublewords in the palette area
        for(int i = 0; i < 3; i++)
            pvi->TrueColorInfo.dwBitMasks[i] = bits555[i];

        SetPaletteEntries(Blue);
        pvi->bmiHeader.biCompression = BI_BITFIELDS;
        pvi->bmiHeader.biBitCount    = 16;
 
    }

	if (m_pixelDepth == BI_RGB && m_bitCount == 8)
    {   // 8 bit palettised

        SetPaletteEntries(Red);
        pvi->bmiHeader.biCompression = BI_RGB;
        pvi->bmiHeader.biBitCount    = 8;
        pvi->bmiHeader.biClrUsed        = iPALETTE_COLORS;
           
    }
    

    // (Adjust the parameters common to all formats...)

    // put the optimal palette in place
    for(int i = 0; i < iPALETTE_COLORS; i++)
    {
        pvi->TrueColorInfo.bmiColors[i].rgbRed      = m_Palette[i].peRed;
        pvi->TrueColorInfo.bmiColors[i].rgbBlue     = m_Palette[i].peBlue;
        pvi->TrueColorInfo.bmiColors[i].rgbGreen    = m_Palette[i].peGreen;
        pvi->TrueColorInfo.bmiColors[i].rgbReserved = 0;
    }

    pvi->bmiHeader.biSize       = sizeof(BITMAPINFOHEADER);
    pvi->bmiHeader.biWidth      = m_iImageWidth;
    pvi->bmiHeader.biHeight     = m_iImageHeight;
    pvi->bmiHeader.biPlanes     = 1;
    pvi->bmiHeader.biSizeImage  = GetBitmapSize(&pvi->bmiHeader);
    pvi->bmiHeader.biClrImportant = 0;

    SetRectEmpty(&(pvi->rcSource)); // we want the whole image area rendered.
    SetRectEmpty(&(pvi->rcTarget)); // no particular destination rectangle

    pmt->SetType(&MEDIATYPE_Video);
    pmt->SetFormatType(&FORMAT_VideoInfo);
    pmt->SetTemporalCompression(FALSE);

    // Work out the GUID for the subtype from the header info.
    const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
    pmt->SetSubtype(&SubTypeGUID);
    pmt->SetSampleSize(pvi->bmiHeader.biSizeImage);

    return NOERROR;

} // GetMediaType


//
// CheckMediaType
//
// We will accept 8, 16, 24 or 32 bit video formats, in any
// image size that gives room to bounce.
// Returns E_INVALIDARG if the mediatype is not acceptable
//
HRESULT CBallStream::CheckMediaType(const CMediaType *pMediaType)
{
    CheckPointer(pMediaType,E_POINTER);

    if((*(pMediaType->Type()) != MEDIATYPE_Video) ||   // we only output video
       !(pMediaType->IsFixedSize()))                   // in fixed size samples
    {                                                  
        return E_INVALIDARG;
    }

    // Check for the subtypes we support
    const GUID *SubType = pMediaType->Subtype();
    if (SubType == NULL)
        return E_INVALIDARG;

    if((*SubType != MEDIASUBTYPE_RGB8)
        && (*SubType != MEDIASUBTYPE_RGB565)
        && (*SubType != MEDIASUBTYPE_RGB555)
        && (*SubType != MEDIASUBTYPE_RGB24)
        && (*SubType != MEDIASUBTYPE_RGB32))
    {
        return E_INVALIDARG;
    }

    // Get the format area of the media type
    VIDEOINFO *pvi = (VIDEOINFO *) pMediaType->Format();

    if(pvi == NULL)
        return E_INVALIDARG;

    // Check the image size. As my default ball is 10 pixels big
    // look for at least a 20x20 image. This is an arbitary size constraint,
    // but it avoids balls that are bigger than the picture...

    if((pvi->bmiHeader.biWidth < 20) || ( abs(pvi->bmiHeader.biHeight) < 20))
    {
        return E_INVALIDARG;
    }

     

    return S_OK;  // This format is acceptable.

} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT CBallStream::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
    CheckPointer(pAlloc,E_POINTER);
    CheckPointer(pProperties,E_POINTER);

    CAutoLock cAutoLock(m_pFilter->pStateLock());
    HRESULT hr = NOERROR;

    VIDEOINFO *pvi = (VIDEOINFO *) m_mt.Format();
	pProperties->cBuffers = max(1, pProperties->cBuffers);
    pProperties->cbBuffer = 1280 * 1024 * 4;

    ASSERT(pProperties->cbBuffer);

    // Ask the allocator to reserve us some sample memory, NOTE the function
    // can succeed (that is return NOERROR) but still not have allocated the
    // memory that we requested, so we must check we got whatever we wanted

    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pProperties,&Actual);
    if(FAILED(hr))
    {
        return hr;
    }

    // Is this allocator unsuitable

    if(Actual.cbBuffer < pProperties->cbBuffer)
    {
        return E_FAIL;
    }
	 
    
    return NOERROR;

} // DecideBufferSize


//
// SetMediaType
//
// Called when a media type is agreed between filters
//
HRESULT CBallStream::SetMediaType(const CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    // Pass the call up to my base class

    HRESULT hr = CSourceStream::SetMediaType(pMediaType);

    if(SUCCEEDED(hr))
    {
        VIDEOINFO * pvi = (VIDEOINFO *) m_mt.Format();
        if (pvi == NULL)
            return E_UNEXPECTED;

        switch(pvi->bmiHeader.biBitCount)
        {
            case 8:     // Make a red pixel

                m_BallPixel[0] = 10;    // 0 is palette index of red
                m_iPixelSize   = 1;
                SetPaletteEntries(Red);
                break;

            case 16:    // Make a blue pixel

                m_BallPixel[0] = 0xf8;  // 00000000 00011111 is blue in rgb555 or rgb565
                m_BallPixel[1] = 0x0;   // don't forget the byte ordering within the mask word.
                m_iPixelSize   = 2;
                SetPaletteEntries(Blue);
                break;

            case 24:    // Make a green pixel

                m_BallPixel[0] = 0x0;
                m_BallPixel[1] = 0xff;
                m_BallPixel[2] = 0x0;
                m_iPixelSize   = 3;
                SetPaletteEntries(Green);
                break;

            case 32:    // Make a yellow pixel

                m_BallPixel[0] = 0x0;
                m_BallPixel[1] = 0xff;
                m_BallPixel[2] = 0xff;
                m_BallPixel[3] = 0x00;
                m_iPixelSize   = 4;
                SetPaletteEntries(Yellow);
                break;

            default:
                // We should never agree any other pixel sizes
                ASSERT(FALSE);
                break;
        }

          

        return NOERROR;
    } 

    return hr;

} // SetMediaType


//
// OnThreadCreate
//
// As we go active reset the stream time to zero
//
HRESULT CBallStream::OnThreadCreate()
{
    CAutoLock cAutoLockShared(&m_cSharedState);
    m_rtSampleTime = 0;

    // we need to also reset the repeat time in case the system
    // clock is turned off after m_iRepeatTime gets very big
    m_iRepeatTime = m_iDefaultRepeatTime;
	 
    return NOERROR;

} // OnThreadCreate


//
// SetPaletteEntries
//
// If we set our palette to the current system palette + the colours we want
// the system has the least amount of work to do whilst plotting our images,
// if this stream is rendered to the current display. The first non reserved
// palette slot is at m_Palette[10], so put our first colour there. Also
// guarantees that black is always represented by zero in the frame buffer
//
HRESULT CBallStream::SetPaletteEntries(Colour color)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    HDC hdc = GetDC(NULL);  // hdc for the current display.
    UINT res = GetSystemPaletteEntries(hdc, 0, iPALETTE_COLORS, (LPPALETTEENTRY) &m_Palette);
    ReleaseDC(NULL, hdc);

    if(res == 0)
        return E_FAIL;

    switch(color)
    {
        case Red:
            m_Palette[10].peBlue  = 0;
            m_Palette[10].peGreen = 0;
            m_Palette[10].peRed   = 0xff;
            break;

        case Yellow:
            m_Palette[10].peBlue  = 0;
            m_Palette[10].peGreen = 0xff;
            m_Palette[10].peRed   = 0xff;
            break;

        case Blue:
            m_Palette[10].peBlue  = 0xff;
            m_Palette[10].peGreen = 0;
            m_Palette[10].peRed   = 0;
            break;

        case Green:
            m_Palette[10].peBlue  = 0;
            m_Palette[10].peGreen = 0xff;
            m_Palette[10].peRed   = 0;
            break;
    }

    m_Palette[10].peFlags = 0;
    return NOERROR;

} // SetPaletteEntries


