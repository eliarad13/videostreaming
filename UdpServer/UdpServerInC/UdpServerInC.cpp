// UdpServerInC.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include "pch.h"
#include <iostream>
#include<stdio.h>
#include<winsock2.h>

#pragma comment(lib,"ws2_32.lib") //Winsock Library

int main()
{
	SOCKET s;
	struct sockaddr_in server, si_other;
	int slen, recv_len;
	char buf[1500];
	WSADATA wsa;

	slen = sizeof(si_other);
 
	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		exit(EXIT_FAILURE);
	}
	//Create a socket
	if ((s = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP)) == INVALID_SOCKET)
	{
		printf("Could not create socket : %d", WSAGetLastError());
	}
	

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	server.sin_addr.s_addr = INADDR_ANY;
	server.sin_port = htons(6000);
	int iOptVal = 0;
	int iOptLen = sizeof(int);
	//Bind
	if (bind(s, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
	{
		exit(EXIT_FAILURE);
	}
	int recvBufSize;
	getsockopt(s, SOL_SOCKET, SO_RCVBUF, (char *)&recvBufSize, &iOptLen);
	 
	int a = 65535;
	if (setsockopt(s, SOL_SOCKET, SO_RCVBUF, (char *)&a, sizeof(int)) == -1) 
	{
		
	}

	//keep listening for data
	int count = 0;
	while (1)
	{
		 		 
		//try to receive some data, this is a blocking call
		if ((recv_len = recv(s, buf, 1500, 0)) == SOCKET_ERROR)
		{
			exit(EXIT_FAILURE);
		}
		count += recv_len;
		printf("count = %d\r", count);
	}

	closesocket(s);
	WSACleanup();
}
 
 