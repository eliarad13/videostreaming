//------------------------------------------------------------------------------
// File: BoutechNetworkSender.cpp
//
// Desc: DirectShow sample code - implementation of a renderer that dumps
//       the samples it receives into a text file.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

  
 
#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include <stdint.h>
#include "BoutechNetworkSenderuids.h"
#include "BoutechTCPNetworkSender.h"
#include <thread> 
using namespace std;
#include <memory> 

#define USE_AES  1

CFifo  fifo;
bool m_appRunning = true;
uint8_t key[] = { 0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4 };

CRITICAL_SECTION CriticalSection;
CDumpFilter  *pFilter = NULL;
#define USE_THREAD  0
HANDLE connectThread = NULL;
HANDLE sendingThread = NULL;
WSADATA wsaData;
struct addrinfo *result = NULL;
SOCKET socketC;
struct sockaddr_in serverInfo;
struct AES_ctx ctx;
bool m_running = false;
int m_connectedSocket = -1;
DWORD WINAPI SendingThread(void* data);

// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
    &CLSID_BoutechNetworkSender,                // Filter CLSID
    L"Bauotech TCP Network Sender",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
    L"Bauotech TCP Network Sender", &CLSID_BoutechNetworkSender, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;

void LogMessage(char *msg)
{
	FILE *handle = fopen("c:\\network.txt", "a+t");
	fprintf(handle, "%s\n", msg);
	fclose(handle);
}


DWORD WINAPI ConnectThread(void* data)
{

	if (socketC != -1)
	{
		closesocket(socketC);
		socketC = -1;
	}
	socketC = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

	u_long iMode = 1;
	ioctlsocket(socketC, FIONBIO, &iMode);

	m_connectedSocket = -1;
	while (m_running == true)
	{
		m_connectedSocket = connect(socketC, (struct sockaddr *)&serverInfo, sizeof(serverInfo));
		if (m_connectedSocket == SOCKET_ERROR)
		{
			int iError = WSAGetLastError();
			if (iError == WSAEWOULDBLOCK)
			{

				fd_set Write, Err;
				TIMEVAL Timeout;
				int TimeoutSec = 10; // timeout after 10 seconds

				FD_ZERO(&Write);
				FD_ZERO(&Err);
				FD_SET(socketC, &Write);
				FD_SET(socketC, &Err);

				Timeout.tv_sec = 1;
				Timeout.tv_usec = 0;

				int iResult = select(0,			//ignored
					NULL,		//read
					&Write,	//Write Check
					&Err,		//Error Check
					&Timeout);
				if (iResult == 0)
				{					 
					if (socketC != -1)
					{
						closesocket(socketC);
						socketC = -1;
					}
					if (m_running == false)
						break;
					socketC = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
					u_long iMode = 1;
					ioctlsocket(socketC, FIONBIO, &iMode);
				}
				else
				{
					if (FD_ISSET(socketC, &Write))
					{
						m_connectedSocket = 0;
					}

					while (FD_ISSET(socketC, &Write))
					{
						Sleep(1000);
						if (m_connectedSocket == -1)
							break;
					}

					if (FD_ISSET(socketC, &Err))
					{
						Sleep(1000);
						if (socketC != -1)
						{
							closesocket(socketC);
							socketC = -1;
						}
						if (m_running == false)
							break;
						socketC = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
						u_long iMode = 1;
						ioctlsocket(socketC, FIONBIO, &iMode);
					}
				}
			}
			else
			{
				if (socketC != -1)
				{
					closesocket(socketC);
					socketC = -1;
				}

				socketC = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
				u_long iMode = 1;
				ioctlsocket(socketC, FIONBIO, &iMode);
				Sleep(1000);
			}
		}
	}
	connectThread = INVALID_HANDLE_VALUE;
	return NULL;
}


// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
    CBaseFilter(NAME("CDumpFilter"), pUnk, pLock, CLSID_BoutechNetworkSender),
    m_pDump(pDump)
{

	
	pFilter = this;
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}


//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


DWORD WINAPI SendingThread(void* data)
{
	uint8_t buffer[13160];
	while (m_appRunning)
	{
		uint64_t size = fifo.GetFifoFullness();
		if (size < 1316)
		{
			Sleep(0);
			continue;
		}

		if (m_connectedSocket == INVALID_SOCKET)
		{
			fifo.FifoClear();
			Sleep(0);
			continue;
		}
		 
		size = 1328;
 
		int retry = 20;
		do
		{
			int res;
			fifo.FifoPull(buffer, size);
#if USE_AES
			int i = 0;
			for (i = 0; i < 1328; i += 16)
			{
				AES_ECB_encrypt(&ctx, buffer + i);
			}
			size = 1328;
#endif 
			 
			if (m_running == false)
				break;
			res = send(socketC, (const char *)buffer, size, 0);
			
			if (res == -1)
			{
				retry--;
				if (retry > 0)
				{
					Sleep(5);
					continue;
				}

				m_connectedSocket = INVALID_SOCKET;
				if (m_running == true && connectThread == INVALID_HANDLE_VALUE)
				{
					connectThread = CreateThread(NULL, 0, ConnectThread, NULL, 0, NULL);					
					break;
				}
			}
			
			size -= res;
			Sleep(0);
		} while (size > 0);
	}
	return NULL;
}

//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
	CAutoLock cObjectLock(m_pLock);

	m_running = false;

	fifo.FifoClose();

	if (socketC != -1)
		closesocket(socketC);
	socketC = -1;

	if (connectThread != NULL && connectThread != INVALID_HANDLE_VALUE)
		WaitForSingleObject(connectThread, INFINITE);


    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);

    if (m_pDump)
    {
         
    }

    return CBaseFilter::Pause();
}

//
// Run
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	CAutoLock cObjectLock(m_pLock);
	 
	m_running = true;
	fifo.FifoClear();

	if(socketC != -1)
		closesocket(socketC);
	socketC = -1;

	if (connectThread != NULL && connectThread != INVALID_HANDLE_VALUE)
		WaitForSingleObject(connectThread, INFINITE);
		
	connectThread = CreateThread(NULL, 0, ConnectThread, NULL, 0, NULL);

	  
    return CBaseFilter::Run(tStart);
}
 
//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{
	AES_init_ctx(&ctx, key);

	fifo.CreateFifo(13160000);	
}

 
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}

 
HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
	if (m_running == false || m_connectedSocket == -1)
		return S_OK; 

    CheckPointer(pSample,E_POINTER);

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
	 
    
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    } 
	 
	int sizeToSend = pSample->GetActualDataLength();
	fifo.FifoPush(pbData, sizeToSend);
	return hr;
}
 
STDMETHODIMP CDumpInputPin::EndOfStream(void)
{
    CAutoLock lock(m_pReceiveLock);
    return CRenderedInputPin::EndOfStream();

} 
 
 
STDMETHODIMP CDumpInputPin::NewSegment(REFERENCE_TIME tStart,
                                       REFERENCE_TIME tStop,
                                       double dRate)
{
    m_tLast = 0;
    return S_OK;

} // NewSegment

//
//  CDump class
//

CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
    CUnknown(NAME("BoutechNetworkSender"), pUnk),
    m_pFilter(NULL),
    m_pPin(NULL),
    m_pPosition(NULL),
    m_hFile(INVALID_HANDLE_VALUE),
    m_pFileName(0),
    m_fWriteError(0)
{
    ASSERT(phr);

	 

	int len = sizeof(serverInfo);
    
    m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
    if (m_pFilter == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

    m_pPin = new CDumpInputPin(this,GetOwner(),
                               m_pFilter,
                               &m_Lock,
                               &m_ReceiveLock,
                               phr);
    if (m_pPin == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    } 
	 
	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		return;
	}

	

	serverInfo.sin_family = AF_INET;
	serverInfo.sin_port = htons(6000);
	serverInfo.sin_addr.s_addr = inet_addr("82.81.105.171");
	//serverInfo.sin_addr.s_addr = inet_addr("10.0.0.10");

	 
}
 
CDump::~CDump()
{
	m_running = false;
	m_appRunning = false;
	
	if (socketC > 0)
		closesocket(socketC);

	if (connectThread != NULL && connectThread != INVALID_HANDLE_VALUE)
		WaitForSingleObject(connectThread, INFINITE);


	if (sendingThread != NULL && sendingThread != INVALID_HANDLE_VALUE)
		WaitForSingleObject(sendingThread, INFINITE);


	fifo.FreeFifo();

    delete m_pPin;
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);


	InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x00000400);
	sendingThread = CreateThread(NULL, 0, SendingThread, NULL, 0, NULL);

    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance


//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);

	  
    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Boutech TCP Network Sender Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

}  
  
 

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

