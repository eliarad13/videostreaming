#include "MFDMutex.h"


void CMutex::InitializeMutex()
{
	if (!InitializeCriticalSectionAndSpinCount(&commCriticalSection, 0x00000400))
		return;
}
void CMutex::MutexEnter()
{
	EnterCriticalSection(&commCriticalSection);
}
void CMutex::MutexLeave()
{
	LeaveCriticalSection(&commCriticalSection);
}

void CMutex::DeleteMutex()
{
	DeleteCriticalSection(&commCriticalSection);
}

