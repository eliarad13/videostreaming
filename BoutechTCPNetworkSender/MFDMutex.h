#ifndef MFD_MUTEX_H
#define MFD_MUTEX_H
#include <Windows.h>


class CMutex
{

public:


	void InitializeMutex();
	void MutexEnter();
	void MutexLeave();
	void DeleteMutex();

private:
	CRITICAL_SECTION commCriticalSection;
};

#endif 