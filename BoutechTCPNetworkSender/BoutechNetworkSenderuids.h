//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

 
// {1D91DF16-3108-4e22-A20E-ED91EE93DAB4}
DEFINE_GUID(CLSID_BoutechNetworkSender,
	0x1d91df16, 0x3108, 0x4e22, 0xa2, 0xe, 0xed, 0x91, 0xee, 0x93, 0xda, 0xb4);

