//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

 
// {A59AF369-8B00-40CA-8AA1-811F9F689720}
DEFINE_GUID(CLSID_BoutechNetworkSender,
	0xa59af369, 0x8b00, 0x40ca, 0x8a, 0xa1, 0x81, 0x1f, 0x9f, 0x68, 0x97, 0x20);

 
// {5720E469-69EA-47C0-A5D0-C8B4B0283C4A}
DEFINE_GUID(IID_IBoutechUDPSender,
	0x5720e469, 0x69ea, 0x47c0, 0xa5, 0xd0, 0xc8, 0xb4, 0xb0, 0x28, 0x3c, 0x4a);



DECLARE_INTERFACE_(IBoutechUDPSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};