#ifndef MDS_FIFO_H
#define MDS_FIFO_H
#include <stdint.h>
#include "Mutex.h"

class CFifo
{

public:
	void CreateFifo(int size);
	int GetFifoFullness();
	int GetFifoEmptiness();
	void FifoPush(uint8_t *buffer, uint32_t size);
	void FifoGet(uint8_t *buffer);
	bool FifoPull(uint8_t *buffer, uint32_t size);
	void FreeFifo();
	bool FifoPull4Check(uint8_t *buffer, uint32_t size);
	void Close();

private:

	uint8_t *fifo;
	uint32_t fifo_wr;
	uint32_t fifo_rd;
	uint32_t fifo_size;

	CMutex mutex;
	bool m_running;

}; 
#endif 
