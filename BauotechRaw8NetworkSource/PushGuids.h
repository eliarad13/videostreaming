//------------------------------------------------------------------------------
// File: PushGuids.h
//
// Desc: DirectShow sample code - GUID definitions for PushSource filter set
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#pragma once

#ifndef __PUSHGUIDS_DEFINED
#define __PUSHGUIDS_DEFINED

 
// {16560CDE-F629-4E18-8431-5C163BAE2CD5}
DEFINE_GUID(CLSID_BauotechPushSourceBitmap,
	0x16560cde, 0xf629, 0x4e18, 0x84, 0x31, 0x5c, 0x16, 0x3b, 0xae, 0x2c, 0xd5);

 

#endif
