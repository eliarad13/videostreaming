#ifndef MY_MUTEX_H
#define MY_MUTEX_H
#include <Windows.h>

class CMutex
{

private:
	CRITICAL_SECTION commCriticalSection;
	void InitializeMutex();
public:

	CMutex();
	~CMutex();	
	void MutexEnter();
	void MutexLeave();

};
 
#endif 