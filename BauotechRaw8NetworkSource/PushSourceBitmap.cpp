//------------------------------------------------------------------------------
// File: PushSourceBitmap.cpp
//
// Desc: DirectShow sample code - In-memory push mode source filter
//       Provides a static bitmap as the video output stream.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <winsock2.h>
#include <ws2ipdef.h>
#include <streams.h>
#include "PushSource.h"
#include "PushGuids.h"
#include <math.h>
#include <stdint.h>
#include "bmp_info.c"
#include <memory>
#include <thread>

using namespace std;
 
uint8_t *buffer;

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
    &MEDIATYPE_Video,       // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

 

/**********************************************
 *
 *  CPushPinBitmap Class
 *  
 *
 **********************************************/

CPushPinBitmap::CPushPinBitmap(HRESULT *phr, CSource *pFilter)
      : CSourceStream(NAME("Push Source Bitmap"), phr, pFilter, L"Out"),
        m_FramesWritten(0),
        m_bZeroMemory(0),
        m_pBmi(0),
        m_cbBitmapInfo(0),
        m_hFile(INVALID_HANDLE_VALUE),
        m_pFile(NULL),
        m_pImage(NULL),
        m_iFrameNumber(0),
        m_rtFrameLength(FPS_5) // Display 5 bitmap frames per second
{
  
	
	m_bmpFromFile = false;
	handle = NULL;
	 
}


void CPushPinBitmap::ProcessReceive()
{
	uint8_t buffer[8192];
	while (m_running)
	{
		int size = recv(
			m_socket,
			(char *)buffer,
			8192,
			0);

		if (size > 0)
		{
			fifo.FifoPush(buffer, size);
		}
	}	 

}
HRESULT CPushPinBitmap::OnThreadCreate()
{
	CAutoLock cAutoLockShared(&m_cSharedState);
	if (handle != NULL)
	{
		fclose(handle);
		handle = NULL;
	}
	 
	buffer = (uint8_t *)malloc(m_pvi.bmiHeader.biWidth * m_pvi.bmiHeader.biHeight);
	m_iFrameNumber = 0;
	if (m_bmpFromFile == 1)
	{
		handle = fopen("d:\\africa_israel.raw", "r+b");
	}
	else
	{		
		m_syncRequired = true;
		if (SetupUdp("127.0.0.1", 6900, NULL) == 1)
		{
			fifo.CreateFifo((m_pvi.bmiHeader.biWidth * m_pvi.bmiHeader.biHeight) * 5);
			m_running = 1;
			shared_ptr<thread> t1 = make_shared<thread>(&CPushPinBitmap::ProcessReceive, this);
			t1->detach();
		}
	}
	 
	return NOERROR;

} // OnThreadCreate



CPushPinBitmap::~CPushPinBitmap()
{   
    DbgLog((LOG_TRACE, 3, TEXT("Frames written %d"),m_iFrameNumber));

    if (m_pFile)
    {
        delete [] m_pFile;
    }

    // The constructor might quit early on error and not close the file...
    if (m_hFile != INVALID_HANDLE_VALUE)
    {
        CloseHandle(m_hFile);
    }
}


// GetMediaType: This method tells the downstream pin what types we support.

// Here is how CSourceStream deals with media types:
//
// If you support exactly one type, override GetMediaType(CMediaType*). It will then be
// called when (a) our filter proposes a media type, (b) the other filter proposes a
// type and we have to check that type.
//
// If you support > 1 type, override GetMediaType(int,CMediaType*) AND CheckMediaType.
//
// In this case we support only one type, which we obtain from the bitmap file.

int CPushPinBitmap::LoadFirstBitmap()
{

	TCHAR szCurrentDir[MAX_PATH], szFileCurrent[MAX_PATH], szFileMedia[MAX_PATH];

	m_hFile = CreateFile(L"c:\\bmp_4_8bit_bauotech_filter.bmp", GENERIC_READ, 0, NULL, OPEN_EXISTING,FILE_ATTRIBUTE_NORMAL, NULL);
	if (m_hFile == INVALID_HANDLE_VALUE)
	{
		return 0;
		 
	}

	DWORD dwFileSize = GetFileSize(m_hFile, NULL);
	if (dwFileSize == INVALID_FILE_SIZE)
	{
		return 0;
	}

	m_pFile = new BYTE[dwFileSize];
	if (!m_pFile)
	{
		return 0;
	}

	DWORD nBytesRead = 0;
	if (!ReadFile(m_hFile, m_pFile, dwFileSize, &nBytesRead, NULL))
	{
		return 0;
	}

	int cbFileHeader = sizeof(BITMAPFILEHEADER);

	// Store the size of the BITMAPINFO 
	BITMAPFILEHEADER *pBm = (BITMAPFILEHEADER*)m_pFile;
	m_cbBitmapInfo = pBm->bfOffBits - cbFileHeader;

	// Store a pointer to the BITMAPINFO
	m_pBmi = (BITMAPINFO*)(m_pFile + cbFileHeader);

	// Store a pointer to the starting address of the pixel bits
	m_pImage = m_pFile + cbFileHeader + m_cbBitmapInfo;

	// Close and invalidate the file handle, since we have copied its bitmap data
	CloseHandle(m_hFile);
	m_hFile = INVALID_HANDLE_VALUE;
	
	return 1;
}

HRESULT CPushPinBitmap::GetMediaType(CMediaType *pMediaType)
{
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    CheckPointer(pMediaType, E_POINTER);

    
	 
	if (LoadFirstBitmap() == 0)
		return E_FAIL;
		// If the bitmap file was not loaded, just fail here.
	if (!m_pImage)
		return E_FAIL;

	// Allocate enough room for the VIDEOINFOHEADER and the color tables
	VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER*)pMediaType->AllocFormatBuffer(SIZE_PREHEADER + m_cbBitmapInfo);
	if (pvi == 0)
		return(E_OUTOFMEMORY);

	ZeroMemory(pvi, pMediaType->cbFormat);
	pvi->AvgTimePerFrame = m_rtFrameLength;

	// Copy the header info
	memcpy(&(pvi->bmiHeader), m_pBmi, m_cbBitmapInfo);
	memcpy(&m_pvi, pvi, sizeof(VIDEOINFOHEADER));

	// Set image size for use in FillBuffer
	pvi->bmiHeader.biSizeImage = GetBitmapSize(&pvi->bmiHeader);

	// Clear source and target rectangles
	SetRectEmpty(&(pvi->rcSource)); // we want the whole image area rendered
	SetRectEmpty(&(pvi->rcTarget)); // no particular destination rectangle

	pMediaType->SetType(&MEDIATYPE_Video);
	pMediaType->SetFormatType(&FORMAT_VideoInfo);
	pMediaType->SetTemporalCompression(FALSE);

	// Work out the GUID for the subtype from the header info.
	const GUID SubTypeGUID = GetBitmapSubtype(&pvi->bmiHeader);
	pMediaType->SetSubtype(&SubTypeGUID);
	pMediaType->SetSampleSize(pvi->bmiHeader.biSizeImage);
	 
		 
	 
    return S_OK;
}


HRESULT CPushPinBitmap::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *pRequest)
{
    HRESULT hr;
    CAutoLock cAutoLock(m_pFilter->pStateLock());

    CheckPointer(pAlloc, E_POINTER);
    CheckPointer(pRequest, E_POINTER);

    // If the bitmap file was not loaded, just fail here.
    if (!m_pImage)
        return E_FAIL;

    VIDEOINFOHEADER *pvi = (VIDEOINFOHEADER*) m_mt.Format();
    
    // Ensure a minimum number of buffers
    if (pRequest->cBuffers == 0)
    {
        pRequest->cBuffers = 2;
    }
    pRequest->cbBuffer = pvi->bmiHeader.biSizeImage;

    ALLOCATOR_PROPERTIES Actual;
    hr = pAlloc->SetProperties(pRequest, &Actual);
    if (FAILED(hr)) 
    {
        return hr;
    }

    // Is this allocator unsuitable?
    if (Actual.cbBuffer < pRequest->cbBuffer) 
    {
        return E_FAIL;
    }

    return S_OK;
}



// This is where we insert the DIB bits into the video stream.
// FillBuffer is called once for every sample in the stream.
byte sync[] = { 0x71, 0x12, 0x57, 0x61, 0xF1, 0x5A, 0x92, 0x0, 0x11, 0x12, 0x13, 0x14, 0x15, 0x88, 0x99, 0xAA };
HRESULT CPushPinBitmap::FillBuffer(IMediaSample *pSample)
{
    BYTE *pData;
    long cbData;

    CheckPointer(pSample, E_POINTER);

    // If the bitmap file was not loaded, just fail here.
    if (!m_pImage)
        return E_FAIL;

    CAutoLock cAutoLockShared(&m_cSharedState);

    // Access the sample's data buffer
    pSample->GetPointer(&pData);
    cbData = pSample->GetSize();
 

	if (m_bmpFromFile == 1)
	{
		int r = fread((uint8_t *)buffer, 1, cbData, handle);
		if (r != cbData)
		{
			fseek(handle, 0, SEEK_SET);
			fread((uint8_t *)buffer, 1, cbData, handle);
		}

		for (int y = 0; y < m_pvi.bmiHeader.biHeight; y++)
		{
			for (int x = 0; x < m_pvi.bmiHeader.biWidth; x++)
			{
				pData[m_pvi.bmiHeader.biWidth * y + x] = buffer[m_pvi.bmiHeader.biWidth * (m_pvi.bmiHeader.biHeight - (y + 1)) + x];
			}
		}
	}
	else
	{
		if (m_syncRequired == true)
		{
			while (true)
			{
				if (fifo.FifoPull4Check(buffer, 16) == 0)
				{
					pSample->SetActualDataLength(0);
					return S_OK;
				}
				if (memcmp(buffer, sync, 16) == 0)
				{					 
					fifo.FifoPull(buffer, 16);
					break;						
				}
				else
				{
					fifo.FifoPull(buffer, 1);
				}
			}
		}

		if (fifo.FifoPull(buffer, cbData) == 0)
		{
			pSample->SetActualDataLength(0);
			return S_OK;
		}

		for (int y = 0; y < m_pvi.bmiHeader.biHeight; y++)
		{
			for (int x = 0; x < m_pvi.bmiHeader.biWidth; x++)
			{
				pData[m_pvi.bmiHeader.biWidth * y + x] = buffer[m_pvi.bmiHeader.biWidth * (m_pvi.bmiHeader.biHeight - (y + 1)) + x];
			}
		}		
	}
	  
    REFERENCE_TIME rtStart = m_iFrameNumber * m_rtFrameLength;
    REFERENCE_TIME rtStop  = rtStart + m_rtFrameLength;

    pSample->SetTime(&rtStart, &rtStop);
    m_iFrameNumber++;

    // Set TRUE on every sample for uncompressed frames
    pSample->SetSyncPoint(TRUE);

    return S_OK;
}


/**********************************************
 *
 *  CPushSourceBitmap Class
 *
 **********************************************/

STDMETHODIMP CPushSourceBitmap::Run(REFERENCE_TIME tStart)
{

	return CBaseFilter::Run(tStart);
}
STDMETHODIMP CPushSourceBitmap::Pause()
{
	return CBaseFilter::Pause();

}
STDMETHODIMP CPushSourceBitmap::Stop()
{
	m_pPin->fifo.Close();
	m_pPin->m_running = 0;
	if (m_pPin->m_bmpFromFile == 0)
	{
		closesocket(m_pPin->m_socket);
	}
	
	return CBaseFilter::Stop();
}


CPushSourceBitmap::CPushSourceBitmap(IUnknown *pUnk, HRESULT *phr)
	: CSource(NAME("PushSourceBitmap"), pUnk, CLSID_BauotechPushSourceBitmap)
{
    // The pin magically adds itself to our pin array.
    m_pPin = new CPushPinBitmap(phr, this);

    if (phr)
    {
        if (m_pPin == NULL)
            *phr = E_OUTOFMEMORY;
        else
            *phr = S_OK;
    }  
}


CPushSourceBitmap::~CPushSourceBitmap()
{
    delete m_pPin;
}


CUnknown * WINAPI CPushSourceBitmap::CreateInstance(IUnknown *pUnk, HRESULT *phr)
{
    CPushSourceBitmap *pNewFilter = new CPushSourceBitmap(pUnk, phr );

    if (phr)
    {
        if (pNewFilter == NULL) 
            *phr = E_OUTOFMEMORY;
        else
            *phr = S_OK;
    }

    return pNewFilter;
}

int CPushPinBitmap::SetupUdp(char *interfaceAddress, int port, char *multicastAddress)
{
	int iResult;

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		perror("WSAStartup");
		return  0;
	}

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		WSACleanup();
		return 0;
	}
	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;

	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return 0;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	DWORD ip = inet_addr(interfaceAddress);
	addr.sin_addr.s_addr = ip;
	addr.sin_port = htons(port);

	// bind to receive address
	//
	if (::bind(ReceivingSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return 0;
	}
	m_socket = ReceivingSocket;


	if (multicastAddress != NULL)
	{
		IP_MREQ mreq;
		mreq.imr_multiaddr.s_addr = inet_addr("234.5.5.5");
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if (setsockopt(ReceivingSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
		{
			return 0;
		}
	}

	return 1;

}

