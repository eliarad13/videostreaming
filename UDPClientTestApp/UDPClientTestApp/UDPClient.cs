﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace UDPClientTestApp
{
    public class UDPClient
    {
        Socket m_receiverSocket;
        bool m_running = true;
        byte[] buffer = new byte[1504];
        UdpClient listener;
        public void MulticastServerRecorder(string mcastGroup, int port)
        {
            listener = new UdpClient();
            listener.Client.Bind(new IPEndPoint(IPAddress.Parse("192.168.1.28"), 8090));
        }

        public void Test()
        {
            var client = new UdpClient();
            IPEndPoint ep = new IPEndPoint(IPAddress.Parse("84.108.170.204"), 8090); 
            client.Connect(ep);

            listener.Connect(ep);

            listener.Send(buffer, 10);
     

            // then receive data
            var receivedData = client.Receive(ref ep);

            Console.Write("receive data from " + ep.ToString());

            Console.Read();
        }
    }
}
