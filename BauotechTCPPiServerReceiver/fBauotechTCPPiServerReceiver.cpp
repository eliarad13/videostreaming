//------------------------------------------------------------------------------
// File: fBauotechRawInputSource.cpp
//------------------------------------------------------------------------------

#include <winsock2.h>
#include <ws2ipdef.h>
#include <streams.h>
#include <olectl.h>
#include <initguid.h>
#include "fBauotechTCPPiServerReceiver.h"
#include "myInterface.h"
#include <io.h>
#include <memory>



int m_frameRate = 25;


uint8_t key[] = { 0x60, 0x3d, 0xeb, 0x10, 0x15, 0xca, 0x71, 0xbe, 0x2b, 0x73, 0xae, 0xf0, 0x85, 0x7d, 0x77, 0x81,
0x1f, 0x35, 0x2c, 0x07, 0x3b, 0x61, 0x08, 0xd7, 0x2d, 0x98, 0x10, 0xa3, 0x09, 0x14, 0xdf, 0xf4 };

#define IMAGE_SIZE 1316 * 10

#pragma warning(disable:4710)  // 'function': function not inlined (optimzation)
using namespace std;

FILE *debugh = NULL;

// Setup data

const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{
    &MEDIATYPE_Video,       // Major type
    &MEDIASUBTYPE_NULL      // Minor type
};

const AMOVIESETUP_PIN sudOpPin =
{
    L"Output",              // Pin string name
    FALSE,                  // Is it rendered
    TRUE,                   // Is it an output
    FALSE,                  // Can we have none
    FALSE,                  // Can we have many
    &CLSID_NULL,            // Connects to filter
    NULL,                   // Connects to pin
    1,                      // Number of types
    &sudOpPinTypes };       // Pin details

const AMOVIESETUP_FILTER sudBallax =
{
	&CLSID_BauotechTCPPiServerReceiver,    // Filter CLSID
    L"Bauotech TCP Pi Server Receiver",       // String name
    MERIT_DO_NOT_USE,       // Filter merit
    1,                      // Number pins
    &sudOpPin               // Pin details
};


// COM global table of objects in this dll

CFactoryTemplate g_Templates[] = {
  { L"Bauotech TCP Pi Server Receiver"
  , &CLSID_BauotechTCPPiServerReceiver
  , CBauotechTcpServerSource::CreateInstance
  , NULL
  , &sudBallax }
};
int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterServer
//
// Exported entry points for registration and unregistration
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

//
// CreateInstance
//
// The only allowed way to create Bauotech Raw Input Sources!
//
CUnknown * WINAPI CBauotechServerSourceFilter::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);

	CUnknown *punk = new CBauotechServerSourceFilter(lpunk, phr);
    if(punk == NULL)
    {
        if(phr)
            *phr = E_OUTOFMEMORY;
    }
    return punk;

} // CreateInstance


CBauotechServerSourceFilter::~CBauotechServerSourceFilter()
{
	m_appRunning = false;
	
	if (ReceivingSocket != -1)
		closesocket(ReceivingSocket);

	if (clientSocket != -1)
		closesocket(clientSocket);

	fifo.FreeFifo();

	if (m_paStreams != NULL)
	{
		delete m_paStreams[0];
		delete m_paStreams;
		m_paStreams = NULL;
	}
}

  
//
// Constructor
//
// Initialise a CBallStream object so that we have a pin.
//
CBauotechServerSourceFilter::CBauotechServerSourceFilter(LPUNKNOWN lpunk, HRESULT *phr) :
CSource(NAME("Bauotech TCP Pi Server Receiver"), lpunk, CLSID_BauotechTCPPiServerReceiver)
{

	m_appRunning = true;
	clientSocket = INVALID_SOCKET;
	ReceivingSocket = INVALID_SOCKET;

	if (!InitializeCriticalSectionAndSpinCount(&CriticalSection,
		0x00000400))
		return;

	fifo.CreateFifo(13160000);
	  
	m_running = false;
	  
    ASSERT(phr);
    CAutoLock cAutoLock(&m_cStateLock);

	strcpy(IpAddress, "10.0.0.10");
	m_port = 6000;

	m_receiveThread = make_shared<thread>(&CBauotechServerSourceFilter::ReceiveThread, this);
	m_receiveThread->detach();
	  
	CreateSource();
}

void CBauotechServerSourceFilter::CreateSource()
{
	HRESULT phr;

	m_paStreams = (CSourceStream **) new CBallStream*[1];
	if (m_paStreams == NULL)
	{		
		return;
	}

	m_paStreams[0] = new CBallStream(&phr, this, L"Out");
	if (m_paStreams[0] == NULL)
	{		   
		return;
	}
	 

}


CBallStream::CBallStream(HRESULT *phr,
	CBauotechServerSourceFilter *pParent,
                         LPCWSTR pPinName) :
    CSourceStream(NAME("Bauotech Raw Input Source"),phr, pParent, pPinName),   
    m_iDefaultRepeatTime(30)
{
	
	AES_init_ctx(&ctx, key);
	test_encrypt_ecb();


	m_bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
	m_bmpInfo.biCompression = BI_RGB;
	m_bmpInfo.biBitCount = 32;
	m_bmpInfo.biHeight = 1080;
	m_bmpInfo.biWidth = 1920;
	m_bmpInfo.biPlanes = 1;
	m_bmpInfo.biSizeImage = GetBitmapSize(&m_bmpInfo);
	m_bmpInfo.biClrImportant = 0;
	m_bmpInfo.biClrUsed = 0;
	m_bmpInfo.biXPelsPerMeter = 0;
	m_bmpInfo.biYPelsPerMeter = 0;
	  


    ASSERT(phr);
    CAutoLock cAutoLock(&m_cSharedState);
	m_pFilter = pParent;
	 
}  
 
CBallStream::~CBallStream()
{
    CAutoLock cAutoLock(&m_cSharedState);

	DeleteCriticalSection(&m_pFilter->CriticalSection);
 
}  
  
int CBauotechTcpServerSource::Setup()
{
	int iResult;
	 
	 
	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) {
		perror("WSAStartup");
		return 1;
	}

}

int CBauotechServerSourceFilter::StartTCPServer(char *interfaceAddress, int port, char *multicastAddress)
{
	if (ReceivingSocket != INVALID_SOCKET)
	{
		closesocket(ReceivingSocket);
	}

	ReceivingSocket = INVALID_SOCKET;
	
	//Create a socket
	if ((ReceivingSocket = socket(AF_INET, SOCK_STREAM, 0)) == INVALID_SOCKET)
	{
		return 0;
	} 

	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;
	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return 0;
	} 

	//Prepare the sockaddr_in structure
	memset(&server, 0, sizeof(server));
	server.sin_family = AF_INET;
	DWORD ip = inet_addr(interfaceAddress);
	server.sin_addr.s_addr = ip;
	server.sin_port = htons(port);
 
	if (::bind(ReceivingSocket, (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
	{
		return -1;
	}
	  

	int iVal = 10000;
	unsigned int  sz = sizeof(iVal);
	int ret = setsockopt(ReceivingSocket, SOL_SOCKET, SO_RCVTIMEO, (char *)&iVal, sz);
	if (ret == SOCKET_ERROR)
	{
		printf("setsockopt() failed with error code %d\n", WSAGetLastError());
		return -1;
	}
	listen(ReceivingSocket, 100);
  

	return 1;
}

#define USE_AES     1

#if USE_AES
#define SEND_SIZE 1328
#else 
#define SEND_SIZE 1316 
#endif 




void CBauotechServerSourceFilter::StartThreads()
{
	fifo.FifoClear();

	if (StartTCPServer(IpAddress, m_port, "") != 1)
	{
		return;
	}

	m_acceptThread = make_shared<thread>(&CBauotechServerSourceFilter::AcceptThread, this);
	m_acceptThread->detach();
}

void CBauotechServerSourceFilter::AcceptThread()
{ 
	int c;

	fifo.FifoClear();

	if (clientSocket != INVALID_SOCKET)
	{
		closesocket(clientSocket);
		clientSocket = INVALID_SOCKET;
	}

	while (m_running)
	{

		c = sizeof(struct sockaddr_in);
		clientSocket = accept(ReceivingSocket, (struct sockaddr *)&client, &c);
		if (clientSocket == INVALID_SOCKET)
		{
			if (m_running == false)
				return;
			Sleep(1000);
			continue;
		}
		break;
	}
}

void CBauotechServerSourceFilter::ReceiveThread()
{

	uint8_t buffer[15040];

	int SelectTiming;
	while (m_appRunning)
	{

		if (clientSocket == INVALID_SOCKET)
		{
			Sleep(0);
			continue;
		}
	
		int size = recv(
			clientSocket,
			(char *)buffer,
			SEND_SIZE,
			0);
		if (size <= 0)
		{
			fifo.FifoClose();
			if (m_running == true)
				StartThreads();
			continue;
		}
		fifo.FifoPush(buffer, size);
	}
	if (m_running == true)
		StartThreads();
}
 
HRESULT CBallStream::FillBuffer(IMediaSample *pms)
{
	if (m_pFilter->m_running == false || m_pFilter->clientSocket == INVALID_SOCKET)
		return S_OK;

	CheckPointer(pms, E_POINTER);

	BYTE *pData;
	long lDataLen;

	int size = SEND_SIZE;
	pms->GetPointer(&pData);
	lDataLen = pms->GetSize();
	  
	m_pFilter->fifo.FifoPull(pData, size);
	 

#if USE_AES
	for (int i = 0; i < size; i += 16)
	{
		AES_ECB_decrypt(&ctx, pData + i);
	}
	size = 1328;
#endif 

	pms->SetActualDataLength(size);
	 

#if 0 
	// The current time is the sample's start
	CRefTime rtStart = m_rtSampleTime;

	// Increment to find the finish time
	m_rtSampleTime += (LONG)m_iRepeatTime;

	pms->SetTime((REFERENCE_TIME *)&rtStart, (REFERENCE_TIME *)&m_rtSampleTime);
#endif 
	//pms->SetSyncPoint(TRUE);
	return NOERROR;
	
	 
}  
 
  
//
// Notify
//
// Alter the repeat rate according to quality management messages sent from
// the downstream filter (often the renderer).  Wind it up or down according
// to the flooding level - also skip forward if we are notified of Late-ness
//
STDMETHODIMP CBallStream::Notify(IBaseFilter * pSender, Quality q)
{
    // Adjust the repeat rate.
    if(q.Proportion<=0)
    {
        m_iRepeatTime = 1000;        // We don't go slower than 1 per second
    }
    else
    {
        m_iRepeatTime = m_iRepeatTime*1000 / q.Proportion;
        if(m_iRepeatTime>1000)
        {
            m_iRepeatTime = 1000;    // We don't go slower than 1 per second
        }
        else if(m_iRepeatTime<10)
        {
            m_iRepeatTime = 10;      // We don't go faster than 100/sec
        }
    }

    // skip forwards
    if(q.Late > 0)
        m_rtSampleTime += q.Late;

    return NOERROR;

} // Notify

HRESULT CBallStream::GetMediaType(int iPosition, CMediaType *pmt)
{
	CAutoLock cAutoLock(m_pLock);

	if (iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if (iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)pmt->AllocFormatBuffer(sizeof(VIDEOINFOHEADER));
	if (!vih)
	{
		return E_OUTOFMEMORY;
	}

	vih->bmiHeader = m_bmpInfo;
	if (m_frameRate != 0)
	{
		vih->AvgTimePerFrame = UNITS / m_frameRate;
	}
	else
	{
		vih->AvgTimePerFrame = 0;
	}


	switch (iPosition)
	{
	case 0:
		pmt->SetType(&MEDIATYPE_Video);
		pmt->SetSubtype(&MEDIASUBTYPE_H264);
		pmt->SetFormatType(&FORMAT_VideoInfo);
		pmt->SetTemporalCompression(TRUE /*FALSE*/);
		pmt->SetSampleSize(m_bmpInfo.biSizeImage);
		break;
	case 1:
		pmt->SetType(&MEDIATYPE_Stream);
		pmt->SetSubtype(&GUID_NULL);
		pmt->SetFormatType(&GUID_NULL);
		break;
	}

	return S_OK;

} // GetMediaType

 
HRESULT CBallStream::CheckMediaType(const CMediaType *pMediaType)
{
	return S_OK;  // This format is acceptable.

} // CheckMediaType


//
// DecideBufferSize
//
// This will always be called after the format has been sucessfully
// negotiated. So we have a look at m_mt to see what size image we agreed.
// Then we can ask for buffers of the correct size to contain them.
//
HRESULT CBallStream::DecideBufferSize(IMemAllocator *pAlloc,
                                      ALLOCATOR_PROPERTIES *pProperties)
{
	CheckPointer(pAlloc, E_POINTER);
	CheckPointer(pProperties, E_POINTER);

	CAutoLock cAutoLock(m_pFilter->pStateLock());
	HRESULT hr = NOERROR;


	pProperties->cBuffers = 1;
	pProperties->cbBuffer = IMAGE_SIZE;

	ASSERT(pProperties->cbBuffer);

	// Ask the allocator to reserve us some sample memory, NOTE the function
	// can succeed (that is return NOERROR) but still not have allocated the
	// memory that we requested, so we must check we got whatever we wanted

	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(pProperties, &Actual);
	if (FAILED(hr))
	{
		return hr;
	}

	// Is this allocator unsuitable

	if (Actual.cbBuffer < pProperties->cbBuffer)
	{
		return E_FAIL;
	}

	// Make sure that we have only 1 buffer (we erase the ball in the
	// old buffer to save having to zero a 200k+ buffer every time
	// we draw a frame)

	ASSERT(Actual.cBuffers == 1);
	return NOERROR;

} // DecideBufferSize
 
//
// OnThreadCreate
//
// As we go active reset the stream time to zero
//

HRESULT CBallStream::OnThreadCreate()
{

	m_pFilter->fifo.FifoClear();
	  
	m_pFilter->m_running = true;
	 
	
	m_pFilter->StartThreads();

	
    CAutoLock cAutoLockShared(&m_cSharedState);
    m_rtSampleTime = 0;
	
    // we need to also reset the repeat time in case the system
    // clock is turned off after m_iRepeatTime gets very big
    m_iRepeatTime = m_iDefaultRepeatTime;

    return NOERROR;

} // OnThreadCreate

  
STDMETHODIMP CBauotechServerSourceFilter::Run(REFERENCE_TIME tStart)
{ 
	 
	
	return CBaseFilter::Run(tStart);
}
STDMETHODIMP CBauotechServerSourceFilter::Pause()
{
	return CBaseFilter::Pause();
}
STDMETHODIMP CBauotechServerSourceFilter::Stop()
{ 
	m_running = false;

	if (clientSocket != -1)
		closesocket(clientSocket);
	clientSocket = -1;

	if (ReceivingSocket != -1)
		closesocket(ReceivingSocket);
	ReceivingSocket = -1;
	fifo.FifoClose();

	m_running = false;
	return CBaseFilter::Stop();
}

STDMETHODIMP CBauotechServerSourceFilter::SetIpAddressAndPort(WCHAR *ipAddress, int port)
{	 
	 
	wcstombs(IpAddress, ipAddress, 300);
	m_port = port;

	
	return S_OK;
} 
 
 
STDMETHODIMP CBauotechTcpServerSource::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	CheckPointer(ppv, E_POINTER);
	 

	 
	if (riid == IID_IFileSourceFilter)
	{
		return GetInterface((IFileSourceFilter *)this, ppv);
	}
	else
		if (riid == IID_IBoutechRaspeberryTCPServerReceiver)
	{
		return GetInterface((IBoutechRaspeberryTCPServerReceiver*)this, ppv);
	}
	else if (riid == IID_IAMFilterMiscFlags)
	{
		return GetInterface((IAMFilterMiscFlags*) this, ppv);
	}
	else
	{
		return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
	}
}


STDMETHODIMP CBauotechServerSourceFilter::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	CheckPointer(ppv, E_POINTER);
	if (riid == IID_IBoutechRaspeberryTCPServerReceiver)
	{		
		return GetInterface((IBoutechRaspeberryTCPServerReceiver*)this, ppv);
	}	 
	else
	{
		return CSource::NonDelegatingQueryInterface(riid, ppv);
	}
}

 


CBauotechTcpServerSource::CBauotechTcpServerSource(LPUNKNOWN pUnk, HRESULT *phr) :
CUnknown(NAME("CBauotechTcpServerSource"), pUnk),
m_pFilter(NULL),
m_pPin(NULL)
{
	ASSERT(phr);
  
	Setup();

	m_pFilter = new CBauotechServerSourceFilter(pUnk, phr);
	if (m_pFilter == NULL)
	{

	}

}

//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//

CUnknown * WINAPI CBauotechTcpServerSource::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
	ASSERT(phr);
	CBauotechTcpServerSource *pNewObject = NULL;
	pNewObject = new CBauotechTcpServerSource(punk, phr);
	if (pNewObject == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
	}

	return pNewObject;

}

CBauotechTcpServerSource::~CBauotechTcpServerSource()
{

}

 