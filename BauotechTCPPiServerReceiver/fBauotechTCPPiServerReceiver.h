#include "myInterface.h"
#include <iterator> 
#include <map> 
#include <list>
#include "aes.h"
#include <thread>
#include <memory>
#include "MFDFifo.h"

class CBallStream;
class CBauotechServerSourceFilter;

using namespace std;

 

// {1ECFFEE9-27BA-424C-8C16-98F3DFE499B4}
static const GUID CLSID_BauotechTCPPiServerReceiver =
{ 0x1ecffee9, 0x27ba, 0x424c, { 0x8c, 0x16, 0x98, 0xf3, 0xdf, 0xe4, 0x99, 0xb4 } };

 
class CBauotechTcpServerSource : public CUnknown
{
	friend class CDumpFilter;
	friend class CDumpInputPin;

public:
	CBauotechServerSourceFilter   *m_pFilter;       // Methods for filter interfaces
	CBallStream *m_pPin;          // A simple rendered input pin
	int Setup();

public:

	DECLARE_IUNKNOWN

	CBauotechTcpServerSource(LPUNKNOWN pUnk, HRESULT *phr);
	~CBauotechTcpServerSource();

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);

	  

private:

	// Overriden to say what interfaces we support where
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

};
 

class CBauotechServerSourceFilter : public CSource, public IBoutechRaspeberryTCPServerReceiver
{
public:

	DECLARE_IUNKNOWN;
 
    // The only allowed way to create Bouncing balls!
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);

	char IpAddress[100];
	int m_port;
	bool m_running;

	STDMETHODIMP SetIpAddressAndPort(WCHAR *ipAddress, int Port);

	void StartThreads();
	CRITICAL_SECTION CriticalSection;
	void ReceiveThread();
	void AcceptThread();

	CFifo fifo;

	shared_ptr<thread> m_receiveThread;
	shared_ptr<thread> m_acceptThread;

	struct sockaddr_in server;
	struct sockaddr_in client;

	SOCKET ReceivingSocket;
	SOCKET clientSocket;
	bool m_appRunning;
	struct sockaddr_in addr;
	int StartTCPServer(char *interfaceAddress, int port, char *multicastAddress);

	 

	void CreateSource();
	CCritSec m_Lock;                // Main renderer critical section
    // It is only allowed to to create these objects with CreateInstance
	CBauotechServerSourceFilter(LPUNKNOWN lpunk, HRESULT *phr);
	~CBauotechServerSourceFilter();

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	char m_sFileName[500];

	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv);

}; // CRawInputSourceFilter

 
//------------------------------------------------------------------------------
// Class CBallStream
//
// This class implements the stream which is used to output the bouncing ball
// data from the source filter. It inherits from DirectShows's base
// CSourceStream class.
//------------------------------------------------------------------------------
class CBallStream : public CSourceStream  
{

public:

	CBallStream(HRESULT *phr, CBauotechServerSourceFilter *pParent, LPCWSTR pPinName);
    ~CBallStream();



	BITMAPINFOHEADER m_bmpInfo;
    // plots a ball into the supplied video frame
    HRESULT FillBuffer(IMediaSample *pms);
	 
	struct AES_ctx ctx;
    // Ask for buffers of the size appropriate to the agreed media type
    HRESULT DecideBufferSize(IMemAllocator *pIMemAlloc,
                             ALLOCATOR_PROPERTIES *pProperties);

    
    // Because we calculate the ball there is no reason why we
    // can't calculate it in any one of a set of formats...
    HRESULT CheckMediaType(const CMediaType *pMediaType);
    HRESULT GetMediaType(int iPosition, CMediaType *pmt);
	

	int GetSize(int *cyclic);

    // Resets the stream time to zero
    HRESULT OnThreadCreate(void);

    // Quality control notifications sent to us
    STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);
	
	CBauotechServerSourceFilter *m_pFilter;
	void ScanSeqFile();
	void ScanSeqList();
	//int m_inMbSeqRgbVideoFrames;

public:
	CCritSec m_Lock;                // Main renderer critical section
    int m_iImageHeight;                 // The current image height
    int m_iImageWidth;                  // And current image width
    int m_iRepeatTime;                  // Time in msec between frames
    const int m_iDefaultRepeatTime;     // Initial m_iRepeatTime

    

    CCritSec m_cSharedState;            // Lock on m_rtSampleTime and m_Ball
    CRefTime m_rtSampleTime;            // The time stamp for each sample
    //CBall *m_Ball;                      // The current ball object

}; // CBallStream
    
