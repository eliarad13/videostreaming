#ifndef MFD_FIFO_H
#define MFD_FIFO_H
#include <Windows.h>
#include <stdint.h>
#include "MFDMutex.h"

class CFifo
{

public:

	CFifo()
	{
		fifo = NULL;
		fifo_wr = 0;
		fifo_rd = 0;
		running = 0;
	}

	~CFifo()
	{

	}
	void CreateFifo(int size);
	uint32_t GetFifoFullness();
	uint32_t GetFifoEmptiness();
	void FifoPush(uint8_t *buffer, uint32_t size);
	void FifoGet(uint8_t *buffer);
	void FifoPull(uint8_t *buffer, uint32_t size);
	void FifoClear();
	void FreeFifo();
	void FifoClose();

private:

	CMutex mutex;


	uint8_t *fifo;
	uint32_t fifo_wr;
	uint32_t fifo_rd;
	uint8_t  running;
	uint32_t fifo_size;
};

#endif 
