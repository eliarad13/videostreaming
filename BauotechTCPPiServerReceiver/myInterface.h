#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

  
 

// {9EFA53C9-C8E9-4120-B67A-134B759F8E20}
static const GUID IID_IBoutechRaspeberryTCPServerReceiver =
{ 0x9efa53c9, 0xc8e9, 0x4120, { 0xb6, 0x7a, 0x13, 0x4b, 0x75, 0x9f, 0x8e, 0x20 } };



DECLARE_INTERFACE_(IBoutechRaspeberryTCPServerReceiver, IUnknown)
{

	STDMETHOD(SetIpAddressAndPort)(WCHAR *IpAddress, int Port) PURE;

};
