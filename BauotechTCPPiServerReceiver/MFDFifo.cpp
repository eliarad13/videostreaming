#include "MFDFifo.h"
#include "MFDMutex.h"


void CFifo::CreateFifo(int size)
{
	if (fifo == NULL)
		fifo = (uint8_t *)malloc(size);
	fifo_wr = 0;
	fifo_rd = 0;
	fifo_size = size;
	running = 1;
	mutex.InitializeMutex();
}

uint32_t CFifo::GetFifoFullness()
{

	if (running == 0)
		return 0;

	mutex.MutexEnter();
	uint32_t x;
	if (fifo_wr == fifo_rd)
		x = 0;
	else 
	if (fifo_wr > fifo_rd)
		x = fifo_wr - fifo_rd;
	else 
		x  = fifo_size - fifo_rd + fifo_wr;

	mutex.MutexLeave();
	return x;
}
uint32_t CFifo::GetFifoEmptiness()
{

	if (running == 0)
		return 0;

	mutex.MutexEnter();
	uint32_t x;
	if (fifo_wr == fifo_rd)
		x = fifo_size;
	else
	if (fifo_wr > fifo_rd)
		x = fifo_size - (fifo_wr - fifo_rd);
	else
		x = fifo_size - (fifo_size - fifo_rd + fifo_wr);

	mutex.MutexLeave();
	return x;

}
void CFifo::FifoPush(uint8_t *buffer, uint32_t size)
{ 

	uint32_t x;

	if (running == 0)
		return;

	while ((x = GetFifoEmptiness()) < size)
	{
		if (running == 0)
			return;
		Sleep(0);
	}
	int _size = size;
	mutex.MutexEnter();
	if (fifo_wr >= fifo_rd)
	{
		x = fifo_size - fifo_wr;
		if (x >= size)
			memcpy(fifo + fifo_wr, buffer, size);
		else
		{
			memcpy(fifo + fifo_wr, buffer, x);
			size -= x;
			memcpy(fifo, buffer + x, size);
		}
	}
	else
	{
		memcpy(fifo + fifo_wr, buffer, size);
	}
	 
	fifo_wr = (fifo_wr + _size) % fifo_size;
	mutex.MutexLeave();
}

void CFifo::FifoGet(uint8_t *buffer)
{
	int size = 1;

	if (running == 0)
		return;

	while (GetFifoFullness() < 1)
	{
		if (running == 0)
			return;
		Sleep(0);
	}
	mutex.MutexEnter();
	int _size = 1;
	if (fifo_wr > fifo_rd)
	{
		*buffer = fifo[fifo_rd];
	}
	else
	{
		int x = fifo_size - fifo_rd;
		if (size <= x)
		{
			*buffer = fifo[fifo_rd];
		}
		else
		{
			*buffer = fifo[fifo_rd];
		}
	}

	fifo_rd = (fifo_rd + 1) % fifo_size;
	mutex.MutexLeave();
}


void CFifo::FifoClear()
{ 
	mutex.MutexEnter();
	fifo_rd = fifo_wr = 0;
	mutex.MutexLeave();
	running = 1;
	 
}
void CFifo::FifoPull(uint8_t *buffer, uint32_t size)
{ 
	if (running == 0)
		return;

	while (GetFifoFullness() < size)
	{
		if (running == 0)
			return;
		Sleep(0);
	}
	mutex.MutexEnter();
	uint32_t _size = size;
	if (fifo_wr > fifo_rd)
	{
		memcpy(buffer, fifo + fifo_rd , size);
	}
	else
	{
		int x = fifo_size - fifo_rd;
		if (size <= x)
		{
			memcpy(buffer, fifo + fifo_rd, size);
		}
		else 
		{
			memcpy(buffer, fifo + fifo_rd, x);
			size -= x;
			if (size > 0)
			{
				memcpy(buffer + x, fifo, size);
			}
		}
	}	

	fifo_rd = (fifo_rd + _size) % fifo_size;
	mutex.MutexLeave();
}
 
void CFifo::FifoClose()
{
	running = 0;
}
 
void CFifo::FreeFifo()
{
	running = 0;		
	if (fifo != NULL)
	{
		free(fifo);
		fifo = NULL;
	}
}