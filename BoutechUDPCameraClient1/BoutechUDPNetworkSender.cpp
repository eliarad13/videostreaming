//------------------------------------------------------------------------------
// File: BoutechNetworkSender.cpp
//
// Desc: DirectShow sample code - implementation of a renderer that dumps
//       the samples it receives into a text file.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

  
 
#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include <stdint.h>
#include "BoutechNetworkSenderuids.h"
#include "BoutechUDPNetworkSender.h"
#include <thread>
using namespace std;
#include <memory>

bool m_connecting = false;

CDumpFilter  *pFilter = NULL;
const int MAX_FIFO = 10000000;
#define USE_THREAD  0
WSADATA wsaData;
struct addrinfo *result = NULL;
SOCKET socketC;
struct sockaddr_in serverInfo;
CRITICAL_SECTION CriticalSection;
int m_clientConnected = 0;
bool m_running = false;
enum NetworkType
{
	UDP,
	TCP
};

NetworkType  m_networkType = UDP;

void InitFifo();
int GetFifoSize();
int SendFromFifo(int size);



// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
    &CLSID_BoutechNetworkSender,                // Filter CLSID
    L"Boutech UDP Camera Client 1",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[1]= {
    L"Bauotech UDP Camera Client 1", &CLSID_BoutechNetworkSender, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;

 

 

void LogMessage(char *msg)
{
	FILE *handle = fopen("c:\\network.txt", "a+t");
	fprintf(handle, "%s\n", msg);
	fclose(handle);
}

// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
    CBaseFilter(NAME("Bauotech UDP Camera Client 1"), pUnk, pLock, CLSID_BoutechNetworkSender),
    m_pDump(pDump)
{

	pFilter = this;
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}


//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	 
	m_running = false;
	m_connecting = false;
	 
	if (socketC != -1)
		closesocket(socketC);
	socketC = -1;

    
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);

    if (m_pDump)
    {
         
    }

    return CBaseFilter::Pause();
}


DWORD WINAPI ReceiveThread(void* data)
{

	while (m_running)
	{
		 
		SendFromFifo(1504);
	}

	return NULL;
}

//
// Run
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	CAutoLock cObjectLock(m_pLock);
	 
	m_running = true;
	  
    return CBaseFilter::Run(tStart);
}

 

//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{

	InitFifo();

	
	
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}


//
// BreakConnect
//
// Break a connection
//
HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}

uint8_t *fifoBuffer = NULL;
unsigned int fifo_write = 0;
unsigned int fifo_read = 0;
 

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

void InitFifo()
{
	fifoBuffer = (uint8_t *)malloc(MAX_FIFO);
}

void AddToFifo(uint8_t *data, int size)
{
	
	int x = MAX_FIFO - GetFifoSize();
	if (x < size)
	{	 
		return;
	}

	int d = MIN(size, MAX_FIFO - fifo_write);
	memcpy(fifoBuffer + fifo_write, data, d);
	
	EnterCriticalSection(&CriticalSection);
	fifo_write = ( fifo_write + d ) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);
	 

	size -= d;
	if (size > 0)
	{
		memcpy(fifoBuffer + fifo_write, data, size);
		EnterCriticalSection(&CriticalSection);
		fifo_write = (fifo_write + size) % MAX_FIFO;
		LeaveCriticalSection(&CriticalSection);
 	}
}
 
int SendFromFifo(int size)
{
	if (GetFifoSize() < size)
		return 0;

	int d = MIN(size, MAX_FIFO - fifo_read);
	 
	 
	if (sendto(socketC, (const char *)(fifoBuffer + fifo_read), d, 0,
		(sockaddr*)&serverInfo, sizeof(serverInfo)) != SOCKET_ERROR)
	{

	} 

	EnterCriticalSection(&CriticalSection);
	fifo_read = (fifo_read + d) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);
	size -= d;
	if (size > 0)
	{
		 
		if (sendto(socketC, (const char *)(fifoBuffer + fifo_read), size, 0,
			(sockaddr*)&serverInfo, sizeof(serverInfo)) != SOCKET_ERROR)
		{


		}
		 
		EnterCriticalSection(&CriticalSection);
		fifo_read = (fifo_read + size) % MAX_FIFO;
		LeaveCriticalSection(&CriticalSection);
	}

	return 1; 
}


int GetFifoSize()
{
	 
	EnterCriticalSection(&CriticalSection);

	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return 0;
	}


	if (fifo_write > fifo_read)
	{
		int x = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return x;
	}

	int x = (MAX_FIFO - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return x;
}


//
// Receive
//
// Do something with this media sample
//
STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
	if (m_running == false)
		return S_OK; 

    CheckPointer(pSample,E_POINTER);

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
	 
    
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    } 

#if USE_THREAD
	AddToFifo(pbData, pSample->GetActualDataLength());
#else 

	if (m_networkType == UDP)
	{
		if (sendto(socketC, (const char *)pbData, pSample->GetActualDataLength(), 0,
			(sockaddr*)&serverInfo, sizeof(serverInfo)) != SOCKET_ERROR)
		{

		}
	}
	else
	{
		if (m_clientConnected == 1)
		{
			int sizeToSend = pSample->GetActualDataLength();
			int res = send(socketC, (const char *)pbData, sizeToSend, 0);			
		}		 
	}

#endif 
	 
	return hr;
}
 
//
// EndOfStream
//
STDMETHODIMP CDumpInputPin::EndOfStream(void)
{
    CAutoLock lock(m_pReceiveLock);
    return CRenderedInputPin::EndOfStream();

} // EndOfStream


//
// NewSegment
//
// Called when we are seeked
//
STDMETHODIMP CDumpInputPin::NewSegment(REFERENCE_TIME tStart,
                                       REFERENCE_TIME tStop,
                                       double dRate)
{
    m_tLast = 0;
    return S_OK;

} // NewSegment

//
//  CDump class
//
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
CUnknown(NAME("Boutech UDP Camera Client 1"), pUnk),
m_pFilter(NULL),
m_pPin(NULL),
m_pPosition(NULL),
m_hFile(INVALID_HANDLE_VALUE),
m_pFileName(0),
m_fWriteError(0)
{
	ASSERT(phr);


	int len = sizeof(serverInfo);

	m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
	if (m_pFilter == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
		return;
	}

	m_pPin = new CDumpInputPin(this, GetOwner(),
		m_pFilter,
		&m_Lock,
		&m_ReceiveLock,
		phr);
	if (m_pPin == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
		return;
	}



	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		return;
	}

	serverInfo.sin_family = AF_INET;
	//
	serverInfo.sin_port = htons(8428);
	//serverInfo.sin_addr.s_addr = inet_addr("227.1.1.1");
	serverInfo.sin_addr.s_addr = inet_addr("127.0.0.1");
	//serverInfo.sin_addr.s_addr = inet_addr("192.168.1.15");

	socketC = socket(AF_INET, SOCK_DGRAM, 0);
	 

}
 
CDump::~CDump()
{
	m_running = false;
	m_connecting = false;
	
	if (socketC > 0)
		closesocket(socketC);


    delete m_pPin;
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance


//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);


	// Initialize the critical section one time only.
	InitializeCriticalSectionAndSpinCount(&CriticalSection,0x00000400);
	 
    // Do we have this interface

    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Boutech UDP Camera Client 1 Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

}  
  
 

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

