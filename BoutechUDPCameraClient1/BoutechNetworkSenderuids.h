//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

 
// {771AE2E6-8EBA-4A13-93FF-C2EFB32011D3}
DEFINE_GUID(CLSID_BoutechNetworkSender,
	0x771ae2e6, 0x8eba, 0x4a13, 0x93, 0xff, 0xc2, 0xef, 0xb3, 0x20, 0x11, 0xd3);
