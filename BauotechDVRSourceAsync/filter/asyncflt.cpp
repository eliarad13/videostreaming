//------------------------------------------------------------------------------
// File: AsyncFlt.cpp
//
// Desc: DirectShow sample code - implementation of CAsyncFilter.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#include <streams.h>

#include "asyncio.h"
#include "asyncrdr.h"

#pragma warning(disable:4710)  // 'function' not inlined (optimization)
#include "asyncflt.h"
#include <iostream>
#include <string>
#include <fstream>


bool m_running = false;;
bool m_pinConnected = false;
FILE *handle = NULL;
list<string> m_dvrList;
list<string> m_dvrListOrig;
list<int> m_dvrSizeOrig;
list<int> m_dvrSize;
int m_curFileIndex = -1;
uint32_t m_totalLeftSize = 0;
uint32_t m_totalLeftSizeOrig = 0;
uint32_t m_totalReadSize = 0;


//
// Setup data for filter registration
//
const AMOVIESETUP_MEDIATYPE sudOpPinTypes =
{ &MEDIATYPE_Stream     // clsMajorType
, &MEDIASUBTYPE_NULL }; // clsMinorType

const AMOVIESETUP_PIN sudOpPin =
{ L"Output"          // strName
, FALSE              // bRendered
, TRUE               // bOutput
, FALSE              // bZero
, FALSE              // bMany
, &CLSID_NULL        // clsConnectsToFilter
, L"Input"           // strConnectsToPin
, 1                  // nTypes
, &sudOpPinTypes };  // lpTypes

const AMOVIESETUP_FILTER sudAsync =
{ &CLSID_BauotectDVRSourceAsync              // clsID
, L"Bauotech DVR Source Async"  // strName
, MERIT_UNLIKELY                  // dwMerit
, 1                               // nPins
, &sudOpPin };                    // lpPin


//
//  Object creation template
//
CFactoryTemplate g_Templates[1] = {
    { L"Bauotech DVR Source Async"
	, &CLSID_BauotectDVRSourceAsync
    , CAsyncFilter::CreateInstance
    , NULL
    , &sudAsync }
};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2(FALSE);
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}


//* Create a new instance of this class
CUnknown * WINAPI CAsyncFilter::CreateInstance(LPUNKNOWN pUnk, HRESULT *phr)
{
    ASSERT(phr);

    //  DLLEntry does the right thing with the return code and
    //  the returned value on failure

    return new CAsyncFilter(pUnk, phr);
}


using std::string;

string getPathName(const string& s) {

	char sep = '/';

#ifdef _WIN32
	sep = '\\';
#endif

	size_t i = s.rfind(sep, s.length());
	if (i != string::npos) {
		return(s.substr(0, i));
	}

	return("");
}


int GetFileSize(string fileName)
{
	ifstream mySource;
	mySource.open(fileName, ios_base::binary);
	mySource.seekg(0, ios_base::end);
	int size = mySource.tellg();
	mySource.close();
	return size;
}


STDMETHODIMP CAsyncFilter::Run(REFERENCE_TIME tStart)
{
	
	return CBaseFilter::Run(tStart);
}
STDMETHODIMP CAsyncFilter::Pause()
{
	m_running = false;

	return CBaseFilter::Pause();
}
STDMETHODIMP CAsyncFilter::Stop()
{
	m_running = false;
	return CBaseFilter::Stop();
}

BOOL CAsyncFilter::ReadTheFile(LPCTSTR lpszFileName)
{
    DWORD dwBytesRead;

	size_t size = wcstombs(NULL, lpszFileName, 0);
	char* fileName = new char[size + 1];
	memset(fileName, 0, size + 1);
	wcstombs(fileName, lpszFileName, size + 1);

	m_llSize = 0;
	handle = fopen(fileName, "r+t");
	if (handle == NULL)
		return false;
	
	string fullFileName;
	string path = getPathName(fileName);
	char line[250];
	while (true)
	{
		char *p = fgets(line, 250, handle);
		if (p == NULL)
			break;
		p = strchr(line, '\n');
		if (p != NULL)
			*p = 0;
		fullFileName = path + "\\" + line;
		m_dvrList.push_back(fullFileName);
		m_dvrListOrig.push_back(fullFileName);
		int sizeOfFile = GetFileSize(fullFileName);
		m_llSize += sizeOfFile;
		m_dvrSize.push_back(sizeOfFile);
		m_dvrSizeOrig.push_back(sizeOfFile);
	}
	 
	m_totalLeftSize = m_llSize;
	m_totalLeftSizeOrig = m_llSize;

	fclose(handle);
	handle = NULL;
	delete fileName;
    return TRUE;
}

