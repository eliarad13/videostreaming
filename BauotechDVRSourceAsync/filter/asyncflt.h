//------------------------------------------------------------------------------
// File: AsyncFlt.h
//
// Desc: DirectShow sample code - header file for async filter.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <strsafe.h>
#include <vector>
#include <stdint.h>
#include <algorithm>
#include <list>
using namespace std;


// {E3F87BBC-C056-447E-8DDD-7ED90CC1E436}
static const GUID CLSID_BauotectDVRSourceAsync =
{ 0xe3f87bbc, 0xc056, 0x447e, { 0x8d, 0xdd, 0x7e, 0xd9, 0xc, 0xc1, 0xe4, 0x36 } };


extern bool m_pinConnected;
extern bool m_running;
extern list<string> m_dvrList;
extern list<string> m_dvrListOrig;
extern list<int> m_dvrSizeOrig;
extern list<int> m_dvrSize;
extern int m_curFileIndex;
extern uint32_t m_totalLeftSize;
extern uint32_t m_totalReadSize;
extern FILE *handle;
//  NOTE:  This filter does NOT support AVI format

//
//  Define an internal filter that wraps the base CBaseReader stuff
//

class CMemStream : public CAsyncStream
{
public:
    CMemStream() :
        m_llPosition(0)
    {
    }

    /*  Initialization */
    void Init(LPBYTE pbData, LONGLONG llLength, DWORD dwKBPerSec = INFINITE)
    {
        m_pbData = pbData;
        m_llLength = llLength;
        m_dwKBPerSec = dwKBPerSec;
        m_dwTimeStart = timeGetTime();
    }

    HRESULT SetPointer(LONGLONG llPos)
    {
        if (llPos < 0 || llPos > m_llLength) {
            return S_FALSE;
        } else {
            m_llPosition = llPos;
            return S_OK;
        }
    }

	
	LONGLONG m_llPositionLast = 0;
	char msgbuf[250];
    HRESULT Read(PBYTE pbBuffer,
                 DWORD dwBytesToRead,
                 BOOL bAlign,
                 LPDWORD pdwBytesRead)
    {
        CAutoLock lck(&m_csLock);
        DWORD dwReadLength;

		if (m_pinConnected == true && m_running == false)
		{
			if (handle != NULL)
				fclose(handle);
			handle = NULL;
			m_dvrSize = m_dvrSizeOrig;
			m_dvrList = m_dvrListOrig;
			m_totalReadSize = 0;
			m_totalLeftSize = m_totalLeftSizeOrig;
			m_curFileIndex = -1;
			m_running = true;
		}
		 
        /*  Wait until the bytes are here! */
        DWORD dwTime = timeGetTime();

        //if (m_llPosition + dwBytesToRead > m_llLength) {
           // dwReadLength = (DWORD)(m_llLength - m_llPosition);
        //} else {
            dwReadLength = dwBytesToRead;
        //}
        DWORD dwTimeToArrive =
            ((DWORD)m_llPosition + dwReadLength) / m_dwKBPerSec;

        if (dwTime - m_dwTimeStart < dwTimeToArrive) {
            Sleep(dwTimeToArrive - dwTime + m_dwTimeStart);
        }


		int sizeToRead = dwReadLength;
		int chunk;		
		int index = 0;

		

		m_llPositionLast = m_llPosition;
		while (sizeToRead > 0)
		{
			list<int>::iterator it = m_dvrSize.begin();
			if (m_curFileIndex > -1)
				advance(it, m_curFileIndex);
			if (m_curFileIndex >= 0 && *it > 0)
			{
				chunk = min(sizeToRead, *it);
				*it -= chunk;

				fread(pbBuffer + index , chunk, 1, handle);
				index += chunk;
				sizeToRead -= chunk;
				//CopyMemory((PVOID)pbBuffer, (PVOID)(m_pbData + m_llPosition),
					//dwReadLength);
			} 
			else
			{
				if (m_dvrList.size() == 0)
				{
					*pdwBytesRead = 0;
					return S_OK;
				}
				if (handle != NULL)
					fclose(handle);
				handle = NULL;
				m_curFileIndex++;
				m_workingFileName = m_dvrList.front();
				m_dvrList.pop_front();
				handle = fopen(m_workingFileName.c_str(), "r+b");
			
				sprintf(msgbuf, "FileName %s\n", m_workingFileName.c_str());
				OutputDebugStringA(msgbuf);
			}
		}		 

		m_totalLeftSize -= dwReadLength;
        m_llPosition += dwReadLength;
        *pdwBytesRead = dwReadLength;

	 
		sprintf(msgbuf, "m_llPosition  %d  dwReadLength  %d\n", m_llPosition, dwReadLength);
		OutputDebugStringA(msgbuf);

        return S_OK;
    }

    LONGLONG Size(LONGLONG *pSizeAvailable)
    {
        LONGLONG llCurrentAvailable =
            static_cast <LONGLONG> (UInt32x32To64((timeGetTime() - m_dwTimeStart),m_dwKBPerSec));
 
       *pSizeAvailable =  min(m_llLength, llCurrentAvailable);
	 
        return m_llLength;
    }

    DWORD Alignment()
    {
        return 1;
    }

    void Lock()
    {
        m_csLock.Lock();
    }

    void Unlock()
    {
        m_csLock.Unlock();
    }

private:
    CCritSec       m_csLock;
    PBYTE          m_pbData;
    LONGLONG       m_llLength;
    LONGLONG       m_llPosition;
    DWORD          m_dwKBPerSec;
    DWORD          m_dwTimeStart;
	string		   m_workingFileName;
};

class CAsyncFilter : public CAsyncReader, public IFileSourceFilter
{
public:
    CAsyncFilter(LPUNKNOWN pUnk, HRESULT *phr) :
        CAsyncReader(NAME("Mem Reader"), pUnk, &m_Stream, phr),
        m_pFileName(NULL),
        m_pbData(NULL)
    {


    }

    ~CAsyncFilter()
    {
        delete [] m_pbData;
        delete [] m_pFileName;
    }
	CCritSec m_csFilter;
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN, HRESULT *);

    DECLARE_IUNKNOWN

	// Open and close the file as necessary
	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv)
    {
        if (riid == IID_IFileSourceFilter) {
            return GetInterface((IFileSourceFilter *)this, ppv);
        } else {
            return CAsyncReader::NonDelegatingQueryInterface(riid, ppv);
        }
    }

    /*  IFileSourceFilter methods */

    //  Load a (new) file
    STDMETHODIMP Load(LPCOLESTR lpwszFileName, const AM_MEDIA_TYPE *pmt)
    {
        CheckPointer(lpwszFileName, E_POINTER);

        // lstrlenW is one of the few Unicode functions that works on win95
        int cch = lstrlenW(lpwszFileName) + 1;

#ifndef UNICODE
        TCHAR *lpszFileName=0;
        lpszFileName = new char[cch * 2];
        if (!lpszFileName) {
      	    return E_OUTOFMEMORY;
        }
        WideCharToMultiByte(GetACP(), 0, lpwszFileName, -1,
    			lpszFileName, cch, NULL, NULL);
#else
        TCHAR lpszFileName[MAX_PATH]={0};
        (void)StringCchCopy(lpszFileName, NUMELMS(lpszFileName), lpwszFileName);
#endif
        CAutoLock lck(&m_csFilter);

        /*  Check the file type */
        CMediaType cmt;
        if (NULL == pmt) {
            cmt.SetType(&MEDIATYPE_Stream);
            cmt.SetSubtype(&MEDIASUBTYPE_NULL);
        } else {
            cmt = *pmt;
        }

        if (!ReadTheFile(lpszFileName)) {
#ifndef UNICODE
            delete [] lpszFileName;
#endif
            return E_FAIL;
        }

        m_Stream.Init(m_pbData, m_llSize);

        m_pFileName = new WCHAR[cch];

        if (m_pFileName!=NULL)
    	    CopyMemory(m_pFileName, lpwszFileName, cch*sizeof(WCHAR));

        // this is not a simple assignment... pointers and format
        // block (if any) are intelligently copied
    	m_mt = cmt;

        /*  Work out file type */
        cmt.bTemporalCompression = TRUE;	       //???
        cmt.lSampleSize = 1;

        return S_OK;
    }

    // Modeled on IPersistFile::Load
    // Caller needs to CoTaskMemFree or equivalent.

    STDMETHODIMP GetCurFile(LPOLESTR * ppszFileName, AM_MEDIA_TYPE *pmt)
    {
        CheckPointer(ppszFileName, E_POINTER);
        *ppszFileName = NULL;

        if (m_pFileName!=NULL) {
        	DWORD n = sizeof(WCHAR)*(1+lstrlenW(m_pFileName));

            *ppszFileName = (LPOLESTR) CoTaskMemAlloc( n );
            if (*ppszFileName!=NULL) {
                  CopyMemory(*ppszFileName, m_pFileName, n);
            }
        }

        if (pmt!=NULL) {
            CopyMediaType(pmt, &m_mt);
        }

        return NOERROR;
    }

private:
    BOOL CAsyncFilter::ReadTheFile(LPCTSTR lpszFileName);

private:
    LPWSTR     m_pFileName;
    LONGLONG   m_llSize;
    PBYTE      m_pbData;
    CMemStream m_Stream;
};
