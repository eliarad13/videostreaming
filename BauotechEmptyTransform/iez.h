//------------------------------------------------------------------------------
// File: IEZ.h
//
// Desc: DirectShow sample code - custom interface to allow the user to
//       perform image special effects.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __IEZ__
#define __IEZ__

#ifdef __cplusplus
extern "C" {
#endif

  
	// {B11C6DBA-0BDA-466F-8193-72054EE3F107}
	DEFINE_GUID(IID_BauotechEmptyTransform,
		0xb11c6dba, 0xbda, 0x466f, 0x81, 0x93, 0x72, 0x5, 0x4e, 0xe3, 0xf1, 0x7);



	DECLARE_INTERFACE_(IBauotechEmptyTransform, IUnknown)
    {
		 

		STDMETHOD(BAlgoPutIPEffect) (int effectNum) PURE;


		STDMETHOD(BAlgoSetParam1) (THIS_ int a, int b, float c, float d, double e) PURE;
		STDMETHOD(BAlgoSetParam2) (THIS_ double a, double b, double c, double d) PURE;



    };

#ifdef __cplusplus
}
#endif

#endif // __IEZ__

