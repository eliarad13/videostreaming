//------------------------------------------------------------------------------
// File: EZRGB24.h
//
// Desc: DirectShow sample code - special effects filter header file.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


class CBauotechEmptyTransform : public CTransformFilter,
		 public IBauotechEmptyTransform,
         public ISpecifyPropertyPages,
         public CPersistStream
{

public:

    DECLARE_IUNKNOWN;
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);
	HINSTANCE hDLL;               // Handle to DLL
    // Reveals IEZrgb24 and ISpecifyPropertyPages
    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

    // CPersistStream stuff
    HRESULT ScribbleToStream(IStream *pStream);
    HRESULT ReadFromStream(IStream *pStream);

    // Overrriden from CTransformFilter base class

    HRESULT Transform(IMediaSample *pIn, IMediaSample *pOut);
    HRESULT CheckInputType(const CMediaType *mtIn);
    HRESULT CheckTransform(const CMediaType *mtIn, const CMediaType *mtOut);
    HRESULT DecideBufferSize(IMemAllocator *pAlloc,
                             ALLOCATOR_PROPERTIES *pProperties);
    HRESULT GetMediaType(int iPosition, CMediaType *pMediaType);

    // These implement the custom IIPEffect interface
	 
	STDMETHODIMP BAlgoPutIPEffect(int IPEffect);
	STDMETHODIMP BAlgoSetParam1(int a, int b, float c, float d, double e);
	STDMETHODIMP BAlgoSetParam2(double a, double b, double c, double d);


    // ISpecifyPropertyPages interface
    STDMETHODIMP GetPages(CAUUID *pPages);

    // CPersistStream override
    STDMETHODIMP GetClassID(CLSID *pClsid);

private:

    // Constructor
	CBauotechEmptyTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr);
	BOOL LoadAlgoDll();

    // Look after doing the special effect
    BOOL CanPerformEZrgb24(const CMediaType *pMediaType) const;
    HRESULT Copy(IMediaSample *pSource, IMediaSample *pDest) const;
    HRESULT Transform(IMediaSample *pMediaSample);

    CCritSec    m_EZrgb24Lock;          // Private play critical section
    int         m_effect;               // Which effect are we processing
    CRefTime    m_effectStartTime;      // When the effect will begin
    CRefTime    m_effectTime;           // And how long it will last for
    const long m_lBufferRequest;        // The number of buffers to use

}; // EZrgb24

