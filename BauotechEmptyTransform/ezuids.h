//------------------------------------------------------------------------------
// File: EZUIDs.h
//
// Desc: DirectShow sample code - special effects filter CLSIDs.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


// Special effects filter CLSID

 
// {DA1A87F1-1065-49B1-8F7A-ECCEACF64A7E}
DEFINE_GUID(CLSID_BauotechEmptyTransform ,
	0xda1a87f1, 0x1065, 0x49b1, 0x8f, 0x7a, 0xec, 0xce, 0xac, 0xf6, 0x4a, 0x7e);


// And the property page we support

 

// {6E7A0BEC-543E-452C-A27D-8F14E61D1673}
DEFINE_GUID(CLSID_BauotechPropertyPage,
	0x6e7a0bec, 0x543e, 0x452c, 0xa2, 0x7d, 0x8f, 0x14, 0xe6, 0x1d, 0x16, 0x73);
