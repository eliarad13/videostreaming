using System.Net.Sockets;
using System.Net;
using System;
using System.IO;

namespace MulticastServer
{

	public class MyStreaming
	{
        byte[] buffer = new byte[8192];

        bool m_running = false;
        Socket m_senderSocket;
        Socket m_receiverSocket;


        public void MulticastClientRecorder(string mcastGroup, string port, string fileName)
        {
            m_running = true;
            m_senderSocket = CreateMulticastSender(mcastGroup, port, "2", "1");

            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                while (m_running)
                {
                    int size = m_senderSocket.Receive(buffer);
                    writer.Write(buffer, 0, size);
                }
            }
            m_senderSocket.Close();
        }
         

        public void MulticastServerRecorder(string mcastGroup, string port, string fileName)
        {
            m_running = true;

            m_receiverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            IPEndPoint ipep = new IPEndPoint(IPAddress.Any, int.Parse(port));
            m_receiverSocket.Bind(ipep);

            IPAddress ip = IPAddress.Parse(mcastGroup);

            m_receiverSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip, IPAddress.Any));

            using (BinaryWriter writer = new BinaryWriter(File.Open(fileName, FileMode.Create)))
            {
                while (m_running)
                {
                    int size = m_receiverSocket.Receive(buffer);
                    writer.Write(buffer, 0, size);
                }
            }
            m_receiverSocket.Close();

        }

        public void MulticastToMulticast(string mcastGroup, string port , string mcastGroup1, string port1) 
		{
            m_running = true;
            m_receiverSocket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);

            m_senderSocket = CreateMulticastSender(mcastGroup1, port1, "2", "1");


            IPEndPoint ipep=new IPEndPoint(IPAddress.Any,int.Parse(port));
            m_receiverSocket.Bind(ipep);
			
			IPAddress ip=IPAddress.Parse(mcastGroup);

            m_receiverSocket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.AddMembership, new MulticastOption(ip,IPAddress.Any));

			while(m_running) 
			{				
				int size = m_receiverSocket.Receive(buffer);
                m_senderSocket.Send(buffer, size, SocketFlags.None);                
            }
            m_senderSocket.Close();
            m_receiverSocket.Close();
		}

        public void Stop()
        {
            m_running = false;
            m_senderSocket.Close();
        }

        private Socket CreateMulticastSender(string mcastGroup, string port, string ttl, string rep)
        {
            IPAddress ip;
            try
            {
                Console.WriteLine("MCAST Send on Group: {0} Port: {1} TTL: {2}", mcastGroup, port, ttl);
                ip = IPAddress.Parse(mcastGroup);

                Socket s = new Socket(AddressFamily.InterNetwork,
                                SocketType.Dgram, ProtocolType.Udp);

                s.SetSocketOption(SocketOptionLevel.IP,
                    SocketOptionName.AddMembership, new MulticastOption(ip));

                s.SetSocketOption(SocketOptionLevel.IP,
                    SocketOptionName.MulticastTimeToLive, int.Parse(ttl));


                IPEndPoint ipep = new IPEndPoint(IPAddress.Parse(mcastGroup), int.Parse(port));

                s.Connect(ipep);
                return s;         
            }
            catch (System.Exception e)
            {
                Console.Error.WriteLine(e.Message);
                return null;
            }
        }         
    }







}