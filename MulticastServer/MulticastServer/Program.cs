﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace MulticastServer
{
    class Program
    {
        static void Main(string[] args)
        {

            

            MyStreaming streaming = new MyStreaming();
            //streaming.MulticastToMulticast("227.1.1.1", "30121", "227.1.1.1", "30122");

            string IpAdress = "84.108.170.204";
            int port = 10200;
             
            try
            {
                if (args.Length == 3)
                {
                    IpAdress = args[0];
                    port = int.Parse(args[1]);
                }
                Console.WriteLine("Default Multicast {0}:{1}", IpAdress, port);
                Console.WriteLine("Select 1 to stream to Client");
                Console.WriteLine("Select 2 to stream to Server");
                string option = Console.ReadLine();
                if (option == "1")
                {
                    Console.WriteLine("Press enter to stop");
                    streaming.MulticastClientRecorder(IpAdress, port.ToString(), "output.ts");
                    Console.ReadLine();
                    Console.WriteLine("Finished");
                } else 
                if (option == "2")
                {
                    Console.WriteLine("Press enter to stop or ctrl + C to break");
                    streaming.MulticastServerRecorder(IpAdress, port.ToString(), "output.ts");
                    Console.ReadLine();
                    Console.WriteLine("Finished");
                }
                else
                {
                    Console.WriteLine("Not valid option");
                }                
            }
            catch (Exception err)
            {
                Console.WriteLine(err.Message);
            }


        }
    }
}
