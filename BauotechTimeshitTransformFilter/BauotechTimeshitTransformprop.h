 

#include <strsafe.h>

#ifndef __GARGPROP__
#define __GARGPROP__

#ifdef __cplusplus
extern "C" {
#endif

 


class CBauotechTimeshiftTransformProperties : public CBasePropertyPage
{

public:

    static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);

    // Overrides from CBasePropertyPage
    HRESULT OnConnect(IUnknown * punk);
    HRESULT OnDisconnect(void);

    HRESULT OnDeactivate(void);

	CBauotechTimeshiftTransformProperties(LPUNKNOWN lpunk, HRESULT *phr);

private:

     

    HWND        m_hwndSlider;   // handle of slider

	IBauotechTimeshiftTransform   *m_pGargle;       
                                 
								 
    int        m_iGargleRate;    
                                 
    int        m_iGargleShape;   

}; 

#ifdef __cplusplus
}
#endif

#endif // __GARGPROP__
