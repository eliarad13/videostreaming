#pragma once 
#include <stdint.h>
#include <time.h>
#include "mpegts.hpp"
#include "AutoResetEvent.h"
 
class CBauotechTimeshiftTransform
	// Inherited classes
	: public CTransInPlaceFilter       // Main DirectShow interfaces
	, public IBauotechTimeshiftTransform                   


{  

public:

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);

	DECLARE_IUNKNOWN;

	//
	// --- CTransInPlaceFilter Overrides --
	//

	HRESULT CheckInputType(const CMediaType *mtIn);

	// Basic COM - used here to reveal our property interface.
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);


	STDMETHODIMP GoToLive();
	STDMETHODIMP SeekInTime(uint64_t ts_rd);
	STDMETHODIMP SetTimeShiftBuffer(uint64_t bufferSize);
	STDMETHODIMP SetTimeShiftCallback(TimeShiftCallback p);
	STDMETHODIMP SetTimeDiffToLog(int difftime);
	STDMETHODIMP PauseTS();
	STDMETHODIMP ResumeTS();



	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	 
	uint64_t m_timshiftBufferSize;
	uint8_t *timeShiftBuffer;
	uint64_t timeshift_wr = 0;
	uint64_t timeshift_rd = 0;
	bool m_goLive;
	int m_timeDiffToLog;
	time_t m_lastTime;
	TimeShiftCallback  pTimeShiftCallback;
private:
	bool logI;
	uint8_t *tempBuffer;
	// Constructor
	CBauotechTimeshiftTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr);
	~CBauotechTimeshiftTransform();
	uint64_t GetFifoSize(uint64_t rd);

	AutoResetEvent m_event;

	bool m_pause_ts;
	bool m_searchSync4Live;
	bool m_searchSync4Shift;
	void InitTSParser();
	util::mpegts_info info;
	// Overrides the PURE virtual Transform of CTransInPlaceFilter base class
	// This is where the "real work" is done.
	HRESULT Transform(IMediaSample *pSample);

	// This is where the real work is really done (called from Transform)
	void MessItAbout(PBYTE pb, int cb);

	// Overrides a CTransformInPlace function.  Called as part of connecting.
	virtual HRESULT SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt);

	// If there are multiple instances of this filter active, it's
	// useful for debug messages etc. to know which one this is.
	// This variable has no other purpose.
	static int m_nInstanceCount;                   // total instances
	int m_nThisInstance;

	int                 m_Shape;               // 0==triangle, 1==square
	int                 m_SamplesPerSec;       // Current sample format
	int                 m_BytesPerSample;      // Current sample format
	int                 m_Channels;            // Current sample format
	int                 m_Phase;               // See MessItAbout in gargle.cpp
	CCritSec            m_GargleLock;          // To serialise access.


	bool m_canStart;

	util::mpegts_parser tsParser;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;


};  
