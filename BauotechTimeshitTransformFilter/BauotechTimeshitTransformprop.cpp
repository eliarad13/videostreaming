//------------------------------------------------------------------------------
// File: GargProp.cpp
//
// Desc: DirectShow sample code - implementation of CGargleProperties class.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#include <streams.h>

// Eliminate two expected level 4 warnings from the Microsoft compiler.
// The class does not have an assignment or copy operator, and so cannot
// be passed by value.  This is normal.  This file compiles clean at the
// highest (most thorough) warning level (-W4).
#pragma warning(disable: 4511 4512)

#include <commctrl.h>
#include <olectl.h>
#include <memory.h>
#include <math.h>

#include "resource.h"
#include "iBauotechTimeshitTransform.h"
#include "BauotechTimeshitTransformprop.h"

 
CUnknown * WINAPI CBauotechTimeshiftTransformProperties::CreateInstance(LPUNKNOWN lpunk, HRESULT *phr)
{
    ASSERT(phr);
    
	CUnknown *punk = new CBauotechTimeshiftTransformProperties(lpunk, phr);
    if (punk == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return punk;

} // CreateInstance


 
CBauotechTimeshiftTransformProperties::CBauotechTimeshiftTransformProperties(LPUNKNOWN lpunk, HRESULT *phr)
    : CBasePropertyPage(NAME("BauotechTimeshiftTransform Property Page"), lpunk, 
                        IDD_GARGPROP, IDS_NAME)
    , m_pGargle(NULL)
{

}  

 
HRESULT CBauotechTimeshiftTransformProperties::OnConnect(IUnknown * punk)
{
    

    return NOERROR;

}  

 
HRESULT CBauotechTimeshiftTransformProperties::OnDisconnect()
{
   

    return(NOERROR);

} 


 
HRESULT CBauotechTimeshiftTransformProperties::OnDeactivate(void)
{
  
      

    return NOERROR;

}  
 

#pragma warning(disable: 4514) // "unreferenced inline function has been removed"


