
#include <streams.h>

// Eliminate two expected level 4 warnings from the Microsoft compiler.
// The class does not have an assignment or copy operator, and so cannot
// be passed by value.  This is normal.  This file compiles clean at the
// highest (most picky) warning level (-W4).
#pragma warning(disable: 4511 4512)


#include <initguid.h>
#if (1100 > _MSC_VER)
#include <olectlid.h>
#else
#include <olectl.h>
#endif
#include "BauotechTimeshitTransformuids.h"             
#include "iBauotechTimeshitTransform.h"              / 
#include "BauotechTimeshitTransform.h"

const uint64_t MAX_TIMESHIFT_BUFFER = 1024 * 1024 * 188 * 5;


//------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------


#define DbgFunc(a) DbgLog(( LOG_TRACE                        \
                          , 2                                \
                          , TEXT("CBauotechTimeshiftTransform(Instance %d)::%s") \
                          , m_nThisInstance                  \
                          , TEXT(a)                          \
                         ));


// Self-registration data structures

const AMOVIESETUP_MEDIATYPE
sudPinTypes =   { &MEDIATYPE_Audio        // clsMajorType
                , &MEDIASUBTYPE_NULL };   // clsMinorType

const AMOVIESETUP_PIN
psudPins[] = { { L"Input"            // strName
               , FALSE               // bRendered
               , FALSE               // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Output"           // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             , { L"Output"           // strName
               , FALSE               // bRendered
               , TRUE                // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Input"            // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             };

const AMOVIESETUP_FILTER
sudGargle = { &CLSID_BauotechTimeShiftTransform                   // class id
            , L"Bauotech Time Shift Transform"                       // strName
            , MERIT_DO_NOT_USE                // dwMerit
            , 2                               // nPins
            , psudPins                        // lpPin
            };

// Needed for the CreateInstance mechanism
CFactoryTemplate g_Templates[1]= { { L"Bauotech Time Shift Transform"
									, &CLSID_BauotechTimeShiftTransform
                                   , CBauotechTimeshiftTransform::CreateInstance
                                   , NULL
                                   , &sudGargle
                                   }                                 
                                 };

int g_cTemplates = sizeof(g_Templates)/sizeof(g_Templates[0]);

// initialise the static instance count.
int CBauotechTimeshiftTransform::m_nInstanceCount = 0;



CBauotechTimeshiftTransform::~CBauotechTimeshiftTransform()
{
	if (timeShiftBuffer != NULL)
	{
		delete timeShiftBuffer;
		timeShiftBuffer = NULL;
	}
	if (tempBuffer != NULL)
	{
		delete tempBuffer;
		tempBuffer = NULL;
	}
}


void CBauotechTimeshiftTransform::InitTSParser()
{

	show_pcr_time = false;
	show_frame_pos = false;
	show_frame_pts = false;
	show_frame_dts = false;
	show_key_frame = true;


	vc = 0;
	sc = 0;
	offset = 0;
	pts = 0;
	dts = 0;
	vknown_type = false;
	aknown_type = false;


	si.pid_ = 40;
	si.type_ = util::stream_info::stream_video;
	si.stream_type_ = tsParser.stream_type(std::string("H264"));
	streams.push_back(si);
	si.pid_ = 50;
	si.type_ = util::stream_info::stream_audio;
	si.stream_type_ = tsParser.stream_type(std::string("AAC"));
	streams.push_back(si);
	tsParser.init_streams(streams);

	mi.pid_ = 40;
	mi.is_video_ = true;
	mi.is_audio_ = false;

	m_canStart = false;
}

CBauotechTimeshiftTransform::CBauotechTimeshiftTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr)
	: CTransInPlaceFilter(tszName, punk, CLSID_BauotechTimeShiftTransform, phr)
    , m_SamplesPerSec     (0)
    , m_BytesPerSample    (0)
    , m_Channels          (0)
    , m_Phase             (0)
    , m_Shape             (0)
{


	InitTSParser();
	logI = false;
	m_pause_ts = false;
	m_searchSync4Shift = false;
	m_searchSync4Live = false;
	pTimeShiftCallback = NULL;
    m_nThisInstance = ++m_nInstanceCount; // Useful for debug, no other purpose
	timeShiftBuffer = NULL;
	m_timeDiffToLog = 5;
 
	m_timshiftBufferSize = MAX_TIMESHIFT_BUFFER;	  
	timeShiftBuffer = new uint8_t[m_timshiftBufferSize];
	tempBuffer = new uint8_t[1024 * 100];

    //DbgFunc("CBauotechTimeshiftTransform");

}  

CUnknown * WINAPI CBauotechTimeshiftTransform::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CBauotechTimeshiftTransform *pNewObject = new CBauotechTimeshiftTransform(NAME("Bauotech Time Shift Transform Filter"), punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

}


STDMETHODIMP CBauotechTimeshiftTransform::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
    CheckPointer(ppv,E_POINTER);

	if (riid == IID_IBaoutechTimeShiftTransform) {
		return GetInterface((IBauotechTimeshiftTransform *) this, ppv);

    } else if (riid == IID_IPersistStream) {
        return GetInterface((IPersistStream *) this, ppv);

    } else {
        // Pass the buck
        return CTransInPlaceFilter::NonDelegatingQueryInterface(riid, ppv);
    }

} // NonDelegatingQueryInterface

 
 
STDMETHODIMP CBauotechTimeshiftTransform::Pause()
{

	//m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP CBauotechTimeshiftTransform::Stop()
{

	//m_pOutputPin->m_graphStat = GRAPH_STOP;

 

	return CBaseFilter::Stop();
}

STDMETHODIMP CBauotechTimeshiftTransform::Run(REFERENCE_TIME tStart)
{

	timeshift_wr = 0;
	timeshift_rd = 0;
	m_lastTime = time(NULL);
 
	m_goLive = true;
	return CBaseFilter::Run(tStart);
}

//
// Transform
//
// Override CTransInPlaceFilter method.
// Convert the input sample into the output sample.
//
int k = 0;
HRESULT CBauotechTimeshiftTransform::Transform(IMediaSample *pSample)
{
    
	 
    BYTE *pSampleBuffer;
    int iSize = pSample->GetActualDataLength();
    pSample->GetPointer(&pSampleBuffer);

	memcpy(timeShiftBuffer + timeshift_wr, pSampleBuffer, iSize);

	if (k < 5)
	{
		time_t t = time(NULL);
		struct tm tm = *localtime(&t);
		struct tm tmLast = *localtime(&m_lastTime);
		double diff = difftime(t, m_lastTime);
		offset = 0;
		if (diff >= m_timeDiffToLog || logI == true)
		{

			logI = true;
			for (int i = 0; i < iSize; i += 188)
			{
				memset(&info, 0, sizeof(info));
				auto suc = tsParser.do_parser(pSampleBuffer + offset, info);

				//if (info.type_ == util::mpegts_info::idr)
				if (info.pict_type_ == util::av_picture_type_i)
				{
					m_lastTime = t;

					if (pTimeShiftCallback != NULL)
						pTimeShiftCallback(timeshift_wr + offset, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);

					k++;
					break;
				}
				offset += 188;

			}
		}
	}

	timeshift_wr = (timeshift_wr + iSize) % m_timshiftBufferSize;

	int offset = 0;
	if (m_searchSync4Live == true)
	{
		for (int i = 0; i < iSize; i += 188)
		{
			memset(&info, 0, sizeof(info));
			auto suc = tsParser.do_parser(pSampleBuffer + offset, info);

			//if (info.type_ == util::mpegts_info::idr)
			if (info.pict_type_ == util::av_picture_type_i)
			{
				memcpy(tempBuffer, pSampleBuffer + offset, iSize - offset);
				m_searchSync4Live = false;
				memcpy(pSampleBuffer, tempBuffer, iSize - offset);
				pSample->SetActualDataLength(iSize - offset);
				return NOERROR;
			}
			offset += 188;
		}
		pSample->SetActualDataLength(0);
		return NOERROR;
	} 
	  

	if (m_goLive == false)
	{
		if (m_searchSync4Shift == true)
		{
			offset = 0;
			for (int i = timeshift_rd; i < timeshift_wr; i += 188)
			{
				memset(&info, 0, sizeof(info));
				auto suc = tsParser.do_parser(timeShiftBuffer + timeshift_rd + offset, info);

				//if (info.type_ == util::mpegts_info::idr)
				if (info.pict_type_ == util::av_picture_type_i)
				{				 
					m_searchSync4Shift = false;
					int s = min(iSize, timeshift_wr - (timeshift_rd + offset));
					memcpy(pSampleBuffer, timeShiftBuffer + timeshift_rd + offset, s);
					timeshift_rd += (offset + s); 
					pSample->SetActualDataLength(s);
					return NOERROR;
				}
				offset += 188;
			}			
			pSample->SetActualDataLength(0);
			return NOERROR;			
		} 
		else 
		{
			int x = max(iSize, iSize * 2);
			memcpy(pSampleBuffer, timeShiftBuffer + timeshift_rd, x);
			timeshift_rd = (timeshift_rd + x) % m_timshiftBufferSize;
			pSample->SetActualDataLength(x);
		}		
	} 
	 
	if (m_pause_ts == true)
	{
		pSample->SetActualDataLength(0);		
	}
	  
    return NOERROR;

} // Transform


//
// CheckInputType
//
// Override CTransformFilter method.
// Part of the Connect process.
// Ensure that we do not get connected to formats that we can't handle.
// We only work for wave audio, 8 or 16 bit, uncompressed.
//
HRESULT CBauotechTimeshiftTransform::CheckInputType(const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);

	return NOERROR;

    DisplayType(TEXT("CheckInputType"), pmt);

    WAVEFORMATEX *pwfx = (WAVEFORMATEX *) pmt->pbFormat;

    // Reject non-Audio types.
    //
    if (pmt->majortype != MEDIATYPE_Audio) {
        return VFW_E_TYPE_NOT_ACCEPTED;
    }

    // Reject invalid format blocks
    //
    if (pmt->formattype != FORMAT_WaveFormatEx)
        return VFW_E_TYPE_NOT_ACCEPTED;

    // Reject compressed audio
    //
    if (pwfx->wFormatTag != WAVE_FORMAT_PCM) {
        return VFW_E_TYPE_NOT_ACCEPTED;
    }

    // Accept only 8 or 16 bit
    //
    if (pwfx->wBitsPerSample!=8 && pwfx->wBitsPerSample!=16) {
        return VFW_E_TYPE_NOT_ACCEPTED;
    }

    return NOERROR;

} // CheckInputType


//
// SetMediaType
//
// Override CTransformFilter method.
// Called when a connection attempt has succeeded. If the output pin
// is being connected and the input pin's media type does not agree then we
// reconnect the input (thus allowing its media type to change,) and vice versa.
//
HRESULT CBauotechTimeshiftTransform::SetMediaType(PIN_DIRECTION direction,const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);
    //DbgFunc("SetMediaType");


	return NOERROR;
 

} // SetMediaType



STDMETHODIMP CBauotechTimeshiftTransform::GoToLive()
{
	m_searchSync4Live = true;
	m_goLive = true;
	return S_OK;

}
STDMETHODIMP CBauotechTimeshiftTransform::SeekInTime(uint64_t ts_rd)
{
	m_goLive = false;
	m_searchSync4Shift = true;
	  
	timeshift_rd = ts_rd;
	return S_OK;
}

STDMETHODIMP CBauotechTimeshiftTransform::PauseTS()
{
	m_pause_ts = true;
	return S_OK;
}

STDMETHODIMP CBauotechTimeshiftTransform::ResumeTS()
{
	if (m_pause_ts == true)
	{
		m_searchSync4Shift = true;
		m_pause_ts = false;
	}
	return S_OK;
}
 
STDMETHODIMP CBauotechTimeshiftTransform::SetTimeShiftBuffer(uint64_t bufferSize)
{
	m_timshiftBufferSize = bufferSize;
	return S_OK;
}

STDMETHODIMP CBauotechTimeshiftTransform::SetTimeShiftCallback(TimeShiftCallback p)
{

	pTimeShiftCallback = p;
	return S_OK;
}

STDMETHODIMP CBauotechTimeshiftTransform::SetTimeDiffToLog(int difftime)
{
	m_timeDiffToLog = difftime;
	return S_OK;
}


uint64_t CBauotechTimeshiftTransform::GetFifoSize(uint64_t rd)
{
	if (timeshift_wr == rd)
		return 0;
	if (timeshift_wr > rd)
		return timeshift_wr - rd;

	return (m_timshiftBufferSize - rd) + timeshift_wr;
}

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

STDAPI DllRegisterServer()
{
  return AMovieDllRegisterServer2( TRUE );
}


STDAPI DllUnregisterServer()
{
  return AMovieDllRegisterServer2( FALSE );
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

#pragma warning(disable: 4514) // "unreferenced inline function has been removed"



