//------------------------------------------------------------------------------
// File: IGargle.h
//
// Desc: DirectShow sample code - custom interface that allows the user
//       to adjust the modulation rate.  It defines the interface between
//       the user interface component (the property sheet) and the filter
//       itself.  This interface is exported by the code in Gargle.cpp and
//       is used by the code in GargProp.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __IGARGLE__
#define __IGARGLE__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

 
 
// {704E088B-BB2A-4A5C-B79D-659EAF2504B4}
DEFINE_GUID(IID_IBaoutechTimeShiftTransform,
	0x704e088b, 0xbb2a, 0x4a5c, 0xb7, 0x9d, 0x65, 0x9e, 0xaf, 0x25, 0x4, 0xb4);



typedef void(__stdcall * TimeShiftCallback)(uint64_t loaction, uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec);

 
DECLARE_INTERFACE_(IBauotechTimeshiftTransform, IUnknown) {

	STDMETHOD(GoToLive)() PURE;
	STDMETHOD(SeekInTime)(uint64_t ts_rd) PURE;
	STDMETHOD(SetTimeShiftBuffer)(uint64_t bufferSize) PURE;
	STDMETHOD(SetTimeShiftCallback)(TimeShiftCallback p) PURE;
	STDMETHOD(SetTimeDiffToLog)(int difftime) PURE;
	STDMETHOD(PauseTS)() PURE;
	STDMETHOD(ResumeTS)()PURE;

};


#ifdef __cplusplus
}
#endif

#endif // __IGARGLE__
