//------------------------------------------------------------------------------
// File: GargUIDs.h
//
// Desc: DirectShow sample code - definition of CLSIDs.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __GARGUIDS__
#define __GARGUIDS__

#ifdef __cplusplus
extern "C" {
#endif

  
// {F2252BCE-D9B6-403D-8054-5520A18C3DBF}
DEFINE_GUID(CLSID_BauotechTimeShiftTransform,
	0xf2252bce, 0xd9b6, 0x403d, 0x80, 0x54, 0x55, 0x20, 0xa1, 0x8c, 0x3d, 0xbf);

  


#ifdef __cplusplus
}
#endif

#endif // __GARGUIDS__
