#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>

 
// {2C3990CE-F3BF-414C-B937-B1F05CC1DC67}
static const GUID IID_IBauotechNoSourceCallback =
{ 0x2c3990ce, 0xf3bf, 0x414c, { 0xb9, 0x37, 0xb1, 0xf0, 0x5c, 0xc1, 0xdc, 0x67 } };



typedef void (CALLBACK *NoSourceCallback)(int nosource);


DECLARE_INTERFACE_(IBauotechNoSourceCallback, IUnknown)
{		 
	STDMETHOD(SetNoSourceCallback)(NoSourceCallback noSourceCallback) PURE;
};