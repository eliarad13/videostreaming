#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include "myInterface.h"
#include "BauotechNoSourceCallbackuids.h"
#include "BauotechNoSourceCallback.h"

NoSourceCallback m_pProgressCallback = nullptr;
bool m_running = false;
uint32_t m_lastCounter = 0;
uint32_t m_counter = 0;

// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
	&CLSID_BauotechNoSourceCallback,                // Filter CLSID
    L"Bauotech No Source Callback",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
	L"Bauotech No Source Callback", &CLSID_BauotechNoSourceCallback, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;


// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
						 CBaseFilter(NAME("CDumpFilter"), pUnk, pLock, CLSID_BauotechNoSourceCallback),
    m_pDump(pDump)
{
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}
 
//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
	 
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	m_running = false;
    
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);
	 

    return CBaseFilter::Pause();
}
 

DWORD WINAPI myThreadFunction(LPVOID lpParam) 
{
	while (m_running == true)
	{
		Sleep(1000);
		if (m_running == false)
		{
			return 0;
		}
		if (m_lastCounter == m_counter)
		{
			if (m_pProgressCallback != nullptr)
			{
				m_pProgressCallback(1);
			}
		}
		m_lastCounter = m_counter;
	}
 
	return 0;
}
 
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	m_running = true;
	m_lastCounter = 0;
	m_counter = 0;
    CAutoLock cObjectLock(m_pLock);
	   
	// Create a thread and execute the function
	myThread = CreateThread(
		NULL,                   // default security attributes
		0,                      // use default stack size
		myThreadFunction,       // thread function
		NULL,                   // argument to thread function
		0,                      // use default creation flags
		NULL                    // thread identifier
		);

	if (myThread == NULL)
	{	 
		return 1;
	}
  
    return CBaseFilter::Run(tStart);
}


//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)	
{
}


HRESULT
CDumpInputPin::SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt)
{
	HRESULT hr = S_OK;
	  
	return hr;
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *pmt)
{
	return S_OK;
}

 
HRESULT CDumpInputPin::BreakConnect()
{

    return CRenderedInputPin::BreakConnect();
}
 
STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
    CheckPointer(pSample,E_POINTER);

	m_counter++;
	
	return S_OK;
}


//
//  CDump class
//
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
    CUnknown(NAME("CDump"), pUnk),
    m_pFilter(NULL),
    m_pPin(NULL)
{
    ASSERT(phr);
	
	
	pProgressCallback = NULL;


    m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
    if (m_pFilter == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

    m_pPin = new CDumpInputPin(this,GetOwner(),
                               m_pFilter,
                               &m_Lock,
                               &m_ReceiveLock,
                               phr);
    if (m_pPin == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }
}
  
CDump::~CDump()
{
  
    delete m_pPin;
    delete m_pFilter;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance
 


STDMETHODIMP CDump::SetNoSourceCallback(NoSourceCallback progressCallback)
{
	pProgressCallback = progressCallback;
	m_pProgressCallback = progressCallback;
	return S_OK;
}

//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);

    // Do we have this interface

	if (riid == IID_IBauotechNoSourceCallback) {
		return GetInterface((IBauotechNoSourceCallback *) this, ppv);
	}
	if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    
    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

} // NonDelegatingQueryInterface
 
   
//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

