#pragma once
#include <stdint.h> 
#include "mpegts.hpp"
#include <string>
#include <iostream>
#include <fstream>
#include <list>
#include <WinSock2.h>
#include <sys\timeb.h>
#include <memory.h>
#include <ws2ipdef.h>


using namespace std;

typedef void(__stdcall * DVRStreamerCallback)(int streamerId, char *fileName, long fileSize, double AvgBitrate, double fileDuration);

class DVRStreamer
{

public:

	DVRStreamer();
	~DVRStreamer();

	void Close();
	bool AddClient(int clientId, char *strAddr, int port, bool multicast, int multicastTTL);
	bool Stop();
	void Init(int streamerId);
	bool Start(const char * fileName,
		uint64_t *totalFilesSize,
		double startPCR,
		int *numFiles);
	void SetDVRStreamerCallback(DVRStreamerCallback p);




private:
	DVRStreamerCallback  pDVRStreamerCallback;
	bool InitUDPServer(int clientId, char *strAddr, int port, bool multicast, int multicastTTL, bool asServer);
	bool ReadChunk(uint8_t *buffer);
	uint64_t GetFifoFullSize();
	uint64_t GetFifoEmptySize();
	int diffTime2();
	int diffTime(double startTime);
	void TakeTimeStart2();
	void TakeTimeStart();
	bool LoadFile(const char *fileName, int *numFiles);
	BOOL FileExists(char* szPath);
	uint64_t GetFileSize(string fileName);
	string getPathName(const string& s);
	int m_streamerId;

	void ParseData();
	void SendToClient();



	char msgbuf[100];
	uint8_t NullPacketBuffer[188];
	int pcrDirToReset = 6000;
	HANDLE thread1 = NULL;
	HANDLE thread2 = NULL;
	CRITICAL_SECTION CriticalSection;
	uint64_t  totalFileSize = 0;
	uint64_t  totalFileSize2 = 0;
	uint64_t  totalFileSizeRead = 0;
	DWORD WINAPI SendToClient(void* data);
	uint64_t totalBytesSent = 0;
	double actualAvgBitRate = 0;
	uint64_t index = 0;
	char msgbuf2[200];
	double avgBitRate = 0;
	double m_startPCR = 0;
	double m_lastPCR = 0;
	double m_startDTS = 0;
	double m_startPTS = 0;
	FILE *handle = NULL;
	uint8_t *sendBuffer;
	double *avgBitRateBuffer;
	int avgDataIndex_wr = 0;
	int avgDataIndex_rd = 0;
	uint64_t *totalBytesToSendBuffer;


	uint64_t fifo_write = 0;
	uint64_t fifo_read = 0;
	util::mpegts_parser tsParser;
	uint64_t fifosize = 188 * 1000000;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;
	uint64_t m_llSize;
	list<string> m_dvrList;
	list<uint64_t> m_dvrSize;
	list<double> m_dvrFileDuration;
	list<double> m_dvrFileAvgBitrate;
	list<long> m_dvrFileSize;
	uint64_t m_totalLeftSize = 0;
	int64_t m_curFileIndex = -1;
	bool m_running = false;
	string m_workingFileName;
	bool m_sendAsServer = false;
	#define MAX_CONNECTIONS 10
	SOCKET m_server[MAX_CONNECTIONS];
	struct timeb start;
	struct timeb start2;
	DWORD WINAPI ParseData(void* data);
	double GetFileDuration(char *fileName, long *fileSize);
	const int MAX_AVG_RATE_BUFFER = 1000000;

};