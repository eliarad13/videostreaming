#include "stdafx.h"
#include "BauotechDVRStreamerApi.h"
#include"BauotechDVRStreamer.h"

#define MAX_STREAMERS 100
DVRStreamer *Streamer[MAX_STREAMERS];

BSTREAMER_API bool BSTREAMER_Stop(int streamerId)
{
	if (Streamer[streamerId] == NULL)
		return false;
	return Streamer[streamerId]->Stop();

}

BSTREAMER_API bool BSTREAMER_AddClient(int streamerId, int clientId, char *strAddr, int port, bool multicast, int multicastTTL)
{
	if (Streamer[streamerId] == NULL)
		return false;
	return Streamer[streamerId]->AddClient(clientId, strAddr, port, multicast, multicastTTL);
}

BSTREAMER_API void BSTREAMER_Close(int streamerId)
{

	if (Streamer[streamerId] != NULL)
		Streamer[streamerId]->Close();

}

BSTREAMER_API void BSTREAMER_SetDVRStreamerCallback(int streamerId, DVRStreamerCallback p)
{
	if (Streamer[streamerId] != NULL)
		Streamer[streamerId]->SetDVRStreamerCallback(p);
}

BSTREAMER_API void BSTREAMER_CloseAll()
{
	for (int i = 0; i < MAX_STREAMERS; i++)
	{
		if (Streamer[i] != NULL)
		{
			Streamer[i]->Stop();
			Streamer[i]->Close();
		}
	}

}

BSTREAMER_API void BSTREAMER_Init(int streamerId)
{
	 

	if (Streamer[streamerId] == NULL)
	{
		Streamer[streamerId] = new DVRStreamer();
	}
	Streamer[streamerId]->Init(streamerId);
}

BSTREAMER_API bool BSTREAMER_Start(int streamerId, const char * fileName,
	uint64_t *totalFilesSize,
	double startPCR,
	int *numFiles)
{
	if (Streamer[streamerId] == NULL)
		return false;

	return Streamer[streamerId]->Start(fileName,
		totalFilesSize,
		startPCR,
		numFiles);

}