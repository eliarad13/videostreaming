#pragma once
#include <stdint.h> 

#define BSTREAMER_API __declspec(dllexport)

typedef void(__stdcall * DVRStreamerCallback)(int streamerId, char *fileName, long fileSize, double AvgBitrate, double fileDuration);


extern "C"
{

	BSTREAMER_API void BSTREAMER_SetDVRStreamerCallback(int streamerId, DVRStreamerCallback p);

	BSTREAMER_API bool BSTREAMER_Start(int streamerId, const char * fileName,
		uint64_t *totalFilesSize,
		double startPCR,
		int *numFiles);

	BSTREAMER_API void BSTREAMER_CloseAll();
	BSTREAMER_API void BSTREAMER_Close(int streamerId);
	BSTREAMER_API bool BSTREAMER_Stop(int streamerId);
	BSTREAMER_API void BSTREAMER_Init(int streamerId);
	BSTREAMER_API bool BSTREAMER_AddClient(int streamerId, int clientId, char *strAddr, int port, bool multicast, int multicastTTL);

}