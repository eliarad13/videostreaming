// BauotechDVRStreamer.cpp : Defines the exported functions for the DLL application.
//

#include "stdafx.h"
#include "BauotechDVRStreamer.h"
#include <thread>
#include <memory>

using namespace std;

DVRStreamer::DVRStreamer()
{

}
DVRStreamer::~DVRStreamer()
{

}

bool DVRStreamer::InitUDPServer(int clientId, char *strAddr, int port,  bool multicast, int multicastTTL , bool asServer)
{
	 
	struct sockaddr_in server, si_other;
	int slen, recv_len;

	WSADATA wsa;
	m_sendAsServer = asServer;
	slen = sizeof(si_other);


	if (WSAStartup(MAKEWORD(2, 2), &wsa) != 0)
	{
		return false;
	}


	if ((m_server[clientId] = ::socket(AF_INET, SOCK_DGRAM, 0)) == INVALID_SOCKET)
	{
		return false;
	}

	//Prepare the sockaddr_in structure
	server.sin_family = AF_INET;
	DWORD ip = inet_addr(strAddr);
	server.sin_addr.s_addr = ip;
	server.sin_port = htons(port);

	if (asServer == true)
	{
		//Bind
		if (::bind(m_server[clientId], (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
		{
			return false;
		}
		//pserverThread = std::make_shared<std::thread>(std::bind(&TSDemux::ServerNagtation, this));
	}
	else
	{
		if (multicast)
		{

		 
			if (setsockopt(m_server[clientId], IPPROTO_IP, IP_MULTICAST_TTL, (const char *)&multicastTTL,
				sizeof(multicastTTL)) < 0)
			{
				return -9;
			}

			IP_MREQ mreq;
			mreq.imr_multiaddr.s_addr = inet_addr(strAddr);
			mreq.imr_interface.s_addr = htonl(INADDR_ANY);
			if (setsockopt(m_server[clientId], IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
			{

				return -5;
			}
		}
		 
		  
		if (::connect(m_server[clientId], (struct sockaddr *)&server, sizeof(server)) == SOCKET_ERROR)
		{
			return -11;
		}
	}

	return true;
}

string DVRStreamer::getPathName(const string& s)
{

	char sep = '/';

#ifdef _WIN32
	sep = '\\';
#endif

	size_t i = s.rfind(sep, s.length());
	if (i != string::npos) {
		return(s.substr(0, i));
	}

	return("");
}

uint64_t DVRStreamer::GetFileSize(string fileName)
{
	ifstream mySource;
	mySource.open(fileName, ios_base::binary);
	mySource.seekg(0, ios_base::end);
	uint64_t size = mySource.tellg();
	mySource.close();
	return size;
}

BOOL DVRStreamer::FileExists(char* szPath)
{
	DWORD dwAttrib = GetFileAttributesA(szPath);

	return (dwAttrib != INVALID_FILE_ATTRIBUTES &&
		!(dwAttrib & FILE_ATTRIBUTE_DIRECTORY));
}

bool DVRStreamer::LoadFile(const char *fileName, int *numFiles)
{
	
	DWORD dwBytesRead;
  
	*numFiles = 0;
	m_llSize = 0;
	handle = fopen(fileName, "r+t");
	if (handle == NULL)
		return false;

	m_dvrSize.clear();
	m_dvrList.clear();
	m_dvrFileDuration.clear();
	m_dvrFileSize.clear();
	m_dvrFileAvgBitrate.clear();

	string fullFileName;
	string path = getPathName(fileName);
	char line[250];
	while (true)
	{
		char *p = fgets(line, 250, handle);
		if (p == NULL)
			break;
		p = strchr(line, '\n');
		if (p != NULL)
			*p = 0;
		fullFileName = line;
		if (FileExists((char *)fullFileName.c_str()) == false)
			continue;

		m_dvrList.push_back(fullFileName);
		uint64_t sizeOfFile = GetFileSize(fullFileName);
		m_llSize += sizeOfFile;
		m_dvrSize.push_back(sizeOfFile);		
		long fileSize;
		double fileDuration = GetFileDuration((char *)fullFileName.c_str(), &fileSize);
		m_dvrFileDuration.push_back(fileDuration);
		m_dvrFileSize.push_back(fileSize);
		
		double fileAvgBitrate = fileSize * 8 / (fileDuration / 1000.0);
		m_dvrFileAvgBitrate.push_back(fileSize);
		char msgbuf[100];
		sprintf(msgbuf, "fileSize %f\n", fileAvgBitrate);
		OutputDebugStringA(msgbuf);

		if (pDVRStreamerCallback != NULL)
		{
			pDVRStreamerCallback(m_streamerId, (char *)fullFileName.c_str(), fileSize, fileAvgBitrate, fileDuration);
		}
		 
	}

	
	if (m_dvrList.size() == 0)
	{
		return false;
	}
	*numFiles = m_dvrList.size();
	m_totalLeftSize = m_llSize;

	fclose(handle);
	handle = NULL;	 
	return TRUE;
}

bool DVRStreamer::Stop()
{
	m_running = false;

	if (thread1 != NULL)
		WaitForSingleObject(thread1, INFINITE);
	if (thread2 != NULL)
		WaitForSingleObject(thread2, INFINITE);

	thread1 = NULL;
	thread2 = NULL;


	for (int i = 0; i < MAX_CONNECTIONS; i++)
	{
		if (m_server[i] != -1)
		{
			closesocket(m_server[i]);
			m_server[i] = -1;
		}
	}
	return true;
}



bool DVRStreamer::AddClient(int clientId, char *strAddr, int port, bool multicast, int multicastTTL)
{

	if (m_server[clientId] == -1)
	{
		return InitUDPServer(clientId, strAddr, port, multicast, multicastTTL, false);
	}
	return false;
}



void DVRStreamer::Close()
{
	if (totalBytesToSendBuffer != NULL)
	{
		delete totalBytesToSendBuffer;
		totalBytesToSendBuffer = NULL;
	}

	if (avgBitRateBuffer != NULL)
	{
		delete avgBitRateBuffer;
		avgBitRateBuffer = NULL;
	}
	if (sendBuffer != NULL)
	{
		free(sendBuffer);
		sendBuffer = NULL;
	}
	DeleteCriticalSection(&CriticalSection);
}
 
void DVRStreamer::Init(int streamerId)
{
	m_streamerId = streamerId;
	for (int i = 0; i < MAX_CONNECTIONS; i++)
	{
		if (m_server[i] != -1)
		{
			m_server[i] = -1;
		}
	}
}
 

bool DVRStreamer::Start(const char * fileName,
	uint64_t *totalFilesSize,
	double startPCR,
	int *numFiles)
{
	*numFiles = 0;
	if (m_running == true)
		return false;

	InitializeCriticalSectionAndSpinCount(&CriticalSection, 0x00000400);

	if (sendBuffer == NULL)
		sendBuffer = (uint8_t *)malloc(fifosize);
	if (avgBitRateBuffer == NULL)
		avgBitRateBuffer = new double[MAX_AVG_RATE_BUFFER];
	if (totalBytesToSendBuffer == NULL)
		totalBytesToSendBuffer = new uint64_t[MAX_AVG_RATE_BUFFER];

	for (int i = 0; i < MAX_AVG_RATE_BUFFER; i++)
	{
		avgBitRateBuffer[i] = -1;
	}
	avgDataIndex_rd = 0;
	avgDataIndex_wr = 0;

	fifo_write = 0;
	fifo_read = 0;
	m_startPCR = 0;
	m_startDTS = 0;
	m_startPTS = 0;

	totalFileSize = 0;
	totalFileSizeRead = 0;

	m_curFileIndex = -1;
	*totalFilesSize = 0;
	if (LoadFile(fileName, numFiles) == false)
		return false;
	m_startPCR = 0;
	m_running = true;

	thread thread1 = std::thread([this] { ParseData(); });
	thread1.detach();

	thread thread2 = std::thread([this] { SendToClient(); });
	thread2.detach();
	 
 

	return true;
}

void DVRStreamer::TakeTimeStart()
{
	int i = 0;
	ftime(&start);
}

void DVRStreamer::TakeTimeStart2()
{
	int i = 0;
	ftime(&start2);
}

int DVRStreamer::diffTime(double startTime)
{
	struct timeb end;
	int diff;
	ftime(&end);
	diff = (int)(1000.0 * (end.time - start.time) + (end.millitm - start.millitm));
	diff += startTime;

	//printf("\nOperation took %u milliseconds\n", diff);
	return diff;
}

int DVRStreamer::diffTime2()
{
	struct timeb end;
	int diff;
	ftime(&end);
	diff = (int)(1000.0 * (end.time - start2.time) + (end.millitm - start2.millitm));

	return diff;
}

uint64_t DVRStreamer::GetFifoEmptySize()
{
	EnterCriticalSection(&CriticalSection);
	 
	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return fifosize;
	}

	if (fifo_write > fifo_read)
	{
		int size = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return fifosize - size;
	}

	int size = (fifosize - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return fifosize - size;
}

uint64_t DVRStreamer::GetFifoFullSize()
{
	EnterCriticalSection(&CriticalSection);

	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return 0;
	}

	if (fifo_write > fifo_read)
	{
		int size = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return size;
	}

	int size = (fifosize - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return size;
}

double DVRStreamer::GetFileDuration(char *fileName, long *fileSize)
{
	uint8_t buffer[188];
	util::mpegts_info info;
	FILE *handle = fopen(fileName, "r+b");

	double startPCR = 0;
	while (true)
	{
		if (fread(buffer, 1, 188, handle) != 188)
			return 0;

		auto suc = tsParser.do_parser(buffer, info);
		if (info.pcr_ != -1)
		{
			startPCR = info.pcr_;
			break;
		}
	}

	fseek(handle, 0, SEEK_END);
	*fileSize = ftell(handle);
	fseek(handle, *fileSize - 188 * 100, SEEK_SET);

	double lastPCR = 0;
	while (true)
	{
		if (fread(buffer, 1, 188, handle) != 188)
			break;

		auto suc = tsParser.do_parser(buffer, info);
		if (info.pcr_ != -1)
		{
			lastPCR = info.pcr_;
		}	
	}

	double fileDuration = lastPCR - startPCR;

	fclose(handle);
	handle = NULL;
	return fileDuration;

}
 
void DVRStreamer::ParseData()
{
	double OCRVal1;
	double OCRVal2;
	int pcrState = 0;
	double bitRate = 0;
	util::mpegts_info info;
	#define MAX_CHUNK_SIZE 188 * 7
	uint64_t totalBytesToSend = 0;
	uint8_t buffer[MAX_CHUNK_SIZE];
	uint64_t totalBytesSent4Pick = 0;
	uint64_t totalBytesSentBetweenPCR = 0;
	double pickBitRate = 0;
	double diff4pick = 0;
	double pcrDiff = 0;
	TakeTimeStart2();
	//SendEmptyPacket();
	
	bool firstIFrame = false;
	bool fn = false;

	
	while (m_running)
	{
		int index = 0;
		for (index = 0; index < MAX_CHUNK_SIZE; )
		{
			if (ReadChunk(buffer + index) == false)
			{

				double diff = (m_lastPCR - m_startPCR) / 1000.0;
				avgBitRate = 8 * (totalBytesToSend / diff);

				avgBitRateBuffer[avgDataIndex_wr] = avgBitRate;
				totalBytesToSendBuffer[avgDataIndex_wr] = totalBytesToSend;
				avgDataIndex_wr = (avgDataIndex_wr + 1) % MAX_AVG_RATE_BUFFER;

				memcpy((uint8_t *)&sendBuffer[fifo_write], buffer, index);
				fifo_write = (fifo_write + index) % fifosize;

				printf("totalFileSize: %d\n", totalFileSize);
				return;
			}
			info.pcr_ = 0;
			auto suc = tsParser.do_parser(buffer + index, info);
			  
#if 0 
			if (info.pid_ > 8000 && fn == false)
			{
				memcpy(NullPacketBuffer, buffer + index, 188);
				fn = true;
			} 
#endif 
#if 0 
			if (info.pts_ > 0)
			{
				sprintf(msgbuf, "DTS %f\n", info.dts_);
				OutputDebugStringA(msgbuf);
			}
#endif 

			if (info.pcr_ != -1)
			{
				m_lastPCR = info.pcr_;
				if (m_startPCR == 0)
				{
					m_startPCR = info.pcr_;
					totalBytesToSend += 188;
					index += 188;
					totalFileSize += 188;
					continue;
				}
				else if ((info.pcr_ - m_startPCR) > 5000)
				{
					double diff = (info.pcr_ - m_startPCR) / 1000.0;
					avgBitRate = 8 * (totalBytesToSend / diff);

					avgBitRateBuffer[avgDataIndex_wr] = avgBitRate;
					totalBytesToSendBuffer[avgDataIndex_wr] = totalBytesToSend;
					avgDataIndex_wr = (avgDataIndex_wr + 1) % MAX_AVG_RATE_BUFFER;

					//sprintf(msgbuf, "avgBitRate %f  byte to send: %ld\n", avgBitRate, totalBytesToSend);
					//OutputDebugStringA(msgbuf);

					m_startPCR = info.pcr_;
					totalBytesToSend = 0;
 
				}
				else
				{
				//	sprintf(msgbuf, "info.pcr_: %f m_startPCR: %f\n", info.pcr_, m_startPCR);
				//	OutputDebugStringA(msgbuf);

					if (info.pcr_ > 0 && info.pcr_ < m_startPCR) {
					//	addition = m_startPCR;
						m_startPCR = 1;
					}
					totalBytesToSend += 188;
					index += 188;
					totalFileSize += 188;
					continue;
				}
				//sprintf(msgbuf, "PCR %f\n", info.pcr_ - m_startPCR);
				//OutputDebugStringA(msgbuf);				
			}
			index += 188;
			totalFileSize += 188;
			totalBytesToSend += 188;
		}
		int res;

		int fsize;
		while ((fsize = GetFifoEmptySize()) < index)
		{
			Sleep(1);
		}
		Sleep(0);
  	 
		memcpy((uint8_t *)&sendBuffer[fifo_write], buffer, index);
		fifo_write = (fifo_write + index) % fifosize;		 
		  
	}
	return;
}


void DVRStreamer::SendToClient()
{
	#define CHUNK_TO_SEND 1316
	uint64_t tosendbyte = 0;

	int chunkToSend;
	while (m_running)
	{ 
		while (((chunkToSend = GetFifoFullSize()) < CHUNK_TO_SEND) || (avgBitRateBuffer[avgDataIndex_rd] == -1))
		{
			if (totalFileSize > 0 && (totalFileSizeRead == totalFileSize))
			{
				return;// finished
			}
			 
			if (totalFileSize2 < CHUNK_TO_SEND && totalFileSize2 > 0)
			{
				chunkToSend = GetFifoFullSize();
				break;
			}
			Sleep(1);
		}
		chunkToSend = min(chunkToSend, CHUNK_TO_SEND);
		 
		double actualdiff = diffTime2() / 1000.0;
		double actualAvgBitRate = 8 * (tosendbyte / actualdiff);

		while (actualAvgBitRate > (avgBitRateBuffer[avgDataIndex_rd]))
		{			
			actualdiff = diffTime2() / 1000.0;
			actualAvgBitRate = 8 * (tosendbyte / actualdiff);
			continue;
		}
		 
		int res;
		if ((res = ::send(m_server[0], (const char *)sendBuffer + fifo_read, chunkToSend, 0)) == SOCKET_ERROR)
		{
			//sprintf(msgbuf, "Error send data %d\n", res);
			//OutputDebugStringA(msgbuf);
		}
		
		fifo_read = (fifo_read + chunkToSend) % fifosize;
		tosendbyte += chunkToSend;
		totalFileSizeRead += chunkToSend;
		totalFileSize2 -= chunkToSend;		 
		 
		if (tosendbyte > totalBytesToSendBuffer[avgDataIndex_rd])
		{

			TakeTimeStart2();

			sprintf(msgbuf2, "avgBitRateBuffer[%d] = %.4f   %ld  %f  wr: %d   rd :%d\n",
				avgDataIndex_rd , avgBitRateBuffer[avgDataIndex_rd], totalBytesToSendBuffer[avgDataIndex_rd],
				actualAvgBitRate , avgDataIndex_wr,  avgDataIndex_rd);
			OutputDebugStringA(msgbuf2);

			avgDataIndex_rd = (avgDataIndex_rd + 1) % MAX_AVG_RATE_BUFFER;
			tosendbyte = 0;

		}

	
	}

	return;
}

void DVRStreamer::SetDVRStreamerCallback(DVRStreamerCallback p)
{
	pDVRStreamerCallback = p;
}
bool DVRStreamer::ReadChunk(uint8_t *buffer)
{
	uint64_t sizeToRead = 188;
	int chunk;

	list<uint64_t>::iterator it = m_dvrSize.begin();
	while (sizeToRead > 0)
	{
		if (m_curFileIndex > -1)
			advance(it, m_curFileIndex);
		if (m_curFileIndex >= 0 && *it > 0)
		{
			chunk = min(sizeToRead, *it);
			*it -= chunk;

			fread(buffer, chunk, 1, handle);			 
			sizeToRead -= chunk;
		}
		else
		{
			if (m_dvrList.size() == 0)
			{
				return false;
			}
			if (handle != NULL)
				fclose(handle);
			handle = NULL;
			m_curFileIndex++;
			m_workingFileName = m_dvrList.front();
			m_dvrList.pop_front();
			it = m_dvrSize.begin();
			totalFileSize2 += m_dvrFileSize.front();
			m_dvrFileSize.pop_front();
			handle = fopen(m_workingFileName.c_str(), "r+b");
			if (handle == NULL)
				return false;
		}
	}
	return true;		 
}