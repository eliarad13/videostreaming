﻿using BauotechDVRStreamerCSLib;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static BauotechDVRStreamerCSLib.BauotechDVRStreamer;

namespace BauotechDVRStreamerApp
{
    public partial class Form1 : Form
    {
        BauotechDVRStreamer m_streamer = new BauotechDVRStreamer();
        public Form1()
        {
            InitializeComponent();

            string line= File.ReadAllText(txtDvrList.Text);
            textBox1.Text = line;
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            long totalFileSize = 0;

            m_streamer.Init();
            bool isMulticast;
            string [] p = txtIpAddress.Text.Split('.');
            if ((int.Parse(p[0]) >= 0xE0))
            {
                isMulticast = true;
            }
            else
            {
                isMulticast = false;
            }
            DVRStreamerCallback p1= new DVRStreamerCallback(DVRStreamerCallbackMsg);
            m_streamer.SetDVRStreamerCallback(p1);


            if (m_streamer.AddClient(0, 
                                    txtIpAddress.Text, 
                                    int.Parse(txtPort.Text) ,
                                    isMulticast, 
                                    int.Parse(txtTTL.Text))== false)
            {
                MessageBox.Show("Failed to add client to streamer");
                return;
            }

            //m_streamer.Start(@"D:\tempBauotechDVRStorage\2019_07_03_10_13_39\2019_07_03_10_13_43\dvrlist.txt", 
            if (m_streamer.Start(txtDvrList.Text,
                               ref totalFileSize, 
                               0,
                               out int numFiles) == false)
            {
                MessageBox.Show("Failed to start " + numFiles);
            }
        }
        void DVRStreamerCallbackMsg(int streamerId, string fileName, long fileSize, double AvgBitrate, double fileDuration)
        {
            listBox1.Items.Add(fileSize + " | " + AvgBitrate + "  | " + fileDuration + " | ");
        }
        private void btnStop_Click(object sender, EventArgs e)
        {
            m_streamer.Stop();
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_streamer.Stop();

            BauotechDVRStreamer.BSTREAMER_CloseAll();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            try
            {
                System.Diagnostics.Process.Start("d:\\dvrlist.txt");
            }
            catch (Exception err)
            {
                MessageBox.Show(err.Message);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            string line = File.ReadAllText(@"d:\dvrlist.txt");
            textBox1.Text = line;
        }
    }
}
