#pragma once 
#include <stdint.h>

typedef enum MbSeqSegmentType_t
{
	MB_SEQ_SEGMENT_TYPE_VIDEO = 0,
	MB_SEQ_SEGMENT_TYPE_TELEMETRY = 1,
	MB_SEQ_SEGMENT_TYPE_OTHER = 255
} MbSeqSegmentType;

typedef struct Size2i_t
{
	int height;
	int width;
} Size2i;

typedef enum MbVideoCodecType
{
	ABC22,
	BCCC222
} MbVideoCodecType;

typedef enum MbTelemetryCodecType
{
	ABC1,
	BCCC1
} MbTelemetryCodecType;

struct MbSeqHeader {

	uint32_t marker;
	uint16_t headerVersion;
	uint8_t sessionId[16];
	uint64_t timeUtcMicros;
	uint16_t sessionFileIndex;
	uint16_t issuerVersion;
	MbVideoCodecType videoCodecId;
	Size2i videoDimensions;
	uint8_t videoBitDepth;
	MbTelemetryCodecType telemetryCodecId;
	uint8_t padding[200];
};

typedef struct MbSegmentHeader_t
{
	uint32_t marker;
	MbSeqSegmentType segmentType;
	uint32_t segmentSizeBytes;
	uint64_t timeUtcMicros;
	uint8_t padding[40];
} MbSegmentHeader;

typedef struct MbVideoSegmentHeader_t
{
	uint16_t metadataSizeBytes;
	uint64_t dataSizeBytes;
} MbVideoSegmentHeader;

typedef struct MbTelemetrySegmentHeader_t
{
	uint16_t	descriptorSizeBytes;
	uint64_t	dataSizeBytes;
}MbTelemetrySegmentHeader;