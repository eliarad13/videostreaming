/*******************************************************************************#
#           guvcview              http://guvcview.berlios.de                    #
#                                                                               #
#           Paulo Assis <pj.assis@gmail.com>                                    #
#           Nobuhiro Iwamatsu <iwamatsu@nigauri.org>                            #
#                                                                               #
# This program is free software; you can redistribute it and/or modify          #
# it under the terms of the GNU General Public License as published by          #
# the Free Software Foundation; either version 2 of the License, or             #
# (at your option) any later version.                                           #
#                                                                               #
# This program is distributed in the hope that it will be useful,               #
# but WITHOUT ANY WARRANTY; without even the implied warranty of                #
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                 #
# GNU General Public License for more details.                                  #
#                                                                               #
# You should have received a copy of the GNU General Public License             #
# along with this program; if not, write to the Free Software                   #
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA      #
#                                                                               #
********************************************************************************/
 
#include <stdlib.h>
#include <string.h>
//#include "colorspaces.h"
#include <stdint.h>


// raw bayer functions 
// from libv4l bayer.c, (C) 2008 Hans de Goede <j.w.r.degoede@hhs.nl>
//Note: original bayer_to_bgr24 code from :
//  1394-Based Digital Camera Control Library
// 
//  Bayer pattern decoding functions
// 
//  Written by Damien Douxchamps and Frederic Devernay
static void convert_border_bayer_line_to_bgr24(uint8_t* bayer, uint8_t* adjacent_bayer,
	uint8_t *bgr, int width, bool start_with_green, bool blue_line)
{
	int t0, t1;

	if (start_with_green)
	{
		/* First pixel */
		if (blue_line)
		{
			*bgr++ = bayer[1];
			*bgr++ = bayer[0];
			*bgr++ = adjacent_bayer[0];
		}
		else
		{
			*bgr++ = adjacent_bayer[0];
			*bgr++ = bayer[0];
			*bgr++ = bayer[1];
		}
		/* Second pixel */
		t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
		t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = (uint8_t)t1;
		}
		else
		{
			*bgr++ = (uint8_t)t1;
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
		}
		bayer++;
		adjacent_bayer++;
		width -= 2;
	}
	else
	{
		/* First pixel */
		t0 = (bayer[1] + adjacent_bayer[0] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = bayer[0];
			*bgr++ = (uint8_t)t0;
			*bgr++ = adjacent_bayer[1];
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[0];
		}
		width--;
	}

	if (blue_line)
	{
		for (; width > 2; width -= 2)
		{
			t0 = (bayer[0] + bayer[2] + 1) >> 1;
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
			bayer++;
			adjacent_bayer++;

			t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
			t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = (uint8_t)t1;
			bayer++;
			adjacent_bayer++;
		}
	}
	else
	{
		for (; width > 2; width -= 2)
		{
			t0 = (bayer[0] + bayer[2] + 1) >> 1;
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
			bayer++;
			adjacent_bayer++;

			t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
			t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
			*bgr++ = (uint8_t)t1;
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			bayer++;
			adjacent_bayer++;
		}
	}

	if (width == 2)
	{
		/* Second to last pixel */
		t0 = (bayer[0] + bayer[2] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
		}
		/* Last pixel */
		t0 = (bayer[1] + adjacent_bayer[2] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = bayer[2];
			*bgr++ = (uint8_t)t0;
			*bgr++ = adjacent_bayer[1];
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[2];
		}
	}
	else
	{
		/* Last pixel */
		if (blue_line)
		{
			*bgr++ = bayer[0];
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = bayer[0];
		}
	}
}

/* From libdc1394, which on turn was based on OpenCV's Bayer decoding */
void bayer_to_rgbbgr24(uint8_t *bayer,
	uint8_t *bgr, int width, int height,
	bool start_with_green, bool blue_line)
{
	/* render the first line */
	convert_border_bayer_line_to_bgr24(bayer, bayer + width, bgr, width,
		start_with_green, blue_line);
	bgr += width * 3;

	/* reduce height by 2 because of the special case top/bottom line */
	for (height -= 2; height; height--)
	{
		int t0, t1;
		/* (width - 2) because of the border */
		uint8_t *bayerEnd = bayer + (width - 2);

		if (start_with_green)
		{
			/* OpenCV has a bug in the next line, which was
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1; */
			t0 = (bayer[1] + bayer[width * 2 + 1] + 1) >> 1;
			/* Write first pixel */
			t1 = (bayer[0] + bayer[width * 2] + bayer[width + 1] + 1) / 3;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width];
			}
			else
			{
				*bgr++ = bayer[width];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
			}

			/* Write second pixel */
			t1 = (bayer[width] + bayer[width + 2] + 1) >> 1;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
			}
			else
			{
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t0;
			}
			bayer++;
		}
		else
		{
			/* Write first pixel */
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width];
				*bgr++ = bayer[width + 1];
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = bayer[width];
				*bgr++ = (uint8_t)t0;
			}
		}

		if (blue_line)
		{
			for (; bayer <= bayerEnd - 2; bayer += 2)
			{
				t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
					bayer[width * 2 + 2] + 2) >> 2;
				t1 = (bayer[1] + bayer[width] +
					bayer[width + 2] + bayer[width * 2 + 1] +
					2) >> 2;
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];

				t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
				t1 = (bayer[width + 1] + bayer[width + 3] +
					1) >> 1;
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width + 2];
				*bgr++ = (uint8_t)t1;
			}
		}
		else
		{
			for (; bayer <= bayerEnd - 2; bayer += 2)
			{
				t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
					bayer[width * 2 + 2] + 2) >> 2;
				t1 = (bayer[1] + bayer[width] +
					bayer[width + 2] + bayer[width * 2 + 1] +
					2) >> 2;
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;

				t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
				t1 = (bayer[width + 1] + bayer[width + 3] +
					1) >> 1;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 2];
				*bgr++ = (uint8_t)t0;
			}
		}

		if (bayer < bayerEnd)
		{
			/* write second to last pixel */
			t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
				bayer[width * 2 + 2] + 2) >> 2;
			t1 = (bayer[1] + bayer[width] +
				bayer[width + 2] + bayer[width * 2 + 1] +
				2) >> 2;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
			}
			/* write last pixel */
			t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width + 2];
				*bgr++ = bayer[width + 1];
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = bayer[width + 2];
				*bgr++ = (uint8_t)t0;
			}
			bayer++;
		}
		else
		{
			/* write last pixel */
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1;
			t1 = (bayer[1] + bayer[width * 2 + 1] + bayer[width] + 1) / 3;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
			}
		}

		/* skip 2 border pixels */
		bayer += 2;

		blue_line = !blue_line;
		start_with_green = !start_with_green;
	}

	/* render the last line */
	convert_border_bayer_line_to_bgr24(bayer + width, bayer, bgr, width,
		!start_with_green, !blue_line);
}

/*convert bayer raw data to rgb24
* args:
*      pBay: pointer to buffer containing Raw bayer data data
*      pRGB24: pointer to buffer containing rgb24 data
*      width: picture width
*      height: picture height
*      pix_order: bayer pixel order (0=gb/rg   1=gr/bg  2=bg/gr  3=rg/bg)
*/
void
bayer_to_rgb24(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order)
{
	switch (pix_order)
	{
		//conversion functions are build for bgr, by switching b and r lines we get rgb
	case 0: /* gbgbgb... | rgrgrg... (V4L2_PIX_FMT_SGBRG8)*/
		bayer_to_rgbbgr24(pBay, pRGB24, width, height, true, false);
		break;

	case 1: /* grgrgr... | bgbgbg... (V4L2_PIX_FMT_SGRBG8)*/
		bayer_to_rgbbgr24(pBay, pRGB24, width, height, true, true);
		break;

	case 2: /* bgbgbg... | grgrgr... (V4L2_PIX_FMT_SBGGR8)*/
		bayer_to_rgbbgr24(pBay, pRGB24, width, height, false, false);
		break;

	case 3: /* rgrgrg... ! gbgbgb... (V4L2_PIX_FMT_SRGGB8)*/
		bayer_to_rgbbgr24(pBay, pRGB24, width, height, false, true);
		break;

	default: /* default is 0*/
		bayer_to_rgbbgr24(pBay, pRGB24, width, height, true, false);
		break;
	}
}



static void convert_border_bayer_line_to_bgr32(uint8_t* bayer, uint8_t* adjacent_bayer,
	uint8_t *bgr, int width, bool start_with_green, bool blue_line)
{
	int t0, t1;

	if (start_with_green)
	{
		/* First pixel */
		if (blue_line)
		{
			*bgr++ = bayer[1];
			*bgr++ = bayer[0];
			*bgr++ = adjacent_bayer[0];
			*bgr++ = 0;// alpha
		}
		else
		{
			*bgr++ = adjacent_bayer[0];
			*bgr++ = bayer[0];
			*bgr++ = bayer[1];
			*bgr++ = 0;// alpha
		}
		/* Second pixel */
		t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
		t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = (uint8_t)t1;
			*bgr++ = 0;// alpha
		}
		else
		{
			*bgr++ = (uint8_t)t1;
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			*bgr++ = 0;// alpha
		}
		bayer++;
		adjacent_bayer++;
		width -= 2;
	}
	else
	{
		/* First pixel */
		t0 = (bayer[1] + adjacent_bayer[0] + 1) >> 1;
		if (blue_line)
		{		
			*bgr++ = bayer[0];
			*bgr++ = (uint8_t)t0;
			*bgr++ = adjacent_bayer[1];			
			*bgr++ = 0;// alpha
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[0];			
			*bgr++ = 0;// alpha
		}
		width--;
	}

	if (blue_line)
	{
		for (; width > 2; width -= 2)
		{
			t0 = (bayer[0] + bayer[2] + 1) >> 1;
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
			*bgr++ = 0;// alpha
			
			bayer++;
			adjacent_bayer++;

			t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
			t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = (uint8_t)t1;
			*bgr++ = 0;// alpha
			
			bayer++;
			adjacent_bayer++;
		}
	}
	else
	{
		for (; width > 2; width -= 2)
		{
			t0 = (bayer[0] + bayer[2] + 1) >> 1;
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = 0;// alpha
			
			bayer++;
			adjacent_bayer++;

			t0 = (bayer[0] + bayer[2] + adjacent_bayer[1] + 1) / 3;
			t1 = (adjacent_bayer[0] + adjacent_bayer[2] + 1) >> 1;
			*bgr++ = (uint8_t)t1;
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			*bgr++ = 0;// alpha
			
			bayer++;
			adjacent_bayer++;
		}
	}

	if (width == 2)
	{
		/* Second to last pixel */
		t0 = (bayer[0] + bayer[2] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];		
			*bgr++ = 0;// alpha
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = (uint8_t)t0;			
			*bgr++ = 0;// alpha
		}
		/* Last pixel */
		t0 = (bayer[1] + adjacent_bayer[2] + 1) >> 1;
		if (blue_line)
		{
			*bgr++ = bayer[2];
			*bgr++ = (uint8_t)t0;
			*bgr++ = adjacent_bayer[1];		
			*bgr++ = 0;// alpha
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = (uint8_t)t0;
			*bgr++ = bayer[2];
			*bgr++ = 0;// alpha
		}
	}
	else
	{
		/* Last pixel */
		if (blue_line)
		{
			*bgr++ = bayer[0];
			*bgr++ = bayer[1];
			*bgr++ = adjacent_bayer[1];
			*bgr++ = 0;// alpha
		}
		else
		{
			*bgr++ = adjacent_bayer[1];
			*bgr++ = bayer[1];
			*bgr++ = bayer[0];
			*bgr++ = 0;// alpha
			
		}
	}
}

/* From libdc1394, which on turn was based on OpenCV's Bayer decoding */
void bayer_to_rgbbgr32(uint8_t *bayer,
	uint8_t *bgr, int width, int height,
	bool start_with_green, bool blue_line)
{
	/* render the first line */
	convert_border_bayer_line_to_bgr32(bayer, bayer + width, bgr, width,
		start_with_green, blue_line);
	bgr += width * 4;

	/* reduce height by 2 because of the special case top/bottom line */
	for (height -= 2; height; height--)
	{
		int t0, t1;
		/* (width - 2) because of the border */
		uint8_t *bayerEnd = bayer + (width - 2);

		if (start_with_green)
		{
			/* OpenCV has a bug in the next line, which was
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1; */
			t0 = (bayer[1] + bayer[width * 2 + 1] + 1) >> 1;
			/* Write first pixel */
			t1 = (bayer[0] + bayer[width * 2] + bayer[width + 1] + 1) / 3;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width];
				*bgr++ = 0;// alpha
			}
			else
			{			
				*bgr++ = bayer[width];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha
			}

			/* Write second pixel */
			t1 = (bayer[width] + bayer[width + 2] + 1) >> 1;
			if (blue_line)
			{				
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = 0;// alpha
			}
			else
			{
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha
			}
			bayer++;
		}
		else
		{
			/* Write first pixel */
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width];
				*bgr++ = bayer[width + 1];
				*bgr++ = 0;// alpha
			
			}
			else
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha
				*bgr++ = bayer[width + 1];
				*bgr++ = bayer[width];
			}
		}

		if (blue_line)
		{
			for (; bayer <= bayerEnd - 2; bayer += 2)
			{
				t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
					bayer[width * 2 + 2] + 2) >> 2;
				t1 = (bayer[1] + bayer[width] +
					bayer[width + 2] + bayer[width * 2 + 1] +
					2) >> 2;
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
				*bgr++ = 0;// alpha
				

				t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
				t1 = (bayer[width + 1] + bayer[width + 3] +
					1) >> 1;
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width + 2];
				*bgr++ = (uint8_t)t1;
				*bgr++ = 0;// alpha
			}
		}
		else
		{
			for (; bayer <= bayerEnd - 2; bayer += 2)
			{
				t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
					bayer[width * 2 + 2] + 2) >> 2;
				t1 = (bayer[1] + bayer[width] +
					bayer[width + 2] + bayer[width * 2 + 1] +
					2) >> 2;
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha
				

				t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
				t1 = (bayer[width + 1] + bayer[width + 3] +
					1) >> 1;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 2];
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha				
			}
		}

		if (bayer < bayerEnd)
		{
			/* write second to last pixel */
			t0 = (bayer[0] + bayer[2] + bayer[width * 2] +
				bayer[width * 2 + 2] + 2) >> 2;
			t1 = (bayer[1] + bayer[width] +
				bayer[width + 2] + bayer[width * 2 + 1] +
				2) >> 2;
			if (blue_line)
			{				
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
				*bgr++ = 0;// alpha	
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha
				
			}
			/* write last pixel */
			t0 = (bayer[2] + bayer[width * 2 + 2] + 1) >> 1;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = bayer[width + 2];
				*bgr++ = bayer[width + 1];
				*bgr++ = 0;// alpha
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = bayer[width + 2];
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha
			
			}
			bayer++;
		}
		else
		{
			/* write last pixel */
			t0 = (bayer[0] + bayer[width * 2] + 1) >> 1;
			t1 = (bayer[1] + bayer[width * 2 + 1] + bayer[width] + 1) / 3;
			if (blue_line)
			{
				*bgr++ = (uint8_t)t0;
				*bgr++ = (uint8_t)t1;
				*bgr++ = bayer[width + 1];
				*bgr++ = 0;// alpha	
			}
			else
			{
				*bgr++ = bayer[width + 1];
				*bgr++ = (uint8_t)t1;
				*bgr++ = (uint8_t)t0;
				*bgr++ = 0;// alpha	
			}
		}

		/* skip 2 border pixels */
		bayer += 2;

		blue_line = !blue_line;
		start_with_green = !start_with_green;
	}

	/* render the last line */
	convert_border_bayer_line_to_bgr32(bayer + width, bayer, bgr, width,
		!start_with_green, !blue_line);
}

void
bayer_to_rgb32(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order)
{
	switch (pix_order)
	{
		//conversion functions are build for bgr, by switching b and r lines we get rgb
	case 0: /* gbgbgb... | rgrgrg... (V4L2_PIX_FMT_SGBRG8)*/
		bayer_to_rgbbgr32(pBay, pRGB24, width, height, true, false);
		break;

	case 1: /* grgrgr... | bgbgbg... (V4L2_PIX_FMT_SGRBG8)*/
		bayer_to_rgbbgr32(pBay, pRGB24, width, height, true, true);
		break;

	case 2: /* bgbgbg... | grgrgr... (V4L2_PIX_FMT_SBGGR8)*/
		bayer_to_rgbbgr32(pBay, pRGB24, width, height, false, false);
		break;

	case 3: /* rgrgrg... ! gbgbgb... (V4L2_PIX_FMT_SRGGB8)*/
		bayer_to_rgbbgr32(pBay, pRGB24, width, height, false, true);
		break;

	default: /* default is 0*/
		bayer_to_rgbbgr32(pBay, pRGB24, width, height, true, false);
		break;
	}
}
