#pragma once
#include <stdint.h>

void bayer_to_rgb24(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order);


void bayer_to_rgbbgr24(uint8_t *bayer,uint8_t *bgr, int width, int height,bool start_with_green, bool blue_line);

void bayer_to_rgb32(uint8_t *pBay, uint8_t *pRGB24, int width, int height, int pix_order);

void bayer_to_rgbbgr32(uint8_t *bayer,
	uint8_t *bgr, int width, int height,
	bool start_with_green, bool blue_line);
