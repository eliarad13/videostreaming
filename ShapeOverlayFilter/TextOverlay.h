#pragma once

#include "streams.h"
#include "gdiplus.h"
#include <wchar.h>
#include <map>
#include "Overlay.h"
#include "ITextAdditor.h"

using namespace Gdiplus;
using namespace std;

class CTextOverlay : public CTransInPlaceFilter, public ITextAdditor
{
public:
	DECLARE_IUNKNOWN;

	CTextOverlay(LPUNKNOWN pUnk, HRESULT *phr);
	virtual ~CTextOverlay(void);

	virtual HRESULT CheckInputType(const CMediaType* mtIn);
	virtual HRESULT SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt);
	virtual HRESULT Transform(IMediaSample *pSample);

	static CUnknown *WINAPI CreateInstance(LPUNKNOWN pUnk, HRESULT *phr); 
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);


	STDMETHODIMP AddCircle(int id, int   x, int   y, int radios_w, int radios_h,  COLORREF color, int width);
	STDMETHODIMP AddTextOverlay(WCHAR* text, int id, int    left,
		int    top,
		int    right,
		int    bottom,
		COLORREF color, 
		float fontSize,
		FontStyle fontStyle);

	STDMETHODIMP AddTextOverlay2(WCHAR* text, int id, int    left,
		int    top,
		int    right,
		int    bottom,
		COLORREF color,
		float fontSize);

	STDMETHODIMP Clear(void);	
	STDMETHODIMP Visible(int id, bool visible);
	STDMETHODIMP AddLine(int id, int  x1, int   y1, int    x2, int    y2, COLORREF color, int width);

	STDMETHODIMP Close();


private:
	ULONG_PTR m_gdiplusToken;
	VIDEOINFOHEADER m_videoInfo;
	PixelFormat m_pixFmt;
	int m_stride;
	map<int, Overlay*> m_overlays;
};

