#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <winsock2.h>
#include <stdio.h>
#include <Windows.h>
#include <thread>
#include "TCPServer.h"
 
 
 
using namespace std;

bool m_filterAlive;
 
typedef enum GRAPHSTAT
{
	GRAPH_STOP,
	GRAPH_PAUSE,
	GRAPH_RUN,
} GRAPHSTAT;
 
char			   m_ipAddress[100];
int                m_port;

GRAPHSTAT  m_graphStat;

SOCKET  m_AllClients[MAX_CLIENTS];

SOCKET             ListenSocket = INVALID_SOCKET;

CTCPServer::~CTCPServer()
{
	m_filterAlive = false;
	m_running = false;

	if (ListenSocket != INVALID_SOCKET)
		closesocket(ListenSocket);
	ListenSocket = INVALID_SOCKET;
	 
	CloseAllClients();

	m_running = false;
	WSACleanup();

}

int m_isBind = 0;
  

void CloseAllClients()
{
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (m_AllClients[i] != INVALID_SOCKET)
		{
			//shutdown(ListenSocket, 0);
			closesocket(m_AllClients[i]);
			m_AllClients[i] = INVALID_SOCKET;
		}
	}
}

void CTCPServer::Init()
{
	m_filterAlive = true;
	ListenSocket = INVALID_SOCKET;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	// Initialize the critical section one time only.

	strcpy(m_ipAddress, "127.0.0.1");
	m_port = 8090;

	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		m_AllClients[i] = INVALID_SOCKET;
	}

	m_running = false;
	BufLength = RECEIVE_SIZE;
 
}
  

const wchar_t *CTCPServer::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

void LogMessage(const char *msg)
{

	FILE *handle = fopen("c:\\network.txt", "a+t");
	fprintf(handle , "%s\n" , msg);
	fclose(handle);
	 
}

void AddClient(int sid)
{
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (m_AllClients[i] == INVALID_SOCKET)
		{
			m_AllClients[i] = sid;
			break;
		}
	}
}
DWORD WINAPI AcceptThread(void* data)
{
	//LogMessage("Start Accepting..");

	// Accept a client socket
	int sid = accept(ListenSocket, NULL, NULL);
	if (sid == INVALID_SOCKET)
	{
		//printf("accept failed with error: %d\n", WSAGetLastError());
		//LogMessage("Failed Accepting..");
		if (ListenSocket != INVALID_SOCKET)
		{
			closesocket(ListenSocket);
			ListenSocket = INVALID_SOCKET;
		}
		if (m_filterAlive == true)
			Setup(true);
		return NULL;
	}
	AddClient(sid);
	//LogMessage("Accepted connection..");
	HANDLE thread = CreateThread(NULL, 0, AcceptThread, NULL, 0, NULL);
}

int Setup(bool allInterface)
{

	// Create a new socket to receive datagrams on.
	ListenSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ListenSocket == INVALID_SOCKET)
	{
		printf("Server: Error at socket(): %ld\n", WSAGetLastError());
		return -1;
	}

	// The IPv4 family
	SOCKADDR_IN        ReceiverAddr;

	ReceiverAddr.sin_family = AF_INET;
	ReceiverAddr.sin_port = htons(m_port);
	
	DWORD ip = inet_addr(m_ipAddress); 
	if (allInterface == false)
		ReceiverAddr.sin_addr.s_addr = htonl(ip);
	else 
		ReceiverAddr.sin_addr.s_addr = htonl(INADDR_ANY);
 
	if (m_isBind == 0)
	{
		int res = ::bind(ListenSocket, (SOCKADDR *)&ReceiverAddr, sizeof(ReceiverAddr));
		if (res == SOCKET_ERROR)
		{
			// Close the socket
			closesocket(ListenSocket);
			ListenSocket = INVALID_SOCKET;
			// Do the clean up
			// and exit with error
			return -1;
		}

		int iResult = listen(ListenSocket, SOMAXCONN);
		if (iResult == SOCKET_ERROR) {
			printf("listen failed with error: %d\n", WSAGetLastError());
			closesocket(ListenSocket);
			ListenSocket = INVALID_SOCKET;
			return 1;
		}

		char enable = 1;
		if (setsockopt(ListenSocket, SOL_SOCKET, SO_REUSEADDR, &enable, sizeof(int)) < 0)
		{

		}
		m_isBind = 1;
	}


	DWORD dwThreadId;
	HANDLE thread = CreateThread(NULL, 0, AcceptThread, NULL, 0, NULL);

	  
	 
	return 1;

}



void CTCPServer::Pause()
{

}
void CTCPServer::Stop()
{
	m_running = false;

	CloseAllClients();
}
void CTCPServer::Start()
{
	if (m_running == true)
		return;

}