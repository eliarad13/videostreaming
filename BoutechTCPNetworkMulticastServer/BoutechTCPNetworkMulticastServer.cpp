//------------------------------------------------------------------------------
// File: BoutechNetworkSender.cpp
//
// Desc: DirectShow sample code - implementation of a renderer that dumps
//       the samples it receives into a text file.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

  
 
#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include <stdint.h>
#include "BoutechNetworkSenderuids.h"
#include "BoutechTCPNetworkMulticastServer.h"
#include <thread>
#include "WinAES.h"
using namespace std;
#include <memory>

#define USE_AES 0
bool m_connecting = false;
void InitializeAES();
CDumpFilter  *pFilter = NULL;
WinAES aes;
WSADATA wsaData;
struct addrinfo *result = NULL;
byte *ciphertext = new byte[1024 * 1024];

int m_clientConnected = 0;
bool m_running = false;
enum NetworkType
{
	UDP,
	TCP
};

NetworkType  m_networkType = TCP;


// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
    &CLSID_BoutechNetworkSender,                // Filter CLSID
    L"Bauotech TCP Network Multicast",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
    L"Bauotech TCP Network Multicast", &CLSID_BoutechNetworkSender, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;

void LogMessage(char *msg)
{
	FILE *handle = fopen("c:\\network.txt", "a+t");
	fprintf(handle, "%s\n", msg);
	fclose(handle);
}

 


// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
    CBaseFilter(NAME("CDumpFilter"), pUnk, pLock, CLSID_BoutechNetworkSender),
    m_pDump(pDump)
{

	pFilter = this;
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}

 
int CDumpFilter::GetPinCount()
{
    return 1;
}
 
 
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	 
	m_running = false;
	m_connecting = false;

	m_pDump->m_tcpServer->Stop();
	    
	 
    return CBaseFilter::Stop();
}
 
//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);

    

    return CBaseFilter::Pause();
}
 
 
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	CAutoLock cObjectLock(m_pLock);
	 
	m_running = true;
	 
	 
    return CBaseFilter::Run(tStart);
}
 

//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{
	 
	
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}


//
// BreakConnect
//
// Break a connection
//
HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}


void InitializeAES()
{
	

	// WinAES aes( NULL, WinAES::THROW_EXCEPTION );

	byte key[WinAES::KEYSIZE_128] = { 0xF5, 0x13, 0x1E, 0xD8, 0x7F, 0xB3, 0x06, 0x94, 0x07, 0x7D, 0x85, 0xFD, 0xE0, 0x4C, 0x9C, 0xAC };
	byte iv[WinAES::BLOCKSIZE] = { 0xD8, 0xCD, 0xCF, 0x1E, 0xC2, 0x9E, 0xE8, 0x81, 0x98, 0x5C, 0xD3, 0x5B, 0x7A, 0xB1, 0xDA, 0x2A };

#if 0 
	aes.GenerateRandom(key, sizeof(key));
	aes.GenerateRandom(iv, sizeof(iv));

	FILE *fp;
	fp = fopen("c:\\aes_key.bin", "w");
	fwrite(key, 1, sizeof(key), fp);
	fclose(fp);
	fp = fopen("c:\\aes_iv.bin", "w");
	fwrite(iv, 1, sizeof(iv), fp);
	fclose(fp);
#endif 
	 
	try
	{
		// Oops - no iv
		// aes.SetKey( key, sizeof(key) );

		// Oops - no key
		// aes.SetIv( iv, sizeof(iv) );

		// Set the key and IV
		aes.SetKeyWithIv(key, sizeof(key), iv, sizeof(iv));

		// Done with key material - Microsoft owns it now...
		ZeroMemory(key, sizeof(key));
		 
		
	}
	catch (const WinAESException& e)
	{
		 
	}



}

extern SOCKET  m_AllClients[MAX_CLIENTS];

STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
	if (m_running == false)
		return S_OK; 

    CheckPointer(pSample,E_POINTER);

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
	 
    
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    } 
	int dataSize = pSample->GetActualDataLength();

#if USE_AES
	size_t csize = 0;
	if (aes.MaxCipherTextSize(dataSize, csize))
	{
		 
	}
	
	if (aes.Encrypt((byte*)pbData, dataSize, ciphertext, csize) == false)
	{
		::MessageBox(NULL, L"Failed to encrypt", L"eee", 0);
	}
	 
	 
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (m_AllClients[i] != INVALID_SOCKET)
		{
			int res = send(m_AllClients[i], (const char *)ciphertext, csize, 0);
			if (res == -1)
			{
				m_AllClients[i] = INVALID_SOCKET;
			}
		}
	}

#else 
	   
	for (int i = 0; i < MAX_CLIENTS; i++)
	{
		if (m_AllClients[i] != INVALID_SOCKET)
		{
			int res = send(m_AllClients[i], (const char *)pbData, dataSize, 0);
			//if (res == -1)
			{
				//m_AllClients[i] = INVALID_SOCKET;
			}
		}
		else {
			break;
		}
	}
#endif 
	   
	return hr;
}
 
//
// EndOfStream
//
STDMETHODIMP CDumpInputPin::EndOfStream(void)
{
    CAutoLock lock(m_pReceiveLock);
    return CRenderedInputPin::EndOfStream();

} // EndOfStream


//
// NewSegment
//
// Called when we are seeked
//
STDMETHODIMP CDumpInputPin::NewSegment(REFERENCE_TIME tStart,
                                       REFERENCE_TIME tStop,
                                       double dRate)
{
    m_tLast = 0;
    return S_OK;

} // NewSegment

//
//  CDump class
//
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
    CUnknown(NAME("BoutechNetworkSender"), pUnk),
    m_pFilter(NULL),
    m_pPin(NULL),
    m_pPosition(NULL),
    m_hFile(INVALID_HANDLE_VALUE),
    m_pFileName(0),
    m_fWriteError(0)
{
    ASSERT(phr);


	  
    m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
    if (m_pFilter == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

    m_pPin = new CDumpInputPin(this,GetOwner(),
                               m_pFilter,
                               &m_Lock,
                               &m_ReceiveLock,
                               phr);
    if (m_pPin == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }
	 

#if USE_AES
	InitializeAES();
#endif 
	 
	 
	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		return;
	}
  
	m_tcpServer = make_shared<CTCPServer>();
	Setup(true);

	 

}
 
CDump::~CDump()
{
	m_running = false;
	m_connecting = false;

	delete ciphertext;
	
	 
    delete m_pPin;
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance


//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);


    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Boutech TCP Network Multicast Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

}  
  
 

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

