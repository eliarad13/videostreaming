//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

 
// {31A6AD41-197C-48f5-844C-6E232444EF8B}
DEFINE_GUID(CLSID_BoutechNetworkSender,
	0x31a6ad41, 0x197c, 0x48f5, 0x84, 0x4c, 0x6e, 0x23, 0x24, 0x44, 0xef, 0x8b);
