#pragma once
#include <thread>
#include <memory>
using namespace std;
#define MAX_CLIENTS    30 

#define FIFO_SIZE 1024 * 1024 * 100
int Setup(bool allInterface);
void CloseAllClients();

class CTCPServer
{
public:
	CTCPServer( )   
	{
		Init();
		  
	}
	 
	~CTCPServer();
	 
	void Start();
	void Stop();
	void Pause();
 

#define RECEIVE_SIZE  188 * 30
	
private:
	WSADATA			   wsaData;
	
	uint8_t			   ReceiveBuf[RECEIVE_SIZE];
	uint8_t			   FrameBuf[RECEIVE_SIZE];
	int                BufLength;
	int                SenderAddrSize;
	int                ByteReceived;	
	int				   ErrorCode;
	bool			   m_running;

	int				   m_writeIndex;
	int				   m_readIndex;


 
	shared_ptr<thread> m_receiveThread;
	shared_ptr<thread> m_frameThread;

private:
	void ReceiveThread();
 
	int recvfromTimeOutTCP(SOCKET socket, long sec, long usec);	
	const wchar_t *GetWC(const char *c);
 

	void Init();	 
	 

};

