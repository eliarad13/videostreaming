#include <streams.h>
#include <stdio.h>
#include <string>
#include <time.h>
#include <math.h>
#include <chrono>
#include <ctime>

// Eliminate two expected level 4 warnings from the Microsoft compiler.
// The class does not have an assignment or copy operator, and so cannot
// be passed by value.  This is normal.  This file compiles clean at the
// highest (most picky) warning level (-W4).
#pragma warning(disable: 4511 4512)


#include <initguid.h>
#if (1100 > _MSC_VER)
#include <olectlid.h>
#else
#include <olectl.h>
#endif
#include "BauotechInPlaceTransformuids.h"             
#include "IBauotechInPlaceTransform.h"              / 
#include "BauotechInPlaceTransform.h"

 

typedef UINT(CALLBACK* LPRunAlgoColors)(BAUOTECH_AND_BENNY_KAROV_ALGO algo,
	uint8_t* pData,
	uint32_t width,
	uint32_t height,
	uint32_t pixelWidth,
	uint32_t image_size,
	uint8_t youDraw,
	ALGO_DETECTION_OBJECT_DATA* pObjects,
	uint32_t* objectCount);



typedef UINT(CALLBACK* LPInitAlgoColors)(BAUOTECH_AND_BENNY_KAROV_ALGO algo,
	uint32_t width,
	uint32_t height,
	uint32_t pixelWidth,  // always 32
	uint32_t image_size);


typedef UINT(CALLBACK* LPAlgoSetTime)(int hout, int min, int sec);
typedef UINT(CALLBACK* LPInitGPU)();

typedef UINT(CALLBACK* LPAlgoTerminate)();


LPRunAlgoColors lpfnRunAlgoColors;
LPAlgoSetTime  lpfnAlgoSetTime;
LPInitAlgoColors  lpfnInitAlgoColors;

LPAlgoTerminate  lpfnAlgoTerminate;

LPInitGPU  lpfnInitGPU;




#define DbgFunc(a) DbgLog(( LOG_TRACE                        \
                          , 2                                \
                          , TEXT("CBauotechInPlaceTransform(Instance %d)::%s") \
                          , m_nThisInstance                  \
                          , TEXT(a)                          \
                         ));


// Self-registration data structures

const AMOVIESETUP_MEDIATYPE
sudPinTypes =   { &MEDIATYPE_Audio        // clsMajorType
                , &MEDIASUBTYPE_NULL };   // clsMinorType

const AMOVIESETUP_PIN
psudPins[] = { { L"Input"            // strName
               , FALSE               // bRendered
               , FALSE               // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Output"           // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             , { L"Output"           // strName
               , FALSE               // bRendered
               , TRUE                // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Input"            // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             };

const AMOVIESETUP_FILTER
sudGargle = { &CLSID_BauotechInPlaceAIAlgoTransform                   // class id
            , L"Bauotech AI Algo Transform"                       // strName
            , MERIT_DO_NOT_USE                // dwMerit
            , 2                               // nPins
            , psudPins                        // lpPin
            };

// Needed for the CreateInstance mechanism
CFactoryTemplate g_Templates[1]= { { L"Bauotech AI Algo Transform"
, &CLSID_BauotechInPlaceAIAlgoTransform
, CBauotechInPlaceTransform::CreateInstance
                                   , NULL
                                   , &sudGargle
                                   }                                 
                                 };

int g_cTemplates = sizeof(g_Templates)/sizeof(g_Templates[0]);

// initialise the static instance count.
int CBauotechInPlaceTransform::m_nInstanceCount = 0;



CBauotechInPlaceTransform::~CBauotechInPlaceTransform()
{
	
}

 
 
CBauotechInPlaceTransform::CBauotechInPlaceTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr)
	: CTransInPlaceFilter(tszName, punk, CLSID_BauotechInPlaceAIAlgoTransform, phr)
    , m_SamplesPerSec     (0)
    , m_BytesPerSample    (0)
    , m_Channels          (0)
    , m_Phase             (0)
    , m_Shape             (0)
{

	m_algoNumber = ALGO_RAMI_LEVI;
	LoadAlgoDll();
	m_pause_ts = false;

}  

CUnknown * WINAPI CBauotechInPlaceTransform::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
	CBauotechInPlaceTransform *pNewObject = new CBauotechInPlaceTransform(NAME("Bauotech AI Algo Transform Filter"), punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

}


STDMETHODIMP CBauotechInPlaceTransform::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
    CheckPointer(ppv,E_POINTER);

	if (riid == IID_IBaoutechInPlaceAIAlgoTransform) {
		return GetInterface((IBauotechAIAlgoTransform *) this, ppv);

    } else if (riid == IID_IPersistStream) {
        return GetInterface((IPersistStream *) this, ppv);

    } else {
        // Pass the buck
        return CTransInPlaceFilter::NonDelegatingQueryInterface(riid, ppv);
    }

} // NonDelegatingQueryInterface

 
 
STDMETHODIMP CBauotechInPlaceTransform::Pause()
{

	//m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP CBauotechInPlaceTransform::Stop()
{

	//m_pOutputPin->m_graphStat = GRAPH_STOP;

 

	return CBaseFilter::Stop();
}

STDMETHODIMP CBauotechInPlaceTransform::Run(REFERENCE_TIME tStart)
{
	m_frameCount = 0;
	m_lastTime = time(NULL);

	time_t rawtime;
	struct tm * timeinfo;
	time(&rawtime);
	timeinfo = localtime(&rawtime);
	//lpfnAlgoSetTime(timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
 
	 
	return CBaseFilter::Run(tStart);
}
 

BOOL CBauotechInPlaceTransform::LoadAlgoDll()
{
	 

	DWORD dwParam1;
	UINT  uParam2, uReturnVal;

	hDLL = LoadLibrary(L"C:\\Program Files\\Bauotech\\Dll\\Algo\\BAlgoApi.dll");
	if (hDLL != NULL)
	{
		lpfnRunAlgoColors = (LPRunAlgoColors)GetProcAddress(hDLL, "RunAlgoColors");
		if (!lpfnRunAlgoColors)
		{
			::MessageBox(NULL, L"BAlgoApi Not loaded: RunAlgoColors", L"Bauotech", 0);
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		}

		lpfnAlgoTerminate = (LPAlgoTerminate)GetProcAddress(hDLL, "AlgoTerminate");
		if (!lpfnAlgoTerminate)
		{
			::MessageBox(NULL, L"BAlgoApi Not loaded: AlgoTerminate", L"Bauotech", 0);
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		} 

#if 0 
		lpfnInitAlgoColors = (LPInitAlgoColors)GetProcAddress(hDLL, "InitAlgoColors");
		if (!lpfnInitAlgoColors)
		{
			::MessageBox(NULL, L"BAlgoApi Not loaded : InitAlgoColors", L"Bauotech", 0);
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		}
#endif 

#if 0

		lpfnInitGPU = (LPInitGPU)GetProcAddress(hDLL, "InitGPU");
		if (!lpfnInitGPU)
		{
			::MessageBox(NULL, L"BAlgoApi Not loaded : InitGPU", L"Bauotech", 0);
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		}
		 
#endif 
		 

#if 0 
		lpfnAlgoSetTime = (LPAlgoSetTime)GetProcAddress(hDLL, "AlgoSetTime");
		if (!lpfnAlgoSetTime)
		{
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		}
#endif 		 
		return 1;
	}
	::MessageBox(NULL, L"BAlgoApi Version 1.2 Not loaded", L"Bauotech", 0);
	return 0;
}



//
// Transform
//
// Override CTransInPlaceFilter method.
// Convert the input sample into the output sample.
//


HRESULT CBauotechInPlaceTransform::Transform(IMediaSample *pSample)
{
  
    BYTE *pSampleBuffer;
    int iSize = pSample->GetActualDataLength();
    pSample->GetPointer(&pSampleBuffer);
	uint32_t objectCount = 0;

	 
 
#if 0 
	if (m_frameCount > 60000)
	{
		time_t rawtime;
		struct tm * timeinfo;
		time(&rawtime);
		timeinfo = localtime(&rawtime);
		lpfnAlgoSetTime(timeinfo->tm_hour, timeinfo->tm_min, timeinfo->tm_sec);
		m_frameCount = 0;
	}
#endif 

	auto now1 = std::chrono::system_clock::now();
	//std::time_t end_time = std::chrono::system_clock::to_time_t(now1);
	 
	 
	int uReturnVal = lpfnRunAlgoColors(m_algoNumber,			
									   pSampleBuffer, 
									   m_pvideoInfo.rcTarget.right,
									   m_pvideoInfo.rcTarget.bottom,
									   32,
									   m_pvideoInfo.rcTarget.right * m_pvideoInfo.rcTarget.bottom * 4, // we transform to 32 bit rgba
									   true, 
									   AlgoObjects,
									   &objectCount);

#if 1 
	auto now2 = std::chrono::system_clock::now();
	//std::time_t end_time = std::chrono::system_clock::to_time_t(now2);
	
	typedef std::chrono::duration<float, std::milli> duration;
	duration elapsed = now2 - now1;
	if (elapsed.count() > 35)
	{
		std::string str  = std::to_string(elapsed.count());
		::MessageBoxA(NULL, str.c_str(), "", 0);
	}
#endif 


	if (uReturnVal == 0)
	{
		::MessageBox(NULL, L"Failed to run algo", L"Bauotech", 0);
	}

	m_frameCount++;
	pAlgoObjectsCallback((uint8_t *)&AlgoObjects, sizeof(ALGO_DETECTION_OBJECT_DATA) * objectCount, objectCount, m_videoId);
	
	  
    return NOERROR;

} // Transform


//
// CheckInputType
//
// Override CTransformFilter method.
// Part of the Connect process.
// Ensure that we do not get connected to formats that we can't handle.
// We only work for wave audio, 8 or 16 bit, uncompressed.
//
HRESULT CBauotechInPlaceTransform::CheckInputType(const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);

    VIDEOINFOHEADER* pvi = (VIDEOINFOHEADER*)pmt->pbFormat;
	memcpy(&m_pvideoInfo, pvi, sizeof(VIDEOINFOHEADER));


    return NOERROR;

} // CheckInputType


//
// SetMediaType
//
// Override CTransformFilter method.
// Called when a connection attempt has succeeded. If the output pin
// is being connected and the input pin's media type does not agree then we
// reconnect the input (thus allowing its media type to change,) and vice versa.
//
HRESULT CBauotechInPlaceTransform::SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);
    VIDEOINFOHEADER* pvi = (VIDEOINFOHEADER*)pmt->pbFormat;


	return NOERROR;
 

} // SetMediaType
 
 
STDMETHODIMP CBauotechInPlaceTransform::SetAlgoDetectionCallback(AlgoObjectsCallback p, int videoId)
{

	pAlgoObjectsCallback = p;
	m_videoId = videoId;
	return S_OK;
}

STDMETHODIMP CBauotechInPlaceTransform::SetAlgoNumber(BAUOTECH_AND_BENNY_KAROV_ALGO algoNumber)
{
	m_algoNumber = algoNumber;
	return S_OK;
}


STDMETHODIMP CBauotechInPlaceTransform::InitAlgo(BAUOTECH_AND_BENNY_KAROV_ALGO algoNumber,
												 uint32_t width,
												 uint32_t height,
												 uint32_t pixelWidth,
												 uint32_t image_size)
{
	
	lpfnInitAlgoColors(algoNumber, width, height, pixelWidth, image_size);
	return S_OK;
}



STDMETHODIMP CBauotechInPlaceTransform::AlgoTerminate()
{

	lpfnAlgoTerminate();
	return S_OK;
}


STDMETHODIMP CBauotechInPlaceTransform::InitGPU()
{

	if (lpfnInitGPU() == true)
	{
		return S_OK;
	}
	else
	{
		return E_FAIL;
	}
}
 
 
STDAPI DllRegisterServer()
{
  return AMovieDllRegisterServer2( TRUE );
}


STDAPI DllUnregisterServer()
{
  return AMovieDllRegisterServer2( FALSE );
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

#pragma warning(disable: 4514) // "unreferenced inline function has been removed"



