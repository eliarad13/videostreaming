#pragma once 
#include <stdint.h>
#include <time.h>

 

 
class CBauotechInPlaceTransform
	// Inherited classes
	: public CTransInPlaceFilter       // Main DirectShow interfaces
	, public IBauotechAIAlgoTransform


{  

public:

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);

	DECLARE_IUNKNOWN;

	//
	// --- CTransInPlaceFilter Overrides --
	//

	HRESULT CheckInputType(const CMediaType *mtIn);

	// Basic COM - used here to reveal our property interface.
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);
 


	STDMETHODIMP SetAlgoDetectionCallback(AlgoObjectsCallback p, int videoId);
	STDMETHODIMP SetAlgoNumber(BAUOTECH_AND_BENNY_KAROV_ALGO algoNumber);

	STDMETHODIMP InitAlgo(BAUOTECH_AND_BENNY_KAROV_ALGO algoNumber,
		uint32_t width,
		uint32_t height,
		uint32_t pixelWidth,
		uint32_t image_size);


	STDMETHODIMP AlgoTerminate();

	STDMETHODIMP InitGPU();

 
	VIDEOINFOHEADER  m_pvideoInfo;

	
	 

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();


	ALGO_DETECTION_OBJECT_DATA  AlgoObjects[50];
	BAUOTECH_AND_BENNY_KAROV_ALGO m_algoNumber;
	time_t m_lastTime;
	AlgoObjectsCallback  pAlgoObjectsCallback;
	int m_videoId = 0;


private:


	// Constructor
	CBauotechInPlaceTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr);
	~CBauotechInPlaceTransform();
	uint64_t GetFifoSize(uint64_t rd);
	 
	uint32_t m_frameCount;

	bool m_pause_ts;
	HINSTANCE hDLL;               // Handle to DLL
	BOOL LoadAlgoDll();
	// Overrides the PURE virtual Transform of CTransInPlaceFilter base class
	// This is where the "real work" is done.
	HRESULT Transform(IMediaSample *pSample);

	// This is where the real work is really done (called from Transform)
	void MessItAbout(PBYTE pb, int cb);

	// Overrides a CTransformInPlace function.  Called as part of connecting.
	virtual HRESULT SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt);

	// If there are multiple instances of this filter active, it's
	// useful for debug messages etc. to know which one this is.
	// This variable has no other purpose.
	static int m_nInstanceCount;                   // total instances
	int m_nThisInstance;

	int                 m_Shape;               // 0==triangle, 1==square
	int                 m_SamplesPerSec;       // Current sample format
	int                 m_BytesPerSample;      // Current sample format
	int                 m_Channels;            // Current sample format
	int                 m_Phase;               // See MessItAbout in gargle.cpp
	CCritSec            m_GargleLock;          // To serialise access.

  
};  
