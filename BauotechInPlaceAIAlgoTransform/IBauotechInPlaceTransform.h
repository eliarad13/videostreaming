#ifndef __IGARGLE__
#define __IGARGLE__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>


typedef struct ALGO_DETECTION_OBJECT_DATA
{
	int X;
	int Y;
	int Width;
	int Height;
	int CountUpTime;
	int ObjectType; // optional
	int DetectionPercentage;// optional
	int reserved1;
	int reserved2;
} ALGO_DETECTION_OBJECT_DATA;

typedef enum BAUOTECH_AND_BENNY_KAROV_ALGO
{
	ALGO_NONE = 0,
	ALGO_RAMI_LEVI = 2001,
	ALGO_ZOSMAN = 2002,
	ALGO_DEFAULT = 2003,
	ALGO_EMBOSS = 2004,
	ALGO_GREY = 2005,
	ALGO_BLUR = 2006,
	ALGO_POSTERIZE = 2007,
	ALGO_XOR = 2008,
	ALGO_DARKEN = 2009,
	ALGO_BLUE = 2010,
	ALGO_GREEN = 2011,
	ALGO_RED = 2012,
	BLACK_WHITE = 2013
} BAUOTECH_AND_BENNY_KAROV_ALGO;
 
// {C5D27C1E-F847-4CBB-90C3-B2293CD3146A}
DEFINE_GUID(IID_IBaoutechInPlaceAIAlgoTransform,
	0xc5d27c1e, 0xf847, 0x4cbb, 0x90, 0xc3, 0xb2, 0x29, 0x3c, 0xd3, 0x14, 0x6a);

 

typedef void(__stdcall* AlgoObjectsCallback)(uint8_t *data, uint32_t size, uint32_t objectCount, int videoId);
 
DECLARE_INTERFACE_(IBauotechAIAlgoTransform, IUnknown) {

 
	STDMETHOD(SetAlgoDetectionCallback)(AlgoObjectsCallback p, int videoId) PURE;
	STDMETHOD(SetAlgoNumber)(BAUOTECH_AND_BENNY_KAROV_ALGO algoNumber) PURE;

	STDMETHOD(InitAlgo)(BAUOTECH_AND_BENNY_KAROV_ALGO algoNumber,
		uint32_t width,
		uint32_t height,
		uint32_t pixelWidth,
		uint32_t image_size)PURE;

	STDMETHOD(InitGPU)() PURE;

	STDMETHOD(AlgoTerminate)() PURE;
  
};


#ifdef __cplusplus
}
#endif

#endif // __IGARGLE__
