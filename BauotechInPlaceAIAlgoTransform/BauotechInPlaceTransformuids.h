//------------------------------------------------------------------------------
// File: GargUIDs.h
//
// Desc: DirectShow sample code - definition of CLSIDs.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __GARGUIDS__
#define __GARGUIDS__

#ifdef __cplusplus
extern "C" {
#endif

  
  
// {3A4BC832-0F77-4EFA-8AAE-BE41288FF7D6}
DEFINE_GUID(CLSID_BauotechInPlaceAIAlgoTransform,
	0x3a4bc832, 0xf77, 0x4efa, 0x8a, 0xae, 0xbe, 0x41, 0x28, 0x8f, 0xf7, 0xd6);
 


#ifdef __cplusplus
}
#endif

#endif // __GARGUIDS__
