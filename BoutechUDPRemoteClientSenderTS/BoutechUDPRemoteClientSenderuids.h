//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 
 

// {64E9640C-5E33-4A6C-A11B-CBCC1EF747CF}
DEFINE_GUID(CLSID_BoutechNetworkRemoteClientSender,
	0x64e9640c, 0x5e33, 0x4a6c, 0xa1, 0x1b, 0xcb, 0xcc, 0x1e, 0xf7, 0x47, 0xcf);
