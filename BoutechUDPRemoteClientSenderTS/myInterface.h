#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>

 
// {CD9E3310-3AD9-454B-A5E4-B59B5A0515BC}
static const GUID IID_IBoutechUDPRemoteClient =
{ 0xcd9e3310, 0x3ad9, 0x454b, { 0xa5, 0xe4, 0xb5, 0x9b, 0x5a, 0x5, 0x15, 0xbc } };

 

DECLARE_INTERFACE_(IBoutechUDPRemoteClient, IUnknown)
{	
	STDMETHOD(SetIpAddress)(const WCHAR *IpAddress) PURE;
	STDMETHOD(SetPort)(const int port) PURE;
};