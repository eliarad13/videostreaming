//------------------------------------------------------------------------------
// File: BoutechNetworkSender.cpp
//
// Desc: DirectShow sample code - implementation of a renderer that dumps
//       the samples it receives into a text file.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

  
 
#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include <stdint.h>
#include "BoutechUDPRemoteClientSenderuids.h"
#include "BoutechUDPRemoteClientSender.h"
#include <thread>
using namespace std;
#include <memory>

#define USE_THREAD  0


// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
	&CLSID_BoutechNetworkRemoteClientSender,                // Filter CLSID
    L"Bauotech UDP Remote Client Sender TS",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
	L"Bauotech UDP Remote Client Sender TS", &CLSID_BoutechNetworkRemoteClientSender, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;

void CDumpFilter::SenderThreadX()
{
	SendData();
}


CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
						 CBaseFilter(NAME("CDumpFilter"), pUnk, pLock, CLSID_BoutechNetworkRemoteClientSender),
    m_pDump(pDump)
{

	
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}


//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	 
	m_pDump->m_running = false;
	m_pDump->m_connecting = false;
	 
	if (m_pDump->socketC != -1)
		closesocket(m_pDump->socketC);
	m_pDump->socketC = -1;

    
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);

    if (m_pDump)
    {
         
    }

    return CBaseFilter::Pause();
}

bool m_serverRunning = true;
bool m_receiveFrom = false;
struct sockaddr src_addr;
int fromlen;

 
 
//
// Run
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	CAutoLock cObjectLock(m_pLock);
	
	if (m_pDump->socketC == -1)
		m_pDump->socketC = socket(AF_INET, SOCK_DGRAM, IPPROTO_UDP);
	if (::connect(m_pDump->socketC, (struct sockaddr *)&m_pDump->serverInfo, sizeof(m_pDump->serverInfo)) == SOCKET_ERROR)
	{
		::MessageBox(NULL, L"Failed to connect", L"Bauotech UDP Remote Client Sender TS", 0);
		return CBaseFilter::Run(tStart);
	}
	m_pDump->fifo_read = 0;
	m_pDump->fifo_write = 0;
	m_pDump->m_running = true;
	shared_ptr<thread> m_senderThread = make_shared<thread>(&CDumpFilter::SenderThreadX, this);
	m_senderThread->detach();
	 
	  
    return CBaseFilter::Run(tStart);
}


//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{

  
	
	
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}



HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}



#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

void CDump::InitFifo()
{
	fifoBuffer = (uint8_t *)malloc(MAX_FIFO);
}


bool CDump::AddToFifo(uint8_t *data, int size)
{

	int x = MAX_FIFO - GetFifoSize();
	if (x < size)
	{
		return false;
	}

	int d = MIN(size, MAX_FIFO - fifo_write);
	memcpy(fifoBuffer + fifo_write, data, d);

	EnterCriticalSection(&CriticalSection);
	fifo_write = (fifo_write + d) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);


	size -= d;
	if (size > 0)
	{
		memcpy(fifoBuffer + fifo_write, data, size);
		EnterCriticalSection(&CriticalSection);
		fifo_write = (fifo_write + size) % MAX_FIFO;
		LeaveCriticalSection(&CriticalSection);
	}
	return true;
}


int CDump::GetFifoSize()
{

	EnterCriticalSection(&CriticalSection);

	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return 0;
	}


	if (fifo_write > fifo_read)
	{
		int x = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return x;
	}

	int x = (MAX_FIFO - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return x;
}
  
//
// Receive
//
// Do something with this media sample
//
STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
	if (m_pDump->m_running == false)
		return S_OK; 

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
    
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    } 

	while (m_pDump->AddToFifo(pbData, pSample->GetActualDataLength()) == false)
	{
		if (m_pDump->m_running == false)
			return S_OK;
	}
	 
	return hr;
}
  

void CDumpFilter::QuickConnect(int *count4Connect)
{
	if (*count4Connect == 2)
	{
		::connect(m_pDump->socketC, (struct sockaddr *)&m_pDump->serverInfo, sizeof(m_pDump->serverInfo));
		*count4Connect = 0;
	}
	else
	{
		*count4Connect++;
	}
	Sleep(0);
}

void CDumpFilter::SendData()
{
	//CDump *pDump = (CDump *)data;

	int count4Connect = 0;
	while (m_pDump->m_running)
	{
		int size;
		if ((size = m_pDump->GetFifoSize()) < m_pDump->MTU)
		{
			QuickConnect(&count4Connect);
			continue;
		}
		while (size >= m_pDump->MTU)
		{
			if (m_pDump->m_running == false)
				return; 

			if (size >= m_pDump->MTU)
			{

				int sentSize = ::send(m_pDump->socketC, (const char *)m_pDump->fifoBuffer + m_pDump->fifo_read, m_pDump->MTU, 0);
				Sleep(1);
				if (sentSize != m_pDump->MTU)
				{
					continue;
				}
				EnterCriticalSection(&m_pDump->CriticalSection);
				m_pDump->fifo_read = (m_pDump->fifo_read + m_pDump->MTU) % m_pDump->MAX_FIFO;
				LeaveCriticalSection(&m_pDump->CriticalSection);

				size -= m_pDump->MTU;
			}
		}
	}
}


//
// EndOfStream
//
STDMETHODIMP CDumpInputPin::EndOfStream(void)
{
    CAutoLock lock(m_pReceiveLock);
    return CRenderedInputPin::EndOfStream();

} // EndOfStream


//
// NewSegment
//
// Called when we are seeked
//
STDMETHODIMP CDumpInputPin::NewSegment(REFERENCE_TIME tStart,
                                       REFERENCE_TIME tStop,
                                       double dRate)
{
    m_tLast = 0;
    return S_OK;

} // NewSegment

//
//  CDump class
//
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
CUnknown(NAME("Boutech UDP Remote Client Sender TS"), pUnk),
m_pFilter(NULL),
m_pPin(NULL),
m_pPosition(NULL),
m_hFile(INVALID_HANDLE_VALUE),
m_pFileName(0),
m_fWriteError(0)
{
	ASSERT(phr);

	MTU = 1500;// 9014;
	int len = sizeof(serverInfo);

	m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
	if (m_pFilter == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
		return;
	}

	m_pPin = new CDumpInputPin(this, GetOwner(),
		m_pFilter,
		&m_Lock,
		&m_ReceiveLock,
		phr);
	if (m_pPin == NULL) {
		if (phr)
			*phr = E_OUTOFMEMORY;
		return;
	}

	// Initialize Winsock
	int iResult = WSAStartup(MAKEWORD(2, 2), &wsaData);
	if (iResult != 0) {
		return;
	}
	m_running = true;
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_port = htons(6000);
	m_port = 6000;
	//serverInfo.sin_addr.s_addr = inet_addr("82.81.105.171");
	serverInfo.sin_addr.s_addr = inet_addr("10.0.0.10");
	//serverInfo.sin_addr.s_addr = inet_addr("212.115.111.115");
	InitFifo();


	
	
	//HANDLE thread = CreateThread(NULL, 0, ReceiveThread, NULL, 0, NULL);	 

}

CDumpFilter::~CDumpFilter()
{
	
}


CDump::~CDump()
{
	m_running = false;
	m_connecting = false;

	if (socketC > 0)
		closesocket(socketC);


	DeleteCriticalSection(&CriticalSection);
	if (fifoBuffer != NULL)
	{
		free(fifoBuffer);
		fifoBuffer = NULL;
	}

    delete m_pPin;
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance


STDMETHODIMP CDump::SetIpAddress(const WCHAR *IpAddress)
{ 
	char ip[100];
	wcstombs(ip, IpAddress, sizeof(ip));
	serverInfo.sin_addr.s_addr = inet_addr(ip);

	return S_OK;
}


STDMETHODIMP CDump::SetPort(const int port)
{
	serverInfo.sin_port = htons(port);
	m_port = port;
	return S_OK;
}

STDMETHODIMP CDump::SetMTU(int mtu)
{
	MTU = mtu;

	return S_OK;
}


//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);


	// Initialize the critical section one time only.
	InitializeCriticalSectionAndSpinCount(&CriticalSection,0x00000400);
	 
    // Do we have this interface

	if (riid == IID_IBoutechUDPRemoteClient) {
		return GetInterface((IBoutechUDPRemoteClient *) this, ppv);
	} else 
    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Boutech UDP Remote Client Sender Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

}  
  
 

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

