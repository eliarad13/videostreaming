#include <streams.h>
#include "BauotechRGB32ColorSpaceConverter.h"
#include "BauotechRGB32ColorSpaceConverteruids.h"
#include <aviriff.h>
#include <malloc.h>


// {C18211AD-45CE-4C7A-9A96-974191FBFBA4}
static const GUID CLSID_BauotechRGBSpaceConverter = 
{ 0xc18211ad, 0x45ce, 0x4c7a, { 0x9a, 0x96, 0x97, 0x41, 0x91, 0xfb, 0xfb, 0xa4 }};
 

int m_width = 0;
int m_height = 0;

const AMOVIESETUP_FILTER sudWavDest =
{
	&CLSID_BauotechRGBSpaceConverter,           // clsID
	L"Bauotech RGB32 Space Converter",              // strName
	MERIT_DO_NOT_USE,         // dwMerit
	0,                        // nPins
	0                         // lpPin
};


// Global data
CFactoryTemplate g_Templates[] = {
	{ L"Bauotech RGB32 Space Converter", &CLSID_BauotechRGBSpaceConverter, CWavDestFilter::CreateInstance, NULL, &sudWavDest },
};

int g_cTemplates = sizeof(g_Templates) / sizeof(g_Templates[0]);


// ------------------------------------------------------------------------
// filter constructor

#pragma warning(disable:4355)


CWavDestFilter::CWavDestFilter(LPUNKNOWN pUnk, HRESULT *phr) :
CTransformFilter(NAME("Bauotech RGB32 Space Converter filter"), pUnk, CLSID_BauotechRGBSpaceConverter),
m_cbWavData(0),
m_cbHeader(0)
{
	ASSERT(m_pOutput == 0);
	ASSERT(phr);

	if (SUCCEEDED(*phr))
	{
		// Create an output pin so we can have control over the connection
		// media type.
		CWavDestOutputPin *pOut = new CWavDestOutputPin(this, phr);

		if (pOut)
		{
			if (SUCCEEDED(*phr))
			{
				m_pOutput = pOut;
			}
			else
			{
				delete pOut;
			}
		}
		else
		{
			*phr = E_OUTOFMEMORY;
		}

		//
		// NOTE!: If we've created our own output pin we must also create
		// the input pin ourselves because the CTransformFilter base class 
		// will create an extra output pin if the input pin wasn't created.        
		//
		CTransformInputPin *pIn = new CTransformInputPin(NAME("Transform input pin"),
			this,              // Owner filter
			phr,               // Result code
			L"In");            // Pin name
		// a failed return code should delete the object
		if (pIn)
		{
			if (SUCCEEDED(*phr))
			{
				m_pInput = pIn;
			}
			else
			{
				delete pIn;
			}
		}
		else
		{
			*phr = E_OUTOFMEMORY;
		}
	}
}


// ------------------------------------------------------------------------
// destructor

CWavDestFilter::~CWavDestFilter()
{
}


CUnknown * WINAPI CWavDestFilter::CreateInstance(LPUNKNOWN pUnk, HRESULT * phr)
{
	return new CWavDestFilter(pUnk, phr);
}


//
// CWavDestFilter::CheckTransform
//
// To be able to transform, the formats must be identical
//
HRESULT CWavDestFilter::CheckTransform(const CMediaType *mtIn, const CMediaType *mtOut)
{
	HRESULT hr;

	if (FAILED(hr = CheckInputType(mtIn)))
	{
		return hr;
	}

	return NOERROR;

} // CheckTransform


// overridden because we need to know if Deliver() failed.

HRESULT CWavDestFilter::Receive(IMediaSample *pSample)
{
	ULONG cbOld = m_cbWavData;
	HRESULT hr = CTransformFilter::Receive(pSample);

	// don't update the count if Deliver() downstream fails.
	if (hr != S_OK)
	{
		m_cbWavData = cbOld;
	}

	return hr;
}

//
// CWavDestFilter::Transform
//
//
HRESULT CWavDestFilter::Transform(IMediaSample *pIn, IMediaSample *pOut)
{
	REFERENCE_TIME rtStart, rtEnd;
	 
	// First just copy the data to the output sample
	HRESULT hr = Copy(pIn, pOut);
	if (FAILED(hr))
	{
		return hr;
	}

#if 0 
	// Prepare it for writing    
	LONG lActual = pOut->GetActualDataLength();
	 
	rtStart = m_cbWavData + m_cbHeader;
	rtEnd = rtStart + lActual;
	m_cbWavData += lActual;

	EXECUTE_ASSERT(pOut->SetTime(&rtStart, &rtEnd) == S_OK);
#endif 
	return S_OK;
}


//
// CWavDestFilter::Copy
//
// Make destination an identical copy of source
//
HRESULT CWavDestFilter::Copy(IMediaSample *pSource, IMediaSample *pDest) const
{
	CheckPointer(pSource, E_POINTER);
	CheckPointer(pDest, E_POINTER);

	// Copy the sample data

	BYTE *pSourceBuffer, *pDestBuffer;
	long lSourceSize = pSource->GetActualDataLength();
	long lDestSize = pDest->GetSize();
#ifdef DEBUG    
	ASSERT(lDestSize >= lSourceSize);
#endif

	pSource->GetPointer(&pSourceBuffer);
	pDest->GetPointer(&pDestBuffer);

 // The source start from the end , and in each line it start from the 0
	
 
	int d = 0;
	int line = m_width * m_height * 3;
	for (int i = 0; i < m_height; i++)
	{
		int k = 0;
		for (int j = 0; j < m_width; j++)
		{
			pDestBuffer[d] = pSourceBuffer[line - (m_width * 3) + k ];
			pDestBuffer[d + 1] = pSourceBuffer[line - (m_width * 3) + k + 1];
			pDestBuffer[d + 2] = pSourceBuffer[line - (m_width * 3) + k + 2];
			pDestBuffer[d + 3] = 0;
			k += 3;
			d += 4;
		}
		line -= m_width * 3;
	}
   
	 
	// Copy the sample times

	REFERENCE_TIME TimeStart, TimeEnd;
	if (NOERROR == pSource->GetTime(&TimeStart, &TimeEnd))
	{
		pDest->SetTime(&TimeStart, &TimeEnd);
	}

	LONGLONG MediaStart, MediaEnd;
	if (pSource->GetMediaTime(&MediaStart, &MediaEnd) == NOERROR)
	{
		pDest->SetMediaTime(&MediaStart, &MediaEnd);
	}
 
	pDest->SetActualDataLength(lDestSize);

	return NOERROR;

} // Copy


//
// CheckInputType
//

HRESULT CWavDestFilter::CheckInputType(const CMediaType* mtIn)
{
	if (mtIn->majortype == MEDIATYPE_Video &&
		(mtIn->subtype == MEDIASUBTYPE_RGB24) &&
		mtIn->formattype == FORMAT_VideoInfo)
	{

		VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)mtIn->Format();
		if (!vih)
		{
			return E_OUTOFMEMORY;
		}
		m_width = vih->bmiHeader.biWidth;
		m_height = vih->bmiHeader.biHeight;
		return NOERROR;
	}
	else
		return E_FAIL;
}

//
// GetMediaType
//
HRESULT CWavDestFilter::GetMediaType(int iPosition, CMediaType *pMediaType)
{
	  
 
	if (iPosition == 0)
	{
		CheckPointer(pMediaType, E_POINTER);

		BITMAPINFOHEADER m_bmpInfo;
		m_bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
		m_bmpInfo.biCompression = BI_RGB;
		m_bmpInfo.biBitCount = 32;
		m_bmpInfo.biHeight = m_height;
		m_bmpInfo.biWidth = m_width;
		m_bmpInfo.biPlanes = 1;
		m_bmpInfo.biSizeImage = GetBitmapSize(&m_bmpInfo);
		m_bmpInfo.biClrImportant = 0;
		m_bmpInfo.biClrUsed = 0;
		m_bmpInfo.biXPelsPerMeter = 0;
		m_bmpInfo.biYPelsPerMeter = 0;

		 
		VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)pMediaType->AllocFormatBuffer(sizeof(VIDEOINFOHEADER));
		if (!vih)
		{
			return E_OUTOFMEMORY;
		}

		vih->bmiHeader = m_bmpInfo;
		/*
		if (m_frameRate != 0)
		{
			vih->AvgTimePerFrame = UNITS / m_frameRate;
		}
		else
		{
			vih->AvgTimePerFrame = 0;
		}*/


		pMediaType->SetType(&MEDIATYPE_Video);
		pMediaType->SetSubtype(&MEDIASUBTYPE_RGB32);
		pMediaType->SetFormatType(&FORMAT_VideoInfo);
		vih->rcSource.top = 0;
		vih->rcSource.right = m_width;
		vih->rcSource.left = 0;
		vih->rcSource.bottom = m_height;

		vih->rcTarget.top = 0;
		vih->rcTarget.right = m_width;
		vih->rcTarget.left = 0;
		vih->rcTarget.bottom = m_height;
		 
		pMediaType->SetFormat((byte *)vih, sizeof(VIDEOINFOHEADER));

		return S_OK;
	}

	return VFW_S_NO_MORE_ITEMS;
}

//
// DecideBufferSize
//
// Tell the output pin's allocator what size buffers we
// require. Can only do this when the input is connected
//
HRESULT CWavDestFilter::DecideBufferSize(IMemAllocator *pAlloc,
	ALLOCATOR_PROPERTIES *pProperties)
{
	HRESULT hr = NOERROR;

	// Is the input pin connected
	if (m_pInput->IsConnected() == FALSE)
	{
		return E_UNEXPECTED;
	}

	CheckPointer(pAlloc, E_POINTER);
	CheckPointer(pProperties, E_POINTER);

	pProperties->cBuffers = 1;
	pProperties->cbAlign = 1;

	// Get input pin's allocator size and use that
	ALLOCATOR_PROPERTIES InProps;
	IMemAllocator * pInAlloc = NULL;

	hr = m_pInput->GetAllocator(&pInAlloc);
	if (SUCCEEDED(hr))
	{
		hr = pInAlloc->GetProperties(&InProps);
		if (SUCCEEDED(hr))
		{
			pProperties->cbBuffer = InProps.cbBuffer;
		}
		pInAlloc->Release();
	}

	if (FAILED(hr))
		return hr;

	ASSERT(pProperties->cbBuffer);

	// Ask the allocator to reserve us some sample memory, NOTE the function
	// can succeed (that is return NOERROR) but still not have allocated the
	// memory that we requested, so we must check we got whatever we wanted

	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(pProperties, &Actual);
	if (FAILED(hr))
	{
		return hr;
	}

	ASSERT(Actual.cBuffers == 1);

	if (pProperties->cBuffers > Actual.cBuffers ||
		pProperties->cbBuffer > Actual.cbBuffer)
	{
		return E_FAIL;
	}

	return NOERROR;

} // DecideBufferSize


//
 
HRESULT CWavDestFilter::StartStreaming()
{ 
	return S_OK;
}
 

//
// StopStreaming
//
// Write out the header
//
HRESULT CWavDestFilter::StopStreaming()
{
 
	return S_OK;
}

//
// CWavDestOutputPin::CWavDestOutputPin 
//
CWavDestOutputPin::CWavDestOutputPin(CTransformFilter *pFilter, HRESULT * phr) :
CTransformOutputPin(NAME("WavDest output pin"), pFilter, phr, L"Out")
{
	// Empty
}


//
// CWavDestOutputPin::EnumMediaTypes
//
STDMETHODIMP CWavDestOutputPin::EnumMediaTypes(IEnumMediaTypes **ppEnum)
{
	return CBaseOutputPin::EnumMediaTypes(ppEnum);
}

//
// CWavDestOutputPin::CheckMediaType
//
// Make sure it's our default type
//
HRESULT CWavDestOutputPin::CheckMediaType(const CMediaType* pmt)
{
	CheckPointer(pmt, E_POINTER);

	if (pmt->majortype == MEDIATYPE_Video &&
		(pmt->subtype == MEDIASUBTYPE_RGB32) &&
		pmt->formattype == FORMAT_VideoInfo)
		return NOERROR;
	else
		return E_FAIL;
}


////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

STDAPI DllRegisterServer()
{
	return AMovieDllRegisterServer2(TRUE);
}

STDAPI DllUnregisterServer()
{
	return AMovieDllRegisterServer2(FALSE);
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule,
	DWORD  dwReason,
	LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}


