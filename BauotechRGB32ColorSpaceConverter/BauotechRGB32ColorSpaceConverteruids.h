//------------------------------------------------------------------------------
// File: GargUIDs.h
//
// Desc: DirectShow sample code - definition of CLSIDs.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __GARGUIDS__
#define __GARGUIDS__

#ifdef __cplusplus
extern "C" {
#endif

 
// {C18211AD-45CE-4C7A-9A96-974191FBFBA4}
DEFINE_GUID(CLSID_BauotechRGBSpaceConverter,
	0xc18211ad, 0x45ce, 0x4c7a, 0x9a, 0x96, 0x97, 0x41, 0x91, 0xfb, 0xfb, 0xa4);




#ifdef __cplusplus
}
#endif

#endif // __GARGUIDS__
