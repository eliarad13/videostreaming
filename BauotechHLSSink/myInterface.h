#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

 
// {63A08AF5-8310-4BE0-90E0-07A5A241E562}
DEFINE_GUID(IID_IBoutechHLSSink,
	0x63a08af5, 0x8310, 0x4be0, 0x90, 0xe0, 0x7, 0xa5, 0xa2, 0x41, 0xe5, 0x62);

 
typedef void(__stdcall * HLSSinkCallback)(char *fileCreationTime, char *serverInstance, 
										  int targetDuration, float extInfTime, char *fullPath, 
										  char *serverRelativeDirectory, 
										  char *m3u8TrackFile,
										  char *fileName);

DECLARE_INTERFACE_(IBoutechHLSSink, IUnknown)
{	

	STDMETHOD(ConfigHlsSink)(const WCHAR *rootDirectory, int streamIndex, int chunkDurationInSec, 
							 const WCHAR *hlsName, 
							 int keepDaysDuration, 
							 const WCHAR *HLSStreamName,
							 uint32_t bigBufferSizeInMega) PURE;
	STDMETHOD(SetHLSSinkCallback)(HLSSinkCallback hlsSinkCallback) PURE;
	STDMETHOD(CreateNewHlsSession)() PURE;
	STDMETHOD(ConfigLivePlayList)(int maxFiles) PURE;
};