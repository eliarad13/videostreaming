#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include "BauotechHLSSinkuids.h"
#include "BauotechHLSSink.h"
#include "myInterface.h"
#include "Shlwapi.h"
#include <thread>
#include <memory>

using namespace std;

// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
	&CLSID_BauotechHLSSink,                // Filter CLSID
    L"Bauotech HLS Sink",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
	L"Bauotech HLS Sink", &CLSID_BauotechHLSSink, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;


// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
						 CBaseFilter(NAME("Bauotech HLS Sink"), pUnk, pLock, CLSID_BauotechHLSSink),
    m_pDump(pDump)
{
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}


//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	m_pDump->m_running = 0;
	SetEvent(m_pDump->gKeepDaysEvent);

	if (m_pDump->hFile != NULL)
	{
		DWORD dwBytesWritten = 0;
		WriteFile(m_pDump->hFile, m_pDump->pBigBuffer, m_pDump->m_bigBufferWr, &dwBytesWritten, NULL);
		CloseHandle(m_pDump->hFile);
		m_pDump->hFile = NULL;
	}

	if (m_pDump->m_fileCounter > 0)
	{
		char manifestFileName[500];
		sprintf(manifestFileName, "%s\\hls.m3u8", m_pDump->m_rootDirectoryName);
		FILE *hlsManifestHandle = fopen(manifestFileName, "a+t");
		fprintf(hlsManifestHandle, "#EXT-X-ENDLIST\n");
		fclose(hlsManifestHandle);
		m_pDump->m_fileCounter = 0;
	}

	if (m_pDump->hFile != NULL)
		CloseHandle(m_pDump->hFile);
	m_pDump->hFile = NULL;
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);
	 

    return CBaseFilter::Pause();
}


//
// Run
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
    CAutoLock cObjectLock(m_pLock);

	if (m_pDump->pBigBuffer == NULL)
	{
		m_pDump->pBigBuffer = new uint8_t[188 * m_pDump->m_bigBufferSizeInMega];
	}
 
	m_pDump->StartHLSSession();

    return CBaseFilter::Run(tStart);
}
 
void CDump::StartHLSSession()
{

	time_t t = time(NULL);
	struct tm tm = *localtime(&t);
	m_lastTime = time(NULL);

	m_bigBufferWr = 0;

	hFile = NULL;
	m_chunkCount = 0;
	m_fileCounter = 0;
	m_canStart = false;
	hlsManifestHandle = NULL;
	m_running = 1;

	

	SYSTEMTIME st;
	GetSystemTime(&st);
	char sdate[100];
	sprintf(sdate, "%.4d_%.2d_%.2d_%.2d_%.2d_%.2d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);
	sprintf(m_rootDirectoryName, "%s\\%d\\%s_%s", m_originalRootDirectoryName, m_streamIndex, m_hlsName, sdate);

	sprintf(m_serverRelativeDirectory, "%d\\%s_%s", m_streamIndex, m_hlsName, sdate);

	sprintf(m_serverInstance, "%s_%s", m_hlsName, sdate);

	CreateDirectoryA(m_rootDirectoryName, 0);

	char temp[500];
	sprintf(temp, "%s\\%s", m_rootDirectoryName, m_HLSStreamName);
	CreateDirectoryA(temp, 0);

	shared_ptr<thread> t1 = make_shared<thread>(&CDump::KeepDaysThread, this);
	t1->detach();
}
void CDump::KeepDaysThread()
{
	DWORD time = 1000 * 60 * 60 * 24 * m_keepDaysDuration;	
	WaitForSingleObject(gKeepDaysEvent, time);
	m_timeForNewSession = 1;

}

//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)
{
	 
	m_timeToLogInSec = 600;
	InitTSParser();
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *)
{
    return S_OK;
}


//
// BreakConnect
//
// Break a connection
//
HRESULT CDumpInputPin::BreakConnect()
{
    if (m_pDump->m_pPosition != NULL) {
        m_pDump->m_pPosition->ForceRefresh();
    }

    return CRenderedInputPin::BreakConnect();
}


//
// ReceiveCanBlock
//
// We don't hold up source threads on Receive
//
STDMETHODIMP CDumpInputPin::ReceiveCanBlock()
{
    return S_FALSE;
}

void CDumpInputPin::InitTSParser()
{
	
	show_pcr_time = false;
	show_frame_pos = false;
	show_frame_pts = false;
	show_frame_dts = false;
	show_key_frame = true;


	vc = 0;
	sc = 0;
	offset = 0;
	pts = 0;
	dts = 0;
	vknown_type = false;
	aknown_type = false;
	 
	
	si.pid_ = 40;
	si.type_ = util::stream_info::stream_video;
	si.stream_type_ = tsParser.stream_type(std::string("H264"));
	streams.push_back(si);
	si.pid_ = 50;
	si.type_ = util::stream_info::stream_audio;
	si.stream_type_ = tsParser.stream_type(std::string("AAC"));
	streams.push_back(si);
	tsParser.init_streams(streams);
	
	mi.pid_ = 40;
	mi.is_video_ = true;
	mi.is_audio_ = false;

	 
}

STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
    CheckPointer(pSample,E_POINTER);

	if (m_pDump->m_running == false)
	{
		m_pFilter->Stop();
		return S_OK;
	}

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;
	  
    REFERENCE_TIME tStart, tStop;
    pSample->GetTime(&tStart, &tStop);
	 
   // pSample->GetActualDataLength()));

    m_tLast = tStart;
	 
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    }
	  
	double startPCR;
	double lastPCR;
	int receivedSize = pSample->GetActualDataLength();
	 
	
	util::mpegts_info info;
	DWORD dwBytesWritten = 0;
	int tsChunkIndex = 0;
 
	while (receivedSize > 0)
	{
		memset(&info, 0, sizeof(info));
		auto suc = tsParser.do_parser(pbData + tsChunkIndex, info);
		
		//if (info.type_ == util::mpegts_info::idr)
		if (info.pict_type_ == util::av_picture_type_i)
		{ 

			if (m_pDump->m_canStart == false)
			{	
				//char msgbuf[400];
				//sprintf(msgbuf, "info.pcr_ %I64d\n", info.pcr_);
				//OutputDebugStringA(msgbuf);
				m_pDump->pcrStart = info.pcr_;
				 
				m_pDump->m_canStart = true;
				m_pDump->CreateNewFile(0);
			}
			else 
			{
				//char msgbuf[400];
				//sprintf(msgbuf, "info.pcr_ %I64d\n", info.pcr_);
				//OutputDebugStringA(msgbuf);
				m_pDump->pcrStop = info.pcr_;
				double diff;
				if (m_pDump->pcrStop > m_pDump->pcrStart)
				{
					diff = (m_pDump->pcrStop - m_pDump->pcrStart) / 1000;
					m_pDump->diffWrap = 0;
				}
				else
				{
					m_pDump->diffWrap = (m_pDump->pcrStart - m_pDump->pcrStop) / 1000;
					m_pDump->pcrStart = info.pcr_;
					diff = 0;
				}
				//sprintf(msgbuf, "PCR Diff in sec %f\n", diff);
				//OutputDebugStringA(msgbuf);

				if ((diff + m_pDump->diffWrap) >= m_pDump->m_chunkDurationInSec || m_pDump->m_timeForNewSession)
				{

					if (m_pDump->pHlsSinkCallback != NULL)
						m_pDump->pHlsSinkCallback(m_pDump->m_fileCreationDate,
						m_pDump->m_serverInstance,
						m_pDump->m_chunkDurationInSec, 
						diff, 
						m_pDump->m_rootDirectoryName,
						m_pDump->m_serverRelativeDirectory,
						"hls.m3u8", 
						m_pDump->m_shortFileName);

					m_pDump->pcrStart = info.pcr_;					

					WriteFile(m_pDump->hFile, m_pDump->pBigBuffer, m_pDump->m_bigBufferWr, &dwBytesWritten, NULL);										
					if (m_pDump->m_timeForNewSession == 1)
					{
						m_pDump->m_timeForNewSession = 0;

						if (m_pDump->hFile != NULL)
						{
							CloseHandle(m_pDump->hFile);
							m_pDump->hFile = NULL;
						}
						m_pDump->UpdateManifest(diff);
						m_pDump->StartHLSSession();
						m_pDump->m_canStart = true;
					}

					m_pDump->CreateNewFile(diff);
					m_pDump->m_bigBufferWr = 0;
					if (m_pDump->m_bigBufferWr > (188 * m_pDump->m_bigBufferSizeInMega))
					{
						printf("dd\n");
					}
					memcpy(m_pDump->pBigBuffer + m_pDump->m_bigBufferWr, pbData + tsChunkIndex, 188);
					m_pDump->m_bigBufferWr += 188;
					receivedSize -= 188;
					tsChunkIndex += 188;
					continue;
				}
			}				 
		}
		if (m_pDump->m_canStart == true)
		{
			if (m_pDump->m_bigBufferWr > (188 * m_pDump->m_bigBufferSizeInMega))
			{
				printf("dd\n");
			}
			memcpy(m_pDump->pBigBuffer + m_pDump->m_bigBufferWr, pbData + tsChunkIndex, 188);
			m_pDump->m_bigBufferWr += 188;
		}
		tsChunkIndex += 188;
		receivedSize -= 188;
	}
	   
	return S_OK;// m_pDump->Write(pbData, pSample->GetActualDataLength());
}
 
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
    CUnknown(NAME("CDump"), pUnk),
    m_pFilter(NULL),
    m_pPin(NULL),
    m_pPosition(NULL),    
    m_pFileName(0) 
    
{
    ASSERT(phr);
    
    m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
    if (m_pFilter == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

	m_maxLivePlayListFiles = 60 * 2;
	m_keepDaysDuration = 2;
	m_timeForNewSession = 0;
	gKeepDaysEvent = CreateEvent(
		NULL,               // default security attributes
		FALSE,               // manual-reset event
		FALSE,              // initial state is nonsignaled
		TEXT("KeepDaysEvent")  // object name
		);

	if (gKeepDaysEvent == NULL)
	{	 
		return;
	}


	m_streamIndex = 1;
	pHlsSinkCallback = NULL;
	hFile = NULL;
	m_chunkCount = 0;
	m_fileCounter = 0;
	m_running = 0;
	m_chunkDurationInSec = 60;
	m_bigBufferWr = 0;
	pBigBuffer = NULL;
	m_bigBufferSizeInMega = 1000 * 1000;
	strcpy(m_rootDirectoryName, "c:\\temp\\BauotechDVRStorage");
	 

#if 0
	WIN32_FIND_DATAA fd;
	char file[500];
	sprintf(file, "%s\\hls*.ts", m_rootDirectoryName);
	HANDLE hFind = FindFirstFileA(file, &fd);
	if (hFind != INVALID_HANDLE_VALUE)
	{
		do
		{
			sprintf(file, "%s\\%s", m_rootDirectoryName, fd.cFileName);
			DeleteFileA(file);
		} while (FindNextFileA(hFind, &fd));
		FindClose(hFind);
	}

	char manifestFileName[500];
	sprintf(manifestFileName, "%s\\hls.m3u8", m_rootDirectoryName);

	if (PathFileExistsA(manifestFileName) == TRUE)
	{
		DeleteFileA(manifestFileName);
	}
#endif 

    m_pPin = new CDumpInputPin(this,GetOwner(),
                               m_pFilter,
                               &m_Lock,
                               &m_ReceiveLock,
                               phr);
    if (m_pPin == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

}
 
CDump::~CDump()
{

	if (pBigBuffer != NULL)
	{
		delete pBigBuffer;
		pBigBuffer = NULL;
	}

	if (m_pPin != NULL)
		delete m_pPin;
		if (m_pFilter != NULL)
    delete m_pFilter;
    delete m_pPosition;
    delete m_pFileName;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance


//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);

    // Do we have this interface

	if (riid == IID_IBoutechHLSSink) {
		return GetInterface((IBoutechHLSSink *) this, ppv);
	}
	else
    if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    else if (riid == IID_IMediaPosition || riid == IID_IMediaSeeking) {
        if (m_pPosition == NULL) 
        {

            HRESULT hr = S_OK;
            m_pPosition = new CPosPassThru(NAME("Dump Pass Through"),
                                           (IUnknown *) GetOwner(),
                                           (HRESULT *) &hr, m_pPin);
            if (m_pPosition == NULL) 
                return E_OUTOFMEMORY;

            if (FAILED(hr)) 
            {
                delete m_pPosition;
                m_pPosition = NULL;
                return hr;
            }
        }

        return m_pPosition->NonDelegatingQueryInterface(riid, ppv);
    } 

    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

} // NonDelegatingQueryInterface

  
//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}
 
bool CDump::CreateNewFile(double fileDuration)
{
	if (hFile != NULL)
	{
		CloseHandle(hFile);
		hFile = NULL;		
	}
		 		
	sprintf(m_shortFileName, "%s/hls_%d.ts", m_HLSStreamName, m_fileCounter);
	sprintf(m_fileName, "%s\\%s\\hls_%d.ts", m_rootDirectoryName, m_HLSStreamName, m_fileCounter);

	if (PathFileExistsA(m_fileName) == TRUE)
	{
		DeleteFileA(m_fileName);
	}

	UpdateManifest(fileDuration);


	hFile = CreateFileA(m_fileName,                // name of the write
		GENERIC_WRITE,          // open for writing
		FILE_SHARE_READ,                      // do not share
		NULL,                   // default security
		CREATE_ALWAYS,             // create new file only
		FILE_ATTRIBUTE_NORMAL | FILE_FLAG_WRITE_THROUGH, 
		//FILE_FLAG_WRITE_THROUGH ,
		 //FILE_FLAG_NO_BUFFERING,
		NULL);                  // no attr. template
	if (hFile == INVALID_HANDLE_VALUE)
		return false;


	SYSTEMTIME st;
	GetSystemTime(&st);
	sprintf(m_fileCreationDate, "%.4d_%.2d_%.2d_%.2d_%.2d_%.2d", st.wYear, st.wMonth, st.wDay, st.wHour, st.wMinute, st.wSecond);

	m_fileCounter++;

	return true;
	

}

void CDump::UpdateManifest(double fileDuration)
{

	if (m_fileCounter == 0)
	{
		char manifestFileName[500];
		sprintf(manifestFileName, "%s\\hls.m3u8", m_rootDirectoryName);
		hlsManifestHandle = fopen(manifestFileName, "w+t");
		fprintf(hlsManifestHandle, "#EXTM3U\n");
		fprintf(hlsManifestHandle, "#EXT-X-VERSION:3\n");
		fprintf(hlsManifestHandle, "#EXT-X-TARGETDURATION:60\n");
		fprintf(hlsManifestHandle, "#EXT-X-MEDIA-SEQUENCE:0\n");
		fprintf(hlsManifestHandle, "#EXT-X-PLAYLIST-TYPE:EVENT\n");
		fclose(hlsManifestHandle);
	}
	else if (m_fileCounter > 0)
	{
		char manifestFileName[500];
		sprintf(manifestFileName, "%s\\hls.m3u8", m_rootDirectoryName);
		hlsManifestHandle = fopen(manifestFileName, "a+t");
		fprintf(hlsManifestHandle, "#EXTINF:%f,\n", fileDuration);
		sprintf(m_prevFile, "%s/hls_%d.ts", m_HLSStreamName, m_fileCounter - 1);
		fprintf(hlsManifestHandle, "%s\n", m_prevFile);
		fclose(hlsManifestHandle);
	}
}


STDMETHODIMP CDump::ConfigLivePlayList(int maxFiles)
{
	m_maxLivePlayListFiles = maxFiles;
	return S_OK;
}
 
STDMETHODIMP CDump::ConfigHlsSink(const WCHAR *rootDirectory, 
								  int streamIndex, 
								  int chunkDurationInSec, 
								  const WCHAR *hlsName,
								  int keepDaysDuration,
								  const WCHAR *HLSStreamName,
								  uint32_t bigBufferSizeInMega)
{
	if (wcscmp(rootDirectory, L"") != 0)
	{		
		wcstombs(m_originalRootDirectoryName, rootDirectory, sizeof(m_originalRootDirectoryName));
	}
	wcstombs(m_hlsName, hlsName, sizeof(hlsName));

	wcstombs(m_HLSStreamName, HLSStreamName, sizeof(HLSStreamName));
  
	m_bigBufferSizeInMega = bigBufferSizeInMega;
	m_streamIndex = streamIndex;
	m_keepDaysDuration = keepDaysDuration;
	
	m_chunkDurationInSec = chunkDurationInSec;
	return S_OK;
}

STDMETHODIMP CDump::CreateNewHlsSession()
{
	SetEvent(gKeepDaysEvent);
	return S_OK;

}
STDMETHODIMP CDump::SetHLSSinkCallback(HLSSinkCallback hlsSinkCallback)
{
	pHlsSinkCallback = hlsSinkCallback;
	return S_OK;
}