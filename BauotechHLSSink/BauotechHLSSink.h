#pragma once 
#include "myInterface.h" 
#include "mpegts.hpp"
#include <iostream>
#include <time.h>

class CDumpInputPin;
class CDump;
class CDumpFilter;



// Main filter object

class CDumpFilter : public CBaseFilter
{
    CDump * const m_pDump;

public:

    // Constructor
    CDumpFilter(CDump *pDump,
                LPUNKNOWN pUnk,
                CCritSec *pLock,
                HRESULT *phr);

    // Pin enumeration
    CBasePin * GetPin(int n);
    int GetPinCount();

    // Open and close the file as necessary
    STDMETHODIMP Run(REFERENCE_TIME tStart);
    STDMETHODIMP Pause();
    STDMETHODIMP Stop();
	  
};


//  Pin object

class CDumpInputPin : public CRenderedInputPin
{
    CDump    * const m_pDump;           // Main renderer object
    CCritSec * const m_pReceiveLock;    // Sample critical section
    REFERENCE_TIME m_tLast;             // Last sample receive time

public:

    CDumpInputPin(CDump *pDump,
                  LPUNKNOWN pUnk,
                  CBaseFilter *pFilter,
                  CCritSec *pLock,
                  CCritSec *pReceiveLock,
                  HRESULT *phr);

    // Do something with this media sample
    STDMETHODIMP Receive(IMediaSample *pSample);
    
    STDMETHODIMP ReceiveCanBlock();

	void InitTSParser();

    // Check if the pin can support this specific proposed type and format
    HRESULT CheckMediaType(const CMediaType *);

    // Break connection
    HRESULT BreakConnect();

	util::mpegts_parser tsParser;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;

	int m_timeToLogInSec;
	
  
};


//  CDump object which has filter and pin members

class CDump : public CUnknown, public IBoutechHLSSink
{
    friend class CDumpFilter;
    friend class CDumpInputPin;

    CDumpFilter   *m_pFilter;       // Methods for filter interfaces
    CDumpInputPin *m_pPin;          // A simple rendered input pin

    CCritSec m_Lock;                // Main renderer critical section
    CCritSec m_ReceiveLock;         // Sublock for received samples

    CPosPassThru *m_pPosition;      // Renderer position controls
    LPOLESTR m_pFileName;           // The filename where we dump


public:

    DECLARE_IUNKNOWN

    CDump(LPUNKNOWN pUnk, HRESULT *phr);
    ~CDump();

    static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);

    
    // Write raw data stream to a file
    HRESULT Write(PBYTE pbData, LONG lDataLength);

	 

	STDMETHODIMP ConfigLivePlayList(int maxFiles);
	STDMETHODIMP ConfigHlsSink(const WCHAR *rootDirectory, int streamIndex, 
							   int chunkDurationInSec, 
							   const WCHAR *hlsName, int keepDaysDuration, 
							   const WCHAR *HLSStreamName, 
							   uint32_t bigBufferSizeInMega);

	STDMETHODIMP SetHLSSinkCallback(HLSSinkCallback hlsSinkCallback);
	STDMETHODIMP CreateNewHlsSession();

	HLSSinkCallback pHlsSinkCallback;
 
	int m_chunkCount;
	int m_running;

	HANDLE gKeepDaysEvent;
	void KeepDaysThread();
	int m_keepDaysDuration;
	int m_timeForNewSession;

	FILE *hlsManifestHandle;
	time_t m_lastTime;
	char m_rootDirectoryName[500];
	char m_serverRelativeDirectory[500];
	char m_serverInstance[500];
	char m_prevFile[500];
	char m_originalRootDirectoryName[500];
	char m_hlsName[500];
	char m_HLSStreamName[100];
	int m_streamIndex;
	int m_maxLivePlayListFiles;
	
	char m_startDateTime[200];
	char tsDir[250];
	char tsFile[250];
	bool CreateNewFile(double fileDuration);
	void UpdateManifest(double fileDuration);
	unsigned long m_fileCounter;
	bool m_canStart;
	int64_t pcrStart;
	int64_t pcrStop;
	int64_t diffWrap;
	int m_chunkDurationInSec;
	char m_fileCreationDate[500];
	uint8_t *pBigBuffer;
	uint32_t m_bigBufferWr;
	uint32_t m_bigBufferSizeInMega;


	void StartHLSSession();

private:

    // Overriden to say what interfaces we support where
    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);
	
	HANDLE hFile;
	char m_fileName[500];
	char m_shortFileName[500];
};

