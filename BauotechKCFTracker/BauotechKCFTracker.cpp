#include <streams.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include "kcftracker.hpp"
//#include <dirent.h>
#include <constants_c.h>
#include <time.h>
#include <Windows.h>

// Eliminate two expected level 4 warnings from the Microsoft compiler.
// The class does not have an assignment or copy operator, and so cannot
// be passed by value.  This is normal.  This file compiles clean at the
// highest (most picky) warning level (-W4).
#pragma warning(disable: 4511 4512)


using namespace std;
using namespace cv;


#include <initguid.h>
#if (1100 > _MSC_VER)
#include <olectlid.h>
#else
#include <olectl.h>
#endif
#include "BauotechKCFTrackeruids.h"             
#include "IBauotechKCFTracker.h"              / 
#include "BauotechKCFTracker.h"



//------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------


#define DbgFunc(a) DbgLog(( LOG_TRACE                        \
                          , 2                                \
                          , TEXT("CBauotechTimeshiftTransform(Instance %d)::%s") \
                          , m_nThisInstance                  \
                          , TEXT(a)                          \
                         ));


// Self-registration data structures

const AMOVIESETUP_MEDIATYPE
sudPinTypes =   { &MEDIATYPE_Audio        // clsMajorType
                , &MEDIASUBTYPE_NULL };   // clsMinorType

const AMOVIESETUP_PIN
psudPins[] = { { L"Input"            // strName
               , FALSE               // bRendered
               , FALSE               // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Output"           // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             , { L"Output"           // strName
               , FALSE               // bRendered
               , TRUE                // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Input"            // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             };

const AMOVIESETUP_FILTER
sudGargle = { &CLSID_BauotechKCFTracker                   // class id
            , L"Bauotech KCF Tracker"                    // strName
            , MERIT_DO_NOT_USE                // dwMerit
            , 2                               // nPins
            , psudPins                        // lpPin
            };

// Needed for the CreateInstance mechanism
CFactoryTemplate g_Templates[1]= { { L"Bauotech KCF Tracker"
, &CLSID_BauotechKCFTracker
, BauotechKCFTracker::CreateInstance
                                   , NULL
                                   , &sudGargle
                                   }                                 
                                 };

int g_cTemplates = sizeof(g_Templates)/sizeof(g_Templates[0]);

// initialise the static instance count.
int BauotechKCFTracker::m_nInstanceCount = 0;



BauotechKCFTracker::~BauotechKCFTracker()
{

}

BauotechKCFTracker::BauotechKCFTracker(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr)
	: CTransInPlaceFilter(tszName, punk, CLSID_BauotechKCFTracker, phr)
    , m_SamplesPerSec     (0)
    , m_BytesPerSample    (0)
    , m_Channels          (0)
    , m_Phase             (0)
    , m_Shape             (0)
{
    m_nThisInstance = ++m_nInstanceCount; // Useful for debug, no other purpose
	

	HOG = true;
	FIXEDWINDOW = false;
	MULTISCALE = true;
	SILENT = true;
	LAB = false;

	// Create KCFTracker object
	tracker = new KCFTracker(HOG, FIXEDWINDOW, MULTISCALE, LAB);

	// Using min and max of X and Y for groundtruth rectangle
	xMin = 100;
	yMin = 100;
	width = 100;
	height = 100;
	 

	ext = ".row";

	nFrames = 0;
	 

}  


 
CUnknown * WINAPI BauotechKCFTracker::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
	BauotechKCFTracker *pNewObject = new BauotechKCFTracker(NAME("Bauotech KCF Tracker Filter"), punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

}


STDMETHODIMP BauotechKCFTracker::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
    CheckPointer(ppv,E_POINTER);

	if (riid == IID_IBaoutechKCFTracker) {
		return GetInterface((IBauotechKCFTracker *) this, ppv);

    } else if (riid == IID_IPersistStream) {
        return GetInterface((IPersistStream *) this, ppv);

    } else {
        // Pass the buck
        return CTransInPlaceFilter::NonDelegatingQueryInterface(riid, ppv);
    }

} // NonDelegatingQueryInterface

  
STDMETHODIMP BauotechKCFTracker::Pause()
{

	//m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP BauotechKCFTracker::Stop()
{

	//m_pOutputPin->m_graphStat = GRAPH_STOP;

 

	return CBaseFilter::Stop();
}

STDMETHODIMP BauotechKCFTracker::Run(REFERENCE_TIME tStart)
{
	nFrames = 0;
	return CBaseFilter::Run(tStart);
}
 

HRESULT BauotechKCFTracker::Transform(IMediaSample *pSample)
{
    CheckPointer(pSample,E_POINTER);   
    
   
    BYTE *pSampleBuffer;
    int iSize = pSample->GetActualDataLength();
    pSample->GetPointer(&pSampleBuffer);

#if 0 
	Mat frame(600, 800, CV_8UC3, pSampleBuffer);
	//Mat frame(600, 800, CV_8UC4, imagedata);
	flip(frame, frame, 0);

	if (nFrames == 0)
	{
		clock_t t;
		t = clock();
		tracker->init(Rect(xMin, yMin, width, height), frame);
		//rectangle(frame, Point(xMin, yMin), Point(xMin + width, yMin + height), Scalar(0, 255, 255), 1, 8);
		t = clock() - t;
		double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds 
		printf("time_taken = %f", time_taken);
		printf("%d,%d  %d,%d\n", result.x, result.y, result.width, result.height);
	}
	// Update
	else {
		clock_t t;
		t = clock();
		result = tracker->update(frame);
		t = clock() - t;
		double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds 
		printf("time_taken = %f", time_taken);
		printf("%d,%d  %d,%d\n", result.x, result.y, result.width, result.height);
		//rectangle(frame, Point(result.x, result.y), Point(result.x + result.width, result.y + result.height), Scalar(0, 255, 255), 1, 8);
		//resultsFile << result.x << "," << result.y << "," << result.width << "," << result.height << endl;
	}

	nFrames++;
#endif 

#if 0
	Mat frame(600, 800, CV_8UC3, pSampleBuffer);
	//Mat frame(600, 800, CV_8UC4, imagedata);
	flip(frame, frame, 0);

	cvtColor(frame, frame, cv::COLOR_BGR2GRAY);

	namedWindow("Display window", WINDOW_NORMAL);// Create a window for display.
	imshow("Display window", frame);
	waitKey();
#endif 

	//pTimeShiftCallback(timeshift_wr, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
	   

    return NOERROR;

} // Transform


//
// CheckInputType
//
// Override CTransformFilter method.
// Part of the Connect process.
// Ensure that we do not get connected to formats that we can't handle.
// We only work for wave audio, 8 or 16 bit, uncompressed.
//
HRESULT BauotechKCFTracker::CheckInputType(const CMediaType *pmt)
{
	 
	if (pmt->majortype == MEDIATYPE_Video &&
		(pmt->subtype == MEDIASUBTYPE_RGB24) &&
		pmt->formattype == FORMAT_VideoInfo)
		return NOERROR;
	else
		return E_FAIL;

} // CheckInputType


//
// SetMediaType
//
// Override CTransformFilter method.
// Called when a connection attempt has succeeded. If the output pin
// is being connected and the input pin's media type does not agree then we
// reconnect the input (thus allowing its media type to change,) and vice versa.
//
HRESULT BauotechKCFTracker::SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);
    //DbgFunc("SetMediaType");


	return NOERROR;
 

} // SetMediaType
 
STDMETHODIMP BauotechKCFTracker::SetTimeShiftCallback(TimeShiftCallback p)
{

	pTimeShiftCallback = p;
	return S_OK;
}
 
////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

STDAPI DllRegisterServer()
{
  return AMovieDllRegisterServer2( TRUE );
}


STDAPI DllUnregisterServer()
{
  return AMovieDllRegisterServer2( FALSE );
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

#pragma warning(disable: 4514) // "unreferenced inline function has been removed"



