//------------------------------------------------------------------------------
// File: IGargle.h
//
// Desc: DirectShow sample code - custom interface that allows the user
//       to adjust the modulation rate.  It defines the interface between
//       the user interface component (the property sheet) and the filter
//       itself.  This interface is exported by the code in Gargle.cpp and
//       is used by the code in GargProp.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __IGARGLE__
#define __IGARGLE__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

 

// {ACB89EBE-6BDF-4414-9CF2-44F50008F726}
DEFINE_GUID(IID_IBaoutechKCFTracker,
	0xacb89ebe, 0x6bdf, 0x4414, 0x9c, 0xf2, 0x44, 0xf5, 0x0, 0x8, 0xf7, 0x26);




typedef void(__stdcall * TimeShiftCallback)(uint64_t loaction, uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec);

 
DECLARE_INTERFACE_(IBauotechKCFTracker, IUnknown) {

	STDMETHOD(SetTimeShiftCallback)(TimeShiftCallback p) PURE;


};


#ifdef __cplusplus
}
#endif

#endif // __IGARGLE__
