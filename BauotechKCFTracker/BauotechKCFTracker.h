#pragma once 
#include <stdint.h>
#include <time.h>
 
class BauotechKCFTracker
	// Inherited classes
	: public CTransInPlaceFilter       // Main DirectShow interfaces
	, public IBauotechKCFTracker                   // Needed for properties only.

{  

public:

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);

	DECLARE_IUNKNOWN;

	//
	// --- CTransInPlaceFilter Overrides --
	//

	HRESULT CheckInputType(const CMediaType *mtIn);

	// Basic COM - used here to reveal our property interface.
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);



	STDMETHODIMP SetTimeShiftCallback(TimeShiftCallback p);
	

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();


	bool HOG = true;
	bool FIXEDWINDOW = false;
	bool MULTISCALE = true;
	bool SILENT = true;
	bool LAB = false;
	KCFTracker *tracker = NULL;

	// Frame readed
	Mat frame;
	// Tracker results
	Rect result;

	// Using min and max of X and Y for groundtruth rectangle
	float xMin = 100;
	float yMin = 100;
	float width = 100;
	float height = 100;


	string frameName;

	string ext = ".row";

	// Frame counter
	int nFrames = 0;



	TimeShiftCallback  pTimeShiftCallback;
private:

	// Constructor
	BauotechKCFTracker(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr);
	~BauotechKCFTracker();

	// Overrides the PURE virtual Transform of CTransInPlaceFilter base class
	// This is where the "real work" is done.
	HRESULT Transform(IMediaSample *pSample);

	// This is where the real work is really done (called from Transform)
	void MessItAbout(PBYTE pb, int cb);

	// Overrides a CTransformInPlace function.  Called as part of connecting.
	virtual HRESULT SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt);



	// If there are multiple instances of this filter active, it's
	// useful for debug messages etc. to know which one this is.
	// This variable has no other purpose.
	static int m_nInstanceCount;                   // total instances
	int m_nThisInstance;

	int                 m_Shape;               // 0==triangle, 1==square
	int                 m_SamplesPerSec;       // Current sample format
	int                 m_BytesPerSample;      // Current sample format
	int                 m_Channels;            // Current sample format
	int                 m_Phase;               // See MessItAbout in gargle.cpp
	CCritSec            m_GargleLock;          // To serialise access.

};  
