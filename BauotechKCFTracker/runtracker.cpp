#include <iostream>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <opencv2\core\core.hpp>
#include <opencv2\highgui\highgui.hpp>
#include "kcftracker.hpp"
//#include <dirent.h>
#include <constants_c.h>
#include <time.h>
#include <Windows.h>


using namespace std;
using namespace cv;

bool LoadRowImage8Bit(char *fileName, int width, int height, Mat *img)
{
	
	FILE *fp = NULL;
	char *imagedata = NULL;
	int framesize = width * height;

	//Open raw Bayer image.
	fp = fopen(fileName, "rb");

	//Memory allocation for bayer image data buffer.
	imagedata = (char*)malloc(sizeof(char) * framesize);

	//Read image data and store in buffer.
	fread(imagedata, sizeof(char), framesize, fp);

	//Create Opencv mat structure for image dimension. For 8 bit bayer, type should be CV_8UC1.
	img->create(width, height, CV_8UC1);

	memcpy(img->data, imagedata, framesize);

	free(imagedata);

	fclose(fp);

	return true;
}

bool LoadRowImage24Bit(char *fileName, int width, int height, Mat *img)
{

	
	FILE *fp = NULL;
	uchar *imagedata = NULL;
	int framesize = width * height * 3;

	//Open raw Bayer image.
	fp = fopen(fileName, "rb");
	if (fp == NULL)
		return false;

	//Memory allocation for bayer image data buffer.
	imagedata = (uchar*)malloc(sizeof(char) * framesize);

	//Read image data and store in buffer.
	fread(imagedata, sizeof(uchar), framesize, fp);

	  
	//Create Opencv mat structure for image dimension. For 8 bit bayer, type should be CV_8UC1.
	img->create(width  , height, CV_8UC3);
	memcpy(img->data, imagedata, framesize); 
	 

	free(imagedata);
	fclose(fp);

	return true;
}

bool LoadRowImage32Bit(char *fileName, int width, int height, Mat *img)
{


	FILE *fp = NULL;
	char *imagedata = NULL;
	int framesize = width * height * 4;

	//Open raw Bayer image.
	fp = fopen(fileName, "rb");

	//Memory allocation for bayer image data buffer.
	imagedata = (char*)malloc(sizeof(char) * framesize);

	//Read image data and store in buffer.
	fread(imagedata, sizeof(char), framesize, fp);

	//Create Opencv mat structure for image dimension. For 8 bit bayer, type should be CV_8UC1.
	img->create(width, height, CV_8UC4);

	memcpy(img->data, imagedata, framesize);

	free(imagedata);

	fclose(fp);

	return true;
}
  
int ShowImage(char *imageName)
{
	Mat image;
	 
	image = imread(imageName, CV_LOAD_IMAGE_COLOR);   // Read the file

	if (!image.data)                              // Check for invalid input
	{
		cout << "Could not open or find the image" << std::endl;
		return -1;
	}

	namedWindow("Display window", WINDOW_NORMAL);// Create a window for display.
	imshow("Display window", image);

	return 1;
}

bool AddBMPHeaderAndSave(char *rowFileName, int width, int height, int bitsPerPixel)
{

	FILE *f;
	 
	int filesize = 54 + 3 * width * height;  //w is your image width, h is image height, both int
	 
	unsigned char bmpfileheader[14] = { 'B','M', 0,0,0,0, 0,0, 0,0, 54,0,0,0 };
	unsigned char bmpinfoheader[40] = { 40,0,0,0, 0,0,0,0, 0,0,0,0, 1,0, 24,0 };
	unsigned char bmppad[3] = { 0,0,0 };
 
	bmpinfoheader[14] = bitsPerPixel;	 

	bmpfileheader[2] = (unsigned char)(filesize);
	bmpfileheader[3] = (unsigned char)(filesize >> 8);
	bmpfileheader[4] = (unsigned char)(filesize >> 16);
	bmpfileheader[5] = (unsigned char)(filesize >> 24);

	bmpinfoheader[4] = (unsigned char)(width);
	bmpinfoheader[5] = (unsigned char)(width >> 8);
	bmpinfoheader[6] = (unsigned char)(width >> 16);
	bmpinfoheader[7] = (unsigned char)(width >> 24);
	bmpinfoheader[8] = (unsigned char)(height);
	bmpinfoheader[9] = (unsigned char)(height >> 8);
	bmpinfoheader[10] = (unsigned char)(height >> 16);
	bmpinfoheader[11] = (unsigned char)(height >> 24);

	f = fopen("c:\\img.bmp", "wb");
	fwrite(bmpfileheader, 1, 14, f);
	fwrite(bmpinfoheader, 1, 40, f);

	FILE *fp = NULL;
	uchar *imagedata = NULL;
	int framesize = width * height * 3;

	//Open raw Bayer image.
	fp = fopen(rowFileName, "rb");
	if (fp == NULL)
		return false;

	//Memory allocation for bayer image data buffer.
	imagedata = (uchar*)malloc(sizeof(char) * framesize);

	//Read image data and store in buffer.
	fread(imagedata, sizeof(uchar), framesize, fp);
	fwrite(imagedata, 1, framesize, f);
	 
	free(imagedata);
	fclose(fp);
	fclose(f);

	return true;
 
}


bool AddBMPHeaderAndLoad(char *rowFileName, int width, int height, uchar *imagedata , int byteForPixel)
{

	FILE *f;
	
	int framesize = width * height * byteForPixel; // 3 for 24 bit , 4 for 32 bit 
	   
	FILE *fp = NULL;
	 
	  
	fp = fopen(rowFileName, "rb");
	if (fp == NULL)
		return false;
	 

	//Read image data and store in buffer.
	fread(imagedata, sizeof(uchar), framesize, fp);
 	fclose(fp);
	  
	return true;

}

int main(int argc, char* argv[]){

	if (argc > 5) return -1;

	bool HOG = true;
	bool FIXEDWINDOW = false;
	bool MULTISCALE = true;
	bool SILENT = true;
	bool LAB = false;

	int framesize = 800 * 600 * 4;
	uchar *imagedata = (uchar*)malloc(sizeof(char) * framesize);

#if 0 
	Mat img;
	

	AddBMPHeaderAndLoad("C:\BauotechFrames\\samplegrabber_snapshot_0.row", 800, 600, imagedata);

	//Mat image(800, 600, CV_8UC3, Scalar(255, 00, 00));
	Mat image(600, 800, CV_8UC3, imagedata);
	flip(image, image, 0);
	 
	//cvtColor(image, RGBImage, COLOR_BGR2RGB);
	//memcpy(image.data, imagedata, framesize + 54);

	namedWindow("Display window", WINDOW_NORMAL);// Create a window for display.
	imshow("Display window", image);
	waitKey();

	// Show our image inside it.
	 
	Mat p;
#endif 
 
	//LoadRowImage24Bit("D:\\Customers\\ArielL\\MegaPopPrivate\\master\\18_3_2019\\megapopprivate\\PanoramaApi\\SampleGrabberTestApp\\bin\\Debug\\Data\\row_800_600_6.row", 800, 600, &p);
#if 0
	
	LoadRowImage24Bit("C:\BauotechFrames\\samplegrabber_snapshot_0.row", 800, 600, &p);
	 
	namedWindow("Display window", WINDOW_NORMAL);// Create a window for display.
	imshow("Display window", p);
	waitKey();
#endif 
	//ShowImage("C:\BauotechFrames\\samplegrabber_snapshot_0.bmp");
	//waitKey();



	for(int i = 0; i < argc; i++){
		if ( strcmp (argv[i], "hog") == 0 )
			HOG = true;
		if ( strcmp (argv[i], "fixed_window") == 0 )
			FIXEDWINDOW = true;
		if ( strcmp (argv[i], "singlescale") == 0 )
			MULTISCALE = false;
		if ( strcmp (argv[i], "show") == 0 )
			SILENT = false;
		if ( strcmp (argv[i], "lab") == 0 ){
			LAB = true;
			HOG = true;
		}
		if ( strcmp (argv[i], "gray") == 0 )
			HOG = false;
	}
	
	// Create KCFTracker object
	KCFTracker tracker(HOG, FIXEDWINDOW, MULTISCALE, LAB);

	// Frame readed
	Mat frame;

	// Tracker results
	Rect result;

	 
	// Using min and max of X and Y for groundtruth rectangle
	float xMin = 100; 
	float yMin = 100; 
	float width = 100; 
	float height = 100; 

	 
	string frameName;
	 
	string ext = ".row";

	// Frame counter
	int nFrames = 0;
	 
	//while ( getline(listFramesFile, frameName) ){
		//frameName = frameName;
	while (true)
	{
		  
		 
		  
		frameName = "C:\\BauotechFrames\\samplegrabber_snapshot_" + std::to_string(nFrames) + ext;
		// Read each frame from the list
		//frame = imread(frameName, CV_LOAD_IMAGE_COLOR);
		//if (frame.empty())
			//break;

		if (AddBMPHeaderAndLoad((char *)frameName.c_str(), 800, 600, imagedata, 4) == false)
			break;

		//Mat frame(800, 600, CV_8UC4, Scalar(255, 00, 00,00));
		Mat frame(600, 800, CV_8UC3, imagedata);
		//Mat frame(600, 800, CV_8UC4, imagedata);
		flip(frame, frame, 0);

#if 0 
		namedWindow("Display window", WINDOW_NORMAL);// Create a window for display.
		imshow("Display window", frame);
		waitKey();
		return 1;
#endif 


		// First frame, give the groundtruth to the tracker
		if (nFrames == 0)
		{
			clock_t t;
			t = clock();
			tracker.init(Rect(xMin, yMin, width, height), frame);
			//rectangle(frame, Point(xMin, yMin), Point(xMin + width, yMin + height), Scalar(0, 255, 255), 1, 8);
			t = clock() - t;
			double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds 
			printf("time_taken = %f", time_taken);
			printf("%d,%d  %d,%d\n", result.x, result.y, result.width, result.height);
		}
		// Update
		else {
			clock_t t;
			t = clock();
			result = tracker.update(frame);
			t = clock() - t;
			double time_taken = ((double)t) / CLOCKS_PER_SEC; // in seconds 
			printf("time_taken = %f", time_taken);
			printf("%d,%d  %d,%d\n", result.x, result.y, result.width, result.height);
			//rectangle(frame, Point(result.x, result.y), Point(result.x + result.width, result.y + result.height), Scalar(0, 255, 255), 1, 8);
			//resultsFile << result.x << "," << result.y << "," << result.width << "," << result.height << endl;
		}

		nFrames++;

		if (!SILENT) {
			 
			namedWindow("Display window", WINDOW_AUTOSIZE);// Create a window for display.
			imshow("Display window", frame);
			waitKey();
		}
	}	
}
