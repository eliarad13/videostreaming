//------------------------------------------------------------------------------
// File: GargUIDs.h
//
// Desc: DirectShow sample code - definition of CLSIDs.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __GARGUIDS__
#define __GARGUIDS__

#ifdef __cplusplus
extern "C" {
#endif

 
// {BE8BE809-D6DA-455E-B288-5A3B75F5426A}
DEFINE_GUID(CLSID_BauotechKCFTracker,
	0xbe8be809, 0xd6da, 0x455e, 0xb2, 0x88, 0x5a, 0x3b, 0x75, 0xf5, 0x42, 0x6a);



// {0C44DD70-04CC-4B57-8B9B-753B586D4601}
DEFINE_GUID(CLSID_BauotechKCFTrackerProp,
	0xc44dd70, 0x4cc, 0x4b57, 0x8b, 0x9b, 0x75, 0x3b, 0x58, 0x6d, 0x46, 0x1);

 
 


#ifdef __cplusplus
}
#endif

#endif // __GARGUIDS__
