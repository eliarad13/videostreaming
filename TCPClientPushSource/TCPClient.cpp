#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <winsock2.h>
#include <stdio.h>
#include <Windows.h>
#include <thread>
#include "TCPClient.h"
#include <wxdebug.h>
#include "LiveSource.h"
#include <io.h> 
#define access    _access_s

using namespace std;

extern CLiveSourceStream* g_pOutputPin;
extern GRAPHSTAT  m_graphStat;

FILE *PreFileHandle = NULL;
bool m_PushFinished = false;

CTCPClient::~CTCPClient()
{
	m_running = false;
	WSACleanup();
}

void CTCPClient::ReceiveThread()
{
	int SelectTiming;
	while (m_running)
	{
		SelectTiming = recvfromTimeOutTCP(ReceivingSocket, 1, 0);
		ProcessMessage(SelectTiming);
	}

}

bool FileExists(const std::string &Filename)
{
	return access(Filename.c_str(), 0) == 0;
}

int CTCPClient::GetFifoSize()
{

	if (m_writeIndex == m_readIndex)
		return 0;

	if (m_writeIndex > m_readIndex)
		return m_writeIndex - m_readIndex;

	return (FIFO_SIZE - m_readIndex) + m_writeIndex;
}

  
void CTCPClient::ProcessMessage(int SelectTiming)
{
	SOCKADDR_IN        SenderAddr;
	SenderAddrSize = sizeof(SenderAddr);

	switch (SelectTiming)
	{
		case 0:
			// Timed out, do whatever you want to handle this situation
			//printf("Server: Timeout lor while waiting you bastard client!...\n");
		break;
		case -1:
			// Error occurred, maybe we should display an error message?
			// Need more tweaking here and the recvfromTimeOutUDP()...
			//printf("Server: Some error encountered with code number: %ld\n", WSAGetLastError());
		break;
		default:
		{
			while (m_running)
			{
				if (m_graphStat == GRAPH_RUN)
				{			
					g_pOutputPin->AddFrame(ReceivingSocket);
				}
				else
				{							
					if (FileExists("c:\\avc_4_demux.ts") == true)
					{
						if (PreFileHandle == NULL)
						{
							PreFileHandle = fopen("c:\\avc_4_demux.ts", "r+b");
						}
						if (m_PushFinished == false)
						{
							g_pOutputPin->AddFrameFromFile(PreFileHandle);
						}
						else
						{
							Sleep(100);
						}
					}
					else {
						g_pOutputPin->AddFrame(ReceivingSocket);
						Sleep(100);
					}
					 
				}
			}
		}
	}
}

// A sample of the select() return value

int CTCPClient::recvfromTimeOutTCP(SOCKET socket, long sec, long usec)

{

	// Setup timeval variable

	struct timeval timeout;

	struct fd_set fds;



	timeout.tv_sec = sec;

	timeout.tv_usec = usec;

	// Setup fd_set structure

	FD_ZERO(&fds);

	FD_SET(socket, &fds);

	// Return value:

	// -1: error occurred

	// 0: timed out

	// > 0: data ready to be read

	return ::select(0, &fds, 0, 0, &timeout);

}

const wchar_t *CTCPClient::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

void CTCPClient::ConnectThread()
{
	m_tryConnecting = true;
	while (m_tryConnecting == true)
	{
		int iResult = connect(ReceivingSocket, (struct sockaddr *)&server, sizeof(server));
		if (iResult != SOCKET_ERROR) 
		{
			break;
		}
		Sleep(1000);
	}
}


void CTCPClient::StartReceiveThread()
{
	m_running = true;
	m_receiveThread = make_shared<thread>(&CTCPClient::ReceiveThread, this);
}

int CTCPClient::Setup(bool allInterface)
{
	int iResult;

	m_running = true;

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		printf("Server: Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return -1;
	}
	  
	server.sin_addr.s_addr = inet_addr(m_ipAddress);
	server.sin_family = AF_INET;
	server.sin_port = htons(m_port);
	  
	m_connectThread = make_shared<thread>(&CTCPClient::ConnectThread, this);
	  
	return 1;

}

void CTCPClient::Pause()
{

}
void CTCPClient::Stop()
{
	m_running = false;
	 
	if (m_receiveThread != NULL)
	   m_receiveThread->join();

	if (ReceivingSocket != INVALID_SOCKET)
		closesocket(ReceivingSocket);
	ReceivingSocket = INVALID_SOCKET;
	
		 
}
void CTCPClient::Start()
{

 
}