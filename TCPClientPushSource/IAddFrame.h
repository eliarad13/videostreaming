#pragma once

#define LIVE_FILTER_NAME TEXT("Bauotech TCP Client Push source")
#define LIVE_OUTPIN_NAME L"Out"
 
 
// {9F21E8B6-11E2-452b-B245-4FB1A584C083}
static const GUID IID_ILiveSource =
{ 0x9f21e8b6, 0x11e2, 0x452b, { 0xb2, 0x45, 0x4f, 0xb1, 0xa5, 0x84, 0xc0, 0x83 } };

 

// {B5c19007-D2F1-4ca4-9DCA-15123fba0e84}
static const GUID CLSID_CLiveSource =
{ 0xB5c19007, 0xD2F1, 0x4ca4, { 0x9d, 0xca, 0x15, 0x12, 0x3f, 0xba, 0xe, 0x84 } };

 
// {B8F2C0FE-9EA0-4ec5-8B40-99159E86A627}
static const GUID IID_ITCPPushSource =
{ 0xb8f2c0fe, 0x9ea0, 0x4ec5, { 0x8b, 0x40, 0x99, 0x15, 0x9e, 0x86, 0xa6, 0x27 } };

 

DECLARE_INTERFACE_(ILiveSource, IUnknown)
{
	// Adds bitmap to the video sequence
	STDMETHOD(AddFrame)(HBITMAP hBmp) PURE;

	// Adds pixel data buffer to the video sequence
	STDMETHOD(AddFrame)(BYTE* pBuffer, DWORD size) PURE;

	// Set the video frame info.
	// Default value is width = 704, height = 576 (4CIF) and 32 bits per pixel
	STDMETHOD(SetBitmapInfo)(BITMAPINFOHEADER& bInfo) PURE;

	// Set the expected frame rate of the video.
	// Value should be in range of [0,30]
	// Default value is 0
	STDMETHOD(SetFrameRate)(int frameRate) PURE;
};



DECLARE_INTERFACE_(ITCPPushSource, IUnknown)
{
	STDMETHOD(StartRecording)(WCHAR *FileName) PURE;
	STDMETHOD(StartStreaming)(WCHAR *IpAddress, int port) PURE;
	STDMETHOD(SetIpAddress)(WCHAR *IpAddress) PURE;
	STDMETHOD(SetPort)(int port) PURE;
	STDMETHOD(SetVideoResolution)(int width, int height) PURE;

};
