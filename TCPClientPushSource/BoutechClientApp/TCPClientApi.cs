﻿using Elecard.Helpers;
using Elecard.Utilities;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace BoutechClientApp
{
    public class TCPClientApi
    {
        protected Graph m_graphBuilder;
        protected MediaControl m_mediaControl;
        Filter m_tcpSource = null;
        Filter m_pushDemuxer;
        protected ModuleConfig m_sourceModuleConfig;
        Filter m_decoder = null;
        Filter m_rendererFilter = null;
        public string Build()
        {
            try
            {
                m_graphBuilder = new Graph();
                m_mediaControl = m_graphBuilder.ToMediaControl();
                AddTCPSource();
                AddPushDemux();
                
                ArrayList tcpSourceOutPins = m_tcpSource.GetOutputPinList();			 
			    Pin tcpOutputPin = (Pin)tcpSourceOutPins[0];
                

                ArrayList demuxInPins = m_pushDemuxer.GetInputPinList();
                Pin demuxInPin = (Pin)demuxInPins[0];

                m_graphBuilder.Connect(tcpOutputPin, demuxInPin);


                ArrayList outputPinList = m_pushDemuxer.GetOutputPinList();
                while (outputPinList.Count == 0)
                {
                    Thread.Sleep(10);
                    outputPinList = m_pushDemuxer.GetOutputPinList();
                }
                AddElecardDecoder();

                ArrayList decoderInPins = m_decoder.GetInputPinList();
                Pin decoderInPin = (Pin)decoderInPins[0];

                m_graphBuilder.Connect((Pin)outputPinList[0], decoderInPin);

                ArrayList decoderOutPins = m_decoder.GetOutputPinList();
                Pin decoderOutPin = (Pin)decoderOutPins[0];

                //Guid clsidVideoRenderer9 = new Guid(Elecard.DSUids.Filters.CLSID_VideoMixingRenderer9);
                //m_rendererFilter = VideoRendererFilter.CreateVideoRendererFilter(ref clsidVideoRenderer9);

                Guid clsidEVR = new Guid(Elecard.DSUids.Filters.CLSID_EnhancedVideoRenderer);
                m_rendererFilter = VideoRendererFilter.CreateVideoRendererFilter(ref clsidEVR);


                //Guid clsidVideoRendererDef = new Guid(Elecard.DSUids.Filters.CLSID_VideoMixingRenderer);
                //m_rendererFilter = VideoRendererFilter.CreateVideoRendererFilter(ref clsidVideoRendererDef);
                 

                m_graphBuilder.Render(decoderOutPin);

                return "ok";

            }
            catch (Exception err)
            {
                return err.Message;
            }
        }
        public Filter VideoRenderer
        {
            get
            {
                return m_rendererFilter;
            }
        }
        public void Run()
        {
            m_mediaControl.Run();
        }
        public void Stop()
        {
            m_mediaControl.Stop();
        }

        public void Close()
        {
            if (m_sourceModuleConfig != null)
                m_sourceModuleConfig.Dispose();
            m_graphBuilder.RemoveFilter(m_decoder);
            m_graphBuilder.RemoveFilter(m_pushDemuxer);
            m_graphBuilder.RemoveFilter(m_tcpSource);

            m_tcpSource.Dispose();
            m_decoder.Dispose();
            m_pushDemuxer.Dispose();
            m_mediaControl.Dispose();
            m_graphBuilder.Dispose();
        }

        void AddElecardDecoder()
        {
        
            Guid clsid = new Guid(Elecard.ElUids.Filters.CLSID_EAVCDEC);
            m_decoder = new Filter(ref clsid);
            m_graphBuilder.AddFilter(m_decoder, "Elecard AVC Video Decoder");
            ActivateFilter(m_decoder);
            ModuleConfig mc = m_decoder.GetConfigInterface();

            object value = 0;
            string strclsidDecFrameBuf = "9A1044D4-5017-4888-8ECF-9A8F2553942F";
            Guid clsidDecFrameBuf = new Guid(strclsidDecFrameBuf);
            
            object res = mc.GetParamValue(ref clsidDecFrameBuf, value);
            value = 0;
            mc.SetParamValue(ref clsidDecFrameBuf, value);
            res = mc.GetParamValue(ref clsidDecFrameBuf, value);
            Console.WriteLine(res);


            Guid EAVCDEC_FullThreadLoad = new Guid("294C64BC-ECB1-4cae-98A9-258C291E9489");
            res = mc.GetParamValue(ref EAVCDEC_FullThreadLoad, value);
            Console.WriteLine(res);
            value = 0;
            mc.SetParamValue(ref EAVCDEC_FullThreadLoad, value);
            res = mc.GetParamValue(ref EAVCDEC_FullThreadLoad, value);
            Console.WriteLine(res);

            mc.CommitChanges();
            mc.Dispose();

        }
        void AddPushDemux()
        {
            // Create Elecard Push Demuxer
            Guid clsidDemuxer = new Guid(Elecard.ElUids.Filters.CLSID_MPGPDMX);
            if (m_pushDemuxer == null)
                m_pushDemuxer = new Filter(ref(clsidDemuxer));
            m_graphBuilder.AddFilter(m_pushDemuxer, "Elecard Push Demuxer");
            ActivateFilter(m_pushDemuxer);
            ModuleConfig mc = m_pushDemuxer.GetConfigInterface();
            object value = 0;
            Guid clsid = new Guid(Elecard.ElUids.Properties.EMPGPDMX_LATENCY_VALUE);
            value = mc.GetParamValue(ref clsid, value);
            value = 0;
            mc.SetParamValue(ref clsid, value);


            Guid EMPGPDMX_INITIAL_PARSING_DONE = new Guid(Elecard.ElUids.Properties.EMPGPDMX_INITIAL_PARSING_DONE);
            value = mc.GetParamValue(ref EMPGPDMX_INITIAL_PARSING_DONE, value);      
            Guid EMPGPDMX_ENABLE_TIME_ADJUST = new Guid(Elecard.ElUids.Properties.EMPGPDMX_ENABLE_TIME_ADJUST);
            value = mc.GetParamValue(ref EMPGPDMX_ENABLE_TIME_ADJUST, value);      
            Guid EMPGPDMX_TIME_ADJUST_MODE = new Guid(Elecard.ElUids.Properties.EMPGPDMX_TIME_ADJUST_MODE);
            value = mc.GetParamValue(ref EMPGPDMX_TIME_ADJUST_MODE, value);      
            Guid EMPGPDMX_STREAMTYPE = new Guid(Elecard.ElUids.Properties.EMPGPDMX_STREAMTYPE);
            value = mc.GetParamValue(ref EMPGPDMX_STREAMTYPE, value);      
            Guid EMPGPDMX_BUFFERS_COUNT = new Guid(Elecard.ElUids.Properties.EMPGPDMX_BUFFERS_COUNT);
            value = mc.GetParamValue(ref EMPGPDMX_BUFFERS_COUNT, value);      
            Guid EMPGPDMX_INIT_BUFFER_SIZE = new Guid(Elecard.ElUids.Properties.EMPGPDMX_INIT_BUFFER_SIZE);
            value = mc.GetParamValue(ref EMPGPDMX_INIT_BUFFER_SIZE, value);      			
   		    Guid EMPGPDMX_BUFFERS_USED = new Guid(Elecard.ElUids.Properties.EMPGPDMX_BUFFERS_USED);
            value = mc.GetParamValue(ref EMPGPDMX_BUFFERS_USED, value);

            mc.CommitChanges();
            mc.Dispose();

        }

        protected void ActivateFilter(Filter fltr)
        {
            ModuleConfig mc = fltr.GetConfigInterface();
            if (mc != null)
            {
                Guid clsidKey = new Guid(Elecard.ElUids.BaseUids.CLSID_ActivationKey);
                mc.SetParamValue(ref clsidKey, null);
                mc.Dispose();
            }
        }
        void AddTCPSource()
        {

            string CLSID_BoutechTCPSource = "B5c19007-D2F1-4ca4-9DCA-15123fba0e84";

            Guid clsidAVCDecoder = new Guid(CLSID_BoutechTCPSource);
            m_tcpSource = new Filter(ref clsidAVCDecoder);
            m_graphBuilder.AddFilter(m_tcpSource, "TCP Source");
            ActivateFilter(m_tcpSource);
            //m_sourceModuleConfig = m_eFilter.GetConfigInterface();
        }

    }
}
