﻿using Elecard.Player;
using SimplePlayer;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BoutechClientApp
{
    public partial class VideoPanel : Form
    {
       
        TCPClientApi m_client = new TCPClientApi();
         
         VideoWindow video_window;

        public VideoPanel()
        {
            InitializeComponent();
             
            Task<string> t = Task.Run(() => ConnectThread());
            t.ContinueWith((t1) =>
            {
                if (InvokeRequired)
                    Invoke((Action)(() =>
                    {
                        video_window = new VideoWindow(m_client.VideoRenderer.GetIUnknown);                      
                        //video_window.Show();
                        m_client.Run();


                        //if (t1.Result == "ok")
                        //AttachVideoWindow();                         
                    }));
             
            });
              
        }
      
        private void Stop()
        {
            
        }

        string ConnectThread()
        {
            return m_client.Build();
        }

        private void VideoPanel_FormClosing(object sender, FormClosingEventArgs e)
        {
            m_client.Stop();
            m_client.Close();
            
            if (video_window != null)
            {
                video_window.Dispose();
                video_window = null;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
             
        }
    }
}
