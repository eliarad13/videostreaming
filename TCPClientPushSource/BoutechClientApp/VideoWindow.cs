using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Elecard.MFInterfaces;

namespace SimplePlayer
{
    [StructLayout(LayoutKind.Sequential)]
    public struct BITMAPINFOHEADER
    {
        public uint biSize;
        public int biWidth;
        public int biHeight;
        public ushort biPlanes;
        public ushort biBitCount;
        public uint biCompression;
        public uint biSizeImage;
        public int biXPelsPerMeter;
        public int biYPelsPerMeter;
        public uint biClrUsed;
        public uint biClrImportant;

        public void Init()
        {
            biSize = (uint)Marshal.SizeOf(this);
        }
    }

    public partial class VideoWindow : Form
    {
        public delegate void UserAbortEventHandler();
        public event UserAbortEventHandler UserAbort;

        private IMFVideoDisplayControl displayControl = null;
        private Boolean bFullScreen = false;

        private Rectangle rcCurrent;
        private Rectangle rcFullScreen;
        
        public VideoWindow(IntPtr baseInterface)
        {
            InitializeComponent();

            rcFullScreen = Screen.PrimaryScreen.Bounds;

            IMFGetService getService = (IMFGetService)Marshal.GetTypedObjectForIUnknown(
                baseInterface, typeof(IMFGetService));
            if (getService == null)
                return;

            Guid MR_VIDEO_RENDER_SERVICE = new Guid("{1092a86c-ab1a-459a-a336-831fbc4d11ff}");
            Guid IID_IMFVideoDisplayControl = new Guid("{a490b1e4-ab84-4d31-a1b2-181e03b1077a}");

            IntPtr ptr;
            getService.GetService(ref MR_VIDEO_RENDER_SERVICE, ref IID_IMFVideoDisplayControl, out ptr);

            displayControl = (IMFVideoDisplayControl)Marshal.GetTypedObjectForIUnknown(
                ptr, typeof(IMFVideoDisplayControl));

            if (displayControl == null)
                return;

            displayControl.SetVideoWindow(this.Handle);
            displayControl.SetAspectRatioMode(MFVideoAspectRatioMode.PreservePicture);

            Size sz1, sz2;
            displayControl.GetNativeVideoSize(out sz1, out sz2);
            this.ClientSize = sz1;
        }

        public Boolean FullScreen
        {
            get
            {
                return bFullScreen;
            }
            set
            {
                if (value == bFullScreen)
                    return;

                if (value)
                {
                    rcCurrent = DesktopBounds;
                    FormBorderStyle = FormBorderStyle.None;
                    DesktopBounds = rcFullScreen;
                    TopMost = true;
                }
                else
                {
                    FormBorderStyle = FormBorderStyle.Sizable;
                    DesktopBounds = rcCurrent;
                    TopMost = false;
                }

                bFullScreen = value;
            }
        }

        public Boolean SaveFrame(String strImgFile)
        {
            Boolean res = true;
            if (displayControl != null)
            {
                IntPtr pDib;
                int dib_size;
                long lTimeStamp = 0;

                BITMAPINFOHEADER bmpih = new BITMAPINFOHEADER();
                bmpih.Init();
                IntPtr pBih = Marshal.AllocCoTaskMem(Marshal.SizeOf(bmpih));
                Marshal.StructureToPtr(bmpih, pBih, false);

                int iRes = displayControl.GetCurrentImage(pBih, out pDib, out dib_size, lTimeStamp);
                if (iRes != 0)
                    return false;

                //get the BITMAPINFOHEADER structure
                Type t = typeof(BITMAPINFOHEADER);
                bmpih = (BITMAPINFOHEADER)Marshal.PtrToStructure(pBih, t);

                //get pixel format
                PixelFormat pformat = PixelFormat.Format32bppRgb;
                switch (bmpih.biBitCount)
                {
                    case 24:
                        pformat = PixelFormat.Format24bppRgb;
                        break;
                    case 32:
                        pformat = PixelFormat.Format32bppRgb;
                        break;
                }

                //get image size
                int size = (int)bmpih.biSizeImage;
                int width = bmpih.biWidth;
                int height = (bmpih.biHeight < 0) ? -bmpih.biHeight : bmpih.biHeight;

                //If biHeight is positive, the bitmap is a bottom-up DIB and its origin is the lower-left corner. 
                //If biHeight is negative, the bitmap is a top-down DIB and its origin is the upper-left corner.
                bool needYFlipped = (bmpih.biHeight > 0);

                //free the BITMAPINFOHEADER structure
                Marshal.DestroyStructure(pBih, t);

                try
                {
                    using (Bitmap bmp = new Bitmap(width, height, pformat))
                    {
                        BitmapData bmpData = bmp.LockBits(new Rectangle(Point.Empty, bmp.Size), ImageLockMode.WriteOnly, bmp.PixelFormat);
                        try
                        {
                            byte[] buff = new byte[dib_size];
                            Marshal.Copy(pDib, buff, 0, buff.Length);
                            Marshal.Copy(buff, 0, bmpData.Scan0, dib_size);
                        }
                        finally
                        {
                            bmp.UnlockBits(bmpData);
                            if (needYFlipped) bmp.RotateFlip(RotateFlipType.RotateNoneFlipY);
                            bmp.Save(strImgFile, System.Drawing.Imaging.ImageFormat.Bmp);
                        }
                    }
                }
                catch (Exception)
                {
                    res = false;
                }
            }

            return res;
        }

        private void VideoWindow_Paint(object sender, PaintEventArgs e)
        {
            if (displayControl != null)
                displayControl.RepaintVideo();
        }

        private void VideoWindow_Resize(object sender, EventArgs e)
        {
            if (displayControl != null)
            {
                displayControl.SetVideoPosition(null, MFRect.FromRectangle(ClientRectangle));
            }
        }

        private void VideoWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason == CloseReason.UserClosing)
            {
                UserAbort();
                e.Cancel = true;
            }
        }

        private void VideoWindow_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Escape && bFullScreen)
                FullScreen = false;
        }
    }
}