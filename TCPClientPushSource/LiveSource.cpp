#include "LiveSource.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include "TCPClient.h"
#include <iostream>
#include <memory>
#include <wmcodecdsp.h>
#include "WinAES.h"

//https://www.codeproject.com/Articles/158053/DirectShow-Filters-Development-Part-Live-Source

using namespace std;

#define USE_AES  0
extern bool m_PushFinished;

WinAES *aes;

FILE *m_recordingHandle = NULL;
IMediaSample *pSLAMediaSample;
SLAFilterParams  m_slaFilterParams;
shared_ptr<CTCPClient> m_tcpClient;
GRAPHSTAT  m_graphStat = GRAPH_STOP;
byte *recovered = NULL;
CLiveSourceStream* g_pOutputPin = NULL;

bool m_created = false;
  
CLiveSource::CLiveSource(LPUNKNOWN pUnk, HRESULT* phr)
	: CBaseFilter(LIVE_FILTER_NAME, pUnk, &m_critSec, CLSID_CLiveSource)
{
	m_pOutputPin = new CLiveSourceStream(this, &m_critSec, phr); 
	

	if (phr)
	{
		if (m_pOutputPin == NULL)
			*phr = E_OUTOFMEMORY;
		else {
			*phr = S_OK;
			g_pOutputPin = m_pOutputPin;
		}
	}  
}

CLiveSource::~CLiveSource(void)
{
	if (aes != NULL)
	{	
		delete aes;
		aes = NULL;
	}
	if (recovered != NULL)
		delete recovered; 
	recovered = NULL;
	delete m_pOutputPin;

}

void InitializeAES()
{
	 

	byte key[WinAES::KEYSIZE_128] = { 0xF5, 0x13, 0x1E, 0xD8, 0x7F, 0xB3, 0x06, 0x94, 0x07, 0x7D, 0x85, 0xFD, 0xE0, 0x4C, 0x9C, 0xAC };
	byte iv[WinAES::BLOCKSIZE] = { 0xD8, 0xCD, 0xCF, 0x1E, 0xC2, 0x9E, 0xE8, 0x81, 0x98, 0x5C, 0xD3, 0x5B, 0x7A, 0xB1, 0xDA, 0x2A };
	
	aes = new WinAES();
 
	
	try
	{
		// Oops - no iv
		// aes.SetKey( key, sizeof(key) );

		// Oops - no key
		// aes.SetIv( iv, sizeof(iv) );

		// Set the key and IV
		aes->SetKeyWithIv(key, sizeof(key), iv, sizeof(iv));

		recovered = new byte[1024 * 10];

		// Done with key material - Microsoft owns it now...
		ZeroMemory(key, sizeof(key));


	}
	catch (const WinAESException& e)
	{

	}
	 

}
 
STDMETHODIMP CLiveSource::Run(REFERENCE_TIME tStart)
{

	m_graphStat = GRAPH_RUN;
	 

	return CBaseFilter::Run(tStart);
}

DWORD WINAPI StopTask(LPVOID lpParam)
{
 
	m_graphStat = GRAPH_STOP;
	m_tcpClient->Stop();


	return 0;
}
 
STDMETHODIMP CLiveSource::Pause()
{
	return S_OK;
	 

	return CBaseFilter::Pause();
}
STDMETHODIMP CLiveSource::Stop()
{
	m_graphStat = GRAPH_STOP;
	/*
	DWORD dwThreadIdArray;
	HANDLE m_stopThread = CreateThread(
		NULL,
		0,
		StopTask,
		NULL,
		0,
		&dwThreadIdArray);*/


	m_graphStat = GRAPH_STOP;
	m_tcpClient->Stop();

	return CBaseFilter::Stop();
}

int CLiveSource::GetPinCount()
{
	CAutoLock cAutoLock(&m_critSec);

	return 1;
}

CBasePin* CLiveSource::GetPin(int n)
{
	CAutoLock cAutoLock(&m_critSec);

	return m_pOutputPin;
}

CUnknown* WINAPI CLiveSource::CreateInstance(LPUNKNOWN pUnk, HRESULT *phr)
{
	CUnknown* pNewFilter = new CLiveSource(pUnk, phr);

	  

	if (phr)
	{
		if (pNewFilter == NULL) 
			*phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}

	// Configure your router to port forwarding with TCP.
	// add here the IP address of your internet provider
	// Use what my ip address in google and enter here.
	m_tcpClient = make_shared<CTCPClient>("81.81.105.171", 8051);
	//m_tcpClient = make_shared<CTCPClient>("127.0.0.1", 11000);
	int res = m_tcpClient->Setup(true);

	return pNewFilter;
}

STDMETHODIMP CLiveSource::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	CheckPointer(ppv, E_POINTER);

	if (riid == IID_ITCPPushSource)
	{
		return GetInterface((ITCPPushSource*)m_pOutputPin, ppv);
	}
	else 
	if(riid == IID_ILiveSource) 
	{
		return GetInterface((ILiveSource*) m_pOutputPin, ppv);
	} 
	else if(riid == IID_IAMPushSource)
	{
		return GetInterface((IAMPushSource*) m_pOutputPin, ppv);
	}
	else if(riid == IID_IAMFilterMiscFlags)
	{
		return GetInterface((IAMFilterMiscFlags*) this, ppv);
	}
	else 
	{
		return CBaseFilter::NonDelegatingQueryInterface(riid, ppv);
	}
}


//=============================================================================



CLiveSourceStream::CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr)
	: CBaseOutputPin(LIVE_FILTER_NAME, pFilter, pLock, phr, LIVE_OUTPIN_NAME),
	m_frameRate(0), m_rtFrameRate(0), m_lastFrame(0),
	m_videoResolutionWidth(640), m_videoResolutionHeight(480)
{
	m_bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
	m_bmpInfo.biCompression = BI_RGB;
	m_bmpInfo.biBitCount = 32;
	m_bmpInfo.biHeight = m_videoResolutionHeight;
	m_bmpInfo.biWidth = m_videoResolutionWidth;
	m_bmpInfo.biPlanes = 1;
	m_bmpInfo.biSizeImage = GetBitmapSize(&m_bmpInfo);	
	m_bmpInfo.biClrImportant = 0;
	m_bmpInfo.biClrUsed = 0;
	m_bmpInfo.biXPelsPerMeter = 0;
	m_bmpInfo.biYPelsPerMeter = 0;
	m_videoFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 4;
	m_videoSLAFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 3;

	SetFrameRate(30);



#if USE_AES
	InitializeAES();
#endif 

}

CLiveSourceStream::~CLiveSourceStream()
{
#if USE_AES
	if (recovered != NULL)
	{
		delete recovered;
		recovered = NULL;
	}
#endif 
}

HRESULT CLiveSourceStream::CheckMediaType(const CMediaType* pmt)
{
	CheckPointer(pmt, E_POINTER);

	return S_OK;
#if 0 
	if (pmt->majortype == MEDIATYPE_Stream)
		return S_OK;

	if (pmt->majortype != MEDIATYPE_Video)
	{
		return E_INVALIDARG;
	}
	if (pmt->subtype != MEDIASUBTYPE_H264)
	{
		return E_INVALIDARG;
	}
	return S_OK;
#endif 
}

HRESULT CLiveSourceStream::GetMediaType(int iPosition, CMediaType* pmt)
{
	CAutoLock cAutoLock(m_pLock);

	if(iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if(iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)pmt->AllocFormatBuffer(sizeof(VIDEOINFOHEADER));
	if(!vih)
	{
		return E_OUTOFMEMORY;
	}

	vih->bmiHeader = m_bmpInfo;
	if(m_frameRate != 0)
	{
		vih->AvgTimePerFrame = UNITS / m_frameRate;
	}
	else
	{
		vih->AvgTimePerFrame = 0;
	}
	 

	switch (iPosition)
	{
		case 0:
			pmt->SetType(&MEDIATYPE_Video);
			pmt->SetSubtype(&MEDIASUBTYPE_H264);
			pmt->SetFormatType(&FORMAT_VideoInfo);
			pmt->SetTemporalCompression(TRUE /*FALSE*/);
			pmt->SetSampleSize(m_bmpInfo.biSizeImage);
		break;
		case 1:
			pmt->SetType(&MEDIATYPE_Stream);
			pmt->SetSubtype(&GUID_NULL);			
			pmt->SetFormatType(&GUID_NULL);
		break;
	}
	
	return S_OK;
}

HRESULT CLiveSourceStream::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest)
{
	CheckPointer(pAlloc,E_POINTER);
	CheckPointer(ppropInputRequest,E_POINTER);

	CAutoLock cAutoLock(m_pLock);
	HRESULT hr = NOERROR;

	ppropInputRequest->cBuffers = 1;
	ppropInputRequest->cbBuffer = m_bmpInfo.biSizeImage;

	ASSERT(ppropInputRequest->cbBuffer);

	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(ppropInputRequest,&Actual);
	if(FAILED(hr))
	{
		return hr;
	}

	if(Actual.cbBuffer < ppropInputRequest->cbBuffer)
	{
		return E_FAIL;
	}

	pAlloc->Commit();
	//m_tcpClient->Start();
	//m_pAllocator->Commit();
	m_tcpClient->StartReceiveThread();

	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetBitmapInfo(BITMAPINFOHEADER& bInfo)
{
	if(bInfo.biHeight == 0 || bInfo.biWidth == 0 || bInfo.biBitCount == 0)
	{
		return E_INVALIDARG;
	}

	m_bmpInfo = bInfo;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::AddFrame(HBITMAP hBmp)
{
	CAutoLock cAutoLock(m_pLock);

	int iSize = 0;
	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if(FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

	hr = GetPixelData(hBmp, &pData, &iSize);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

	hr = pSample->SetActualDataLength(iSize);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

	hr = pSample->SetSyncPoint(TRUE);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	
	hr = this->Deliver(pSample);
	pSample->Release();

	return hr;
}

STDMETHODIMP CLiveSourceStream::AddFrame(BYTE* pBuffer, DWORD size)
{
	CAutoLock cAutoLock(m_pLock);

	if(size > m_bmpInfo.biSizeImage)
	{
		return E_INVALIDARG;
	}

	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if(FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

	memcpy(pData, pBuffer, size);

	hr = pSample->SetActualDataLength(size);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

	hr = pSample->SetSyncPoint(TRUE);
	if(FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	
	hr = this->Deliver(pSample);
	pSample->Release();

	return hr;
}


STDMETHODIMP CLiveSourceStream::AddFrameFromFile(FILE *handle)
{

	CAutoLock cAutoLock(m_pLock);


	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}


#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 

	int size = fread((char *)pData, 1 , RECEIVE_SIZE, handle);
	if (size == 0)
	{
		m_PushFinished = true;
		pSample->Release();
		fclose(handle);
		return hr;
	}

	hr = pSample->SetActualDataLength(size);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	/*
	hr = pSample->SetSyncPoint(TRUE);
	if (FAILED(hr))
	{
	pSample->Release();
	return hr;
	}
	*/

#endif 
	hr = this->Deliver(pSample);
	pSample->Release();
	return hr;

}

STDMETHODIMP CLiveSourceStream::AddFrame(int clientSocket)
{
	CAutoLock cAutoLock(m_pLock);


	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	  

#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {
		 
	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {
		 
	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 
	int size = recv(clientSocket, (char *)pData, RECEIVE_SIZE, 0);

	hr = pSample->SetActualDataLength(size);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	/*
	hr = pSample->SetSyncPoint(TRUE);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	*/

#endif 
	hr = this->Deliver(pSample);
	pSample->Release();

	return hr;


}
////// ISLA3000 interface implementation

STDMETHODIMP CLiveSourceStream::StartRecording(WCHAR *FileName)
{
	char _file[300];
	wcstombs(_file, FileName, 300);

	m_recordingHandle = fopen(_file, "w+b");
	if (m_recordingHandle == NULL)
		return -1;
	 
	return 1;
}

STDMETHODIMP CLiveSourceStream::SetVideoResolution(int width, int height)
{
	
	m_videoResolutionWidth = width;
	m_videoResolutionHeight = height;

	m_bmpInfo.biHeight = m_videoResolutionHeight;
	m_bmpInfo.biWidth = m_videoResolutionWidth;

	m_videoFrameSize = m_videoResolutionWidth * g_pOutputPin->m_videoResolutionHeight * 4;
	m_videoSLAFrameSize = m_videoResolutionWidth * g_pOutputPin->m_videoResolutionHeight * 3;

	return S_OK;
}


STDMETHODIMP CLiveSourceStream::SetPort(int port)
{

	m_slaFilterParams.port = port;

	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetIpAddress(WCHAR *IpAddress)
{
	 
	char _ip[100];
	wchar_t array[] = L"Hello World";
	wcstombs(_ip, IpAddress, 100);

	strcpy(m_slaFilterParams.Addr, _ip);
	return S_OK;
}

 STDMETHODIMP CLiveSourceStream::StartStreaming(WCHAR *IpAddress , int port)
{

	 if (m_created == true)
		 return 0;

	char _ip[100];
	wchar_t array[] = L"Hello World";
	wcstombs(_ip, IpAddress, 100);
	strcpy(m_slaFilterParams.Addr, _ip);
	m_slaFilterParams.port = port;
	m_tcpClient = make_shared<CTCPClient>(m_slaFilterParams.Addr, m_slaFilterParams.port);
	int res = m_tcpClient->Setup(true);

	m_created = 1;

	return res;
}
  

/////////////////////////
HRESULT CLiveSourceStream::GetMediaSample(IMediaSample** ppSample)
{
	REFERENCE_TIME rtStart = m_lastFrame;
	m_lastFrame += m_rtFrameRate;

	//return this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);

	HRESULT hr;
	hr = this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = (*ppSample)->SetTime(&rtStart, &m_lastFrame);

	return hr;

}

STDMETHODIMP CLiveSourceStream::SetFrameRate(int frameRate)
{
	if(frameRate < 0 || frameRate > 30)
	{
		return E_INVALIDARG;
	}

	m_frameRate = frameRate;
	m_rtFrameRate = UNITS / m_frameRate;
	return S_OK;
}

HRESULT CLiveSourceStream::GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize)
{
	ASSERT(hBmp);

	BITMAP bmp = {0};
	int res = ::GetObject(hBmp, sizeof(BITMAP), &bmp);
	if(res != sizeof(BITMAP))
	{
		return E_FAIL;
	}

	if(bmp.bmBitsPixel != m_bmpInfo.biBitCount ||
		bmp.bmHeight != m_bmpInfo.biHeight ||
		bmp.bmWidth != m_bmpInfo.biWidth)
	{
		return E_INVALIDARG;
	}

	*pSize = bmp.bmWidthBytes * bmp.bmHeight;
	memcpy(*ppData, bmp.bmBits, *pSize);

	return S_OK;
}

STDMETHODIMP CLiveSourceStream::GetMaxStreamOffset(REFERENCE_TIME *prtMaxOffset)
{
	*prtMaxOffset = 0;
	return S_OK;
} 

STDMETHODIMP CLiveSourceStream::GetPushSourceFlags(ULONG *pFlags)
{ 
	*pFlags = 0;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::GetStreamOffset(REFERENCE_TIME *prtOffset)
{
	*prtOffset = 0;
	return S_OK;
} 

STDMETHODIMP CLiveSourceStream::SetMaxStreamOffset(REFERENCE_TIME rtMaxOffset)
{
	return E_NOTIMPL;
}

STDMETHODIMP CLiveSourceStream::SetPushSourceFlags(ULONG Flags)
{
	return E_NOTIMPL;
} 

STDMETHODIMP CLiveSourceStream::SetStreamOffset(REFERENCE_TIME rtOffset)
{
	return E_NOTIMPL;
} 

STDMETHODIMP CLiveSourceStream::GetLatency(REFERENCE_TIME *prtLatency)
{ 
	*prtLatency = m_rtFrameRate;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::Notify(IBaseFilter * pSender, Quality q)
{
	if(q.Proportion <= 0)
	{
		m_rtFrameRate = 1000;        
	}
	else
	{
		m_rtFrameRate = m_rtFrameRate * 1000 / q.Proportion;
		if(m_rtFrameRate > 1000)
		{
			m_rtFrameRate = 1000;    
		}
		else if(m_rtFrameRate < 30)
		{
			m_rtFrameRate = 30;      
		}
	}

	if(q.Late > 0)
	{
		m_rtFrameRate += q.Late;
	}

	return S_OK;
}