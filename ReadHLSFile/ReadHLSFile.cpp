// ReadHLSFile.cpp : Defines the entry point for the console application.
//

#include "stdafx.h"
#include <stdio.h>
#include <winsock2.h>
#include <windows.h>
#include <string.h> 
#include <stdint.h>
#include <list>

using namespace std;

SOCKET HTTPConnectToServer(char *IpAddress, int port)
{
	SOCKADDR_IN serverInfo;
	SOCKET sck;
	WSADATA wsaData;
	LPHOSTENT hostEntry;
	WSAStartup(MAKEWORD(2, 2), &wsaData);
	 
	sck = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (sck == INVALID_SOCKET){
		WSACleanup();
		puts("Failed to setup socket");
		getchar();
		return 0;
	}
	serverInfo.sin_family = AF_INET;
	serverInfo.sin_addr.s_addr = inet_addr(IpAddress);
	serverInfo.sin_port = htons(port);
	int i = connect(sck, (LPSOCKADDR)&serverInfo, sizeof(struct sockaddr));

	if (sck == SOCKET_ERROR) return 0;
	if (i != 0) return 0;

	return sck;
}


void HTTPRequestPage(SOCKET s, char *page, char *host)
{
	unsigned int len;
	if (strlen(page)>strlen(host)){
		len = strlen(page);
	}
	else len = strlen(host);

	char message[20 + 100];
	if (strlen(page) <= 0){
		strcpy(message, "GET / HTTP/1.1\r\n");
	}
	else sprintf(message, "GET %s HTTP/1.1\r\n", page);
	send(s, message, strlen(message), 0);

	memset(message, 0, sizeof(message));
	sprintf(message, "Host: %s\r\n\r\n", host);
	send(s, message, strlen(message), 0);
}

BOOL DownloadToBuffer(char * webpage, uint8_t *buffer, unsigned long max, char *relativeAddress, char *ServerIpAddress, int *ServerPort)
{
	if (webpage == NULL || buffer == NULL || max == 0) return FALSE;

	unsigned short shift = 0;
	if (_strnicmp(webpage, "http://", strlen("http://")) == 0){
		shift = strlen("http://");
	}
	if (_strnicmp(webpage + shift, "www.", strlen("www.")) == 0){
		shift += strlen("www.");
	}
	char cut[200];
	strcpy(cut, _strdup(webpage + shift));

	char *p;
	char *server = strtok(cut, "/");
	char ipAddress[100];
	char port[20];
	int _port = 80;
	strcpy(ipAddress, server);
	p = strchr(ipAddress, ':');
	if (p != NULL)
	{
		*p = 0;
		strcpy(ServerIpAddress, ipAddress);
		strcpy(port, server);
		p = strchr(port, ':') + 1;
		strcpy(port, p);
		_port = atoi(port);
		*ServerPort = _port;
	}
	else
	{
		strcpy(ipAddress, server);
		strcpy(ServerIpAddress, ipAddress);
		_port = 80;
	}


	char *page = _strdup(webpage + shift + strlen(server));

	strcpy(relativeAddress, page);
	p = relativeAddress;
	p = p + strlen(relativeAddress) - 1;
	while (*p != '/')
		p--;
	*p = 0;

	SOCKET s = HTTPConnectToServer(ipAddress, _port);
	HTTPRequestPage(s, page, server);

	int i = recv(s, (char *)buffer, max, 0);
	closesocket(s);

	if (i <= 0) return FALSE;
	return TRUE;
}

uint8_t *buffer = NULL;

list<string> m_hlsFileList;
list<float> m_hlsTimeLine;

int _tmain(int argc, _TCHAR* argv[])
{
	char ServerIpAddress[100];
	int ServerPort;
	char temp[500];
	char relativeAddress[500];
	buffer = (uint8_t*)malloc(1024 * 1024 * 20);
	memset(buffer, 0, 1024 * 1024 * 20);
	DownloadToBuffer("http://10.0.0.10:8080/1/playlist/mpuser/Request_2020430_22238_1.m3u8", buffer,  10000, relativeAddress, ServerIpAddress, &ServerPort);
	char *p = strchr((char *)buffer, '\n');
	char response[30];
	memset(response, 0, sizeof(response));
	memcpy(response, buffer, p - (char *)buffer);
	p = strchr(response, '\r');
	if (p != NULL)
		*p = NULL;
	if (strcmp(response, "HTTP/1.1 200 OK") == 0)
	{

		char* token = strtok((char *)buffer, "\n");
		while (token != NULL) 
		{
			token = strtok(NULL, "\n");
			if (strstr(token ,"EXTINF") != NULL)
			{
				while (token != NULL) 
				{
					if (strstr(token, "EXTINF") != NULL)
					{
						strcpy(temp, token);
						p = strchr(temp, ':');
						p++;
						strcpy(temp, p);
						if (strchr(temp, ',') != NULL)
							*p = 0;
						m_hlsTimeLine.push_back(atof(temp));
						if (m_hlsTimeLine.size() == 185)
							printf("f");
					}
					else
					{
						strcpy(temp, token);
						if ((p = strchr(temp, '\r')) != NULL)
							*p = 0;
						m_hlsFileList.push_back(temp);
					}
					token = strtok(NULL, "\n");
				}
				break;
			}				
		}


		SOCKET s = HTTPConnectToServer(ServerIpAddress , ServerPort);
		char url[1000];
		sprintf(url, "GET %s/%s HTTP/1.0\r\n\r\n", relativeAddress, m_hlsFileList.front().c_str());
		//char *packet = "GET /1/mp_2020_04_15_07_55_55/MegaPop/hls_0.ts HTTP/1.0\r\n\r\n";
		send(s, url, strlen(url), 0);
		int bufIndex = 0;
		int r = recv(s, (char *)buffer, 2000, 0);
		bufIndex += r;
		int i = 235;
		while (buffer[i] != 0x47)
			i++;

		while (r > 0)
		{
			r = recv(s, (char *)buffer, 2000, 0);			 
		}		
		closesocket(s);
		printf("ok");
	}
	else {
		printf("failed");
	}

	if (buffer != NULL)
	{
		free(buffer);
	}
	return 0;
}

