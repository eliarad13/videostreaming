#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>

 
 
// {C709C5CF-5D7F-483C-8C4B-5E1DD4633767}
static const GUID IID_IBauotechDumpFrames =
{ 0xc709c5cf, 0x5d7f, 0x483c, { 0x8c, 0x4b, 0x5e, 0x1d, 0xd4, 0x63, 0x37, 0x67 } };



typedef void (CALLBACK *MANAGEDCALLBACKPROC2)(BYTE* pdata, long len, int width, int bitCount, int height, int stride, int fileIndex);


DECLARE_INTERFACE_(IBauotechDumpFrames, IUnknown)
{	
	 
	STDMETHOD(ConfigureDumpFramesDirectory)(const WCHAR *rootDirectory) PURE;
	STDMETHOD(EnableSnapshot)(bool enable) PURE;
	STDMETHOD(GetSnapshotState)(bool *enable) PURE;
	STDMETHOD(SetSnapshotCount)(int snapshotCount) PURE;
	STDMETHOD(EnableContinuesGrabbing)(bool enable) PURE;
	STDMETHOD(SetDumpFramesCallback)(MANAGEDCALLBACKPROC2 progressCallback) PURE;
	STDMETHOD(SetDumpFramesFileIndex)(int fileIndex)PURE;

};