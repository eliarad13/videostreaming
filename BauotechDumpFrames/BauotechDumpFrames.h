//------------------------------------------------------------------------------
// File: Dump.h
//
// Desc: DirectShow sample code - definitions for dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


class CDumpInputPin;
class CDump;
class CDumpFilter;

#define BYTES_PER_LINE 20
#define FIRST_HALF_LINE TEXT  ("   %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x")
#define SECOND_HALF_LINE TEXT (" %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x")


// Main filter object

class CDumpFilter : public CBaseFilter
{
    CDump * const m_pDump;
	
public:

    // Constructor
    CDumpFilter(CDump *pDump,
                LPUNKNOWN pUnk,
                CCritSec *pLock,
                HRESULT *phr);

    // Pin enumeration
    CBasePin * GetPin(int n);
    int GetPinCount();

    // Open and close the file as necessary
    STDMETHODIMP Run(REFERENCE_TIME tStart);
    STDMETHODIMP Pause();
    STDMETHODIMP Stop();
};


//  Pin object

class CDumpInputPin : public CRenderedInputPin
{
    CDump    * const m_pDump;           // Main renderer object
    CCritSec * const m_pReceiveLock;    // Sample critical section
    REFERENCE_TIME m_tLast;             // Last sample receive time

public:

    CDumpInputPin(CDump *pDump,
                  LPUNKNOWN pUnk,
                  CBaseFilter *pFilter,
                  CCritSec *pLock,
                  CCritSec *pReceiveLock,
                  HRESULT *phr);
	
    // Do something with this media sample
    STDMETHODIMP Receive(IMediaSample *pSample);

    // Check if the pin can support this specific proposed type and format
    HRESULT CheckMediaType(const CMediaType *);


	HRESULT SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt);

    // Break connection
    HRESULT BreakConnect();

	int m_Width;
	int m_Height;
	int m_bitCount;
	int m_SampleSize;
	int m_Stride;
	
	WCHAR m_tempFileName[500];

    
};


//  CDump object which has filter and pin members

class CDump : public CUnknown, public IBauotechDumpFrames
{
    friend class CDumpFilter;
    friend class CDumpInputPin;

    CDumpFilter   *m_pFilter;       // Methods for filter interfaces
    CDumpInputPin *m_pPin;          // A simple rendered input pin

    CCritSec m_Lock;                // Main renderer critical section
    CCritSec m_ReceiveLock;         // Sublock for received samples
 

public:

    DECLARE_IUNKNOWN

    CDump(LPUNKNOWN pUnk, HRESULT *phr);
    ~CDump();

    static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);
 
	bool m_running = false;
	int m_fileIndex;
	int m_frameCounter;
	bool m_snapshot = false;
	bool m_enableContinuesGrabbing = false;
	int m_snapshotCount = 0;

	MANAGEDCALLBACKPROC2 pProgressCallback;

	STDMETHODIMP ConfigureDumpFramesDirectory(const WCHAR *rootDirectory);
	STDMETHODIMP EnableSnapshot(bool enable);
	STDMETHODIMP GetSnapshotState(bool *enable);
	STDMETHODIMP EnableContinuesGrabbing(bool enable);
	STDMETHODIMP SetDumpFramesCallback(MANAGEDCALLBACKPROC2 progressCallback);
	STDMETHODIMP SetSnapshotCount(int snapshotCount);
	STDMETHODIMP SetDumpFramesFileIndex(int fileIndex);

	WCHAR m_snapShotFileName[500];
private:

    // Overriden to say what interfaces we support where
    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

};

