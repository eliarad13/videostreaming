#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>

  


// {628CF5A1-E5DD-464B-BCC3-2F3B15583A23}
static const GUID IID_IBoutechRaspeberryClientReceiver =
{ 0x628cf5a1, 0xe5dd, 0x464b, { 0xbc, 0xc3, 0x2f, 0x3b, 0x15, 0x58, 0x3a, 0x23 } };



DECLARE_INTERFACE_(IBoutechRaspeberryClientReceiver, IUnknown)
{

	STDMETHOD(SetIpAddressAndPort)(WCHAR *MulticastIpAddress, WCHAR *NicIpAddress, int port, bool IsMulticast) PURE;

};
