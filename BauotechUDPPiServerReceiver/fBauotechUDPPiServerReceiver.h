#include "myInterface.h"
#include <iterator> 
#include <map> 
#include <list>
#include "aes.h"
#include <thread>
#include <memory>

class CBallStream;
class CBauotechServerSourceFilter;

using namespace std;



// {BE7D54FD-9810-4728-95FA-9743EF36EAB2}
static const GUID CLSID_BauotechUDPPiServerReceiver =
{ 0xbe7d54fd, 0x9810, 0x4728, { 0x95, 0xfa, 0x97, 0x43, 0xef, 0x36, 0xea, 0xb2 } };

 
class CBauotechUdpServerSource : public CUnknown
{
	friend class CDumpFilter;
	friend class CDumpInputPin;

public:
	CBauotechServerSourceFilter   *m_pFilter;       // Methods for filter interfaces
	CBallStream *m_pPin;          // A simple rendered input pin
	int Setup();

public:

	DECLARE_IUNKNOWN

	CBauotechUdpServerSource(LPUNKNOWN pUnk, HRESULT *phr);
	~CBauotechUdpServerSource();

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);
	 

private:

	// Overriden to say what interfaces we support where
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

};
 

class CBauotechServerSourceFilter : public CSource, public IBoutechRaspeberryClientReceiver
{
public:

	DECLARE_IUNKNOWN;
 
    // The only allowed way to create Bouncing balls!
    static CUnknown * WINAPI CreateInstance(LPUNKNOWN lpunk, HRESULT *phr);
	SOCKET ReceivingSocket;
	CRITICAL_SECTION CriticalSection;
	char m_NicIpAddress[100];
	char m_multicastIpAddress[100];
	bool m_isMulticast;
	uint8_t *buffer;
	int m_port;
	bool m_running;

	uint64_t wr = 0;
	uint64_t rd = 0;

	STDMETHODIMP SetIpAddressAndPort(WCHAR *MulticastIpAddress, WCHAR *NicIpAddress, int port, bool IsMulticast);

	void CreateSource();
	CCritSec m_Lock;                // Main renderer critical section
    // It is only allowed to to create these objects with CreateInstance
	CBauotechServerSourceFilter(LPUNKNOWN lpunk, HRESULT *phr);
	~CBauotechServerSourceFilter();

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	char m_sFileName[500];

	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void **ppv);

}; // CRawInputSourceFilter

 
//------------------------------------------------------------------------------
// Class CBallStream
//
// This class implements the stream which is used to output the bouncing ball
// data from the source filter. It inherits from DirectShows's base
// CSourceStream class.
//------------------------------------------------------------------------------
class CBallStream : public CSourceStream  
{

public:

	CBallStream(HRESULT *phr, CBauotechServerSourceFilter *pParent, LPCWSTR pPinName);
    ~CBallStream();
	
	int GetSize();
	void ReceiveThread();

	shared_ptr<thread> m_receiveThread;

	
	struct sockaddr_in addr;
	int Connect(bool multicast, char *interfaceAddress, int port, char *multicastAddress);

	BITMAPINFOHEADER m_bmpInfo;
    // plots a ball into the supplied video frame
    HRESULT FillBuffer(IMediaSample *pms);
	 
	struct AES_ctx ctx;
    // Ask for buffers of the size appropriate to the agreed media type
    HRESULT DecideBufferSize(IMemAllocator *pIMemAlloc,
                             ALLOCATOR_PROPERTIES *pProperties);

    
    // Because we calculate the ball there is no reason why we
    // can't calculate it in any one of a set of formats...
    HRESULT CheckMediaType(const CMediaType *pMediaType);
    HRESULT GetMediaType(int iPosition, CMediaType *pmt);

    // Resets the stream time to zero
    HRESULT OnThreadCreate(void);

    // Quality control notifications sent to us
    STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);
	
	CBauotechServerSourceFilter *m_pFilter;
	void ScanSeqFile();
	void ScanSeqList();
	//int m_inMbSeqRgbVideoFrames;

public:
	CCritSec m_Lock;                // Main renderer critical section
    int m_iImageHeight;                 // The current image height
    int m_iImageWidth;                  // And current image width
    int m_iRepeatTime;                  // Time in msec between frames
    const int m_iDefaultRepeatTime;     // Initial m_iRepeatTime

    

    CCritSec m_cSharedState;            // Lock on m_rtSampleTime and m_Ball
    CRefTime m_rtSampleTime;            // The time stamp for each sample
    //CBall *m_Ball;                      // The current ball object

}; // CBallStream
    
