//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 

 

// {BD5D7B74-021E-4C1E-8ED6-518B8D6CAB23}
DEFINE_GUID(CLSID_BoutechMulticastNetworkSender,
	0xbd5d7b74, 0x21e, 0x4c1e, 0x8e, 0xd6, 0x51, 0x8b, 0x8d, 0x6c, 0xab, 0x23);


 

// {A8654ECC-E241-463A-B6E1-BFF9C3FFCE59}
DEFINE_GUID(IID_IBoutechUDPMulticastSender,
	0xa8654ecc, 0xe241, 0x463a, 0xb6, 0xe1, 0xbf, 0xf9, 0xc3, 0xff, 0xce, 0x59);

 

DECLARE_INTERFACE_(IBoutechUDPMulticastSender, IUnknown)
{
	STDMETHOD(ConfigureMulticastSender)(const WCHAR *IpAddress, const int port, const WCHAR *IpInterfaceAddress) PURE;
};