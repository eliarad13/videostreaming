//------------------------------------------------------------------------------
// File: GargUIDs.h
//
// Desc: DirectShow sample code - definition of CLSIDs.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __GARGUIDS__
#define __GARGUIDS__

#ifdef __cplusplus
extern "C" {
#endif

  
  
// {5E4788B9-88E0-4304-BCED-82485714739A}
	DEFINE_GUID(CLSID_BauotechInPlaceTransform,
	0x5e4788b9, 0x88e0, 0x4304, 0xbc, 0xed, 0x82, 0x48, 0x57, 0x14, 0x73, 0x9a);



#ifdef __cplusplus
}
#endif

#endif // __GARGUIDS__
