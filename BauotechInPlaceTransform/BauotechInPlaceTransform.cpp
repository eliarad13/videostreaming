
#include <streams.h>

// Eliminate two expected level 4 warnings from the Microsoft compiler.
// The class does not have an assignment or copy operator, and so cannot
// be passed by value.  This is normal.  This file compiles clean at the
// highest (most picky) warning level (-W4).
#pragma warning(disable: 4511 4512)


#include <initguid.h>
#if (1100 > _MSC_VER)
#include <olectlid.h>
#else
#include <olectl.h>
#endif
#include "BauotechInPlaceTransformuids.h"             
#include "IBauotechInPlaceTransform.h"              / 
#include "BauotechInPlaceTransform.h"

const uint64_t MAX_TIMESHIFT_BUFFER = 1024 * 1024 * 188 * 5;


//------------------------------------------------------------------------
// Implementation
//------------------------------------------------------------------------


#define DbgFunc(a) DbgLog(( LOG_TRACE                        \
                          , 2                                \
                          , TEXT("CBauotechInPlaceTransform(Instance %d)::%s") \
                          , m_nThisInstance                  \
                          , TEXT(a)                          \
                         ));


// Self-registration data structures

const AMOVIESETUP_MEDIATYPE
sudPinTypes =   { &MEDIATYPE_Audio        // clsMajorType
                , &MEDIASUBTYPE_NULL };   // clsMinorType

const AMOVIESETUP_PIN
psudPins[] = { { L"Input"            // strName
               , FALSE               // bRendered
               , FALSE               // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Output"           // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             , { L"Output"           // strName
               , FALSE               // bRendered
               , TRUE                // bOutput
               , FALSE               // bZero
               , FALSE               // bMany
               , &CLSID_NULL         // clsConnectsToFilter
               , L"Input"            // strConnectsToPin
               , 1                   // nTypes
               , &sudPinTypes        // lpTypes
               }
             };

const AMOVIESETUP_FILTER
sudGargle = { &CLSID_BauotechInPlaceTransform                   // class id
            , L"Bauotech In Place Transform"                       // strName
            , MERIT_DO_NOT_USE                // dwMerit
            , 2                               // nPins
            , psudPins                        // lpPin
            };

// Needed for the CreateInstance mechanism
CFactoryTemplate g_Templates[1]= { { L"Bauotech InPlace Transform"
, &CLSID_BauotechInPlaceTransform
, CBauotechInPlaceTransform::CreateInstance
                                   , NULL
                                   , &sudGargle
                                   }                                 
                                 };

int g_cTemplates = sizeof(g_Templates)/sizeof(g_Templates[0]);

// initialise the static instance count.
int CBauotechInPlaceTransform::m_nInstanceCount = 0;



CBauotechInPlaceTransform::~CBauotechInPlaceTransform()
{
	
}

 
 
CBauotechInPlaceTransform::CBauotechInPlaceTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr)
	: CTransInPlaceFilter(tszName, punk, CLSID_BauotechInPlaceTransform, phr)
    , m_SamplesPerSec     (0)
    , m_BytesPerSample    (0)
    , m_Channels          (0)
    , m_Phase             (0)
    , m_Shape             (0)
{

	m_effect = IDC_RED;
	LoadAlgoDll();
	m_pause_ts = false;

}  

CUnknown * WINAPI CBauotechInPlaceTransform::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
	CBauotechInPlaceTransform *pNewObject = new CBauotechInPlaceTransform(NAME("Bauotech In Place Transform Filter"), punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

}


STDMETHODIMP CBauotechInPlaceTransform::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
    CheckPointer(ppv,E_POINTER);

	if (riid == IID_IBaoutechInPlaceTransform) {
		return GetInterface((IBauotechInPlaceTransform *) this, ppv);

    } else if (riid == IID_IPersistStream) {
        return GetInterface((IPersistStream *) this, ppv);

    } else {
        // Pass the buck
        return CTransInPlaceFilter::NonDelegatingQueryInterface(riid, ppv);
    }

} // NonDelegatingQueryInterface

 
 
STDMETHODIMP CBauotechInPlaceTransform::Pause()
{

	//m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP CBauotechInPlaceTransform::Stop()
{

	//m_pOutputPin->m_graphStat = GRAPH_STOP;

 

	return CBaseFilter::Stop();
}

STDMETHODIMP CBauotechInPlaceTransform::Run(REFERENCE_TIME tStart)
{

	timeshift_wr = 0;
	timeshift_rd = 0;
	m_lastTime = time(NULL);
 
	m_goLive = true;
	return CBaseFilter::Run(tStart);
}


typedef UINT(CALLBACK* LPRunAlgoColors)(int effect, uint8_t *pData, uint32_t width, uint32_t height, uint32_t size);
typedef UINT(CALLBACK* LPRunAlgoAIDetection)(uint8_t *pData, uint32_t width, uint32_t height, uint32_t size);

LPRunAlgoColors lpfnRunAlgoColors;
LPRunAlgoAIDetection lpfnAIAlgo;

BOOL CBauotechInPlaceTransform::LoadAlgoDll()
{


	DWORD dwParam1;
	UINT  uParam2, uReturnVal;

	hDLL = LoadLibrary(L"C:\\Sightline\\x64\\BAlgoApi.dll");
	if (hDLL != NULL)
	{
		lpfnRunAlgoColors = (LPRunAlgoColors)GetProcAddress(hDLL, "RunAlgoColors");
		if (!lpfnRunAlgoColors)
		{
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		}

		lpfnAIAlgo = (LPRunAlgoAIDetection)GetProcAddress(hDLL, "RunAlgoAIDetection");
		if (!lpfnAIAlgo)
		{
			// handle the error
			FreeLibrary(hDLL);
			return 0;
		}

		return 1;
	}
	return 0;
}



//
// Transform
//
// Override CTransInPlaceFilter method.
// Convert the input sample into the output sample.
//
HRESULT CBauotechInPlaceTransform::Transform(IMediaSample *pSample)
{
  
    BYTE *pSampleBuffer;
    int iSize = pSample->GetActualDataLength();
    pSample->GetPointer(&pSampleBuffer);

	// call the function
	int uReturnVal = lpfnRunAlgoColors(m_effect, pSampleBuffer, 1920, 1080, 4);
	  
    return NOERROR;

} // Transform


//
// CheckInputType
//
// Override CTransformFilter method.
// Part of the Connect process.
// Ensure that we do not get connected to formats that we can't handle.
// We only work for wave audio, 8 or 16 bit, uncompressed.
//
HRESULT CBauotechInPlaceTransform::CheckInputType(const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);

	return NOERROR;

    DisplayType(TEXT("CheckInputType"), pmt);

    WAVEFORMATEX *pwfx = (WAVEFORMATEX *) pmt->pbFormat;

    // Reject non-Audio types.
    //
    if (pmt->majortype != MEDIATYPE_Audio) {
        return VFW_E_TYPE_NOT_ACCEPTED;
    }

    // Reject invalid format blocks
    //
    if (pmt->formattype != FORMAT_WaveFormatEx)
        return VFW_E_TYPE_NOT_ACCEPTED;

    // Reject compressed audio
    //
    if (pwfx->wFormatTag != WAVE_FORMAT_PCM) {
        return VFW_E_TYPE_NOT_ACCEPTED;
    }

    // Accept only 8 or 16 bit
    //
    if (pwfx->wBitsPerSample!=8 && pwfx->wBitsPerSample!=16) {
        return VFW_E_TYPE_NOT_ACCEPTED;
    }

    return NOERROR;

} // CheckInputType


//
// SetMediaType
//
// Override CTransformFilter method.
// Called when a connection attempt has succeeded. If the output pin
// is being connected and the input pin's media type does not agree then we
// reconnect the input (thus allowing its media type to change,) and vice versa.
//
HRESULT CBauotechInPlaceTransform::SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt)
{
    CheckPointer(pmt,E_POINTER);
    //DbgFunc("SetMediaType");


	return NOERROR;
 

} // SetMediaType



STDMETHODIMP CBauotechInPlaceTransform::GoToLive()
{
	m_goLive = true;
	return S_OK;

}
STDMETHODIMP CBauotechInPlaceTransform::SeekInTime(uint64_t ts_rd)
{
	 
	return S_OK;
}

STDMETHODIMP CBauotechInPlaceTransform::PauseTS()
{
	m_pause_ts = true;
	return S_OK;
}

STDMETHODIMP CBauotechInPlaceTransform::ResumeTS()
{
	 
	return S_OK;
}
 
STDMETHODIMP CBauotechInPlaceTransform::SetTimeShiftBuffer(uint64_t bufferSize)
{
	m_timshiftBufferSize = bufferSize;
	return S_OK;
}

STDMETHODIMP CBauotechInPlaceTransform::SetTimeShiftCallback(TimeShiftCallback p)
{

	pTimeShiftCallback = p;
	return S_OK;
}

STDMETHODIMP CBauotechInPlaceTransform::SetTimeDiffToLog(int difftime)
{
	m_timeDiffToLog = difftime;
	return S_OK;
}


uint64_t CBauotechInPlaceTransform::GetFifoSize(uint64_t rd)
{
	if (timeshift_wr == rd)
		return 0;
	if (timeshift_wr > rd)
		return timeshift_wr - rd;

	return (m_timshiftBufferSize - rd) + timeshift_wr;
}

////////////////////////////////////////////////////////////////////////
//
// Exported entry points for registration and unregistration 
// (in this case they only call through to default implementations).
//
////////////////////////////////////////////////////////////////////////

STDAPI DllRegisterServer()
{
  return AMovieDllRegisterServer2( TRUE );
}


STDAPI DllUnregisterServer()
{
  return AMovieDllRegisterServer2( FALSE );
}

//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

#pragma warning(disable: 4514) // "unreferenced inline function has been removed"



