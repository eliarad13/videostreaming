#pragma once 
#include <stdint.h>
#include <time.h>





#define IDB_DEFAULT                     1001
#define IDC_EMBOSS                      1002
#define IDC_GREY                        1003
#define IDC_BLUR                        1004
#define IDC_POSTERIZE                   1005
#define IDC_XOR                         1006
#define IDC_DARKEN                      1007
#define IDC_BLUE                        1008
#define IDC_GREEN                       1009
#define IDC_RED                         1010

 
class CBauotechInPlaceTransform
	// Inherited classes
	: public CTransInPlaceFilter       // Main DirectShow interfaces
	, public IBauotechInPlaceTransform


{  

public:

	static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);

	DECLARE_IUNKNOWN;

	//
	// --- CTransInPlaceFilter Overrides --
	//

	HRESULT CheckInputType(const CMediaType *mtIn);

	// Basic COM - used here to reveal our property interface.
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);


	STDMETHODIMP GoToLive();
	STDMETHODIMP SeekInTime(uint64_t ts_rd);
	STDMETHODIMP SetTimeShiftBuffer(uint64_t bufferSize);
	STDMETHODIMP SetTimeShiftCallback(TimeShiftCallback p);
	STDMETHODIMP SetTimeDiffToLog(int difftime);
	STDMETHODIMP PauseTS();
	STDMETHODIMP ResumeTS();



	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	int m_effect;
	uint64_t m_timshiftBufferSize;
	uint8_t *timeShiftBuffer;
	uint64_t timeshift_wr = 0;
	uint64_t timeshift_rd = 0;
	bool m_goLive;
	int m_timeDiffToLog;
	time_t m_lastTime;
	TimeShiftCallback  pTimeShiftCallback;
private:


	// Constructor
	CBauotechInPlaceTransform(TCHAR *tszName, LPUNKNOWN punk, HRESULT *phr);
	~CBauotechInPlaceTransform();
	uint64_t GetFifoSize(uint64_t rd);
	 

	bool m_pause_ts;
	HINSTANCE hDLL;               // Handle to DLL
	BOOL LoadAlgoDll();
	// Overrides the PURE virtual Transform of CTransInPlaceFilter base class
	// This is where the "real work" is done.
	HRESULT Transform(IMediaSample *pSample);

	// This is where the real work is really done (called from Transform)
	void MessItAbout(PBYTE pb, int cb);

	// Overrides a CTransformInPlace function.  Called as part of connecting.
	virtual HRESULT SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt);

	// If there are multiple instances of this filter active, it's
	// useful for debug messages etc. to know which one this is.
	// This variable has no other purpose.
	static int m_nInstanceCount;                   // total instances
	int m_nThisInstance;

	int                 m_Shape;               // 0==triangle, 1==square
	int                 m_SamplesPerSec;       // Current sample format
	int                 m_BytesPerSample;      // Current sample format
	int                 m_Channels;            // Current sample format
	int                 m_Phase;               // See MessItAbout in gargle.cpp
	CCritSec            m_GargleLock;          // To serialise access.

  
};  
