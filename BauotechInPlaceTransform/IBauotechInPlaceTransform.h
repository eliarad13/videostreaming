//------------------------------------------------------------------------------
// File: IGargle.h
//
// Desc: DirectShow sample code - custom interface that allows the user
//       to adjust the modulation rate.  It defines the interface between
//       the user interface component (the property sheet) and the filter
//       itself.  This interface is exported by the code in Gargle.cpp and
//       is used by the code in GargProp.cpp.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------


#ifndef __IGARGLE__
#define __IGARGLE__

#ifdef __cplusplus
extern "C" {
#endif

#include <stdint.h>

 
 
 
// {609D88B5-A153-4180-BD27-82CD7B897B91}
DEFINE_GUID(IID_IBaoutechInPlaceTransform,
	0x609d88b5, 0xa153, 0x4180, 0xbd, 0x27, 0x82, 0xcd, 0x7b, 0x89, 0x7b, 0x91);



typedef void(__stdcall * TimeShiftCallback)(uint64_t loaction, uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min, uint8_t sec);

 
DECLARE_INTERFACE_(IBauotechInPlaceTransform, IUnknown) {

	STDMETHOD(GoToLive)() PURE;
	STDMETHOD(SeekInTime)(uint64_t ts_rd) PURE;
	STDMETHOD(SetTimeShiftBuffer)(uint64_t bufferSize) PURE;
	STDMETHOD(SetTimeShiftCallback)(TimeShiftCallback p) PURE;
	STDMETHOD(SetTimeDiffToLog)(int difftime) PURE;
 

};


#ifdef __cplusplus
}
#endif

#endif // __IGARGLE__
