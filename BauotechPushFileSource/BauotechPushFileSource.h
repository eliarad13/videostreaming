#pragma once
#include <thread>
#include <memory>
using namespace std;

#define FIFO_SIZE 1024 * 1024 * 10


class CUDPMulticastClient
{
public:
	CUDPMulticastClient(const char *IpAddress, int port) : m_port(port)
	{
		Init();
		strcpy(m_ipAddress, IpAddress);
   
	}

	CUDPMulticastClient(int port) : m_port(port)
	{
		Init();

	}
	~CUDPMulticastClient();

	void SetIpAddress(char *p)
	{
		strcpy(m_ipAddress, p);
	}
	void SetPort(int m)
	{
		m_port = m;
	}

	int Setup();
	void StartReceiveThread();
	void StartReceiveThread2();
	void Start();
	void Stop();
	void Pause();


private:
	WSADATA			   wsaData;
	char			   m_ipAddress[100];
	int                m_port;
	uint8_t			   ReceiveBuf[RECEIVE_SIZE];
	uint8_t			   FrameBuf[RECEIVE_SIZE];
	int                BufLength;
	int                SenderAddrSize;
	int                ByteReceived;	
	int				   ErrorCode;

	int				   m_writeIndex;
	int				   m_readIndex;
	bool m_tryConnecting;
 

private:
	struct sockaddr_in server;
	void FrameThread();
	int recvfromTimeOutTCP(SOCKET socket, long sec, long usec);
	void PrintError(const char *msg);
	const wchar_t *GetWC(const char *c);
	void ProcessMessage(int SelectTiming);

	

	void Init()
	{
		
		WSAStartup(MAKEWORD(2, 2), &wsaData);
		BufLength = RECEIVE_SIZE;
	}

	void Enqueue(uint8_t *buffer, int size);
	int Dequeue();

	int GetFifoSize();

};

