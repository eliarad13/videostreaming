#include "LiveSource.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <wmcodecdsp.h>
#include "WinAES.h"
#include <fstream>
#include "TSDemux.h"

//https://www.codeproject.com/Articles/158053/DirectShow-Filters-Development-Part-Live-Source

using namespace std;



#define USE_AES  0
 
TSDemux tsDemux;

#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

  
  
CLiveSource::CLiveSource(LPUNKNOWN pUnk, HRESULT* phr)
	: CBaseFilter(LIVE_FILTER_NAME, pUnk, &m_critSec, CLSID_CBoutechPushFileSource)
{
	m_pOutputPin = new CLiveSourceStream(this, &m_critSec, phr); 
	// Initialize the critical section one time only.
	InitializeCriticalSectionAndSpinCount(&m_pOutputPin->CriticalSection, 0x00000400);	 
	
	if (phr)
	{
		if (m_pOutputPin == NULL)
			*phr = E_OUTOFMEMORY;
		else {
			*phr = S_OK;
		}
	}  
	m_pOutputPin->m_pcrAheadTime = 60000;
	strcpy(m_pOutputPin->m_fileName, "D:\\long_file_for_hls.ts");
}

CLiveSource::~CLiveSource(void)
{

	if (m_pOutputPin->aes != NULL)
	{	
		delete m_pOutputPin->aes;
		m_pOutputPin->aes = NULL;
	} 
	delete m_pOutputPin;
	   
	 
}

void CLiveSourceStream::InitializeAES()
{
	 

	byte key[WinAES::KEYSIZE_128] = { 0xF5, 0x13, 0x1E, 0xD8, 0x7F, 0xB3, 0x06, 0x94, 0x07, 0x7D, 0x85, 0xFD, 0xE0, 0x4C, 0x9C, 0xAC };
	byte iv[WinAES::BLOCKSIZE] = { 0xD8, 0xCD, 0xCF, 0x1E, 0xC2, 0x9E, 0xE8, 0x81, 0x98, 0x5C, 0xD3, 0x5B, 0x7A, 0xB1, 0xDA, 0x2A };
	
	aes = new WinAES();
 
	
	try
	{
		// Oops - no iv
		// aes.SetKey( key, sizeof(key) );

		// Oops - no key
		// aes.SetIv( iv, sizeof(iv) );

		// Set the key and IV
		aes->SetKeyWithIv(key, sizeof(key), iv, sizeof(iv));

		recovered = new byte[1024 * 10];

		// Done with key material - Microsoft owns it now...
		ZeroMemory(key, sizeof(key));


	}
	catch (const WinAESException& e)
	{

	}
	 

}
void CLiveSource::ReceiveThread2()
{
	int SelectTiming;
	 
	while (m_pOutputPin->m_graphStat == GRAPH_RUN)
	{
		if (m_pOutputPin->AddFrame() == S_FALSE)
			return;
	 
	}
 

} 

const wchar_t *CLiveSource::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}


LONGLONG CLiveSource::GetFileSize(string fileName)
{
	ifstream mySource;
	mySource.open(fileName, ios_base::binary);
	mySource.seekg(0, ios_base::end);
	LONGLONG size = mySource.tellg();
	mySource.close();
	return size;
}
 
STDMETHODIMP CLiveSource::Run(REFERENCE_TIME tStart)
{


	m_pOutputPin->m_graphStat = GRAPH_RUN;
	m_pOutputPin->m_curFileSize = GetFileSize(m_pOutputPin->m_fileName);
	tsDemux.PrintConfig(false);
	tsDemux.ReadInput(m_pOutputPin->m_fileName, 7);
	tsDemux.Streaming(m_pOutputPin->m_pcrAheadTime);

	shared_ptr<thread> t2 = make_shared<thread>(&CLiveSource::ReceiveThread2, this);
	t2->detach();
	

	return CBaseFilter::Run(tStart);
}
 
STDMETHODIMP CLiveSource::Pause()
{
	 
	m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP CLiveSource::Stop()
{

	m_pOutputPin->m_graphStat = GRAPH_STOP;
	tsDemux.Stop();
	tsDemux.Close();

	return CBaseFilter::Stop();
}

int CLiveSource::GetPinCount()
{
	CAutoLock cAutoLock(&m_critSec);

	return 1;
}

CBasePin* CLiveSource::GetPin(int n)
{
	CAutoLock cAutoLock(&m_critSec);

	return m_pOutputPin;
}

CUnknown* WINAPI CLiveSource::CreateInstance(LPUNKNOWN pUnk, HRESULT *phr)
{
	CUnknown* pNewFilter = new CLiveSource(pUnk, phr);
	 
	if (phr)
	{
		if (pNewFilter == NULL) 
			*phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) 
	{
		::MessageBox(NULL, L"Failed to call to  WSAStartup", L"Boutech Receiver", 0);
	}

	return pNewFilter;
}

STDMETHODIMP CLiveSource::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	CheckPointer(ppv, E_POINTER);

	 
	if (riid == IID_IBoutechPushFileSource)
	{
		return GetInterface((IBoutechPushFileSource*)m_pOutputPin, ppv);
	} 	
	else if(riid == IID_IAMFilterMiscFlags)
	{
		return GetInterface((IAMFilterMiscFlags*) this, ppv);
	}
	else 
	{
		return CBaseFilter::NonDelegatingQueryInterface(riid, ppv);
	}
}


//=============================================================================



CLiveSourceStream::CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr)
	: CBaseOutputPin(LIVE_FILTER_NAME, pFilter, pLock, phr, LIVE_OUTPIN_NAME),
	m_frameRate(0), m_rtFrameRate(0), m_lastFrame(0),
	m_videoResolutionWidth(640), m_videoResolutionHeight(480)
{
	m_loop = false;
	m_pinType = 0;
	m_bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
	m_bmpInfo.biCompression = BI_RGB;
	m_bmpInfo.biBitCount = 32;
	m_bmpInfo.biHeight = m_videoResolutionHeight;
	m_bmpInfo.biWidth = m_videoResolutionWidth;
	m_bmpInfo.biPlanes = 1;
	m_bmpInfo.biSizeImage = GetBitmapSize(&m_bmpInfo);	
	m_bmpInfo.biClrImportant = 0;
	m_bmpInfo.biClrUsed = 0;
	m_bmpInfo.biXPelsPerMeter = 0;
	m_bmpInfo.biYPelsPerMeter = 0;
	m_videoFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 4;
	m_videoSLAFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 3;
	aes = NULL;
	SetFrameRate(30);
 


#if USE_AES
	InitializeAES();
#endif 

}

CLiveSourceStream::~CLiveSourceStream()
{


#if USE_AES
	if (recovered != NULL)
	{
		delete recovered;
		recovered = NULL;
	}
#endif 
}

HRESULT CLiveSourceStream::CheckMediaType(const CMediaType* pmt)
{
	CheckPointer(pmt, E_POINTER);

	return S_OK;
#if 0 
	if (pmt->majortype == MEDIATYPE_Stream)
		return S_OK;

	if (pmt->majortype != MEDIATYPE_Video)
	{
		return E_INVALIDARG;
	}
	if (pmt->subtype != MEDIASUBTYPE_H264)
	{
		return E_INVALIDARG;
	}
	return S_OK;
#endif 
}

HRESULT CLiveSourceStream::GetMediaType(int iPosition, CMediaType* pmt)
{
	CAutoLock cAutoLock(m_pLock);

	if(iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if(iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)pmt->AllocFormatBuffer(sizeof(VIDEOINFOHEADER));
	if(!vih)
	{
		return E_OUTOFMEMORY;
	}

	vih->bmiHeader = m_bmpInfo;
	if(m_frameRate != 0)
	{
		vih->AvgTimePerFrame = UNITS / m_frameRate;
	}
	else
	{
		vih->AvgTimePerFrame = 0;
	}
	 

	switch (iPosition)
	{
		case 0:
			pmt->SetType(&MEDIATYPE_Video);
			pmt->SetSubtype(&MEDIASUBTYPE_H264);
			pmt->SetFormatType(&FORMAT_VideoInfo);
			pmt->SetTemporalCompression(TRUE /*FALSE*/);
			pmt->SetSampleSize(m_bmpInfo.biSizeImage);
		break;
		case 1:
			pmt->SetType(&MEDIATYPE_Stream);
			pmt->SetSubtype(&GUID_NULL);			
			pmt->SetFormatType(&GUID_NULL);
		break;
	}
	
	return S_OK;
}

 

HRESULT CLiveSourceStream::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest)
{
	CheckPointer(pAlloc,E_POINTER);
	CheckPointer(ppropInputRequest,E_POINTER);

	CAutoLock cAutoLock(m_pLock);
	HRESULT hr = NOERROR;

	ppropInputRequest->cBuffers = 10;
	ppropInputRequest->cbBuffer = 1504 * 1000; // m_bmpInfo.biSizeImage;

	ASSERT(ppropInputRequest->cbBuffer);

	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(ppropInputRequest,&Actual);
	if(FAILED(hr))
	{
		return hr;
	}

	if(Actual.cbBuffer < ppropInputRequest->cbBuffer)
	{
		return E_FAIL;
	}

	pAlloc->Commit();
	//m_pAllocator->Commit();
	m_PushFinished = false;
	
	shared_ptr<thread> t1 = make_shared<thread>(&CLiveSourceStream::ReceiveThread, this);
	t1->detach();
	

	return S_OK;
}

 

void CLiveSourceStream::ReceiveThread()
{
	char pinTypes[2][100] = { "c:\\avc_4_demux.ts", "c:\\avc_row_for_demux.ts" };
	 
	ifstream ifile(pinTypes[m_pinType]);
	if (!ifile) {
		return;
	}
	 

	while (true)
	{


	 
		//if (FileExists("c:\\avc_row_for_demux.ts") == true)
		//if (FileExists("c:\\avc_4_demux.ts") == true)
		{
			if (m_PushFinished == false)
			{
				if (PreFileHandle == NULL)
				{
					PreFileHandle = fopen(pinTypes[m_pinType], "r+b");
					//PreFileHandle = fopen("c:\\avc_row_for_demux.ts", "r+b");
				}
				HRESULT hr = S_OK;
				hr = AddFrameFromFile(PreFileHandle);
				if (hr == S_FALSE)
				{
					fclose(PreFileHandle);
					PreFileHandle = NULL;
					return;
				}
			}
			else
			{
				return;
			}
		}
	}

}

STDMETHODIMP CLiveSourceStream::SetBitmapInfo(BITMAPINFOHEADER& bInfo)
{
	if(bInfo.biHeight == 0 || bInfo.biWidth == 0 || bInfo.biBitCount == 0)
	{
		return E_INVALIDARG;
	}

	m_bmpInfo = bInfo;
	return S_OK;
}

  

STDMETHODIMP CLiveSourceStream::AddFrameFromFile(FILE *handle)
{

	//CAutoLock cAutoLock(m_pLock);


	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}


#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 

	int size = fread((char *)pData, 1 , RECEIVE_SIZE, handle);
	if (size == 0)
	{
		m_PushFinished = true;
		pSample->Release();
		return S_FALSE;
	}

	hr = pSample->SetActualDataLength(size);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	/*
	hr = pSample->SetSyncPoint(TRUE);
	if (FAILED(hr))
	{
	pSample->Release();
	return hr;
	}
	*/

#endif 
	hr = this->Deliver(pSample);
	pSample->Release();
	return hr;

}

STDMETHODIMP CLiveSourceStream::AddFrame()
{
	CAutoLock cAutoLock(m_pLock);
	
	if (m_running == false)
	{
		return S_FALSE;
	}
 
	if (m_curFileSize == 0)
	{
		if (m_loop == false)
			return S_FALSE;
		else
		{
			tsDemux.StreamLoop();
		}
	}

	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr;
	hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	
	int size = 0;
	if (tsDemux.Start(pData, &size) == false)
	{
		pSample->Release();
		m_pFilter->Stop();
		return S_FALSE;
	}


#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#endif 
	 

	hr = pSample->SetActualDataLength(size);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
 
	if (m_curFileSize > 0)
		m_curFileSize -= size;
 
	hr = this->Deliver(pSample);
	 
	pSample->Release();

 
	return hr;


}
   

STDMETHODIMP CLiveSourceStream::SetPcrAheadTime(int time)
{

	m_pcrAheadTime = time;

	return S_OK;
}
 
STDMETHODIMP CLiveSourceStream::SetFileName(WCHAR *fileName)
{
	 
	
	wcstombs(m_fileName, fileName, 1000);
 
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::StopGraph()
{

	m_graphStat = GRAPH_STOP;	 

	return S_OK;
}
  
/////////////////////////
HRESULT CLiveSourceStream::GetMediaSample(IMediaSample** ppSample)
{
	REFERENCE_TIME rtStart = m_lastFrame;
	m_lastFrame += m_rtFrameRate;

	//return this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);

	HRESULT hr;
	hr = this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = (*ppSample)->SetTime(&rtStart, &m_lastFrame);

	return hr;

}

STDMETHODIMP CLiveSourceStream::SetFrameRate(int frameRate)
{
	if(frameRate < 0 || frameRate > 30)
	{
		return E_INVALIDARG;
	}

	m_frameRate = frameRate;
	m_rtFrameRate = UNITS / m_frameRate;
	return S_OK;
}

HRESULT CLiveSourceStream::GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize)
{
	ASSERT(hBmp);

	BITMAP bmp = {0};
	int res = ::GetObject(hBmp, sizeof(BITMAP), &bmp);
	if(res != sizeof(BITMAP))
	{
		return E_FAIL;
	}

	if(bmp.bmBitsPixel != m_bmpInfo.biBitCount ||
		bmp.bmHeight != m_bmpInfo.biHeight ||
		bmp.bmWidth != m_bmpInfo.biWidth)
	{
		return E_INVALIDARG;
	}

	*pSize = bmp.bmWidthBytes * bmp.bmHeight;
	memcpy(*ppData, bmp.bmBits, *pSize);

	return S_OK;
}

  
STDMETHODIMP CLiveSourceStream::Notify(IBaseFilter * pSender, Quality q)
{
	if(q.Proportion <= 0)
	{
		m_rtFrameRate = 1000;        
	}
	else
	{
		m_rtFrameRate = m_rtFrameRate * 1000 / q.Proportion;
		if(m_rtFrameRate > 1000)
		{
			m_rtFrameRate = 1000;    
		}
		else if(m_rtFrameRate < 30)
		{
			m_rtFrameRate = 30;      
		}
	}

	if(q.Late > 0)
	{
		m_rtFrameRate += q.Late;
	}

	return S_OK;
}