#pragma once

#define LIVE_FILTER_NAME TEXT("Bauotech Push File Source")
#define LIVE_OUTPIN_NAME L"Out"

 
 

// {EB6271E8-DC99-4331-B3F6-9E8F4429BA68}
static const GUID IID_IBoutechPushFileSource =
{ 0xeb6271e8, 0xdc99, 0x4331, { 0xb3, 0xf6, 0x9e, 0x8f, 0x44, 0x29, 0xba, 0x68 } };


 
 
// {7485B582-503A-4F78-BE35-76BF21A4A87E}
static const GUID CLSID_CBoutechPushFileSource =
{ 0x7485b582, 0x503a, 0x4f78, { 0xbe, 0x35, 0x76, 0xbf, 0x21, 0xa4, 0xa8, 0x7e } };


 
DECLARE_INTERFACE_(IBoutechPushFileSource, IUnknown)
{
  
	STDMETHOD(SetFileName)(WCHAR *fileName) PURE;	
	STDMETHOD(SetPcrAheadTime)(int time) PURE;
	STDMETHOD(StopGraph)() PURE;
 
};

