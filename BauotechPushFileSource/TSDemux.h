#pragma once
#include "bitfile.h"
#include <iostream>
#include <fstream>
#include <cstring>
#include <fstream>
#include "TSPacket.h"
#include "fifo.h"
#include <thread>
#include <memory>
using namespace std;

#define SYNC_BYTE 0x47

 

class TSDemux
{
public:
	TSDemux();
	~TSDemux();
	void StreamLoop();
	void ReadInput(const char *fileName, int numTsPackets);
	bool Start(BYTE *pData, int *bufferSize);
	bool CreatePIDFile(int pid, const char *fileName);
	void SetBuffer(uint8_t *p, uint32_t size);
	void Process();
	bool PushData(uint8_t *buffer, uint32_t size);
	void PrintConfig(bool showPCR);
	void InitTSWorker(int packets, uint32_t fifoSize);
	void WaitWorker();
	void StopWorker();
	void Streaming(int pcrAheadTime);
	void Stop();
	bool Close();
	void Loop(bool l)
	{
		m_loop = l;
	}

private:
	int m_numTsPackets;
	int transport_packet();
	void adaptation_field();	
	void StreamingWaitPCR();
	bool InitUDPServer(char *strAddr, int port, bool server);
	bool SendData(const char *buf, int size);
	bool SendDataTo(const char *buf, int size);
	void ServerNagtation();
	int diffTime();

private:
	bool m_running;
	bool m_loop;
	bool m_receiveFrom;
	bool m_serverRunning;
	char m_serverIpAddress[100];
	double lastPcr;
	bool m_streaming;
	bool m_sendAsServer;
	bit_file_c bf;
	shared_ptr<thread> pthread;
	shared_ptr<thread> pserverThread;	
	bool m_tsworker;
	TSPacket  m_tsp;
	bool m_showPCR;
	SOCKET m_server;
	CFifo fifo;
	double m_startPCR;
};

