//------------------------------------------------------------------------------
// File: BoutechNetworkSender.h
//
// Desc: DirectShow sample code - definitions for dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------

#include <thread>
#include "myInterface.h"
using namespace std;

class CDumpInputPin;
class CDump;
class CDumpFilter;

#define BYTES_PER_LINE 20
#define FIRST_HALF_LINE TEXT  ("   %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x")
#define SECOND_HALF_LINE TEXT (" %2x %2x %2x %2x %2x %2x %2x %2x %2x %2x")


// Main filter object

class CDumpFilter : public CBaseFilter
{
    CDump * const m_pDump;

public:

    // Constructor
    CDumpFilter(CDump *pDump,
                LPUNKNOWN pUnk,
                CCritSec *pLock,
                HRESULT *phr);

	~CDumpFilter();

    // Pin enumeration
    CBasePin * GetPin(int n);
    int GetPinCount();

    // Open and close the file as necessary
    STDMETHODIMP Run(REFERENCE_TIME tStart);
    STDMETHODIMP Pause();
    STDMETHODIMP Stop();

	void SendData();
	void SenderThreadX();
	void QuickConnect(int *count4Connect);
	HRESULT ReconnectSocket(const char *IpAddress, const int port, const char *IpInterfaceAddress);

};


//  Pin object

class CDumpInputPin : public CRenderedInputPin
{
    CDump    * const m_pDump;           // Main renderer object
    CCritSec * const m_pReceiveLock;    // Sample critical section
    REFERENCE_TIME m_tLast;             // Last sample receive time

 

public:

    CDumpInputPin(CDump *pDump,
                  LPUNKNOWN pUnk,
                  CBaseFilter *pFilter,
                  CCritSec *pLock,
                  CCritSec *pReceiveLock,
                  HRESULT *phr);

    // Do something with this media sample
    STDMETHODIMP Receive(IMediaSample *pSample);
    STDMETHODIMP EndOfStream(void);
    STDMETHODIMP ReceiveCanBlock();

   
    // Check if the pin can support this specific proposed type and format
    HRESULT CheckMediaType(const CMediaType *);

    // Break connection
    HRESULT BreakConnect();

 
    // Track NewSegment
    STDMETHODIMP NewSegment(REFERENCE_TIME tStart,
                            REFERENCE_TIME tStop,
                            double dRate);
};


//  CDump object which has filter and pin members

class CDump : public CUnknown, public IBoutechUDPRemoteClient
{
    friend class CDumpFilter;
    friend class CDumpInputPin;

    CDumpFilter   *m_pFilter;       // Methods for filter interfaces
    CDumpInputPin *m_pPin;          // A simple rendered input pin

    CCritSec m_Lock;                // Main renderer critical section
    CCritSec m_ReceiveLock;         // Sublock for received samples

    CPosPassThru *m_pPosition;      // Renderer position controls

    HANDLE   m_hFile;               // Handle to file for dumping
    LPOLESTR m_pFileName;           // The filename where we dump
    BOOL     m_fWriteError;

public:

    DECLARE_IUNKNOWN

    CDump(LPUNKNOWN pUnk, HRESULT *phr);
    ~CDump();

    static CUnknown * WINAPI CreateInstance(LPUNKNOWN punk, HRESULT *phr);


	STDMETHODIMP SetIpAddress(const WCHAR *IpAddress);		
	STDMETHODIMP SetPort(const int port);
	STDMETHODIMP SetMTU(int mtu);
	STDMETHODIMP SetMulticast(bool multicast);



	void SenderThread();
	int GetFifoSize();

	int MTU = 1500;
	bool m_connecting = false;
	uint8_t *fifoBuffer = NULL;
	unsigned int fifo_write = 0;
	unsigned int fifo_read = 0;
	const int MAX_FIFO = MTU * 100000;
	int m_port;
	char m_ipAddress[100];
	char m_ipInterfaceAddress[100];
	bool m_multicast;
	WSADATA wsaData;
	struct addrinfo *result = NULL;
	SOCKET socketC = -1;
	struct sockaddr_in serverInfo;
	CRITICAL_SECTION CriticalSection;
	int m_clientConnected = 0;
	bool m_running = false;

	void InitFifo();
	void AddToFifo(uint8_t *data, int size);
	  

private:

    // Overriden to say what interfaces we support where
    STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

 
};

