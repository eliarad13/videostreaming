//------------------------------------------------------------------------------
// File: BoutechNetworkSenderUIDs.h
//
// Desc: DirectShow sample code - CLSIDs used by the dump renderer.
//
// Copyright (c) Microsoft Corporation.  All rights reserved.
//------------------------------------------------------------------------------
 
  

// {E922BAB9-8CFE-4675-A86A-2EFB1EE6BEFE}
DEFINE_GUID(CLSID_BoutechNetworkRemoteClientSender,
	0xe922bab9, 0x8cfe, 0x4675, 0xa8, 0x6a, 0x2e, 0xfb, 0x1e, 0xe6, 0xbe, 0xfe);
