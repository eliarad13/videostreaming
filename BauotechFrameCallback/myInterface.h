#pragma once 

#include <streams.h>
#include <stdio.h>
#include <stdlib.h>



// {9CC13541-649B-4228-81FF-B98A71959D7B}
static const GUID IID_IBauotechFrameCallback =
{ 0x9cc13541, 0x649b, 0x4228, { 0x81, 0xff, 0xb9, 0x8a, 0x71, 0x95, 0x9d, 0x7b } };

 


typedef void (CALLBACK *MANAGEDCALLBACKPROC)(BYTE* pdata, long len, int width, int bitCount, int height, int stride);


DECLARE_INTERFACE_(IBauotechFrameCallback, IUnknown)
{		 
	STDMETHOD(SetFramesCallback)(MANAGEDCALLBACKPROC progressCallback) PURE;
};