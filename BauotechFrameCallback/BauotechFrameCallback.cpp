
#include <windows.h>
#include <commdlg.h>
#include <streams.h>
#include <initguid.h>
#include <strsafe.h>
#include "myInterface.h"
#include "BauotechFrameCallbackuids.h"
#include "BauotechFrameCallback.h"


// Setup data

const AMOVIESETUP_MEDIATYPE sudPinTypes =
{
    &MEDIATYPE_NULL,            // Major type
    &MEDIASUBTYPE_NULL          // Minor type
};

const AMOVIESETUP_PIN sudPins =
{
    L"Input",                   // Pin string name
    FALSE,                      // Is it rendered
    FALSE,                      // Is it an output
    FALSE,                      // Allowed none
    FALSE,                      // Likewise many
    &CLSID_NULL,                // Connects to filter
    L"Output",                  // Connects to pin
    1,                          // Number of types
    &sudPinTypes                // Pin information
};

const AMOVIESETUP_FILTER sudDump =
{
	&CLSID_BauotechFrameCallback,                // Filter CLSID
    L"Bauotech Frame Callback",                    // String name
    MERIT_DO_NOT_USE,           // Filter merit
    1,                          // Number pins
    &sudPins                    // Pin details
};


//
//  Object creation stuff
//
CFactoryTemplate g_Templates[]= {
	L"Bauotech Frame Callback", &CLSID_BauotechFrameCallback, CDump::CreateInstance, NULL, &sudDump
};
int g_cTemplates = 1;


// Constructor

CDumpFilter::CDumpFilter(CDump *pDump,
                         LPUNKNOWN pUnk,
                         CCritSec *pLock,
                         HRESULT *phr) :
						 CBaseFilter(NAME("CDumpFilter"), pUnk, pLock, CLSID_BauotechFrameCallback),
    m_pDump(pDump)
{
}


//
// GetPin
//
CBasePin * CDumpFilter::GetPin(int n)
{
    if (n == 0) {
        return m_pDump->m_pPin;
    } else {
        return NULL;
    }
}
 
//
// GetPinCount
//
int CDumpFilter::GetPinCount()
{
    return 1;
}


//
// Stop
//
// Overriden to close the dump file
//
STDMETHODIMP CDumpFilter::Stop()
{
    CAutoLock cObjectLock(m_pLock);
	m_pDump->m_running = false;
    
    return CBaseFilter::Stop();
}


//
// Pause
//
// Overriden to open the dump file
//
STDMETHODIMP CDumpFilter::Pause()
{
    CAutoLock cObjectLock(m_pLock);

    

    return CBaseFilter::Pause();
}

 
STDMETHODIMP CDumpFilter::Run(REFERENCE_TIME tStart)
{
	
	m_pDump->m_fileIndex = 0;
	m_pDump->m_frameCounter = 0;
	m_pDump->m_running = true;

    CAutoLock cObjectLock(m_pLock);
  
    return CBaseFilter::Run(tStart);
}


//
//  Definition of CDumpInputPin
//
CDumpInputPin::CDumpInputPin(CDump *pDump,
                             LPUNKNOWN pUnk,
                             CBaseFilter *pFilter,
                             CCritSec *pLock,
                             CCritSec *pReceiveLock,
                             HRESULT *phr) :

    CRenderedInputPin(NAME("CDumpInputPin"),
                  pFilter,                   // Filter
                  pLock,                     // Locking
                  phr,                       // Return code
                  L"Input"),                 // Pin name
    m_pReceiveLock(pReceiveLock),
    m_pDump(pDump),
    m_tLast(0)	
{
}


HRESULT
CDumpInputPin::SetMediaType(PIN_DIRECTION direction, const CMediaType *pmt)
{
	HRESULT hr = S_OK;
	VIDEOINFOHEADER* vih = (VIDEOINFOHEADER *)pmt->pbFormat;

	if (pmt->majortype == MEDIATYPE_Video &&
		(pmt->subtype == MEDIASUBTYPE_RGB24) &&
		pmt->formattype == FORMAT_VideoInfo &&
		pmt->cbFormat >= sizeof(VIDEOINFOHEADER) &&
		pmt->pbFormat != NULL)
	{
		// calculate the stride for RGB formats
		DWORD dwStride = (vih->bmiHeader.biWidth * (vih->bmiHeader.biBitCount / 8) + 3) & ~3;

		// set video parameters
		m_Width = (int)vih->bmiHeader.biWidth;
		m_Height = (int)vih->bmiHeader.biHeight;
		m_SampleSize = (int)pmt->lSampleSize;

		m_Stride = dwStride;
	}
	else hr = E_FAIL;

	return hr;
}


//
// CheckMediaType
//
// Check if the pin can support this specific proposed type and format
//
HRESULT CDumpInputPin::CheckMediaType(const CMediaType *pmt)
{
	  
 
	return S_OK;

	if (pmt->majortype == MEDIATYPE_Video &&
		(pmt->subtype == MEDIASUBTYPE_RGB24) &&
		pmt->formattype == FORMAT_VideoInfo)
	{
		VIDEOINFOHEADER* vih = (VIDEOINFOHEADER *)pmt->pbFormat;
		if (vih->bmiHeader.biBitCount != 24)
		{
			return E_FAIL;
		}
		m_Width = vih->bmiHeader.biWidth;
		m_Height = vih->bmiHeader.biHeight;
		m_bitCount = vih->bmiHeader.biBitCount;
		return S_OK;
	}
	else
	{
		return E_FAIL;
	}
 
	 
}

 
HRESULT CDumpInputPin::BreakConnect()
{

    return CRenderedInputPin::BreakConnect();
}
 
STDMETHODIMP CDumpInputPin::Receive(IMediaSample *pSample)
{
    CheckPointer(pSample,E_POINTER);


	if (m_pDump->m_running == false)
	{
		return S_OK;
	}

    CAutoLock lock(m_pReceiveLock);
    PBYTE pbData;

      
    REFERENCE_TIME tStart, tStop;
    pSample->GetTime(&tStart, &tStop);
	 
	 
    m_tLast = tStart;

    // Copy the data to the file
    HRESULT hr = pSample->GetPointer(&pbData);
    if (FAILED(hr)) {
        return hr;
    }

	if (m_pDump->pProgressCallback != nullptr)
    {
        m_pDump->pProgressCallback(pbData, pSample->GetActualDataLength(), m_Width, m_Height, m_bitCount, m_Stride);
    }

	return S_OK;
}


//
//  CDump class
//
CDump::CDump(LPUNKNOWN pUnk, HRESULT *phr) :
    CUnknown(NAME("CDump"), pUnk),
    m_pFilter(NULL),
    m_pPin(NULL)
{
    ASSERT(phr);
	
	
	pProgressCallback = NULL;


    m_pFilter = new CDumpFilter(this, GetOwner(), &m_Lock, phr);
    if (m_pFilter == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }

    m_pPin = new CDumpInputPin(this,GetOwner(),
                               m_pFilter,
                               &m_Lock,
                               &m_ReceiveLock,
                               phr);
    if (m_pPin == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
        return;
    }
}
  
CDump::~CDump()
{
  
    delete m_pPin;
    delete m_pFilter;
}


//
// CreateInstance
//
// Provide the way for COM to create a dump filter
//
CUnknown * WINAPI CDump::CreateInstance(LPUNKNOWN punk, HRESULT *phr)
{
    ASSERT(phr);
    
    CDump *pNewObject = new CDump(punk, phr);
    if (pNewObject == NULL) {
        if (phr)
            *phr = E_OUTOFMEMORY;
    }

    return pNewObject;

} // CreateInstance
 

STDMETHODIMP CDump::SetFramesCallback(MANAGEDCALLBACKPROC progressCallback)
{
	 
	pProgressCallback = progressCallback;
	return S_OK;
}

//
// NonDelegatingQueryInterface
//
// Override this to say what interfaces we support where
//
STDMETHODIMP CDump::NonDelegatingQueryInterface(REFIID riid, void ** ppv)
{
    CheckPointer(ppv,E_POINTER);
    CAutoLock lock(&m_Lock);

    // Do we have this interface

	if (riid == IID_IBauotechFrameCallback) {
		return GetInterface((IBauotechFrameCallback *) this, ppv);
	}
	if (riid == IID_IBaseFilter || riid == IID_IMediaFilter || riid == IID_IPersist) {
        return m_pFilter->NonDelegatingQueryInterface(riid, ppv);
    } 
    
    return CUnknown::NonDelegatingQueryInterface(riid, ppv);

} // NonDelegatingQueryInterface
 
   
//
// DllRegisterSever
//
// Handle the registration of this filter
//
STDAPI DllRegisterServer()
{
    return AMovieDllRegisterServer2( TRUE );

} // DllRegisterServer


//
// DllUnregisterServer
//
STDAPI DllUnregisterServer()
{
    return AMovieDllRegisterServer2( FALSE );

} // DllUnregisterServer


//
// DllEntryPoint
//
extern "C" BOOL WINAPI DllEntryPoint(HINSTANCE, ULONG, LPVOID);

BOOL APIENTRY DllMain(HANDLE hModule, 
                      DWORD  dwReason, 
                      LPVOID lpReserved)
{
	return DllEntryPoint((HINSTANCE)(hModule), dwReason, lpReserved);
}

