﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Sockets;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SendVideoApp
{
    public class udp_sender
    {
        UdpClient udpClient = new UdpClient();
        Thread m_thread;
        public void Init(string IpAddress, int port)
        {
            udpClient.Connect(IpAddress, port);

        }
        string m_fileName;
        bool m_loop;
        public bool Start(string fileName, bool loop)
        {
            if (File.Exists(fileName) == false)
                return false;

            m_fileName = fileName;
            m_thread = new Thread(SendThrad);
            m_thread.Start();

            return true;
        }
        bool m_running = false;
        public void Stop()
        {
            m_running = false;
        }
        public void SendThrad()
        {
            m_running = true;
            byte[] buffer = new byte[8192];
            byte[] sync = { 0x71, 0x12, 0x57, 0x61, 0xF1,0x5A, 0x92, 0x0,0x11,0x12,0x13,0x14,0x15,0x88,0x99,0xAA};
            while (m_running)
            {               
                FileInfo fi = new FileInfo(m_fileName);
                long size = fi.Length;
                using (BinaryReader b = new BinaryReader(File.Open(m_fileName, FileMode.Open)))
                {
                    udpClient.Send(sync, sync.Length);
                    while (size > 0)
                    {
                        long s = Math.Min(size, 8192);
                        b.Read(buffer, 0, (int)s);
                        udpClient.Send(buffer, (int)s);
                        Thread.Sleep(5);
                        size -= s;
                        if (m_running == false)
                            return;
                    }
                }
            }
        }
    }
}
