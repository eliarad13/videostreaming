#pragma once

#define LIVE_FILTER_NAME TEXT("Bauotech Push DVR Source")
#define LIVE_OUTPIN_NAME L"Out"

 
 
 
// {E85FEC76-7949-4F63-B76A-46AD90FF384C}
static const GUID IID_IBoutechPushDVRSource =
{ 0xe85fec76, 0x7949, 0x4f63, { 0xb7, 0x6a, 0x46, 0xad, 0x90, 0xff, 0x38, 0x4c } };


// {35525B8F-A479-4350-ACB8-3B39994C1A0C}
static const GUID CLSID_CBoutechPushDVRSource =
{ 0x35525b8f, 0xa479, 0x4350, { 0xac, 0xb8, 0x3b, 0x39, 0x99, 0x4c, 0x1a, 0xc } };

 
 
DECLARE_INTERFACE_(IBoutechPushDVRSource, IUnknown)
{
	 
	STDMETHOD(SetFilePath)(WCHAR *filePath) PURE;
 
};

