#pragma once

#include <streams.h>
#include "IAddFrame.h"
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <thread>
#include "WinAES.h"
#include <time.h>
#include "mpegts.hpp"


#define USE_FIFO 1
using namespace std;

typedef enum GRAPHSTAT
{
	GRAPH_STOP,
	GRAPH_PAUSE,
	GRAPH_RUN,

} GRAPHSTAT;
 
#define RECEIVE_SIZE  1316
 

int ReadParamsteres();

class CLiveSourceStream;

class CLiveSource : public CBaseFilter, public IAMFilterMiscFlags
{
public:
	DECLARE_IUNKNOWN;

	CLiveSource(LPUNKNOWN pUnk, HRESULT* phr);
	virtual ~CLiveSource(void);

	static CUnknown* WINAPI CreateInstance(LPUNKNOWN pUnk, HRESULT *phr);
	STDMETHODIMP NonDelegatingQueryInterface(REFIID riid, void ** ppv);

	int GetPinCount();
	CBasePin* GetPin(int n);

	virtual ULONG STDMETHODCALLTYPE GetMiscFlags( void)
	{
		return AM_FILTER_MISC_FLAGS_IS_SOURCE;
	}

	STDMETHODIMP Run(REFERENCE_TIME tStart);
	STDMETHODIMP Pause();
	STDMETHODIMP Stop();

	void ReceiveThread3();
	void ReceiveThread2();
	int Setup(bool multicast);
	const wchar_t *GetWC(const char *c);
	time_t m_lastTime;

	void InitTSParser();
private:
	CLiveSourceStream* m_pOutputPin;
	CCritSec m_critSec;
	bool m_syncRequired;
	
	SOCKET  ReceivingSocket;
	struct sockaddr_in addr;

	bool m_canStart;
	util::mpegts_parser tsParser;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;
	 
	int m_timeToLogInSec;
};

class CLiveSourceStream : public CBaseOutputPin, public IBoutechLiveAndTimeShiftReceiver
{
public:

	DECLARE_IUNKNOWN;

	CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr);
	virtual ~CLiveSourceStream();

	int m_videoResolutionWidth;
	int m_videoResolutionHeight;
	int m_videoFrameSize;
	int m_videoSLAFrameSize;

	// CBaseOutputPin overrides
	virtual HRESULT GetMediaType(int iPosition, CMediaType* pmt);
	virtual HRESULT CheckMediaType(const CMediaType *pmt);
	virtual HRESULT DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest);

	// ISLA3000 Interface
	virtual STDMETHODIMP SetIpAddress(WCHAR *IpAddress, bool isMulticast);
	virtual STDMETHODIMP SetPort(int port);
	virtual STDMETHODIMP SetPinType(int pinType);
	virtual STDMETHODIMP GoToLive();	
	virtual STDMETHODIMP SeekInTime(uint64_t ts_rd);
	virtual STDMETHODIMP SetTimeShiftBuffer(uint64_t bufferSize);
	virtual STDMETHODIMP SetTimeShiftCallback(TimeShiftCallback p);
	virtual STDMETHODIMP SetTimeDiffToLog(int difftime);


	// ILiveSource members
	virtual STDMETHODIMP AddFrameFromFile(FILE *handle);
	virtual STDMETHODIMP AddFrame(int clientSocket);
	virtual STDMETHODIMP SetFrameRate(int frameRate);
	virtual STDMETHODIMP SetBitmapInfo(BITMAPINFOHEADER& bInfo);

	virtual STDMETHODIMP Notify(IBaseFilter * pSender, Quality q);
	void InitFifo();
	void AddToFifo(uint8_t *data, int size);
	void ReceiveThread();
	bool m_running;
	char			   m_ipAddress[100];
	int                m_port;
	WinAES *aes;
	bool m_isMulticast;
	uint8_t *fifoBuffer = NULL;
	unsigned int fifo_write = 0;
	unsigned int fifo_read = 0;

	uint8_t *timeShiftBuffer = NULL;
	uint64_t timeshift_wr = 0;
	uint64_t timeshift_rd = 0;
	bool m_goLive = true;
	bool m_goLiveNeedSink = false;
	 
	CRITICAL_SECTION CriticalSection;
	GRAPHSTAT  m_graphStat = GRAPH_STOP;
	byte *recovered = NULL;
	uint64_t m_timshiftBufferSize;
	TimeShiftCallback  pTimeShiftCallback;
	int m_timeDiffToLog;
	int m_pinType;
private:
	HRESULT GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize);
	HRESULT GetMediaSample(IMediaSample** ppSample);

	util::mpegts_info info;
	bool m_canStart;
	util::mpegts_parser tsParser;
	bool show_pcr_time;
	bool show_frame_pos;
	bool show_frame_pts;
	bool show_frame_dts;
	bool show_key_frame;
	util::mpegts_info mi;
	int vc;
	int sc;
	int offset;
	int64_t pts;
	int64_t dts = 0;
	bool vknown_type;
	bool aknown_type;
	util::stream_info si;
	std::vector<util::stream_info> streams;
	

	void InitializeAES();
	void setSocket(int s);
	int GetFifoSize();
	const int MAX_FIFO = 100000000;

private:
	BITMAPINFOHEADER m_bmpInfo;
	int m_frameRate;
	REFERENCE_TIME m_rtFrameRate; 
	REFERENCE_TIME m_lastFrame; 
	FILE *PreFileHandle = NULL;
	bool m_PushFinished = false;
	
	
};
