#include <WinSock2.h>
#include "LiveSource.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <memory>
#include <wmcodecdsp.h>
#include "WinAES.h"
#include <fstream>
#include <ws2ipdef.h>


//https://www.codeproject.com/Articles/158053/DirectShow-Filters-Development-Part-Live-Source

using namespace std;



const uint64_t MAX_TIMESHIFT_BUFFER = 1024 * 1024 * 188;


#define USE_AES  0
 


#define MIN(a,b) (((a)<(b))?(a):(b))
#define MAX(a,b) (((a)>(b))?(a):(b))

 
void CLiveSourceStream::InitFifo()
{
	fifoBuffer = (uint8_t *)malloc(MAX_FIFO);
}

#if USE_FIFO
void CLiveSource::ReceiveThread3()
{
	util::mpegts_info info;
	uint8_t buffer[RECEIVE_SIZE];
	int count = 0;




	if (m_pOutputPin->m_pinType == 1)
	{


		while (m_pOutputPin->m_graphStat == GRAPH_RUN)
		{

			int size = recv(
				ReceivingSocket,
				(char *)buffer,
				RECEIVE_SIZE,
				0);
			if (size > 0)
			{ 

				m_pOutputPin->AddToFifo(buffer, size);
				if (m_pOutputPin->pTimeShiftCallback != NULL)
				{
					memcpy(m_pOutputPin->timeShiftBuffer + m_pOutputPin->timeshift_wr, buffer, size);
					m_pOutputPin->timeshift_wr = (m_pOutputPin->timeshift_wr + size) % m_pOutputPin->m_timshiftBufferSize;

					time_t t = time(NULL);
					struct tm tm = *localtime(&t);
					struct tm tmLast = *localtime(&m_lastTime);
					double diff = difftime(t, m_lastTime);
					if (diff >= m_pOutputPin->m_timeDiffToLog)
					{
						m_pOutputPin->pTimeShiftCallback(m_pOutputPin->timeshift_wr, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
						m_lastTime = t;
					}
				}
			}
		} 

		return;
	}


	while (m_pOutputPin->m_graphStat == GRAPH_RUN)
	{

		int size = recv(
			ReceivingSocket,
			(char *)buffer,
			RECEIVE_SIZE,
			0);
		if (size > 0)
		{

			if (m_syncRequired == true)
			{
				memset(&info, 0, sizeof(info));
				auto suc = tsParser.do_parser(buffer, info);

				//if (info.type_ == util::mpegts_info::idr)
				if (info.pict_type_ != util::av_picture_type_i)
				{
					continue;
				}
				m_syncRequired = false;
			}

			m_pOutputPin->AddToFifo(buffer, size);
			if (m_pOutputPin->pTimeShiftCallback != NULL)
			{
				memcpy(m_pOutputPin->timeShiftBuffer + m_pOutputPin->timeshift_wr, buffer, size);
				m_pOutputPin->timeshift_wr = (m_pOutputPin->timeshift_wr + size) % m_pOutputPin->m_timshiftBufferSize;

				time_t t = time(NULL);
				struct tm tm = *localtime(&t);
				struct tm tmLast = *localtime(&m_lastTime);
				double diff = difftime(t, m_lastTime);
				if (diff >= m_pOutputPin->m_timeDiffToLog)
				{
					m_pOutputPin->pTimeShiftCallback(m_pOutputPin->timeshift_wr, tm.tm_year + 1900, tm.tm_mon + 1, tm.tm_mday, tm.tm_hour, tm.tm_min, tm.tm_sec);
					m_lastTime = t;
				}
			}
		}
	}
}
#endif 
  

void CLiveSource::InitTSParser()
{

	show_pcr_time = false;
	show_frame_pos = false;
	show_frame_pts = false;
	show_frame_dts = false;
	show_key_frame = true;


	vc = 0;
	sc = 0;
	offset = 0;
	pts = 0;
	dts = 0;
	vknown_type = false;
	aknown_type = false;


	si.pid_ = 40;
	si.type_ = util::stream_info::stream_video;
	si.stream_type_ = tsParser.stream_type(std::string("H264"));
	streams.push_back(si);
	si.pid_ = 50;
	si.type_ = util::stream_info::stream_audio;
	si.stream_type_ = tsParser.stream_type(std::string("AAC"));
	streams.push_back(si);
	tsParser.init_streams(streams);

	mi.pid_ = 40;
	mi.is_video_ = true;
	mi.is_audio_ = false;

	m_canStart = false;
}



void CLiveSourceStream::AddToFifo(uint8_t *data, int size)
{

	int x = MAX_FIFO - GetFifoSize();
	if (x < size)
	{
		return;
	}

	int d = MIN(size, MAX_FIFO - fifo_write);
	memcpy(fifoBuffer + fifo_write, data, d);

	EnterCriticalSection(&CriticalSection);
	fifo_write = (fifo_write + d) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);


	size -= d;
	if (size > 0)
	{
		memcpy(fifoBuffer + fifo_write, data, size);
		EnterCriticalSection(&CriticalSection);
		fifo_write = (fifo_write + size) % MAX_FIFO;
		LeaveCriticalSection(&CriticalSection);
	}
}

  
int CLiveSourceStream::GetFifoSize()
{

	EnterCriticalSection(&CriticalSection);

	if (fifo_write == fifo_read)
	{
		LeaveCriticalSection(&CriticalSection);
		return 0;
	}


	if (fifo_write > fifo_read)
	{
		int x = fifo_write - fifo_read;
		LeaveCriticalSection(&CriticalSection);
		return x;
	}

	int x = (MAX_FIFO - fifo_read) + fifo_write;
	LeaveCriticalSection(&CriticalSection);
	return x;
}
  
CLiveSource::CLiveSource(LPUNKNOWN pUnk, HRESULT* phr)
	: CBaseFilter(LIVE_FILTER_NAME, pUnk, &m_critSec, CLSID_CBoutechLiveAndTimeShiftReceiver)
{
	m_pOutputPin = new CLiveSourceStream(this, &m_critSec, phr); 
	// Initialize the critical section one time only.
	InitializeCriticalSectionAndSpinCount(&m_pOutputPin->CriticalSection, 0x00000400);
	ReceivingSocket = -1;

	strcpy(m_pOutputPin->m_ipAddress, "234.5.5.5");
	m_pOutputPin->m_port = 6000;
	m_pOutputPin->m_pinType = 0;

#if USE_FIFO
	m_pOutputPin->InitFifo();	
#endif


	if (phr)
	{
		if (m_pOutputPin == NULL)
			*phr = E_OUTOFMEMORY;
		else {
			*phr = S_OK;
		}
	}  
}

CLiveSource::~CLiveSource(void)
{

	if (m_pOutputPin->aes != NULL)
	{	
		delete m_pOutputPin->aes;
		m_pOutputPin->aes = NULL;
	}
	 
	 
	delete m_pOutputPin;
	   
	if (m_pOutputPin->fifoBuffer != NULL)
	{
		free(m_pOutputPin->fifoBuffer);
		m_pOutputPin->fifoBuffer = NULL;
	}
	WSACleanup();
}

void CLiveSourceStream::InitializeAES()
{
	 

	byte key[WinAES::KEYSIZE_128] = { 0xF5, 0x13, 0x1E, 0xD8, 0x7F, 0xB3, 0x06, 0x94, 0x07, 0x7D, 0x85, 0xFD, 0xE0, 0x4C, 0x9C, 0xAC };
	byte iv[WinAES::BLOCKSIZE] = { 0xD8, 0xCD, 0xCF, 0x1E, 0xC2, 0x9E, 0xE8, 0x81, 0x98, 0x5C, 0xD3, 0x5B, 0x7A, 0xB1, 0xDA, 0x2A };
	
	aes = new WinAES();
 
	
	try
	{
		// Oops - no iv
		// aes.SetKey( key, sizeof(key) );

		// Oops - no key
		// aes.SetIv( iv, sizeof(iv) );

		// Set the key and IV
		aes->SetKeyWithIv(key, sizeof(key), iv, sizeof(iv));

		recovered = new byte[1024 * 10];

		// Done with key material - Microsoft owns it now...
		ZeroMemory(key, sizeof(key));


	}
	catch (const WinAESException& e)
	{

	}
	 

}
void CLiveSource::ReceiveThread2()
{
	int SelectTiming;
	 
	while (m_pOutputPin->m_graphStat == GRAPH_RUN)
	{
		m_pOutputPin->AddFrame(ReceivingSocket);
		//Sleep(1);
	}
 

} 

const wchar_t *CLiveSource::GetWC(const char *c)
{
	const size_t cSize = strlen(c) + 1;
	wchar_t* wc = new wchar_t[cSize];
	mbstowcs(wc, c, cSize);

	return wc;
}

int CLiveSource::Setup(bool multicast)
{
	int iResult;

	if (ReceivingSocket != -1)
	{
		closesocket(ReceivingSocket);
		ReceivingSocket = -1;
	}

	// Create a new socket to receive datagrams on.
	ReceivingSocket = ::socket(AF_INET, SOCK_DGRAM, 0);
	if (ReceivingSocket == INVALID_SOCKET)
	{
		printf("Server: Error at socket(): %ld\n", WSAGetLastError());
		WSACleanup();
		return -132;
	}
	// allow multiple sockets to use the same PORT number
	//
	u_int yes = 1;

	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_REUSEADDR, (char*)&yes, sizeof(yes)) < 0)
	{
		return -44;
	}

	memset(&addr, 0, sizeof(addr));
	addr.sin_family = AF_INET;
	if (multicast == false)
	{
		DWORD ip = inet_addr(m_pOutputPin->m_ipAddress);
		addr.sin_addr.s_addr = ip;
	}
	else {
		addr.sin_addr.s_addr = htonl(INADDR_ANY); // differs from sender
	}
	addr.sin_port = htons(m_pOutputPin->m_port);


	int iOptVal = 0;
	int iOptLen = sizeof(int);
	int recvBufSize;
	getsockopt(ReceivingSocket, SOL_SOCKET, SO_RCVBUF, (char *)&recvBufSize, &iOptLen);

	int a = 65535;
	if (setsockopt(ReceivingSocket, SOL_SOCKET, SO_RCVBUF, (char *)&a, sizeof(int)) == -1)
	{
		return -15;
	}


	// bind to receive address
	//
	if (::bind(ReceivingSocket, (struct sockaddr*) &addr, sizeof(addr)) < 0) {
		perror("bind");
		return 1;
	}

	if (multicast)
	{
		IP_MREQ mreq;
		mreq.imr_multiaddr.s_addr = inet_addr(m_pOutputPin->m_ipAddress);
		mreq.imr_interface.s_addr = htonl(INADDR_ANY);
		if (setsockopt(ReceivingSocket, IPPROTO_IP, IP_ADD_MEMBERSHIP, (char*)&mreq, sizeof(mreq)) < 0)
		{

			return -5;
		}
	}


	return 1;

}

 
STDMETHODIMP CLiveSource::Run(REFERENCE_TIME tStart)
{
	
	m_syncRequired = true;
	m_pOutputPin->fifo_write = 0;
	m_pOutputPin->fifo_read = 0;
	if (m_pOutputPin->m_timshiftBufferSize == 0)
	{
		m_pOutputPin->m_timshiftBufferSize = MAX_TIMESHIFT_BUFFER;
	}
	if (m_pOutputPin->timeShiftBuffer == NULL)
	{
		m_pOutputPin->timeShiftBuffer = new uint8_t[m_pOutputPin->m_timshiftBufferSize];
	}
	 
	m_pOutputPin->timeshift_wr = 0;
	m_pOutputPin->timeshift_rd = 0;
	m_pOutputPin->m_goLive = true;

	m_pOutputPin->m_graphStat = GRAPH_RUN;


	int res = Setup(m_pOutputPin->m_isMulticast);
	if (res < 0)
	{
		::MessageBox(NULL, L"Live receiver could not setup socket", L"Live and Timeshift", 0);
		return S_FALSE;
	}

#if USE_FIFO
	shared_ptr<thread> t1 =	make_shared<thread>(&CLiveSource::ReceiveThread3, this);	
	t1->detach();
#endif 
	
	shared_ptr<thread> t2 = make_shared<thread>(&CLiveSource::ReceiveThread2, this);
	t2->detach();
	
	m_lastTime = time(NULL);
	return CBaseFilter::Run(tStart);
}
 
STDMETHODIMP CLiveSource::Pause()
{
	 
	m_pOutputPin->m_graphStat = GRAPH_PAUSE;
	return CBaseFilter::Pause();
}
STDMETHODIMP CLiveSource::Stop()
{

	m_pOutputPin->m_graphStat = GRAPH_STOP;

	if (ReceivingSocket != -1)
	{
		closesocket(ReceivingSocket);
		ReceivingSocket = -1;
	}

	return CBaseFilter::Stop();
}

int CLiveSource::GetPinCount()
{
	CAutoLock cAutoLock(&m_critSec);

	return 1;
}

CBasePin* CLiveSource::GetPin(int n)
{
	CAutoLock cAutoLock(&m_critSec);

	return m_pOutputPin;
}

CUnknown* WINAPI CLiveSource::CreateInstance(LPUNKNOWN pUnk, HRESULT *phr)
{
	CUnknown* pNewFilter = new CLiveSource(pUnk, phr);
	 
	if (phr)
	{
		if (pNewFilter == NULL) 
			*phr = E_OUTOFMEMORY;
		else
			*phr = S_OK;
	}

	WSADATA wsaData;
	if (WSAStartup(0x0101, &wsaData)) 
	{
		::MessageBox(NULL, L"Failed to call to  WSAStartup", L"Boutech Receiver", 0);
	}

	return pNewFilter;
}

STDMETHODIMP CLiveSource::NonDelegatingQueryInterface(REFIID riid, void **ppv)
{
	CheckPointer(ppv, E_POINTER);

	 
	if (riid == IID_IBoutechLiveAndTimeShiftReceiver)
	{
		return GetInterface((IBoutechLiveAndTimeShiftReceiver*)m_pOutputPin, ppv);
	} 	
	else if(riid == IID_IAMFilterMiscFlags)
	{
		return GetInterface((IAMFilterMiscFlags*) this, ppv);
	}
	else 
	{
		return CBaseFilter::NonDelegatingQueryInterface(riid, ppv);
	}
}

 
CLiveSourceStream::CLiveSourceStream(CBaseFilter *pFilter, CCritSec *pLock, HRESULT *phr)
	: CBaseOutputPin(LIVE_FILTER_NAME, pFilter, pLock, phr, LIVE_OUTPIN_NAME),
	m_frameRate(0), m_rtFrameRate(0), m_lastFrame(0),
	m_videoResolutionWidth(640), m_videoResolutionHeight(480)
{
	pTimeShiftCallback = NULL;
	m_timeDiffToLog = 5;
	m_timshiftBufferSize = 0;
	m_isMulticast = true;
	timeShiftBuffer = NULL;
	fifoBuffer = NULL;
	aes = NULL;
	m_pinType = 0;
	m_bmpInfo.biSize = sizeof(BITMAPINFOHEADER);
	m_bmpInfo.biCompression = BI_RGB;
	m_bmpInfo.biBitCount = 32;
	m_bmpInfo.biHeight = m_videoResolutionHeight;
	m_bmpInfo.biWidth = m_videoResolutionWidth;
	m_bmpInfo.biPlanes = 1;
	m_bmpInfo.biSizeImage = GetBitmapSize(&m_bmpInfo);	
	m_bmpInfo.biClrImportant = 0;
	m_bmpInfo.biClrUsed = 0;
	m_bmpInfo.biXPelsPerMeter = 0;
	m_bmpInfo.biYPelsPerMeter = 0;
	m_videoFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 4;
	m_videoSLAFrameSize = m_videoResolutionWidth * m_videoResolutionHeight * 3;

	SetFrameRate(30);


#if USE_AES
	InitializeAES();
#endif 

}

CLiveSourceStream::~CLiveSourceStream()
{

	if (timeShiftBuffer != NULL)
	{
		delete timeShiftBuffer;
		timeShiftBuffer = NULL;
	}


#if USE_AES
	if (recovered != NULL)
	{
		delete recovered;
		recovered = NULL;
	}
#endif 
}

HRESULT CLiveSourceStream::CheckMediaType(const CMediaType* pmt)
{
	CheckPointer(pmt, E_POINTER);

	return S_OK;
#if 0 
	if (pmt->majortype == MEDIATYPE_Stream)
		return S_OK;

	if (pmt->majortype != MEDIATYPE_Video)
	{
		return E_INVALIDARG;
	}
	if (pmt->subtype != MEDIASUBTYPE_H264)
	{
		return E_INVALIDARG;
	}
	return S_OK;
#endif 
}

HRESULT CLiveSourceStream::GetMediaType(int iPosition, CMediaType* pmt)
{
	CAutoLock cAutoLock(m_pLock);

	if(iPosition < 0)
	{
		return E_INVALIDARG;
	}

	if(iPosition >= 2)
	{
		return VFW_S_NO_MORE_ITEMS;
	}

	VIDEOINFOHEADER* vih = (VIDEOINFOHEADER*)pmt->AllocFormatBuffer(sizeof(VIDEOINFOHEADER));
	if(!vih)
	{
		return E_OUTOFMEMORY;
	}

	vih->bmiHeader = m_bmpInfo;
	if(m_frameRate != 0)
	{
		vih->AvgTimePerFrame = UNITS / m_frameRate;
	}
	else
	{
		vih->AvgTimePerFrame = 0;
	}
	 

	switch (iPosition)
	{
		case 0:
			pmt->SetType(&MEDIATYPE_Video);
			pmt->SetSubtype(&MEDIASUBTYPE_H264);
			pmt->SetFormatType(&FORMAT_VideoInfo);
			pmt->SetTemporalCompression(TRUE /*FALSE*/);
			pmt->SetSampleSize(m_bmpInfo.biSizeImage);
		break;
		case 1:
			pmt->SetType(&MEDIATYPE_Stream);
			pmt->SetSubtype(&GUID_NULL);			
			pmt->SetFormatType(&GUID_NULL);
		break;
	}
	
	return S_OK;
}

 

HRESULT CLiveSourceStream::DecideBufferSize(IMemAllocator *pAlloc, ALLOCATOR_PROPERTIES *ppropInputRequest)
{
	CheckPointer(pAlloc,E_POINTER);
	CheckPointer(ppropInputRequest,E_POINTER);

	CAutoLock cAutoLock(m_pLock);
	HRESULT hr = NOERROR;

	ppropInputRequest->cBuffers = 10;
	ppropInputRequest->cbBuffer = 1504 * 1000; // m_bmpInfo.biSizeImage;

	ASSERT(ppropInputRequest->cbBuffer);

	ALLOCATOR_PROPERTIES Actual;
	hr = pAlloc->SetProperties(ppropInputRequest,&Actual);
	if(FAILED(hr))
	{
		return hr;
	}

	if(Actual.cbBuffer < ppropInputRequest->cbBuffer)
	{
		return E_FAIL;
	}

	pAlloc->Commit();
	//m_pAllocator->Commit();
	m_PushFinished = false;
	
	shared_ptr<thread> t1 = make_shared<thread>(&CLiveSourceStream::ReceiveThread, this);
	t1->detach();
	

	return S_OK;
}

 

void CLiveSourceStream::ReceiveThread()
{
	char pinTypes[3][100] = { "c:\\avc_4_demux.ts", "c:\\avc_row_for_demux.ts", "d:\\truck.ts" };
	 
	ifstream ifile(pinTypes[m_pinType]);
	if (!ifile) {
		return;
	}
	 

	while (true)
	{


	 
		//if (FileExists("c:\\avc_row_for_demux.ts") == true)
		//if (FileExists("c:\\avc_4_demux.ts") == true)
		{
			if (m_PushFinished == false)
			{
				if (PreFileHandle == NULL)
				{
					PreFileHandle = fopen(pinTypes[m_pinType], "r+b");
					//PreFileHandle = fopen("c:\\avc_row_for_demux.ts", "r+b");
				}
				HRESULT hr = S_OK;
				hr = AddFrameFromFile(PreFileHandle);
				if (hr == S_FALSE)
				{
					fclose(PreFileHandle);
					PreFileHandle = NULL;
					return;
				}
			}
			else
			{
				return;
			}
		}
	}

}

STDMETHODIMP CLiveSourceStream::SetBitmapInfo(BITMAPINFOHEADER& bInfo)
{
	if(bInfo.biHeight == 0 || bInfo.biWidth == 0 || bInfo.biBitCount == 0)
	{
		return E_INVALIDARG;
	}

	m_bmpInfo = bInfo;
	return S_OK;
}

  

STDMETHODIMP CLiveSourceStream::AddFrameFromFile(FILE *handle)
{

	//CAutoLock cAutoLock(m_pLock);


	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}


#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 

	int size = fread((char *)pData, 1 , RECEIVE_SIZE, handle);
	if (size == 0)
	{
		m_PushFinished = true;
		pSample->Release();
		return S_FALSE;
	}

	hr = pSample->SetActualDataLength(size);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
	/*
	hr = pSample->SetSyncPoint(TRUE);
	if (FAILED(hr))
	{
	pSample->Release();
	return hr;
	}
	*/

#endif 
	hr = this->Deliver(pSample);
	pSample->Release();
	return hr;

}

STDMETHODIMP CLiveSourceStream::AddFrame(int clientSocket)
{
	CAutoLock cAutoLock(m_pLock);

	#define SEND_SIZE RECEIVE_SIZE
	IMediaSample* pSample = NULL;
	BYTE* pData = NULL;

	HRESULT hr;
	hr = GetMediaSample(&pSample);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = pSample->GetPointer(&pData);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}



#if USE_AES

	size_t rsize;
	int size = recv(clientSocket, (char *)pData, 1328, 0);

	if (aes->MaxPlainTextSize(size, rsize)) {

	}

	// re-syncronize under the key - ok
	// aes.SetIv( iv, sizeof(iv) );

	if (!aes->Decrypt(pData, size, recovered, rsize)) {

	}
	memcpy(pData, recovered, rsize);

	hr = pSample->SetActualDataLength(rsize);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}

#else 

#if !USE_FIFO
	
	int size = recv(
		clientSocket,
		(char *)(pData),
		RECEIVE_SIZE * 100,
		0);
	if (size == -1)
	{
		int lastError = WSAGetLastError();
		pSample->Release();
		return hr;
	}
	hr = pSample->SetActualDataLength(size);
#else 


	int sizeToSend;
	if ((sizeToSend = GetFifoSize()) < SEND_SIZE)
	{
		pSample->Release();
		return hr;
	}

	if (m_goLive == true)
	{
		if (m_goLiveNeedSink == true && m_pinType == 0)
		{
			memset(&info, 0, sizeof(info));
			auto suc = tsParser.do_parser(fifoBuffer + fifo_read, info);

			//if (info.type_ == util::mpegts_info::idr)
			if (info.pict_type_ != util::av_picture_type_i)
			{
				return hr;
			}
			m_goLiveNeedSink = false;
		}

		memcpy(pData, fifoBuffer + fifo_read, sizeToSend);	
	}
	else
	{
		memcpy(pData, timeShiftBuffer + timeshift_rd, sizeToSend);
		timeshift_rd = (timeshift_rd + sizeToSend) % m_timshiftBufferSize;
	}
	 
	EnterCriticalSection(&CriticalSection);
	fifo_read = (fifo_read + sizeToSend) % MAX_FIFO;
	LeaveCriticalSection(&CriticalSection);

	hr = pSample->SetActualDataLength(sizeToSend);
	if (FAILED(hr))
	{
		pSample->Release();
		return hr;
	}
#endif 
 
	hr = this->Deliver(pSample);
	 
	pSample->Release();

#endif 
	return hr;


}
  

STDMETHODIMP CLiveSourceStream::SetPort(int port)
{
	m_port = port;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetIpAddress(WCHAR *IpAddress, bool isMulticast)
{
	 
	char _ip[100];
	wchar_t array[] = L"Hello World";
	wcstombs(_ip, IpAddress, 100);
	strcpy(m_ipAddress , _ip);
	m_isMulticast = isMulticast;
	return S_OK;
}
 
STDMETHODIMP CLiveSourceStream::GoToLive()
{
	if (m_goLive == false)
		m_goLiveNeedSink = true;
	m_goLive = true;
	return S_OK;

}
STDMETHODIMP CLiveSourceStream::SeekInTime(uint64_t ts_rd)
{
	m_goLive = false;
	timeshift_rd = ts_rd;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetPinType(int pinType)
{

	m_pinType = pinType;

	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetTimeShiftBuffer(uint64_t bufferSize)
{
	m_timshiftBufferSize = bufferSize;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetTimeShiftCallback(TimeShiftCallback p)
{

	pTimeShiftCallback = p;
	return S_OK;
}

STDMETHODIMP CLiveSourceStream::SetTimeDiffToLog(int difftime)
{
	m_timeDiffToLog = difftime;
	return S_OK;
}

/////////////////////////
HRESULT CLiveSourceStream::GetMediaSample(IMediaSample** ppSample)
{
	REFERENCE_TIME rtStart = m_lastFrame;
	m_lastFrame += m_rtFrameRate;

	//return this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);

	HRESULT hr;
	hr = this->GetDeliveryBuffer(ppSample, &rtStart, &m_lastFrame, 0);
	if (FAILED(hr))
	{
		return hr;
	}

	hr = (*ppSample)->SetTime(&rtStart, &m_lastFrame);

	return hr;

}

STDMETHODIMP CLiveSourceStream::SetFrameRate(int frameRate)
{
	if(frameRate < 0 || frameRate > 30)
	{
		return E_INVALIDARG;
	}

	m_frameRate = frameRate;
	m_rtFrameRate = UNITS / m_frameRate;
	return S_OK;
}

HRESULT CLiveSourceStream::GetPixelData(HBITMAP hBmp, BYTE** ppData, int* pSize)
{
	ASSERT(hBmp);

	BITMAP bmp = {0};
	int res = ::GetObject(hBmp, sizeof(BITMAP), &bmp);
	if(res != sizeof(BITMAP))
	{
		return E_FAIL;
	}

	if(bmp.bmBitsPixel != m_bmpInfo.biBitCount ||
		bmp.bmHeight != m_bmpInfo.biHeight ||
		bmp.bmWidth != m_bmpInfo.biWidth)
	{
		return E_INVALIDARG;
	}

	*pSize = bmp.bmWidthBytes * bmp.bmHeight;
	memcpy(*ppData, bmp.bmBits, *pSize);

	return S_OK;
}

  
STDMETHODIMP CLiveSourceStream::Notify(IBaseFilter * pSender, Quality q)
{
	if(q.Proportion <= 0)
	{
		m_rtFrameRate = 1000;        
	}
	else
	{
		m_rtFrameRate = m_rtFrameRate * 1000 / q.Proportion;
		if(m_rtFrameRate > 1000)
		{
			m_rtFrameRate = 1000;    
		}
		else if(m_rtFrameRate < 30)
		{
			m_rtFrameRate = 30;      
		}
	}

	if(q.Late > 0)
	{
		m_rtFrameRate += q.Late;
	}

	return S_OK;
}