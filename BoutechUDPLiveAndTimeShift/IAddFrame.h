#pragma once

#include <stdint.h>

#define LIVE_FILTER_NAME TEXT("Bauotech UDP Live And Time Shift Receiver")
#define LIVE_OUTPIN_NAME L"Out"

 
typedef void(__stdcall * TimeShiftCallback)(uint64_t loaction , uint8_t year, uint8_t month, uint8_t day, uint8_t hour, uint8_t min , uint8_t sec);
 

// {F483F2C0-EA1D-4E30-AD15-B11550687123}
static const GUID IID_IBoutechLiveAndTimeShiftReceiver =
{ 0xf483f2c0, 0xea1d, 0x4e30, { 0xad, 0x15, 0xb1, 0x15, 0x50, 0x68, 0x71, 0x23 } };

 

// {467F2E53-3BDD-41C8-9DC8-2959207FCDEC}
static const GUID CLSID_CBoutechLiveAndTimeShiftReceiver =
{ 0x467f2e53, 0x3bdd, 0x41c8, { 0x9d, 0xc8, 0x29, 0x59, 0x20, 0x7f, 0xcd, 0xec } };


 
 
DECLARE_INTERFACE_(IBoutechLiveAndTimeShiftReceiver, IUnknown)
{
  
	STDMETHOD(SetIpAddress)(WCHAR *IpAddress, bool isMulticast) PURE;
	STDMETHOD(SetPort)(int port) PURE;
	STDMETHOD(SetPinType)(int pinType) PURE;
	STDMETHOD(GoToLive)() PURE;
	STDMETHOD(SeekInTime)(uint64_t ts_rd) PURE;
	STDMETHOD(SetTimeShiftBuffer)(uint64_t bufferSize) PURE;
	STDMETHOD(SetTimeShiftCallback)(TimeShiftCallback p) PURE;
	STDMETHOD(SetTimeDiffToLog)(int difftime) PURE;

 
	

};

